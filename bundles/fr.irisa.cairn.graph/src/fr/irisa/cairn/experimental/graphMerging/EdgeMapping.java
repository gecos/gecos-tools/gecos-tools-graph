package fr.irisa.cairn.experimental.graphMerging;

import java.util.List;

import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.implement.Node;

/**
 * Class representing the mapping between two edges.
 * It is symbolize as two NodeMapping (one for the source and one for the destination), and 
 * the port number of the destination node.  
 * 
 * @author Pierrick FLORECK
 *
 */
public class EdgeMapping extends Node implements IMapping 
{
	private NodeMapping from;
	private NodeMapping to;
	private int port_number;
	
	/**
	 * Construct an EdgeMapping
	 * @param n1 : NodeMapping of the source
	 * @param n2 : NodeMapping of the destination
	 * @param pn : port_number of the destination node
	 */
	public EdgeMapping(NodeMapping n1, NodeMapping n2, int pn)
	{
		from = n1;
		to = n2;
		port_number = pn;
	}
	
	/**
	 * @return the NodeMapping representing the source of the edge
	 */
	public NodeMapping getFrom() 
	{
		return from;
	}

	/**
	 * Changes the NodeMapping representing the source of the edge
	 * @param from : new NodeMapping
	 */
	public void setFrom(NodeMapping from) 
	{
		this.from = from;
	}

	/**
	 * @return the NodeMapping representing the destination of the edge
	 */
	public NodeMapping getTo() 
	{
		return to;
	}

	/**
	 * Changes the NodeMapping representing the destination of the edge
	 * @param to : new NodeMapping
	 */
	public void setTo(NodeMapping to) 
	{
		this.to = to;
	}

	/**
	 * @return the port number of the destination node
	 */
	public int getPortNumber() 
	{
		return port_number;
	}

	/**
	 * Changes the port number of the destination node
	 * @param port_number : new port number
	 */
	public void setPortNumber(int port_number) 
	{
		this.port_number = port_number;
	}

	/**
	 * @param map : mapping to test
	 * @return true if the current mapping is compatible with "map"
	 */
	public boolean isCompatible(IMapping map)
	{
		return map.isEdgeCompatible(this);
	}

	/**
	 * @param map_edge : EdgeMapping to test
	 * @return true if the current mapping is compatible with "map_edge"
	 */
	public boolean isEdgeCompatible(EdgeMapping map_edge) 
	{
		return (isNodeCompatible(map_edge.getFrom()) && isNodeCompatible(map_edge.getTo()));
	}

	/**
	 * @param map_node : NodeMapping to test
	 * @return true if the current mapping is compatible with "map_node"
	 */
	public boolean isNodeCompatible(NodeMapping map_node) 
	{
		if (getFrom().equals(map_node) || getTo().equals(map_node))
			return true;
		return getFrom().isNodeCompatible(map_node) && getTo().isNodeCompatible(map_node);
	}
	
	/**
	 * @param node : node to test
	 * @return true if the node "node" is contained in the current mapping
	 */
	public boolean containsNode(INode node) 
	{
		return (getFrom().containsNode(node) || getTo().containsNode(node));
	}
	
	/**
	 * Test if the edge between "n_source" and "n_dest" is mapped (i.e the edge is in "mapped_edges").
	 * @param mapped_edges : list of EdgeMapping
	 * @param n_source : source node of th edge
	 * @param n_dest : destination node of the edge
	 * @return true if the edge between "n_source" and "n_dest" is mapped, false otherwise
	 */
	public static boolean isMappedEdge(List<EdgeMapping> mapped_edges, INode n_source, INode n_dest)
	{
		for(EdgeMapping map_edge : mapped_edges)
		{
			INode nf1 = map_edge.getFrom().getNode1();
			INode nf2 = map_edge.getFrom().getNode2();
			INode nt1 = map_edge.getTo().getNode1();
			INode nt2 = map_edge.getTo().getNode2();
			if((nf1.equals(n_source) && nt1.equals(n_dest)) || (nf2.equals(n_source) && nt2.equals(n_dest)))
				return true;
		}
		
		return false;
	}
	
	public String toString()
	{
		return ("{[" + getFrom() + "] -> [" + getTo() + "]}");
	}
}
