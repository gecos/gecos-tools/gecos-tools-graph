package fr.irisa.cairn.experimental.graphMerging;

import fr.irisa.cairn.graph.INode;

/**
 *  A Mapping can be view as a couple of node or an edge couple.
 *  It is symbolized as a node to simplify the construction of the compatibility graph.
 *  
 *  @author Pierrick FLORECK
 *   
 */

public interface IMapping extends INode
{
	/**
	 * @param map : mapping to test
	 * @return true if the current mapping is compatible with "map"
	 */
	public boolean isCompatible(IMapping map);

	/**
	 * @param map_edge : EdgeMapping to test
	 * @return true if the current mapping is compatible with "map_edge"
	 */
	public boolean isEdgeCompatible(EdgeMapping map_edge);

	/**
	 * @param map_node : NodeMapping to test
	 * @return true if the current mapping is compatible with "map_node"
	 */
	public boolean isNodeCompatible(NodeMapping map_node);
	
	/**
	 * @param node : node to test
	 * @return true if the node "node" is contained in the current mapping
	 */
	public boolean containsNode(INode node);
}
