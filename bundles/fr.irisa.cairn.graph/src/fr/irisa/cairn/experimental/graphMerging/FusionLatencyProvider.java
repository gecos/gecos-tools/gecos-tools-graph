package fr.irisa.cairn.experimental.graphMerging;

import java.util.Map;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.analysis.AsapAlgorithm;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

public class FusionLatencyProvider<N extends INode,E extends IEdge> implements ILatencyProvider<N ,E> 
{
	private Map<INode, Integer> ltopological_steps_g1;
	private Map<INode, Integer> ltopological_steps_g2;
	private int maxpath_g1;
	private int maxpath_g2;
	private int method;
	private ILibComponentProvider libcompo;
	
	/**
	 * @param m : 0 to merge graphs only taking into account the latency of nodes
	 * 			  1 to merge graphs only taking into account the critical path
	 * 			  2 to merge graphs with the two methods
	 */
	public FusionLatencyProvider(int m, ILibComponentProvider ilibcompo)
	{
		method = m;
		libcompo = ilibcompo;
	}
	
	
	public FusionLatencyProvider(ILibComponentProvider ilibcompo)
	{
		method = 2;
		libcompo = ilibcompo;
	}
	
	/**
	 * @param e : IEdge to evaluate
	 * @return the latency of the edge e
	 */
	public int getEdgeLatency(IEdge e) 
	{
		return 10;
	}

	/**
	 * @param n : INode to evaluate
	 * @return the latency of the node n 
	 */
	public int getNodeLatency(INode n) 
	{
		if(n instanceof NodeMapping)
		{
			NodeMapping node = (NodeMapping)n;
			INode node_map = node.getNodeMap();
			int cost_path = 0;
			int cost_node = 0;
			
			switch(method)
			{
				case 0 :
					cost_node = getCostNode(node_map);
					break;
				case 1 :
					cost_path = getCostCriticalPath(node);
					break;
				case 2 :
					cost_node = getCostNode(node_map);
					cost_path = getCostCriticalPath(node);
			}			
			return cost_path + cost_node;
		}
		else if(n instanceof EdgeMapping)
			return 10;
		return 0;
	}
	
	/**
	 * @param node_map : node to evaluate
	 * @return the latency of node_map
	 */
	private int getCostNode(INode node_map)
	{
		return libcompo.getCostNode(node_map);
	}
	
	/**
	 * @param nmap : NodeMapping to evaluate
	 * @return the cost of the critical path. 
	 * Higher it is, lower the critical path will be increased. 
	 */
	private int getCostCriticalPath(NodeMapping nmap)
	{
		INode n1 = nmap.getNode1();
		INode n2 = nmap.getNode2();
		if(ltopological_steps_g1 == null)
		{
			//Construct the list of topological steps for the two graph to merge
			topologicalSteps(n1.getGraph(), n2.getGraph());
		}
		//position of the nodes in the critical path
		int pos_n1 = ltopological_steps_g1.get(n1); 
		int pos_n2 = ltopological_steps_g2.get(n2);
		//number of nodes after the position of the nodes in the critical path
		int nap_n1 = maxpath_g1 - pos_n1;
		int nap_n2 = maxpath_g2 - pos_n2;
		//number of nodes before the position of the nodes in the critical path
		int nav_n1 = pos_n1;
		int nav_n2 = pos_n2;
		
		//the critical path is increased if nav_n2 > nav_n1 ou nap_n2 > nap_n1
		int cost = maxpath_g1 + maxpath_g2; //the maximum cost
		if(nav_n2 > nav_n1)
			cost -= nav_n2 - nav_n1;
		if(nap_n2 > nap_n1)
			cost -= nap_n2 - nap_n1;
		
		return cost;
	}
	
	/**
	 * Find the topological steps of the two graph with the ASAP Algorithm.
	 * @param g1 : first graph to evaluate
	 * @param g2 : second graph to evaluate
	 */
	private void topologicalSteps(IGraph g1, IGraph g2)
	{
		AsapAlgorithm asap = new AsapAlgorithm(new TestLatencyProviderASAP(),g1);
		asap.compute();
		ltopological_steps_g1 = asap.getNodesSteps();
		maxpath_g1 = asap.getCriticalPathLength();
		
		AsapAlgorithm asap2 = new AsapAlgorithm(new TestLatencyProviderASAP(),g2);
		asap2.compute();
		ltopological_steps_g2 = asap2.getNodesSteps();
		maxpath_g2 = asap2.getCriticalPathLength();
	}
	
	/**
	 * Provider for the ASAP algorithm in "topologicalSteps"
	 */
	private class TestLatencyProviderASAP implements ILatencyProvider <N,E>
	{
		public int getEdgeLatency(IEdge e) 
		{
			return 0;
		}

		public int getNodeLatency(INode n) 
		{
			return 1;
		}
	}
	
}
