package fr.irisa.cairn.experimental.graphMerging;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;

/**
 * Provider to define the architecture of graphs to be merged.
 * It defines the matching between nodes, the commutativity, ...
 *  
 * @author Pierrick FLORECK
 *
 */
public interface ILibComponentProvider 
{
	/**
	 * @param n1 : first node
	 * @param n2 : second node
	 * @return a node corresponding to the matching between n1 and n2 (without ports)
	 */
	public INode getMatch(INode n1, INode n2);
	
	/**
	 * @param node : node to test
	 * @return true if the node "node" is commutative
	 */
	public boolean isCommutative(INode node);
	
	/**
	 * @param node : node to copy
	 * @return a copy of the node "node" without ports
	 */
	public INode getCopy(INode node);
	
	/**
	 * Connect the two edges "e1" and "e2" to the node "ndest" at the port number "port_number" 
	 * in the graph g.
	 * This method is used if there is a conflict (both edges in the same port).
	 * @param g : graph where add the edges
	 * @param e1 : first edge
	 * @param e2 : second edge
	 * @param ndest : node where connect "e1" and "e2"
	 * @param port_number : port number
	 */
	public void connectEdgeConflict(IGraph g, IEdge e1, IEdge e2, INode ndest, int port_number);
	
	/**
	 * @param node_map : node to evaluate
	 * @return the latency of node_map
	 */
	public int getCostNode(INode node_map);
}
