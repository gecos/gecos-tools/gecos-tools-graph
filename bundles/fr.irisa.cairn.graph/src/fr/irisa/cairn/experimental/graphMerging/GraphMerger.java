package fr.irisa.cairn.experimental.graphMerging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.analysis.MaxWeightedClique;
import fr.irisa.cairn.graph.implement.Edge;
import fr.irisa.cairn.graph.implement.Graph;
import fr.irisa.cairn.graph.implement.Port;
import fr.irisa.cairn.graph.providers.ILatencyProvider;
import fr.irisa.cairn.typedGraph.implement.TypedGraph;

/**
 * Class to merge a list of graph following the algorithm of Moreano.
 * 
 * @author Pierrick FLORECK
 * 
 */
public class GraphMerger 
{

	private IGraph merged_graph;
	private List<IGraph> lgraphs;
	private ILatencyProvider<INode, IEdge> latencyProvider;
	private ILibComponentProvider compoProvider;
	
	/**
	 * Construct the GraphMerger
	 * @param g_init : first graph to merge
	 * @param lg : list of graph to merge with g_init
	 * @param latencyProvider : provider for the area cost (used to search for the maximum clique)  
	 * @param compoProvider : provider for the nodes (define the matching, the commutativity, ...)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GraphMerger(IGraph g_init, List<IGraph> lg, 
			ILatencyProvider latencyProvider, ILibComponentProvider compoProvider) 
	{
		merged_graph = g_init;
		lgraphs = lg;
		this.latencyProvider = latencyProvider;
		this.compoProvider = compoProvider;
	}

	/**
	 * Construct the graph of the merging with all the graphs of lgraphes.
	 * @return the merged graph
	 */
	public IGraph compute() 
	{
		for (IGraph g : lgraphs) 
		{
			//Node Mapping
			List<NodeMapping> lnode_mapping = nodesMapping(g);
			
			//Edge Mapping
			List<EdgeMapping> ledge_mapping = edgesMapping(lnode_mapping);
			
			// Construction of the compatibility graph
			List<IMapping> lmapping = new ArrayList<IMapping>(lnode_mapping);
			lmapping.addAll(ledge_mapping);
			IGraph gc = makeCGraph(lmapping);
			
			//Find the max weight clique
			MaxWeightedClique maxclique = new MaxWeightedClique(latencyProvider);
			Set<INode> clique = maxclique.solve(gc);
//			int max_weight = maxclique.getMaxWeight();
//			System.out.println(max_weight);
			
			// Construction of the resulting graph
			merged_graph = makeResultingGraph(clique, g);
		}
		return merged_graph;
	}

	/**
	 * Construction of the compatibility graph
	 * @param lmapping : the list of mapping
	 * @return graph representing the compatibility between g and the graph 
	 * already merged.
	 */
	private IGraph makeCGraph(List<IMapping> lmapping)
	{
		//The graph is not directed
		IGraph g = new Graph(false);
		
		//All nodes are either a NodeMapping or an EdgeMapping
		for(IMapping map : lmapping)
			g.addNode(map);
		
		//Connect the nodes
		for (int i = 0; i <= (lmapping.size() - 2); i++)
		{
			IMapping first = lmapping.get(i);
			for (int j = i + 1; j < lmapping.size(); j++)
			{
				IMapping second = lmapping.get(j);
				if (first.isCompatible(second)) 
				{
					first.addOutputPort(new Port());
					second.addInputPort(new Port());
					int nbp1 = first.getOutputPorts().size();
					int nbp2 = second.getInputPorts().size();
					g.addEdge(new Edge(first.getOutputPorts().get(nbp1-1), second.getInputPorts().get(nbp2-1)));
				}
			}
		}
		return g;
	}
	
	/**
	 * Gets the mapping of each nodes of g with nodes of the graph already merged.
	 * @param g : graph merge
	 * @return list of NodeMapping
	 */
	private List<NodeMapping> nodesMapping(IGraph g)
	{
		List<NodeMapping> lnodes_mapping = new ArrayList<NodeMapping>();
		
		for (INode node_mg : merged_graph.getNodes()) 
		{
			for(INode node_g : g.getNodes())
			{
				INode n = compoProvider.getMatch(node_mg, node_g);
				if(n != null)
					lnodes_mapping.add(new NodeMapping(node_mg, n, node_g));
			}
		}
		return lnodes_mapping;
	}
	
	/**
	 * Map the edge: Two arc from Gi and Gj can be mapped if their corresponding source 
	 * vertices can be mapped, as well as their destination vertices, and their input port 
	 * match or the operator of the mapped destination vertices is commutative.
	 * @param node_mapping : mapping of nodes
	 * @return list of EdgeMapping
	 */
	private List<EdgeMapping> edgesMapping(List<NodeMapping> node_mapping)
	{
		List<EdgeMapping> ledge_mapping = new ArrayList<EdgeMapping>();
		
		for(NodeMapping map : node_mapping)
		{
			for (IEdge edge1 : map.getNode1().getOutgoingEdges()) 
			{
				for(IEdge edge2 : map.getNode2().getOutgoingEdges())
				{
					IPort p1 = edge1.getSinkPort();
					IPort p2 = edge2.getSinkPort();
					INode n1 = p1.getNode();
					INode n2 = p2.getNode();
					int index = node_mapping.indexOf(new NodeMapping(n1, n2));
					if(index >= 0)
					{
						NodeMapping succ_map = node_mapping.get(index);
						int nb_in1 = n1.getInputPorts().indexOf(p1);
						int nb_in2 = n2.getInputPorts().indexOf(p2);
						
						if((nb_in1 == nb_in2) || compoProvider.isCommutative(succ_map.getNode1()))
						{
							EdgeMapping emap = new EdgeMapping(map, succ_map, nb_in1);
							ledge_mapping.add(emap);
						}
					}
				}
			}
		}
		return ledge_mapping;
	}


	/**
	 * Construction of the resulting graph
	 * @param clique : list of nodes of the maximum clique 
	 * @param g : graph merge
	 * @return the resulting graph of the merging of merged_graph with g
	 */
	private IGraph makeResultingGraph(Set<INode> clique, IGraph g)
	{
		IGraph g_res = new TypedGraph(merged_graph.isDirected());
		List<NodeMapping> mapped_nodes = new ArrayList<NodeMapping>();
		List<EdgeMapping> mapped_edges = new ArrayList<EdgeMapping>();
		Map<INode, INode> map_unmapped_nodes_g = new HashMap<INode, INode>();
		Map<INode, INode> map_unmapped_nodes_mg = new HashMap<INode, INode>();
		
		
		for(INode node : clique)
		{
			//Add the nodes of the maximum clique
			if(node instanceof NodeMapping)
			{
				NodeMapping n_map = (NodeMapping)node;
				g_res.addNode(n_map.getNodeMap());
				mapped_nodes.add(n_map);
			}
			//Save the edges of the maximum clique
			else if(node instanceof EdgeMapping)
			{
				EdgeMapping e_map = (EdgeMapping)node;
				mapped_edges.add(e_map);				
			}
		}
		
		//Add the edges of the maximum clique
		for(EdgeMapping e_map : mapped_edges)
		{
			INode source = e_map.getFrom().getNodeMap();
			INode dest = e_map.getTo().getNodeMap();
			int port_number = e_map.getPortNumber();
			
			source.addOutputPort(new Port());
			for(int i = dest.getInputPorts().size(); i <= port_number; i++)
				dest.addInputPort(new Port());
			int nbp1 = source.getOutputPorts().size();
			IEdge edge = new Edge(source.getOutputPorts().get(nbp1-1), dest.getInputPorts().get(port_number));
			g_res.addEdge(edge);
		}
		
		//Add nodes not included in the maximum clique
		for(INode node : getUnmappedNodesG(g, mapped_nodes))
		{	
			INode new_node = compoProvider.getCopy(node);
			g_res.addNode(new_node);
			map_unmapped_nodes_g.put(node, new_node);
		}
		for(INode node : getUnmappedNodesMG(mapped_nodes))
		{	
			INode new_node = compoProvider.getCopy(node);
			g_res.addNode(new_node);
			map_unmapped_nodes_mg.put(node, new_node);
		}
		
		//Connect mapped nodes
		for(NodeMapping n_map : mapped_nodes)
		{
			INode n1 = n_map.getNode1();
			INode n2 = n_map.getNode2();
			INode nm = n_map.getNodeMap();
			
			addOutputEdges(n1, nm, map_unmapped_nodes_mg, mapped_edges, mapped_nodes, g_res);	
			addOutputEdges(n2, nm, map_unmapped_nodes_g, mapped_edges, mapped_nodes, g_res);
		}
		
		//Connect unmapped nodes
		for(INode node : map_unmapped_nodes_mg.keySet())
		{
			INode nm = map_unmapped_nodes_mg.get(node);
			addOutputEdges(node, nm, map_unmapped_nodes_mg, mapped_edges, mapped_nodes, g_res);
		}
		for(INode node : map_unmapped_nodes_g.keySet())
		{
			INode nm = map_unmapped_nodes_g.get(node);
			addOutputEdges(node, nm, map_unmapped_nodes_g, mapped_edges, mapped_nodes, g_res);
		}
		
		return g_res;
	}
	
	/**
	 * Gets the unmapped nodes of g and merged_graph
	 * @param g : graph merge
	 * @param lmapped_nodes : nodes already merged
	 * @return the list of unmapped nodes
	 */
	private List<INode> getUnmappedNodesG(IGraph g, List<NodeMapping> lmapped_nodes)
	{
		List<INode> lunmapped_nodes = new ArrayList<INode>();
		List<INode> nodes_g = new ArrayList<INode>();
		
		for(NodeMapping n_map : lmapped_nodes)
		{
			nodes_g.add(n_map.getNode2());
		}
		
		for(INode node : g.getNodes())
		{
			if(!nodes_g.contains(node))
				lunmapped_nodes.add(node);
		}
		
		return lunmapped_nodes;
	}
	
	/**
	 * Gets the mapped nodes of merged_graph
	 * @param lmapped_nodes : nodes already merged
	 * @return the list of unmapped nodes
	 */
	private List<INode> getUnmappedNodesMG(List<NodeMapping> lmapped_nodes)
	{
		List<INode> lunmapped_nodes = new ArrayList<INode>();
		List<INode> nodes_mg = new ArrayList<INode>();
		
		for(NodeMapping n_map : lmapped_nodes)
		{
			nodes_mg.add(n_map.getNode1());
		}
		
		for(INode node : merged_graph.getNodes())
		{
			if(!nodes_mg.contains(node))
				lunmapped_nodes.add(node);
		}		
		return lunmapped_nodes;
	}
	
	/**
	 * Add all output edges of node "node" that have not been mapped
	 * @param node : node in the initial graph
	 * @param mapped_node : corresponding node to node not mapped in the graph result
	 * @param map_unmapped_nodes : map of unmapped nodes
	 * @param mapped_edges : list of EdgeMapping included in the maximum clique
	 * @param mapped_nodes : list of NodeMapping included in the maximum clique
	 * @param g : the result graph
	 */
	private void addOutputEdges(INode node, INode mapped_node, Map<INode, INode> map_unmapped_nodes,
			List<EdgeMapping> mapped_edges, List<NodeMapping> mapped_nodes, IGraph g)
	{
		for(IPort p_out : node.getOutputPorts())
		{
			for(IEdge e_out : p_out.getEdges())
			{
				IPort p_in = e_out.getSinkPort();
				INode n_dest = p_in.getNode();
				INode n = map_unmapped_nodes.get(n_dest);
				if(n != null || !EdgeMapping.isMappedEdge(mapped_edges, node, n_dest))
				{
					//Gets the input port number
					int nport_in = n_dest.getInputPorts().indexOf(p_in);
					if(n != null)
						n_dest = n;
					else
					{
						//The destination is a mapped node
						n_dest = NodeMapping.getMappingNode(n_dest, mapped_nodes);
					}
					
					//Add the edge
					mapped_node.addOutputPort(new Port());
					for(int i = n_dest.getInputPorts().size(); i <= nport_in; i++)
						n_dest.addInputPort(new Port());
					int nbp1 = mapped_node.getOutputPorts().size() - 1;
					g.addEdge(new Edge(mapped_node.getOutputPorts().get(nbp1), n_dest.getInputPorts().get(nport_in)));
					
					//Verify if there is no input edge on the input port "p_in" of the node destination
					List<? extends IPort> lports = n_dest.getInputPorts();
					if((lports != null) && (lports.size() > nport_in))
					{
						IPort p = n_dest.getInputPorts().get(nport_in);
						if(p.getDegree() > 1)
						{
							//There is already an arc connected to the port
							Iterator<? extends IEdge> itedge = p.getEdges().iterator();
							IEdge e1 = itedge.next();
							IEdge e2 = itedge.next();
							compoProvider.connectEdgeConflict(g, e1, e2, n_dest, nport_in);
						}
					}
				}
				
			}
		}
	}
}
