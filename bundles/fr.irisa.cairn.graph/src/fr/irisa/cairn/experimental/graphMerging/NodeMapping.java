package fr.irisa.cairn.experimental.graphMerging;

import java.util.List;

import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.implement.Node;
import fr.irisa.cairn.graph.tools.GraphTools;

/**
 * Class representing the mapping between two nodes
 * 
 * @author Pierrick FLORECK
 *
 */
public class NodeMapping extends Node implements IMapping 
{
	private INode node1;
	private INode node2;
	private INode node_map;

	/**
	 * Construct the NodeMapping
	 * @param n1 : first node
	 * @param map : node corresponding to the matching between n1 and n2
	 * @param n2 : second node
	 */
	public NodeMapping(INode n1, INode map, INode n2) 
	{
		node1 = n1;
		node2 = n2;
		node_map = map;
	}

	/**
	 * Construct the NodeMapping
	 * @param n1 : first node
	 * @param n2 : second node
	 */
	public NodeMapping(INode n1, INode n2) 
	{
		node1 = n1;
		node2 = n2;
	}
	

	/**
	 * @return the first node
	 */
	public INode getNode1() 
	{
		return node1;
	}

	/**
	 * Changes the first node
	 * @param node1 : new first node
	 */
	public void setNode1(INode node1) 
	{
		this.node1 = node1;
	}

	/**
	 * @return the node corresponding to the matching between n1 and n2
	 */
	public INode getNodeMap() 
	{
		return node_map;
	}

	/**
	 * Changes the node corresponding to the matching between n1 and n2
	 * @param node_map : new node
	 */
	public void setNodeMap(INode node_map) 
	{
		this.node_map = node_map;
	}

	/**
	 * @return the second node
	 */
	public INode getNode2() 
	{
		return node2;
	}

	/**
	 * Changes the second node
	 * @param node2 : new second node
	 */
	public void setNode2(INode node2) 
	{
		this.node2 = node2;
	}

	/**
	 * @param map : mapping to test
	 * @return true if the current mapping is compatible with "map"
	 */
	public boolean isCompatible(IMapping map) 
	{
		return map.isNodeCompatible(this);
	}

	/**
	 * @param map_edge : EdgeMapping to test
	 * @return true if the current mapping is compatible with "map_edge"
	 */
	public boolean isEdgeCompatible(EdgeMapping map_edge) 
	{
		return map_edge.isNodeCompatible(this);
	}
	
	/**
	 * @param map_node : NodeMapping to test
	 * @return true if the current mapping is compatible with "map_node"
	 */
	public boolean isNodeCompatible(NodeMapping map_node) 
	{
		if(this.equals(map_node))
			return true;
		return (!getNode1().equals(map_node.getNode1()) && !getNode2().equals(map_node.getNode2()) && noCycle(map_node));
	}
	
	/**
	 * @param map_node : NodeMapping to test
	 * @return true if the mapping does not result in a cycle
	 */
	private boolean noCycle(NodeMapping map_node)
	{
		INode n1, n2, n_map1, n_map2;
		n1 = getNode1();
		n2 = getNode2();
		n_map1 = map_node.getNode1();
		n_map2 = map_node.getNode2();
		return (!(GraphTools.isAfter(n1, n_map1) && GraphTools.isAfter(n_map2, n2)))
			&& (!(GraphTools.isBefore(n1, n_map1) && GraphTools.isBefore(n_map2, n2)));
	}
	
	/**
	 * @param node : node to test
	 * @return true if the node "node" is contained in the current mapping
	 */
	public boolean containsNode(INode node) 
	{
		return node.equals(node1) || node.equals(node2);
	}	

	/**
	 * Gets the node that corresponds to the node "node" in "mapped_nodes"
	 * @param node : node to search
	 * @param mapped_nodes : list of NodeMapping
	 * @return the mapped node or null if "node" is not in "mapped_nodes"
	 */
	public static INode getMappingNode(INode node, List<NodeMapping> mapped_nodes)
	{
		for(NodeMapping n_map : mapped_nodes)
		{
			if(n_map.containsNode(node))
				return n_map.getNodeMap();
		}
		return null;
	}
	
	public boolean equals(Object obj)
	{
		if(obj instanceof NodeMapping)
		{
			NodeMapping n = (NodeMapping)obj;
			return (getNode1().equals(n.getNode1()) && getNode2().equals(n.getNode2()));
		}
		return false;
	}

	public String toString()
	{
		return ("{" + getNode1() + " ; " + getNode2() + "} -> " + getNodeMap());
	}
}
