package fr.irisa.cairn.experimental.graphRetiming;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

/**
 * Class for display of retiming edges
 */
public class RetimingEdgeLabelProvider implements IEdgeLabelProvider<IEdge> 
{
	private ILatencyProvider<INode, IEdge> latency_provider;
	
	public RetimingEdgeLabelProvider(ILatencyProvider<INode, IEdge> lat_prov)
	{
		latency_provider = lat_prov;
	}
	
	public String getEdgeLabel(IEdge edge) 
	{
		return "" + latency_provider.getEdgeLatency(edge);
	}
	

}
