package fr.irisa.cairn.experimental.graphRetiming;

import java.util.Map;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

/**
 * Provider for the weight of edges (number of registers) and for the latency of nodes.
 * 
 * @author Pierrick FLORECK
 *
 */
public interface IRetimingProvider<N extends INode,E extends IEdge> extends ILatencyProvider<N,E>
{
	/**
	 * Set the weight w at the edge e
	 * @param e : edge
	 * @param w : weight
	 */
	public void setEdgeLatency(IEdge e, int w);
	
	/**
	 * Set all weights of edges
	 * @param map_latencies : map for the weights of edges
	 */
	public void setEdgesLatencies(Map<IEdge, Integer> map_latencies);

	/**
	 * @param e : edge
	 * @return the weight of the edge e
	 */
	public int getEdgeLatency(IEdge e);
	
	/**
	 * @return all weights of edges
	 */
	public Map<IEdge, Integer> getEdgesLatencies();
	
	/**
	 * @return a temporary node to connect inputs and outputs
	 */
	public INode getTempNode();
	
	/**
	 * Connect the input in_node to the temporary node temp_node in 
	 * the graph "graph"
	 * @param in_node : input node
	 * @param temp_node : temporary node
	 * @param graph
	 */
	public void connectInputToTemp(INode in_node, INode temp_node, IGraph graph);
	
	/**
	 * Connect the output out_node to the temporary node temp_node in 
	 * the graph "graph"
	 * @param out_node : output node
	 * @param temp_node : temporary node
	 * @param graph
	 */
	public void connectOutputToTemp(INode out_node, INode temp_node, IGraph graph);
}