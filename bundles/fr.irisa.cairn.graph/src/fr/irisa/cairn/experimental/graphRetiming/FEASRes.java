package fr.irisa.cairn.experimental.graphRetiming;

import java.util.Map;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;

/**
 * Class for the result of algorithme FEAS
 *
 */
public class FEASRes 
{
	/** New values of edge weigth */
	private Map<IEdge, Integer> retiming_edges;
	/** Values of retiming for each nodes */
	private Map<INode, Integer> retiming_nodes;
	/** Clock period */
	private int clock_period;
	
	public FEASRes(Map<IEdge, Integer> ret_edges, Map<INode, Integer> ret_nodes, int clock)
	{
		retiming_edges = ret_edges;
		retiming_nodes = ret_nodes;
		clock_period = clock;
	}

	public Map<IEdge, Integer> getRetimingEdges() 
	{
		return retiming_edges;
	}

	public void setRetimingEdges(Map<IEdge, Integer> retiming_edges) 
	{
		this.retiming_edges = retiming_edges;
	}

	public Map<INode, Integer> getRetimingNodes() 
	{
		return retiming_nodes;
	}

	public void setRetimingNodes(Map<INode, Integer> retiming_nodes) 
	{
		this.retiming_nodes = retiming_nodes;
	}

	public int getClockPeriod() 
	{
		return clock_period;
	}

	public void setClockPeriod(int clock_period) 
	{
		this.clock_period = clock_period;
	}
	
	
}
