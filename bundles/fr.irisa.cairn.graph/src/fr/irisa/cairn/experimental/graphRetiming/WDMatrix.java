package fr.irisa.cairn.experimental.graphRetiming;

/**
 * Class for the matrix WD.
 *  
 * @author Pierrick FLORECK
 *
 */
public class WDMatrix 
{
	private int length;
	private WDElem[][] matrix;
	
	public WDMatrix(int n)
	{
		length = n;
		matrix = new WDElem[length][length];
	}
	
	public void put(int i, int j, WDElem e)
	{
		matrix[i][j] = e;
	}
	
	public WDElem get(int i, int j)
	{
		return matrix[i][j];
	}
	
	public int getLength()
	{
		return length;
	}
	
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++)
		{
			sb.append(matrix[i][0]);
			for(int j  = 1; j < length; j++)
			{
				sb.append(" | "+matrix[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}	
}
