package fr.irisa.cairn.experimental.graphRetiming;

/**
 * Elements of the matrix WD
 * Between Node u and v there is a path WD[u][v]
 * 
 * @author Pierrick FLORECK
 *
 */
public class WDElem 
{
	/** Number of register */
	private int w;
	/** Delay */
	private int d;
	
	public WDElem()
	{
		w = 0;
		d = 0;
	}
	
	public WDElem(int w, int d)
	{
		this.w = w;
		this.d = d;
	}

	public int getW() 
	{
		return w;
	}

	public void setW(int w) 
	{
		this.w = w;
	}

	public int getD()
	{
		return d;
	}

	public void setD(int d) 
	{
		this.d = d;
	}
	
	/**
	 * @param wde : WDElem to compare
	 * @return true if "this" is optimal regarding wde. 
	 * ie. this.w < wde.w or if equal this.d >= wde.d
	 */
	public boolean opt(WDElem wde)
	{
		if(wde.getW() > getW())
			return true;
		if((wde.getW() == getW()) && (wde.getD() <= getD()))
			return true;
		return false;
	}
	
	public String toString()
	{
		return "("+w+";"+d+")";
	}
}
