package fr.irisa.cairn.experimental.graphRetiming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;

/**
 * Class to resolve the retiming of a graph
 * 
 * @author FLORECK Pierrick
 *
 */
public class GraphRetiming 
{
	/** Graph to retime */
	private IGraph graph;
	/** Provider for the latency of nodes and edges */
	private IRetimingProvider<INode, IEdge> ret_provider;
	/** delta(v) = Max(Sum(d(p)) along any zero-weight directed path p in "graph") */
	private Map<INode, Integer> deltas;
	/** Temporary node : it connect inputs and outputs of the graph */
	private INode temp_node;
	
	/**
	 * Create a graph retiming to retime the graph "g"
	 * @param g : graph to retime
	 * @param latency : provider for the latency of nodes and edges
	 */
	public GraphRetiming(IGraph g, IRetimingProvider<INode, IEdge> r_prov)
	{
		graph = g;
		ret_provider = r_prov;
		initDeltas();
	}

	
	/**
	 * @return the critical path of "graph"
	 */
	public int criticalPath()
	{
		int max = 0;
		int d;
		for (INode node : graph.getNodes()) 
		{
			d = criticalPath(node);
			if(d > max)
				max = d;
		}
		return max;
	}
	
	/**
	 * Resolve the retiming with the FEAS algorithm (FEASible clock period test)
	 * @return a FEASRes that contains the clock period, the map of retiming for nodes, 
	 * and new values of weight of edges.
	 */
	public FEASRes OPT2()
	{
		/* Add a temporary node to connect inputs and outputs */
		addTempNodes();
		/* Compute the matrix WD */
		WDMatrix wd = WDCompute();
		System.out.println("Initial Matrix WD : \n" + wd);
		/* Compute the critical path */
		System.out.println("Clock period : " + criticalPath());
		/* Sort the delays */
		List<Integer> ldelays = sortDelays(wd);
		System.out.println("List of possible clock period : " + ldelays);
		/* Binary search among the elements D(u,v) with the FEAS algorithm */
		FEASRes res = binarySearchFEAS(ldelays);
		if(res != null)
			System.out.println("Best clock period : " + res.getClockPeriod());
		else
			System.out.println("No retiming possible");
		
		/* Remove the temporary node if it exist */
		removeTempNodes(res);
		return res;
	}
	
	/**
	 * Apply the retiming result on the graph "graph"
	 * @param retiming : retiming to apply
	 */
	public void applyRetiming(FEASRes retiming)
	{
		Map<IEdge, Integer> ret_edges = retiming.getRetimingEdges();
		for(IEdge edge : ret_edges.keySet())
		{
			ret_provider.setEdgeLatency(edge, ret_edges.get(edge));
		}
	}
	
	/**
	 * Initialize Map of delta with -1
	 */
	private void initDeltas()
	{
		Set<? extends INode> lnodes = graph.getNodes();
		deltas = new HashMap<INode, Integer>(lnodes.size());
		for(INode n : lnodes)
			deltas.put(n, -1);
	}
	
	/* Add temporary node to connect inputs and outputs */
	private void addTempNodes()
	{
		boolean used = false;
		for(INode n : graph.getNodes())
		{
			if(n.getInDegree() == 0)
			{
				if(!used)
				{
					temp_node = ret_provider.getTempNode();
					graph.addNode(temp_node);
					used = true;
				}
				ret_provider.connectInputToTemp(n, temp_node, graph);
			}
			if(n.getOutDegree() == 0)
			{
				if(!used)
				{
					temp_node = ret_provider.getTempNode();
					graph.addNode(temp_node);
					used = true;
				}
				ret_provider.connectOutputToTemp(n, temp_node, graph);
			}
		}
		if(used)
			initDeltas();
	}
	
	/* Remove temporary node */
	private void removeTempNodes(FEASRes res)
	{
		if(temp_node == null)
			return;
		for(IEdge e : temp_node.getIncomingEdges())
		{
			IPort p = e.getSourcePort();
			p.getNode().removeOutputPort(p);
			p = e.getSinkPort();
			temp_node.removeInputPort(p);
			res.getRetimingEdges().remove(e);
			graph.removeEdge(e);
		}
		for(IEdge e : temp_node.getOutgoingEdges())
		{
			IPort p = e.getSinkPort();
			p.getNode().removeInputPort(p);
			p = e.getSourcePort();
			temp_node.removeOutputPort(p);
			res.getRetimingEdges().remove(e);
			graph.removeEdge(e);
		}
		res.getRetimingNodes().remove(temp_node);
		graph.removeNode(temp_node);
	}
	
	/**
	 * Calcul the delta of the node n.
	 * @param n : node to analyze
	 * @return delta(n) = Max(Sum(d(p)) along any zero-weight directed path p in "graph")
	 */
	private int criticalPath(INode n)
	{
		int d = deltas.get(n);
		if(d == -1)
		{
			d = ret_provider.getNodeLatency(n);
			List<? extends IEdge> l_edge = n.getOutgoingEdges();
			int max = 0, de;
			for (IEdge edge : l_edge) 
			{
				if (ret_provider.getEdgeLatency(edge) == 0)
				{
					de = criticalPath(edge.getSinkPort().getNode());
					if(de > max)
						max = de;
				}
			}		
			d += max;
			deltas.put(n, d);
		}
		return d;
	}
	
	/**
	 * Compute the matrix WD
	 * @return the matrix WD
	 */
	private WDMatrix WDCompute()
	{
		/* Weight each edge e (u->?) in E with the ordered pair (w(e), -d(u)) */
		WDMatrix WD = weightEdgeWD();
		/* Compute the weight of the shortest path joining each connected pair
		 * of vertices with the algorithm Floyd-Warshall */
		FloydWarshall(WD);
		/* For each shortest path weight (x,y) set W(u,v)<- x and D(u,v) <- d(v) - y */
		Set<? extends INode> lnodes = graph.getNodes();
		int i_u = 0, i_v;
		while (i_u < lnodes.size())
//		for (INode u : lnodes)
		{
			i_v = 0;
			for(INode v : lnodes)
			{
				WDElem wde = WD.get(i_u, i_v);
				if(wde != null)
					wde.setD(ret_provider.getNodeLatency(v) - wde.getD());
				i_v++;
			}
			i_u++;
		}
		return WD;
	}
	
	/** 
	 * Weight each edge e (u->?) in E with the ordered pair (w(e), -d(u))
	 * @return the weighting
	 */
	private WDMatrix weightEdgeWD()
	{
		Set<? extends INode> lnodes = graph.getNodes(); 
		WDMatrix WD = new WDMatrix(lnodes.size());
		int i_u = 0;
		for (INode u : lnodes) 
		{
			int du = ret_provider.getNodeLatency(u);
			/* For each edge e u->? */
			for (IEdge edge : u.getOutgoingEdges()) 
			{
				int w = ret_provider.getEdgeLatency(edge);
				INode v = edge.getSinkPort().getNode();
				int i_v = getNumber(lnodes, v);
				WD.put(i_u, i_v, new WDElem(w, -du));
			}
			WD.put(i_u, i_u, new WDElem());
			i_u ++;
		}
		return WD;
	}
	
	/**
	 * Compute the weight of the shortest path joining each connected pair of vertices
	 * by solving an all-pairs shortest-path algorithm (Floyd-Warshall).
	 */
	private void FloydWarshall(WDMatrix weighting)
	{
		Set<? extends INode> lnodes = graph.getNodes();
		int i_u, i_v, i_k = 0;
		WDElem wd_uk, wd_kv, wd_uv;
//		for(INode k : lnodes)
		while (i_k < lnodes.size())
		{
			i_u = 0;
//			for(INode u : lnodes)
			while (i_u < lnodes.size())
			{
				i_v = 0;
//				for(INode v : lnodes)
				while (i_v < lnodes.size())
				{
					wd_uk = weighting.get(i_u, i_k);
					if(wd_uk != null)
					{
						wd_kv = weighting.get(i_k, i_v);
						if(wd_kv != null)
						{
							wd_uv = weighting.get(i_u, i_v);
							int w_ukv = wd_uk.getW() + wd_kv.getW();
							int d_ukv = wd_uk.getD() + wd_kv.getD();
							WDElem wd_ukv = new WDElem(w_ukv, d_ukv);
							if((wd_uv == null) || (!wd_uv.opt(wd_ukv)))
								weighting.put(i_u, i_v, wd_ukv);
						}
					}
					i_v++;
				}
				i_u++;
			}
			i_k++;
		}
	}
	
	/**
	 * @param ens_nodes : all nodes in the graph
	 * @param node : node to search
	 * @return number of node in the set of nodes
	 */
	private int getNumber(Set<? extends INode> ens_nodes, INode node)
	{
		int i = 0;
		for (INode n : ens_nodes) 
		{
			if(n.equals(node))
				return i;
			i++;
		}
		return -1;
	}
	
	/**
	 * Sort the delays of the matrix WD
	 * @param wd : matrix WD to sort
	 * @return list of sorted delays (each delay is present only once in the list)
	 */
	private List<Integer> sortDelays(WDMatrix wd)
	{
		int length = wd.getLength();
		List<Integer> ldelays = new ArrayList<Integer>();
		int n = 0;
		for (int i = 0; i < length; i++) 
		{
			for(int j = 0; j < length; j++)
			{
				WDElem wde = wd.get(i, j);
				if(wde != null)
				{
					int d = wde.getD();
					n += insertListDelays(ldelays, n, d);
				}
			}
		}
		return ldelays;
	}

	/**
	 * Insert the delay x in the right place in the list ldelays, if x is not 
	 * already present.
	 * @param ldelays : sorted list of delays
	 * @param n : number of elements in the list
	 * @param x : delay to insert
	 * @return 1 if x has been inserted, 0 otherwise
	 */
	private int insertListDelays(List<Integer> ldelays, int n, int x)
	{
		int pos = 0;
		int c;
		while(pos < n)
		{
			c = ldelays.get(pos);
			if(c > x)
				break;
			if(c == x)
				return 0;
			pos++;
		}
		ldelays.add(pos, x);
		return 1;
	}
	
	/**
	 * Test whether the potential clock period c is feasible with the algorithm FEAS.
	 * @param c : potential clock period
	 * @return a FEASRes if c is feasible, null otherwise
	 */
	private FEASRes FEAS(int c)
	{
		/* Save weigth of edges */
		Set<? extends IEdge> ledges = graph.getEdges();
		int[] weigth = new int[ledges.size()];
		int i_e = 0;
		for (IEdge e : ledges) 
		{
			weigth[i_e] = ret_provider.getEdgeLatency(e);
			i_e++;
		}
		
		/* Initialization retiming for each vertex */
		Set<? extends INode> lnodes = graph.getNodes();
		int length = lnodes.size();
		Map<INode, Integer> map_retiming = new HashMap<INode, Integer>(length);
		Map<IEdge, Integer> map_new_w = new HashMap<IEdge, Integer>(ledges.size()); 
		
		// For each v such that delta(v) > c, r(v) = 1, 0 otherwise
		int d;
		initDeltas();
		for(INode v : lnodes)
		{
			d = criticalPath(v);;
			if(d > c)
				map_retiming.put(v, 1); 
			else
				map_retiming.put(v, 0); 
		}
		
		for(int j = 1; j < length; j++)
		{
			/* Compute graph Gr with the existing values for r */
			i_e = 0;
			for (IEdge edge : ledges) 
			{
				INode source = edge.getSourcePort().getNode();
				INode sink = edge.getSinkPort().getNode();
				// wr(e) = w(e) + r(u) - r(v)
				int wr = weigth[i_e] + map_retiming.get(source) - map_retiming.get(sink);
				ret_provider.setEdgeLatency(edge, wr);
				map_new_w.put(edge, wr); //save new W
				i_e++;
			}

			/* Compute delta(v) for each vertex */
			initDeltas();
			// For each v such that delta(v) > c, r(v) = 1, 0 otherwise
			for(INode v : lnodes)
			{
				d = criticalPath(v);
				if(d > c)
					map_retiming.put(v, map_retiming.get(v) + 1);
			}
		}
		
		/* Restore values of edges */
		i_e = 0;
		for (IEdge e : ledges) 
		{
			ret_provider.setEdgeLatency(e, weigth[i_e]);
			i_e++;
		}
		
		int clock_period = criticalPath();		
		if(clock_period <= c) //Clock feasible
			return new FEASRes(map_new_w, map_retiming, clock_period);
		else
			return null;
	}
	
	/**
	 * Binary search among the elements of ldelays for the minimum achievable 
	 * clock period. To test whether each potential clock period is feasible, 
	 * the algorithm FEAS is performed.
	 * @param ldelays : sorted list of potential clock period
	 * @return the best result of retiming
	 */
	private FEASRes binarySearchFEAS(List<Integer> ldelays)
	{
		int inf = 0, sup = ldelays.size()-1, m, c;
		FEASRes res, old_res = null;

		while(inf <= sup)
		{
			m = (sup-inf)/2 + inf;
			c = ldelays.get(m);
			res = FEAS(c);
			
			if(res != null)
			{
				//look for a smaller clock (in the first part of the list)
				old_res = res;
				sup = m-1;
			}
			else
			{
				//look for a greater clock (in the second part of the list)
				inf = m + 1;
			}
		}
		return old_res;
	}
}
