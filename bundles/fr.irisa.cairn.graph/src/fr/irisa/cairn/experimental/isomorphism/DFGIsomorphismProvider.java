/*
 * Provider for DFGIsomorphism algorithm
 * version 1.0 (april 27 2011): first version (Chenglong Xiao)
 */
package fr.irisa.cairn.experimental.isomorphism;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.analysis.SubgraphMapping;
import fr.irisa.cairn.graph.providers.ICommutativityProvider;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.graph.providers.IIsomorphismProvider;

public class DFGIsomorphismProvider<G extends IGraph> implements IIsomorphismProvider<G>{
	private IEquivalenceProvider<IEdge, INode, IPort> equivalence;
	private ICommutativityProvider<INode> commutativity;
	public DFGIsomorphismProvider(
			IEquivalenceProvider<IEdge, INode, IPort> equivalence, ICommutativityProvider<INode> commutativity ) {
		this.equivalence = equivalence; 
		this.commutativity = commutativity;
	}

	@Override
	public List<SubgraphMapping> getAllMatches(IGraph currentGraph,
			IGraph pattern) {
		DFGIsomorphismAlgorithm p = new DFGIsomorphismAlgorithm(pattern, currentGraph,false,equivalence,commutativity);
		List<SubgraphMapping> result = p.compute();
	    return result;
	}
	
	@Override
	public SubgraphMapping getMatch(G g1, G g2) {
		// TODO Auto-generated method stub
		if (g1.getNodes().size() == g2.getNodes().size()) 
		{
			// and the same number of edges
			if (g1.getEdges().size() == g2.getEdges().size()) 
			{
				List<IGraph> patterns = new ArrayList<IGraph>();
				patterns.add(g2);
				DFGIsomorphismAlgorithm graphIsomorphism = 
						new DFGIsomorphismAlgorithm(g2, g1,true,equivalence,commutativity);
				List<SubgraphMapping> result = graphIsomorphism.compute();
				//all nodes of the two graphs must be mapped
				for(SubgraphMapping m: result)
				  if((result.size() >= 1) && (m.subjectNodes().size() == g1.getNodes().size())){
					  return m;
				  }
				return null;
			}
		}
		return null;
	}

	@Override
	public boolean isG1IsomorpheToG2(G g1, G g2) {
		// TODO Auto-generated method stub
		if (g1.getNodes().size() == g2.getNodes().size()) 
		{
			// and the same number of edges
			if (g1.getEdges().size() == g2.getEdges().size()) 
			{
				List<IGraph> patterns = new ArrayList<IGraph>();
				patterns.add(g2);
				DFGIsomorphismAlgorithm graphIsomorphism = 
						new DFGIsomorphismAlgorithm(g1, g2,true,equivalence,commutativity);
				List<SubgraphMapping> result = graphIsomorphism.compute();
				//all nodes of the two graphs must be mapped
				for(SubgraphMapping m: result)
				  if((result.size() >= 1) && (m.subjectNodes().size() == g1.getNodes().size())){
					  return true;
				  }
				return false;
			}
		}
		return false;
	}
	

}
