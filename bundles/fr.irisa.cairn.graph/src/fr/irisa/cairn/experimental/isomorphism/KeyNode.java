package fr.irisa.cairn.experimental.isomorphism;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.irisa.cairn.graph.INode;

/**
 * Starting node of a Data Flow Graph. It is a node that has no predecessors.
 * 
 * @author Chenglong Xiao
 * 
 */
public class KeyNode implements Comparable<KeyNode> {

	private INode node;
	private List<Path> paths = new ArrayList<Path>();

	public KeyNode(INode node) {
		this.node = node;
	}

	public INode getNode() {
		return node;
	}

	public void addPath(Path path) {
		paths.add(path);
	}

	public List<Path> getPaths() {
		return paths;
	}

	public Path getPath(int i) {
		return paths.get(i);
	}

	public int getPathDegree() {
		return paths.size();
	}

	public int getLongestSize() {
		Iterator<Path> i = paths.iterator();
		int size = 0;

		while (i.hasNext()) {
			int j = ((Path) i.next()).pathLength();
			if (size < j)
				size = j;
		}
		return size;
	}

	@Override
	public int compareTo(KeyNode arg0) {
		if (this.getPathDegree() == arg0.getPathDegree())
			return arg0.getLongestSize() - this.getLongestSize();
		else
			return arg0.getPathDegree() - this.getPathDegree();
	}
}
