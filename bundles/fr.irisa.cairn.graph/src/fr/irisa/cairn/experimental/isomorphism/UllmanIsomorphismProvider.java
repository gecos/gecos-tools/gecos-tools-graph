/*
 * The provider for Ullman's sub-graph isomorphism algorithm
 * version 1.0 (april 27 2011): first version (Chenglong Xiao)
 */
package fr.irisa.cairn.experimental.isomorphism;


import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.analysis.SubGraphIsomorphism;
import fr.irisa.cairn.graph.analysis.SubgraphMapping;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.graph.providers.IIsomorphismProvider;


public class UllmanIsomorphismProvider<G extends IGraph> implements IIsomorphismProvider<G> 
{
	private IEquivalenceProvider<IEdge, INode, IPort> eqProvider;

	/**
	 * @param equivalenceProvider :
	 *            the equivalence provider on nodes and edges of the IGraph
	 */
	public UllmanIsomorphismProvider(IEquivalenceProvider<IEdge, INode, IPort> equivalenceProvider) 
	{
		this.eqProvider = equivalenceProvider;
	}

	public boolean isG1IsomorpheToG2(G g1, G g2) 
	{
		// the two graphs must have the same number of nodes
		if (g1.getNodes().size() == g2.getNodes().size()) 
		{
			// and the same number of edges
			if (g1.getEdges().size() == g2.getEdges().size()) 
			{
				List<IGraph> patterns = new ArrayList<IGraph>();
				patterns.add(g2);
				SubGraphIsomorphism graphIsomorphism = 
						new SubGraphIsomorphism(g1, patterns, eqProvider);
				graphIsomorphism.setOnlyOne(true);
				List<SubgraphMapping> result = graphIsomorphism.compute();
				//all nodes of the two graphs must be mapped
				for(SubgraphMapping m: result)
				  if((result.size() >= 1) && (m.subjectNodes().size() == g1.getNodes().size())){
					  return true;
				  }
				return false;
			}
		}
		return false;
	}

	public List<SubgraphMapping> getAllMatches(IGraph currentGraph, IGraph pattern) 
	{
		List<SubgraphMapping> res = new ArrayList<SubgraphMapping>();
		List<IGraph> patterns = new ArrayList<IGraph>();
		patterns.add(pattern);
		SubGraphIsomorphism graphIsomorphism = 
				new SubGraphIsomorphism(currentGraph, patterns, eqProvider);
		graphIsomorphism.setOnlyOne(false);
		res = graphIsomorphism.compute();
		return res;
	}
	
	@Override
	public SubgraphMapping getMatch(G g1, G g2) {
		throw new RuntimeException("getMatch(G,G) is not implemented");
	}
	
}
