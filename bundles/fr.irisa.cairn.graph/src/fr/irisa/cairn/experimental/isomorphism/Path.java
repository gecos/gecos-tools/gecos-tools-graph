
package fr.irisa.cairn.experimental.isomorphism;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;

/**
 * A path in the Data Flow Graph.
 * 
 * @author Chenglong Xiao
 * 
 */
public class Path implements Cloneable {
	private IEquivalenceProvider<IEdge, INode, IPort> equivalence;
	private List<INode> nodes;

	public Path(IEquivalenceProvider<IEdge, INode, IPort> equivalence) {
		this.equivalence = equivalence;
		this.nodes = new ArrayList<INode>();
	}

	public void addNode(INode node) {
		nodes.add(node);
	}

	public int pathLength() {
		return nodes.size();
	}

	public INode getNode(int index) {
		return nodes.get(index);
	}

	public List<INode> getNodes() {
		return nodes;
	}

	/**
	 * Return true is the path is (sub)isomorphic to another path.
	 * 
	 * @param path2
	 * @param iso
	 * @return
	 */
	public boolean isIsomorph(Path path2, boolean iso) {
		List<INode> nodes = getNodes();
		int i = 0;
		for (INode node : nodes) {
			INode node2 = path2.getNode(i);
			i++;
			if (!equivalence.nodesEquivalence(node, node2)
					|| node.getInputPorts().size() != node2.getInputPorts()
							.size())
				return false;
			else {
				List<? extends INode> successors = node.getSuccessors();
				if (iso) {
					List<? extends INode> successors2 = node2.getSuccessors();
					if (successors.size() == 0 && successors2.size() == 0)
						continue;
					else if (node.getOutDegree() == node2.getOutDegree()
							&& successors.size() == successors2.size())
						continue;
					else
						return false;
				} else {
					if (!(successors.size() == 0 || node.getOutDegree() == node2
							.getOutDegree()))
						return false;
				}
			}
		}
		return true;
	}

	@Override
	public Path clone() {
		Path p = new Path(equivalence);
		p.nodes = new ArrayList<INode>(nodes);
		return p;
	}
}
