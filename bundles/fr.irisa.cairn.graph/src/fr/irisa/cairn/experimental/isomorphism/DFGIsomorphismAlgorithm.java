/*
 * A sub-graph isomorphism algorithm.
 */
package fr.irisa.cairn.experimental.isomorphism;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.analysis.SubgraphMapping;
import fr.irisa.cairn.graph.implement.providers.EdgeLabelProvider;
import fr.irisa.cairn.graph.providers.ICommutativityProvider;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;

public class DFGIsomorphismAlgorithm {
	private IGraph pattern;
	private IGraph subject;
	private boolean isomorphic;
	private List<KeyNode> starting_nodes_pattern = new ArrayList<KeyNode>();
	private List<Path> paths_pattern = new ArrayList<Path>();
	private List<SubgraphMapping> mappings = new ArrayList<SubgraphMapping>();
	private IEquivalenceProvider<IEdge, INode, IPort> equivalence;
    private ICommutativityProvider<INode> commutativity;
    
	public DFGIsomorphismAlgorithm(IGraph pattern, IGraph subject, boolean isomorphic,
			IEquivalenceProvider<IEdge, INode, IPort> equivalence,ICommutativityProvider<INode> commutativity) {
		this.pattern = pattern;
		this.subject = subject;
		this.isomorphic = isomorphic;
		this.equivalence = equivalence;
		this.commutativity = commutativity;
	}

	// find all the paths originated from key_node (node)
	private void findPaths(Path path, INode node, KeyNode key_node) {
		int size = node.getSuccessors().size();
		if (size > 0) {
			INode inode;
			Iterator<? extends INode> i = node.getSuccessors().iterator();
			if (size == 1) {
				inode = (INode) i.next();
				path.addNode(inode);
				findPaths(path, inode, key_node);
			} else {
				while (i.hasNext()) {
					Path newPath;
					inode = (INode) i.next();
					newPath = (Path) path.clone();
					newPath.addNode(inode);
					findPaths(newPath, inode, key_node);
				}
			}
		} else {
			key_node.addPath(path);
			paths_pattern.add(path);
		}

	}

	// find all the paths for a starting node with specified length
	private void findPaths(Path path, INode node, int length, KeyNode key_node) {
		if (path.pathLength() == 0)
			path.addNode(node);
		int size = node.getSuccessors().size();
		if (size > 0 && length > 1) {
			INode inode;
			Iterator<? extends INode> i = node.getSuccessors().iterator();
			if (size == 1) {
				length--;
				inode = (INode) i.next();
				path.addNode(inode);
				findPaths(path, inode, length, key_node);
			} else if (size > 1) {
				length--;
				while (i.hasNext()) {
					Path newPath;
					inode = (INode) i.next();
					newPath = (Path) path.clone();
					newPath.addNode(inode);
					findPaths(newPath, inode, length, key_node);
				}
			}
		} else {
			key_node.addPath(path);
		}

	}

	// to find the starting nodes and their relevant paths in pattern graph
	private void findKeyNodes(IGraph g) {
		for (INode node : g.getNodes()) {
			if (isStartingNode(node)) {
				KeyNode key_node = new KeyNode(node);
				Path path = new Path(equivalence);
				path.addNode(node);
				findPaths(path, node, key_node);
				starting_nodes_pattern.add(key_node);
			}
		}
	}

	private boolean isStartingNode(INode node) {
		if (node.getPredecessors().size() == 0)
			return true;
		else
			return false;
	}

	public List<SubgraphMapping> compute() {
		findKeyNodes(pattern);
		nodeSort(starting_nodes_pattern);
		// calculate the distance between the first starting node and the other
		// starting nodes
		List<Integer> distances = new ArrayList<Integer>();
		if (starting_nodes_pattern.size() > 1 && !isomorphic)
			distances = caculateDistances(starting_nodes_pattern);
		// for sub-graph isomorphism
		if (!isomorphic)
			recursiveSubIsomorphism(new SubgraphMapping(),
					starting_nodes_pattern, new ArrayList<INode>(), 0,
					distances, null);
		// for graph isomorphism
		else {
			ArrayList<INode> node_area = new ArrayList<INode>();
			for (INode n : subject.getNodes()) {
				if (isStartingNode(n))
					node_area.add(n);
			}
			recursiveIsomorphism(new SubgraphMapping(), starting_nodes_pattern,
					new ArrayList<INode>(), 0, node_area);
		}
//		mappings  = filterIsomorphicMatches(mappings);
		return mappings;
	}
	
	List<SubgraphMapping> filterIsomorphicMatches(List<SubgraphMapping> matches){
		List<SubgraphMapping> filtered = new ArrayList<SubgraphMapping>();
		
		boolean redundants[] = new boolean[matches.size()];
		
		for (int m1 = 0; m1 < matches.size(); m1++) {
			SubgraphMapping subMapping1 = matches.get(m1);
			for (int m2 = 0; m2 < matches.size(); m2++) {
				SubgraphMapping subMapping2 = matches.get(m2);
				if(m1!=m2){
					if(subMapping1.isEquivalent(subMapping2)){
						redundants[m2]=true;
					}
				}
			}
		}
		for (int m1 = 0; m1 < matches.size(); m1++) {
			SubgraphMapping subMapping = matches.get(m1);
			if(!redundants[m1])
				filtered.add(subMapping);
		}

		return filtered;
	}

	

	
	/**
	 * calculate the distance between the first starting node and the other
	 * starting nodes
	 * 
	 * @param startingNodes
	 * @return
	 */
	private static List<Integer> caculateDistances(List<KeyNode> startingNodes) {
		List<Integer> distances = new ArrayList<Integer>();
		for (int i = 1; i < startingNodes.size(); i++) {
			INode node = startingNodes.get(i).getNode();
			int d = findDistance(startingNodes.get(0).getNode(), node);
			distances.add(d);
		}
		return distances;
	}

	private void recursiveIsomorphism(SubgraphMapping mapping,
			List<KeyNode> nodes, List<INode> temp_record, int i,
			List<INode> node_area) {
		if (i < nodes.size()) {
			KeyNode node = nodes.get(i);
			i++;

			List<KeyNode> cnode = findCorrespondingStartingNodes(node,
					node_area);
			int size = longestPathSize(node);
			int j;

			for (j = 0; j < cnode.size(); ++j) {
//				SubgraphMapping new_mapping =  mapping.clone();
				ArrayList<INode> traversed_node = new ArrayList<INode>(temp_record);
				KeyNode temp = (KeyNode) cnode.get(j);

				if (traversed_node.contains(temp.getNode()))
					continue;

				findPaths(new Path(equivalence), temp.getNode(), size, temp);

				if ((longestPathSize(temp) == size)) {
					ArrayList<SubgraphMapping> temp_mappings = new ArrayList<SubgraphMapping>();
					findPartialMappings(temp_mappings, mapping, node, 0, temp,
							new ArrayList<Path>());
					if (temp_mappings.size() > 0) {
						traversed_node.add(temp.getNode());
						for (SubgraphMapping m : temp_mappings)
							recursiveIsomorphism(m, nodes, traversed_node, i,
									node_area);
					}
				}
			}
		} else {
			mappings.add(mapping);
		}
	}

	private void recursiveSubIsomorphism(SubgraphMapping mapping,
			List<KeyNode> roots, List<INode> temp_record, int testedRoot,
			List<Integer> distances, INode first_node) {
//		if(roots.size()==1&&pattern.getNodes().size()==2)
//			System.out.print("this is!");
		if (testedRoot < roots.size()) {
			KeyNode root = roots.get(testedRoot);
			testedRoot++;

			int d = 0;
			if (testedRoot > 1) {
				d = distances.get(testedRoot - 2);
			}

			List<INode> node_area = new ArrayList<INode>();
			if (testedRoot > 1) {
				node_area = findNodesUnderDistance(first_node, d);
			} else {
				node_area.addAll(subject.getNodes());
			}
			List<KeyNode> rootsInSubject = findCorrespondingStartingNodes(root,
					node_area);
			int size = longestPathSize(root);

			for (int j = 0; j < rootsInSubject.size(); ++j) {
//				SubgraphMapping new_mapping = (SubgraphMapping) mapping.clone();
				ArrayList<INode> traversed_nodes = new ArrayList<INode>(
						temp_record);
				KeyNode temp = (KeyNode) rootsInSubject.get(j);

				if (testedRoot == 1 && roots.size() > 1)
					first_node = (INode) rootsInSubject.get(j).getNode();

				if (traversed_nodes.contains(temp.getNode()))
					continue;

				findPaths(new Path(equivalence), temp.getNode(), size, temp);

				if ((!(longestPathSize(temp) < size))) {
					ArrayList<SubgraphMapping> temp_mappings = new ArrayList<SubgraphMapping>();
					findPartialMappings(temp_mappings, mapping, root, 0, temp,
							new ArrayList<Path>());
					if (temp_mappings.size() > 0) {
						traversed_nodes.add(temp.getNode());
						for (SubgraphMapping m : temp_mappings)
							recursiveSubIsomorphism(m, roots, traversed_nodes,
									testedRoot, distances, first_node);
					}
				}
			}
		} else {
			if(mapping.subjectNodes().size()==pattern.getNodes().size())
			    mappings.add(mapping);
		}
	}

	private void findPartialMappings(
			ArrayList<SubgraphMapping> partial_mappings,
			SubgraphMapping mapping, KeyNode node_pattern, int index,
			KeyNode node_subject, ArrayList<Path> temp_record) {
		if (index < node_pattern.getPathDegree()) {
			Path pattern = node_pattern.getPath(index);
			index++;
			int k = 0;
			for (k = 0; k < node_subject.getPathDegree(); k++) {
				ArrayList<Path> traversed_paths = new ArrayList<Path>(temp_record);
				Path subject = node_subject.getPath(k);
				if (traversed_paths.contains(subject))
					continue;
				traversed_paths.add(subject);
				SubgraphMapping partial_mapping = (SubgraphMapping) mapping
						.clone();
				if (pattern.isIsomorph(subject, isomorphic)) {
//					int j = 0;
					boolean tag = true;
					for (int i = 0; i < pattern.pathLength(); i++) {
						if ((partial_mapping.isMapped(subject.getNode(i)) && pattern
								.getNode(i) != partial_mapping
								.getPatternNode(subject.getNode(i)))
								|| (partial_mapping.isMatched(pattern
										.getNode(i)) && partial_mapping
										.getSubjectNode(pattern.getNode(i)) != subject
										.getNode(i))) {
							tag = false;
							break;
						} else if (commutativityCheck(pattern.getNode(i),subject.getNode(i),partial_mapping)&&!partial_mapping
								.isMapped(subject.getNode(i))
								&& !partial_mapping.isMatched(pattern
										.getNode(i))){
							// node mapping
							partial_mapping.put(subject.getNode(i),
									pattern.getNode(i));
							  // output port mapping
						    if(subject.getNode(i).getOutputPorts().size()>0)
						        partial_mapping.put(pattern.getNode(i).getOutputPorts().get(0), subject.getNode(i).getOutputPorts().get(0));
						    // input port mapping
						    for(IPort inp: pattern.getNode(i).getInputPorts()){
						    	EdgeLabelProvider<IEdge> lp = new EdgeLabelProvider<IEdge>();
		                            IEdge ep = null;
		                            String a = null;
		                            if(inp.getEdges().size()>0){
		                               ep = (IEdge)inp.getEdges().toArray()[0];
		                               a = lp.getEdgeLabel(ep);
		                            }
						    		for(IPort ins : subject.getNode(i).getInputPorts()){
						    			IEdge es = null;
						    			String b = null;
						    			if(ins.getEdges().size()>0){
						    			   es=(IEdge) ins.getEdges().toArray()[0];
                                           b = lp.getEdgeLabel(es);
						    			}
						    			if(a!=null&&b!=null&&a.equals(b)&&partial_mapping.getSubjectPort(inp)==null&&partial_mapping.getPatternPort(ins)==null)
								    		partial_mapping.put(inp, ins);
						    			if((ep==null||es==null)&&partial_mapping.getSubjectPort(inp)==null&&partial_mapping.getPatternPort(ins)==null)
						    				partial_mapping.put(inp, ins);

						    		}
					         }		
						  
						}
					}
					if (tag == true)
						findPartialMappings(partial_mappings, partial_mapping,
								node_pattern, index, node_subject,
								traversed_paths);
					}
			}
		} else {

			partial_mappings.add(mapping);

		}

	}

	
	private boolean commutativityCheck(INode pattern, INode subject, SubgraphMapping mapping){
		  //  commutativity check
		   if(!commutativity.isCommutative(pattern)){
		        for(INode n1 : pattern.getPredecessors()){
		        	if(mapping.isMatched(n1)){
		        		IEdge e1 = null;
		        		for(IEdge e11: n1.getOutgoingEdges()){
		        			if(e11.getSinkPort()!=null&&e11.getSinkPort().getNode()==pattern){
		        				e1 = e11;
		        				break;
		        			}
		        		}
		     
		        		INode n2 = mapping.getSubjectNode(n1);
		        		
		        		IEdge e2 = null;
		        		for(IEdge e22: n2.getOutgoingEdges()){
		        			if(e22.getSinkPort()!=null&&e22.getSinkPort().getNode()==subject){
		        				e2 = e22;
		        				break;
		        			}
		        		}
		        		EdgeLabelProvider<IEdge> lp = new EdgeLabelProvider<IEdge>();
		        		String a = "";
		        		if(e1!=null)
		        		    a = lp.getEdgeLabel(e1);
		        		String b = "";
		        		if(e2!=null)
		        		    b = lp.getEdgeLabel(e2);
		        		if(a.equals(b))
		        			return true;
		        		else 
		        			return false;
		        	}
		        }
		   }
		   return true;
	}
	
	// to get the longest size of the paths originated from the specified node
	private int longestPathSize(KeyNode keynode) {

		Iterator<Path> i = keynode.getPaths().iterator();
		int size = 0;

		while (i.hasNext()) {
			int j = ((Path) i.next()).pathLength();
			if (size < j)
				size = j;
		}
		return size;
	}

	/**
	 * Find corresponding possible starting nodes in subject graph
	 * 
	 * @param keynode
	 * @param node_area
	 * @return
	 */
	private List<KeyNode> findCorrespondingStartingNodes(KeyNode keynode,
			List<INode> node_area) {
		List<KeyNode> starting_nodes = new ArrayList<KeyNode>();
		INode startingNode = keynode.getNode();
		for (INode node : node_area) {
			if (equivalence.nodesEquivalence(node, startingNode)) {
				if (startingNode.getSuccessors().size() == 0) {
					KeyNode new_node = new KeyNode(node);
					starting_nodes.add(new_node);
				} else if (startingNode.getSuccessors().size() != 0
						&& startingNode.getOutDegree() == node.getOutDegree()) {
					if (areSuccessorsMapping(keynode.getNode(), node)) {
						if (!isomorphic) {
							KeyNode new_node = new KeyNode(node);
							starting_nodes.add(new_node);
						} else if (startingNode.getSuccessors().size() == node
								.getSuccessors().size()) {
							KeyNode new_node = new KeyNode(node);
							starting_nodes.add(new_node);
						}
					}
				}
			}
		}
		return starting_nodes;
	}

	private static int findDistance(INode node1, INode node2) {
		int distance = 0;
		List<INode> currentNodes = new ArrayList<INode>();
		List<INode> neighbours = new ArrayList<INode>();
		currentNodes.add(node1);
		neighbours = findNeighbours(currentNodes);
		while (neighbours.size() > 0) {
			currentNodes.addAll(neighbours);
			neighbours = findNeighbours(currentNodes);
			distance++;
			if (currentNodes.contains(node2))
				return distance;
		}
		return distance;
	}

	private static List<INode> findNeighbours(List<INode> nodes) {
		List<INode> neighbours = new ArrayList<INode>();
		for (INode node : nodes) {
			for (INode pre : node.getPredecessors())
				if (!nodes.contains(pre))
					neighbours.add(pre);
			for (INode des : node.getSuccessors())
				if (!nodes.contains(des))
					neighbours.add(des);
		}

		return neighbours;

	}

	private static List<INode> findNodesUnderDistance(INode node, int distance) {
		List<INode> ns = new ArrayList<INode>();
		ns.add(node);
		for (int i = 1; i <= distance; i++) {
			List<INode> ts = findNeighbours(ns);
			ns.addAll(ts);
		}
		return ns;
	}

	private boolean areSuccessorsMapping(INode node1, INode node2) {
		int j = 0;
		List<Integer> set2 = new ArrayList<Integer>();
		int size1 = node1.getSuccessors().size();
		int size2 = node2.getSuccessors().size();
		if (size2 < size1)
			return false;
		for (j = 0; j < size2; j++) {
			INode succ = node2.getSuccessors().get(j);
			set2.add(getNodeTypeIdentifier(succ));
		}
		for (j = 0; j < size1; j++) {
			INode succ = node1.getSuccessors().get(j);
			int succID = getNodeTypeIdentifier(succ);
			if (set2.contains(succID))
				set2.remove(new Integer(succID));
			else
				return false;
		}
		return true;
	}

	private Map<NodeType, Integer> typesIdentifiersMap=new HashMap<NodeType, Integer>();

	/**
	 * Get the ID of the type of node (using  the {@link IEquivalenceProvider})
	 * @param n
	 * @return
	 */
	private int getNodeTypeIdentifier(INode n) {
		NodeType type = new NodeType(n, equivalence);
		Integer id = typesIdentifiersMap.get(type);
		if (id == null) {
			id = typesIdentifiersMap.size() + 1;
			typesIdentifiersMap.put(type, id);
		}
		return id;
	}

	/**
	 * Type of a node. Use an {@link IEquivalenceProvider} to test the
	 * equivalence of two types.
	 * 
	 * @author antoine
	 * 
	 */
	private static class NodeType {
		private INode ref;
		private IEquivalenceProvider<IEdge, INode, IPort> equivalence;

		public NodeType(INode ref,
				IEquivalenceProvider<IEdge, INode, IPort> equivalence) {
			this.ref = ref;
			this.equivalence = equivalence;
		}

		@Override
		public int hashCode() {
			return 1;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NodeType other = (NodeType) obj;
			if (ref == null) {
				if (other.ref != null)
					return false;
			} else if (!equivalence.nodesEquivalence(ref, other.ref))
				return false;
			return true;
		}

	}

	// to sort the starting nodes in pattern graph according to the number of
	// paths originated from each starting node
	private void nodeSort(List<KeyNode> nodes) {
		Collections.sort(nodes);
	}

}
