package fr.irisa.cairn.typedGraph;

import fr.irisa.cairn.graph.IEdge;

public interface ITypedGraphFactory {
	public final static ITypedGraphFactory	instance	= new TypedGraphFactory();

	/**
	 * Default connection between two nodes. Use the first output port of the
	 * source node and add a new input port on the sink.
	 * 
	 * @param source
	 *            source node
	 * @param sink
	 *            destination node
	 * @param type
	 *            type of input/output ports
	 * @return
	 */
	public IEdge connect(ITypedNode source, ITypedNode sink, IType type);

	public IEdge connect(ITypedPort source, ITypedPort sink);

	public ITypedCollapsedNode createCollapsedNode(IType type, boolean directed);

	public ITypedGraph createGraph(boolean directed);

	public ITypedNode createNode(IType type);

	/**
	 * Create a node with given number of inputs and outputs ports.
	 * 
	 * @param type
	 *            node/ports type
	 * @param inputs
	 *            number of inputs ports
	 * @param outputs
	 *            number of outputs ports
	 * @return
	 */
	public ITypedNode createNode(IType type, int inputs, int outputs, IType portsType);

	public ITypedPort createPort(IType p);

	public ITypedSubGraph createSubgraph(boolean directed);
}
