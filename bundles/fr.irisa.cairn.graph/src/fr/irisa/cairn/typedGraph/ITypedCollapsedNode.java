package fr.irisa.cairn.typedGraph;

import fr.irisa.cairn.graph.ICollapsedNode;

public interface ITypedCollapsedNode  extends ITypedSubGraph,ICollapsedNode{

}
