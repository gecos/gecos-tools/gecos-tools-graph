package fr.irisa.cairn.typedGraph.implement;

import java.util.Set;

import fr.irisa.cairn.graph.implement.Graph;
import fr.irisa.cairn.typedGraph.ITypedGraph;
import fr.irisa.cairn.typedGraph.ITypedNode;

public class TypedGraph extends Graph implements ITypedGraph {

	public TypedGraph(boolean directed) {
		super(directed);

	}
	
	@SuppressWarnings("unchecked")
	public Set<? extends ITypedNode> getNodes(){
		return (Set<? extends ITypedNode>) super.getNodes();
	}

}
