package fr.irisa.cairn.typedGraph.implement;

import java.util.Hashtable;
import java.util.Map;

import fr.irisa.cairn.typedGraph.IType;

/**
 * An implementation of {@link IType}. Each type has a name and an attribute
 * which can be any object. Two types are equivalent if they share the same name
 * and if their attributes are equals.
 * 
 * @author antoine
 * 
 */
public class Type implements IType {
	protected String name;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	protected Object attributes;

	public Type(String name, Object attributes) {
		this.name = name;
		this.attributes = attributes;
	}

	public Object getAttributes(){
		return attributes;
	}
	
	public boolean equivalent(IType t) {
		if (t instanceof Type) {
			Type res = (Type) t;
			if (name.equals(res.name)) {
				if (attributes.equals(res.attributes))
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Test if the type has the same name than another type.
	 * @param t
	 * @return
	 */
	public boolean sameTypeFamily(IType t){
		if (t instanceof Type) {
			Type res = (Type) t;
			return name.equals(res.name);
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Type) {
			return equivalent((Type) obj);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public String toString() {
		return name + "(" + attributes.toString() + ")";
	}

	/**
	 * A type container give access to singleton instances of types.
	 * 
	 * @author antoine
	 * 
	 */
	public static class DynamicTypeContainer {
		protected Map<String, TypeSingleton> singletons;

		/**
		 * Construct a type container from an array of types names.
		 * 
		 * @param types
		 */
		public DynamicTypeContainer(String... types) {
			this.singletons = new Hashtable<String, TypeSingleton>();
			initialize(types);
		}

		protected void initialize(String types[]) {
			for (String type : types) {
				singletons.put(type, new TypeSingleton(type));
			}
		}

		/**
		 * Add a new type singleton to the container.
		 * 
		 * @param type
		 * @param attributs
		 * @return
		 */
		protected TypeSingleton dynamicTypeSingleton(String type,
				Object attributs) {
			TypeSingleton typeSingleton = new TypeSingleton(type);
			singletons.put(type, typeSingleton);
			return typeSingleton;
		}

		/**
		 * Get the singleton of a type from its name and its attributs.
		 * 
		 * @param type
		 *            type name
		 * @param attributs
		 *            attributs of the type
		 * @return a singleton type
		 */
		public IType getTypeInstance(String type, Object attributs) {
			TypeSingleton typeSingleton = singletons.get(type);
			if (typeSingleton == null) {
				typeSingleton = dynamicTypeSingleton(type, attributs);
			}
			return typeSingleton.getInstance(attributs);
		}
	}

	public static class TypeSingleton {
		protected String name;
		protected Map<Object, IType> instances;

		public TypeSingleton(String name) {
			this.name = name;
			this.instances = new Hashtable<Object, IType>();
		}

		public IType getInstance(Object key) {
			IType instance = instances.get(key);
			if (instance == null) {
				instance = new Type(name, key);
				instances.put(key, instance);
			}
			return instance;
		}
	}
}
