package fr.irisa.cairn.typedGraph.implement.providers;

import java.util.HashMap;

import com.google.inject.Inject;

import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.implement.Type;
import fr.irisa.cairn.typedGraph.providers.ITypeAttributProvider;

public class TypeAttributProvider implements ITypeAttributProvider {

	@Inject
	private TypeAttributProvider() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings({"rawtypes","unchecked"}) 
	public Object getAttribut(IType type, Object key) {
		Type t = (Type) type;
		if (t.getAttributes() instanceof HashMap) {
			HashMap<Object, Object> map = (HashMap) t.getAttributes();
			return map.get(key);
		} else
			return null;
	}

	public Object getAttribut(IType type) {
		Type t = (Type) type;
		return t.getAttributes();
	}

}
