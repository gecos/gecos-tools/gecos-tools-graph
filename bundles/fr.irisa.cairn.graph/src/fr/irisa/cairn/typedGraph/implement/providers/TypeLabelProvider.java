package fr.irisa.cairn.typedGraph.implement.providers;

import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.implement.Type;
import fr.irisa.cairn.typedGraph.providers.ITypeLabelProvider;

public class TypeLabelProvider implements ITypeLabelProvider{

	public String getLabel(IType t) {
		Type type = (Type) t;
		return type.getName();
	}

}
