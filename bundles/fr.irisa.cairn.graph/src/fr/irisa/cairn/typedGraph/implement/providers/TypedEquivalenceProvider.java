package fr.irisa.cairn.typedGraph.implement.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.graph.tools.GraphTools;
import fr.irisa.cairn.typedGraph.ITypedNode;
@SuppressWarnings({"rawtypes"}) 
public class TypedEquivalenceProvider implements IEquivalenceProvider {

	public boolean edgesEquivalence(IEdge e1, IEdge e2) {
		boolean sourcesEquivalence = nodesEquivalence(GraphTools
				.getSourceNode(e1), GraphTools.getSourceNode(e2));

		if (sourcesEquivalence) {
			boolean ssinksEquivalence = nodesEquivalence(GraphTools
					.getSinkNode(e1), GraphTools.getSinkNode(e2));
			if (ssinksEquivalence) {
				ITypedNode s1 = (ITypedNode) e1.getSourcePort();
				ITypedNode s2 = (ITypedNode) e2.getSourcePort();
				if (s1.getType().equivalent(s2.getType())) {
					ITypedNode t1 = (ITypedNode) e1.getSinkPort();
					ITypedNode t2 = (ITypedNode) e2.getSinkPort();
					return t1.getType().equivalent(t2.getType());
				}

			}
		}
		return false;

	}

	public boolean nodesEquivalence(INode n1, INode n2) {
		ITypedNode typedN1 = (ITypedNode) n1;
		ITypedNode typedN2 = (ITypedNode) n2;		
		return (typedN1.getType().equivalent(typedN2.getType()));
	}

	public boolean portsEquivalence(IPort p1, IPort p2) {
		return true;
	}

}
