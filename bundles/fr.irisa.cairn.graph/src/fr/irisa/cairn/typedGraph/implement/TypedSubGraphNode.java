package fr.irisa.cairn.typedGraph.implement;

import java.util.List;
import java.util.Set;

import fr.irisa.cairn.graph.implement.SubGraphNode;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedGraphVisitor;
import fr.irisa.cairn.typedGraph.ITypedNode;
import fr.irisa.cairn.typedGraph.ITypedPort;
import fr.irisa.cairn.typedGraph.ITypedSubGraph;

public class TypedSubGraphNode extends SubGraphNode implements ITypedSubGraph {

	public TypedSubGraphNode(final boolean directed) {
		super(directed);
	}

	@Override
	public void accept(final ITypedGraphVisitor graphVistor, final Object arg) {
		graphVistor.visitTypedSubGraphNode(this, arg);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedPort> getInputPorts() {
		// TODO Auto-generated method stub
		return (List<? extends ITypedPort>) super.getInputPorts();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Set<? extends ITypedNode> getNodes() {
		return (Set<? extends ITypedNode>) super.getNodes();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedPort> getOutputPorts() {
		// TODO Auto-generated method stub
		return (List<? extends ITypedPort>) super.getOutputPorts();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedNode> getPredecessors() {
		// TODO Auto-generated method stub
		return (List<? extends TypedNode>) super.getPredecessors();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedNode> getSuccessors() {
		// TODO Auto-generated method stub
		return (List<? extends TypedNode>) super.getSuccessors();
	}

	@Override
	public IType getType() {
		return null;
	}

	@Override
	protected void initialize(final boolean directed) {
		this.subgraph = new TypedGraph(directed);
		this.subgraph.setParent(this);
	}

	@Override
	public void setType(final IType t) {

	}

}
