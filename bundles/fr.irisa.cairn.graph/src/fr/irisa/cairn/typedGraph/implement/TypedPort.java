package fr.irisa.cairn.typedGraph.implement;

import fr.irisa.cairn.graph.implement.Port;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedPort;

public class TypedPort extends Port implements ITypedPort {
	private IType type;

	public TypedPort(IType type) {
		super();
		this.type = type;
	}



	public IType getType() {
		return type;
	}



	/**
	 * @param type the type to set
	 */
	public void setType(IType type) {
		this.type = type;
	}

}
