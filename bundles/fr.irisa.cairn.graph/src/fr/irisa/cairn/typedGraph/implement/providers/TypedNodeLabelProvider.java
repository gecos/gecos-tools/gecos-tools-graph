/*
 * File : TypedNodeLabelProvider.java
 * Created on Oct 3, 2008
 * Project : fr.irisa.cairn.graph
 */
package fr.irisa.cairn.typedGraph.implement.providers;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.implement.providers.NodeLabelProvider;
import fr.irisa.cairn.typedGraph.implement.TypedNode;
import fr.irisa.cairn.typedGraph.providers.ITypeLabelProvider;

/**
 * @author Kevin Martin
 */
@SuppressWarnings({"rawtypes"}) 
public class TypedNodeLabelProvider extends NodeLabelProvider {

	private ITypeLabelProvider typeLabelProvider;
	
	@Inject
	public TypedNodeLabelProvider(ITypeLabelProvider typeLabelProvider){
		this.typeLabelProvider = typeLabelProvider;
	}
	
	@Override
	public String getNodeLabel(INode n) {
		TypedNode typedNode = (TypedNode) n;
		return typeLabelProvider.getLabel(typedNode.getType());
	}
	
	
}
