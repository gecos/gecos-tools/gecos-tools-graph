package fr.irisa.cairn.typedGraph.implement;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.implement.CollapsedNode;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedCollapsedNode;
import fr.irisa.cairn.typedGraph.ITypedGraphVisitor;
import fr.irisa.cairn.typedGraph.implement.providers.TypedClonerProvider;
@SuppressWarnings({"rawtypes","unchecked"}) 
public class TypedCollapsedNode extends TypedSubGraphNode implements
		ITypedCollapsedNode {
	private IType type;
	private Map<IPort, IPort> inputsMap;
	private Map<IPort, IPort> outputsMap;
	protected ICloneProvider cloner;

	public TypedCollapsedNode(boolean directed) {
		super(directed);
		this.inputsMap = new Hashtable<IPort, IPort>();
		this.outputsMap = new Hashtable<IPort, IPort>();
		this.cloner = new TypedClonerProvider();
	}

	@Override
	public IType getType() {
		return type;
	}

	public List<? extends INode> expand() {
		return CollapsedNode.expandableAspect(this, cloner);
	}

	public IPort getMappedInputPort(IPort in) {
		return inputsMap.get(in);
	}

	public IPort getMappedOutputPort(IPort out) {
		return outputsMap.get(out);
	}

	public void mapInput(IPort in, IPort nestedPort) {
		inputsMap.put(in, nestedPort);

	}

	public void mapOutput(IPort out, IPort nestedPort) {
		outputsMap.put(out, nestedPort);

	}

	public void setType(IType t) {
		this.type = t;
	}
	
	@Override
	public void accept(IGraphVisitor graphVistor, Object arg) {
		graphVistor.visitCollapsedNode(this, arg);
	}
	
	@Override
	public void accept(ITypedGraphVisitor graphVistor, Object arg) {
		graphVistor.visitTypedCollapsedNode(this, arg);
	}
}
