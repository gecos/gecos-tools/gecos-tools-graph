package fr.irisa.cairn.typedGraph.implement.providers;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.implement.Edge;
import fr.irisa.cairn.graph.implement.Graph;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.graph.tools.GraphTools;
import fr.irisa.cairn.typedGraph.ITypedGraphFactory;
import fr.irisa.cairn.typedGraph.ITypedNode;
import fr.irisa.cairn.typedGraph.ITypedPort;
import fr.irisa.cairn.typedGraph.TypedGraphFactory;
import fr.irisa.cairn.typedGraph.implement.TypedNode;
@SuppressWarnings({"rawtypes"}) 
public class TypedClonerProvider implements ICloneProvider {

	@Inject
	public TypedClonerProvider() {
		// TODO Auto-generated constructor stub
	}
	
	public IEdge cloneEdge(IEdge e) {
		return new Edge(e.getSourcePort(), e.getSinkPort());
	}

	public ITypedNode cloneNode(INode n) {
		TypedNode typedN = (TypedNode) n;
		TypedNode clone = new TypedNode(typedN.getType());
		for (IPort in : n.getInputPorts()) {
			clone.addInputPort(clonePort(in));
		}

		for (IPort out : n.getOutputPorts()) {
			clone.addOutputPort(clonePort(out));
		}
		return clone;
	}

	public IGraph createGraph(boolean isDirected) {
		return new Graph(isDirected);
	}

	public ITypedPort clonePort(IPort p) {
		ITypedGraphFactory factory = new TypedGraphFactory();
		ITypedPort tp = (ITypedPort) p;
		return factory.createPort(tp.getType());
	}

	public void reconnectClone(IEdge clone, IEdge edge, INode newSource,
			INode newSink) throws GraphException {
		int pSource = GraphTools.getSourcePortNumber(edge);
		int pSink = GraphTools.getSinkPortNumber(edge);

		clone.reconnectSourcePort(GraphTools.getOutputPort(newSource, pSource));
		clone.reconnectSinkPort(GraphTools.getInputPort(newSink, pSink));
	}

}
