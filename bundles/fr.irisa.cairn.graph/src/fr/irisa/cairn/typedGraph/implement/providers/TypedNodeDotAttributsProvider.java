/*
 * File : TypedNodeDotAttributsProvider.java
 * Created on Oct 3, 2008
 * Project : fr.irisa.cairn.graph
 */
package fr.irisa.cairn.typedGraph.implement.providers;


import java.util.HashMap;
import java.util.Map;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.io.DOTExport.DotAttributs;
import fr.irisa.cairn.graph.io.util.Color;
import fr.irisa.cairn.graph.io.util.ColorPalette;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.implement.TypedNode;
import fr.irisa.cairn.typedGraph.providers.ITypeLabelProvider;

/**
 * A dot attribut provider for typed nodes.
 * 
 * @author antoine
 */
@SuppressWarnings({"rawtypes","unchecked"}) 
public class TypedNodeDotAttributsProvider extends DotAttributsProvider {

	private Map<String, Color> opColors;
	private ITypeLabelProvider typeLabelProvider;

	@Inject
	public TypedNodeDotAttributsProvider(ITypeLabelProvider typesLabelProvider) {
		opColors = new HashMap<String, Color>();
		this.typeLabelProvider = typesLabelProvider;
	}

	@Override
	protected void findNodeAttributs(INode n) {
		super.findNodeAttributs(n);
		addNodeProperty(n, DotAttributs.STYLE, "filled");
		TypedNode typedNode = (TypedNode) n;
		Color color = getTypeColor(typedNode.getType());
		addNodeProperty(n, DotAttributs.NODE_COLOR, ColorPalette.getHexColor(color));
	}


	private Color getTypeColor(IType t) {
		String typeLabel = typeLabelProvider.getLabel(t);
		Color color = opColors.get(typeLabel);
		if (color == null) {
			color = palette.getColor(opColors.size()-1);
			opColors.put(typeLabel, color);
		}
		return color;
	}
	
	@Deprecated
	public static String getHexColor(Color c) {
		return ColorPalette.getHexColor(c);
	}

	
}
