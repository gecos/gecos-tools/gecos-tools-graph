package fr.irisa.cairn.typedGraph.implement.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.typedGraph.ITypedNode;

@SuppressWarnings("rawtypes")
public class TypedSubEquivalenceProvider implements IEquivalenceProvider 
{

	/**
	 * @param ep : edge of pattern
	 * @param es : edge of principal graphe
	 * @return true if the two edges are equivalents
	 */
	public boolean edgesEquivalence(IEdge ep, IEdge es) 
	{
		/* Two arcs are equivalent if their source node are equivalent 
		 * and their destination node are equivalent */
		ITypedNode depp = (ITypedNode)ep.getSourcePort().getNode();
		ITypedNode deps = (ITypedNode)es.getSourcePort().getNode();
		ITypedNode arrp = (ITypedNode)ep.getSinkPort().getNode();
		ITypedNode arrs = (ITypedNode)es.getSinkPort().getNode();
		return ((nodesEquivalence(depp, deps)) && (nodesEquivalence(arrp, arrs)));
	}

	/**
	 * Defined if two nodes are equivalents
	 * @param n1 : first node
	 * @param n2 : second node
	 * @return true if the two node are equivalents
	 */
	public boolean nodesEquivalence(INode n1, INode n2) 
	{
		ITypedNode typedN1 = (ITypedNode) n1;
		ITypedNode typedN2 = (ITypedNode) n2;
		
		//Compare the type of nodes
		if(typedN1.getType().equivalent(typedN2.getType()))
		{
			//Multiple outputs
			boolean mul_outputs = true;  
			//If the pattern node has a successor we verified the number of output arcs
			if(n1.getSuccessors().size() >= 1)
				mul_outputs = (n2.getOutDegree() == n1.getOutDegree());
			return (mul_outputs && ((n2.getInputPorts().size() >= n1.getInputPorts().size()) && (n2.getOutputPorts().size() >= n1.getOutputPorts().size())));
		}
		return false;
	}

	public boolean portsEquivalence(IPort p1, IPort p2) {
		return true;
	}
}
