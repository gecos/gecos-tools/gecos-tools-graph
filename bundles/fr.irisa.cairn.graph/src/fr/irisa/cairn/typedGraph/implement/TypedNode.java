package fr.irisa.cairn.typedGraph.implement;

import java.util.List;

import fr.irisa.cairn.graph.implement.Node;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedGraphVisitor;
import fr.irisa.cairn.typedGraph.ITypedNode;
import fr.irisa.cairn.typedGraph.ITypedPort;

public class TypedNode extends Node implements ITypedNode {
	private IType type;

	public TypedNode(IType type) {
		super();
		this.type = type;
	}

	public IType getType() {
		return type;
	}

	@Override
	public String toString() {
		return type.toString();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedPort> getInputPorts() {
		// TODO Auto-generated method stub
		return (List<? extends ITypedPort>) super.getInputPorts();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedPort> getOutputPorts() {
		// TODO Auto-generated method stub
		return (List<? extends ITypedPort>) super.getOutputPorts();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedNode> getPredecessors() {
		// TODO Auto-generated method stub
		return (List<? extends TypedNode>) super.getPredecessors();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<? extends ITypedNode> getSuccessors() {
		// TODO Auto-generated method stub
		return (List<? extends TypedNode>) super.getSuccessors();
	}

	public void accept(ITypedGraphVisitor graphVistor, Object arg) {
		graphVistor.visitTypedNode(this, arg);
	}

	public void setType(IType t) {
		this.type = t;
	}
}
