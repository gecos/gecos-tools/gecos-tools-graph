package fr.irisa.cairn.typedGraph;

import java.util.List;

import fr.irisa.cairn.graph.INode;

public interface ITypedNode extends INode {
	public List<? extends ITypedPort> getInputPorts();

	public List<? extends ITypedPort> getOutputPorts();

	public List<? extends ITypedNode> getPredecessors();

	public List<? extends ITypedNode> getSuccessors();

	public IType getType();
	
	public void setType(IType t);

	public void accept(ITypedGraphVisitor visitor, Object arg);
}
