package fr.irisa.cairn.typedGraph;

import fr.irisa.cairn.graph.ISubGraphNode;


public interface ITypedSubGraph extends ITypedNode, ISubGraphNode, ITypedGraph {

}
