package fr.irisa.cairn.typedGraph;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.implement.Edge;
import fr.irisa.cairn.typedGraph.implement.TypedCollapsedNode;
import fr.irisa.cairn.typedGraph.implement.TypedGraph;
import fr.irisa.cairn.typedGraph.implement.TypedNode;
import fr.irisa.cairn.typedGraph.implement.TypedPort;
import fr.irisa.cairn.typedGraph.implement.TypedSubGraphNode;

@Singleton
public class TypedGraphFactory implements ITypedGraphFactory {

	@Inject
	public TypedGraphFactory() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public ITypedNode createNode(IType type) {
		return new TypedNode(type);
	}

	public IEdge connect(ITypedNode source, ITypedNode sink, IType type) {
		if (source.getOutputPorts().size() == 0) {
			source.addOutputPort(createPort(type));
		}

		sink.addInputPort(createPort(type));
		
		ITypedPort psource =  source.getOutputPorts().get(0);
		ITypedPort pdest = sink.getInputPorts().get(sink.getInputPorts().size()-1);
		return connect(psource, pdest);
	}

	/**
	 * Connect two ports and add the builded link to the nodes graphs
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	public IEdge connect(ITypedPort source, ITypedPort sink) {
		IEdge e = new Edge(source, sink);
		IGraph gSource = source.getNode().getGraph();
		IGraph gSink = sink.getNode().getGraph();
		if(gSource==null)
			throw new UnsupportedOperationException("Source of the edge isn't in any graph.");
		if(gSink==null)
			throw new UnsupportedOperationException("Sink of the edge isn't in any graph.");
		gSource.addEdge(e);
		
		if (gSource != gSink) {
			gSink.addEdge(e);
		}
		return e;
	}

	public ITypedSubGraph createSubgraph(boolean directed) {
		TypedSubGraphNode n = new TypedSubGraphNode(directed);
		return n;
	}

	public ITypedPort createPort(IType p) {
		return new TypedPort(p);
	}

	public ITypedGraph createGraph(boolean directed) {
		return new TypedGraph(directed);
	}

	
	public ITypedNode createNode(IType type, int inputs, int outputs,IType ptype) {
		ITypedNode n = createNode(type);
		for (int i = 0; i < inputs; ++i) {
			ITypedPort p = createPort(ptype);
			n.addInputPort(p);
		}
		for (int i = 0; i < outputs; ++i) {
			ITypedPort p = createPort(ptype);
			n.addOutputPort(p);
		}
		return n;
	}

	@Override
	public ITypedCollapsedNode createCollapsedNode(IType type, boolean directed) {
		ITypedCollapsedNode n = new TypedCollapsedNode(directed);
		n.setType(type);
		return n;
	}
}
