package fr.irisa.cairn.typedGraph.providers;

import fr.irisa.cairn.typedGraph.IType;

/**
 * Provider for the label of {@link IType}.
 * 
 * @author antoine
 * 
 */
public interface ITypeLabelProvider {
	/**
	 * Get the string label for a given {@link IType}.
	 * 
	 * @param t
	 * @return
	 */
	public String getLabel(IType t);
}
