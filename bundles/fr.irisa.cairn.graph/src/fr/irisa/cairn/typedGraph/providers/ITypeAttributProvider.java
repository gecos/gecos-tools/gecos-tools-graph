package fr.irisa.cairn.typedGraph.providers;

import fr.irisa.cairn.typedGraph.IType;

/**
 * Provider for getting some informations on {@link IType}.
 * 
 * @author antoine
 * 
 */
public interface ITypeAttributProvider {
	/**
	 * Get the attribut of a given {@link IType} for a specified key.
	 * 
	 * @param type
	 * @param key
	 * @return null if type hasn't any attributes for the given key
	 */
	public Object getAttribut(IType type, Object key);

	/**
	 * Get the  attribut of an {@link IType}.
	 * 
	 * @param type
	 * @return
	 */
	public Object getAttribut(IType type);
}
