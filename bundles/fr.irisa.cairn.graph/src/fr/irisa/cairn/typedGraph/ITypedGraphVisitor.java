package fr.irisa.cairn.typedGraph;

import fr.irisa.cairn.graph.IEdge;

public interface ITypedGraphVisitor {
	public void visitTypedNode(ITypedNode n, Object arg);

	public void visitEdge(IEdge e, Object arg);

	public void visitTypedSubGraphNode(ITypedSubGraph n, Object arg);
	
	public void visitTypedCollapsedNode(ITypedCollapsedNode n, Object arg);
}
