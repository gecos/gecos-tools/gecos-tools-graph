package fr.irisa.cairn.typedGraph;

import java.util.Set;

import fr.irisa.cairn.graph.IGraph;

public interface ITypedGraph extends IGraph{

	public Set<? extends ITypedNode> getNodes();

}
