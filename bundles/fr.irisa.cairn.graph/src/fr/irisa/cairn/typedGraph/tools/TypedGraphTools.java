package fr.irisa.cairn.typedGraph.tools;

import java.util.HashSet;
import java.util.Set;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedNode;
import fr.irisa.cairn.typedGraph.providers.ITypeLabelProvider;

public class TypedGraphTools {

	public static Set<ITypedNode> getTypedNodes(IGraph g) {
		Set<ITypedNode> nodes = new HashSet<ITypedNode>();
		for (INode n : g.getNodes()) {
			nodes.add((ITypedNode) n);
		}
		return nodes;
	}

	public static boolean sameFamilyTypes(IType t1, IType t2,
			ITypeLabelProvider familyNameProvider) {
		return familyNameProvider.getLabel(t1).equals(
				familyNameProvider.getLabel(t2));
	}
}
