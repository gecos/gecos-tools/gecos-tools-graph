package fr.irisa.cairn.typedGraph.tools;

import java.util.Hashtable;
import java.util.Map;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedCollapsedNode;
import fr.irisa.cairn.typedGraph.ITypedGraph;
import fr.irisa.cairn.typedGraph.ITypedGraphVisitor;
import fr.irisa.cairn.typedGraph.ITypedNode;
import fr.irisa.cairn.typedGraph.ITypedPort;
import fr.irisa.cairn.typedGraph.ITypedSubGraph;

public class TypeSubstitution implements ITypedGraphVisitor {
	private Map<IType, IType> substitutionMap;

	public void substitute(IType oldType, IType newType, ITypedGraph g) {
		Map<IType, IType> m = new Hashtable<IType, IType>();
		m.put(oldType, newType);
		substitute(m, g);
	}

	public void substitute(Map<IType, IType> substitutionMap, ITypedGraph g) {
		this.substitutionMap = substitutionMap;
		for (ITypedNode n : g.getNodes()) {
			n.accept(this,null);
		}
	}

	public void visitEdge(IEdge e, Object arg) {
	}

	public void visitTypedNode(ITypedNode n, Object arg) {
		IType t = substitutionMap.get(n.getType());
		if (t != null) {
			n.setType(t);
		}
		for (ITypedPort p : n.getInputPorts()) {
			IType tp = substitutionMap.get(p.getType());
			if (tp != null)
				p.setType(tp);
		}
		for (ITypedPort p : n.getOutputPorts()) {
			IType tp = substitutionMap.get(p.getType());
			if (tp != null)
				p.setType(tp);
		}
	}

	public void visitTypedSubGraphNode(ITypedSubGraph n, Object arg) {
		visitTypedNode(n, arg);
		substitute(substitutionMap, n);
	}

	public void visitTypedCollapsedNode(ITypedCollapsedNode n, Object arg) {
		visitTypedSubGraphNode(n,arg);
	}
}
