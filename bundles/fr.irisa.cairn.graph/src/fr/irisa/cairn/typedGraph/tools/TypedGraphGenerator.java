package fr.irisa.cairn.typedGraph.tools;

import static fr.irisa.cairn.graph.tools.GraphTools.getInputPort;
import static fr.irisa.cairn.graph.tools.GraphTools.getOutputPort;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.VertexFactory;
import org.jgrapht.generate.RandomGraphGenerator;
import org.jgrapht.graph.SimpleGraph;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.typedGraph.IType;
import fr.irisa.cairn.typedGraph.ITypedGraph;
import fr.irisa.cairn.typedGraph.ITypedGraphFactory;
import fr.irisa.cairn.typedGraph.ITypedNode;
import fr.irisa.cairn.typedGraph.ITypedPort;
import fr.irisa.cairn.typedGraph.TypedGraphFactory;
import fr.irisa.cairn.typedGraph.implement.Type;

public class TypedGraphGenerator {

	private ITypedGraphFactory factory = new TypedGraphFactory();

	/**
	 * Generate a random undirected graph
	 * 
	 * @param vertices
	 *            number of vertices
	 * @param edges
	 *            number of edges
	 * @return
	 */
	public ITypedGraph randomGraphGenerator(int vertices, int edges) {
		RandomGraphGenerator<ITypedNode, IEdge> generator = new RandomGraphGenerator<ITypedNode, IEdge>(
				vertices, edges);
		ITypedGraph g = factory.createGraph(false);
		Graph<ITypedNode, IEdge> graph = new SimpleGraph<ITypedNode, IEdge>(
				new DefaultEdgeFactory());
		generator.generateGraph(graph, new DefaultVertexFactory(),
				new Hashtable<String, ITypedNode>());

		for (INode n : graph.vertexSet()) {
			g.addNode(n);
		}
		for (IEdge e : graph.edgeSet()) {
			g.addEdge(e);
		}
		return g;
	}

	protected IType createType() {
		return new Type("default", new String());
	}

	protected class DefaultVertexFactory implements VertexFactory<ITypedNode> {
		public ITypedNode createVertex() {
			return factory.createNode(createType());
		}

	}

	protected class DefaultEdgeFactory implements
			EdgeFactory<ITypedNode, IEdge> {

		public IEdge createEdge(ITypedNode source, ITypedNode sink) {
			if (source.getOutputPorts().size() == 0) {
				source.addOutputPort(factory.createPort(createType()));
			}
			IEdge e;
			try {
				ITypedPort p = factory.createPort(createType());
				sink.addInputPort(p);
				e = factory.connect((ITypedPort) getOutputPort(source, 0), p);
			} catch (GraphException e1) {
				throw new RuntimeException(e1);
			}
			return e;
		}

	}

	/**
	 * Generate an acyclic directed graph.
	 * 
	 * @param vertices
	 *            number of vertices
	 * @return
	 */
	public ITypedGraph acyclicGraphGenerator(int vertices, int types) {
		ITypedGraph graph = factory.createGraph(true);
		List<ITypedNode> nodes = new ArrayList<ITypedNode>();
		List<IType> typesList = new ArrayList<IType>();
		for (int t = 0; t < types; ++t) {
			typesList.add(new Type("T" + t, new String()));
		}
		assert (vertices > 15);
		for (int i = 0; i < vertices; i++) {
			int type = (int) (Math.random() * typesList.size());
			ITypedNode n = factory.createNode(typesList.get(type));
			n.addInputPort(factory.createPort(typesList.get(type)));
			n.addInputPort(factory.createPort(typesList.get(type)));
			n.addOutputPort(factory.createPort(typesList.get(type)));
			graph.addNode(n);
			nodes.add(n);
		}

		double sqrt = Math.sqrt(vertices);

		for (int i = (int) (1 + Math.sqrt(vertices)); i < vertices; i++) {
			double r = Math.random();
			if (r > 0.2) {
				int srcIndex0 = i - 1 - (int) (Math.random() * sqrt);
				int srcIndex1 = i - 1 - (int) (Math.random() * sqrt);
				while (srcIndex1 == srcIndex0) {
					srcIndex1 = i - 1 - (int) (Math.random() * sqrt);
				}

				ITypedNode sinkNode = nodes.get(i);
				ITypedNode sourceNode0 = nodes.get(srcIndex0);
				ITypedNode sourceNode1 = nodes.get(srcIndex1);

				try {
					factory.connect((ITypedPort) getOutputPort(sourceNode0, 0),
							(ITypedPort) getInputPort(sinkNode, 0));
					factory.connect((ITypedPort) getOutputPort(sourceNode1, 0),
							(ITypedPort) getInputPort(sinkNode, 1));
				} catch (GraphException e) {
					throw new RuntimeException(e);
				}
			}

		}
		return graph;
	}
}
