package fr.irisa.cairn.typedGraph.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.io.DOTExport;
import fr.irisa.cairn.typedGraph.implement.providers.TypeLabelProvider;
import fr.irisa.cairn.typedGraph.implement.providers.TypedNodeDotAttributsProvider;
import fr.irisa.cairn.typedGraph.implement.providers.TypedNodeLabelProvider;

@Deprecated
public class TypedGraphIOTools {
	
	/**
	 * Default DOT export for a typed graph.
	 * @param out
	 * @param graph
	 */
	public static void dotExport(PrintStream out,IGraph graph){
		TypeLabelProvider typeLabelProvider = new TypeLabelProvider();
		TypedNodeLabelProvider labelProvider = new TypedNodeLabelProvider(typeLabelProvider);
		DOTExport export = new DOTExport(labelProvider,null,new TypedNodeDotAttributsProvider(typeLabelProvider));
		export.export(out, graph);
	}
	
	/**
	 * Default DOT export for a typed graph.
	 * @param file
	 * @param graph
	 */
	public static void dotExport(String file, IGraph graph){
		try {
			dotExport(new PrintStream(new File(file)), graph);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
