package fr.irisa.cairn.typedGraph;

import fr.irisa.cairn.graph.IPort;

public interface ITypedPort extends IPort {
	public IType getType();
	public void setType(IType type);
}
