package fr.irisa.cairn.typedGraph;

public interface IType {
	public boolean equivalent(IType t);
}
