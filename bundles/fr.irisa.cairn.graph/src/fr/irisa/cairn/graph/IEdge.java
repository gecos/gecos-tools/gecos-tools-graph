package fr.irisa.cairn.graph;

/**
 * An {@link IEdge} is defined by two ports. Source port is on the source node
 * of the edge and sink port is on the destination node of the edge.
 * 
 * @author antoine
 * 
 */

/**
 * @model
 */
public interface IEdge {

	/**
	 * @model
	 */
	public IPort getSourcePort();

	/**
	 * @model
	 */
	public IPort getSinkPort();

	/**
	 * @model
	 */
	public void reconnectSourcePort(IPort p);

	/**
	 * @model
	 */
	public void reconnectSinkPort(IPort p);

	/**
	 * @model
	 */
	public void accept(IGraphVisitor graphVistor, Object arg);
	
	public boolean isExternalEdge();
}

