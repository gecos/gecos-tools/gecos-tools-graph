package fr.irisa.cairn.graph;

import java.util.Set;

/**
 * @model
 */
public interface IGraph {

	/**
	 * Type of graph.
	 *
	 * @model default=""
	 * 
	 * @return true if graph is directed
	 */
	public boolean isDirected();

	/**
	 * Get all nodes of this graph
	 *
	 * @model default=""
	 * 
	 * @return a set of nodes
	 */
	public Set<? extends INode> getNodes();

	/**
	 * Get all edges of this graph
	 *
	 * @model default=""
	 * 
	 * @return a set of edges
	 */
	public Set<? extends IEdge> getEdges();

	/**
	 * Add an edge to this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            an Edge
	 */
	public void addEdge(IEdge e);

	/**
	 * Add a node to this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            a node
	 */
	public void addNode(INode e);

	/**
	 * Remove an edge from this graph
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            an edge
	 */
	public void removeEdge(IEdge e);

	/**
	 * Remove a node from this graph.
	 *
	 * @model default=""
	 * 
	 * @param e
	 *            a node
	 */
	public void removeNode(INode e);

	/**
	 * Get all edges linking a node from another graph to a node of this graph.
	 *
	 * @model default=""
	 * 
	 * @return a set of edges
	 */
	public Set<? extends IEdge> getExternalInputs();

	/**
	 * Get all edges linking a node from this graph to a node from another
	 * graph.
	 *
	 * @model default=""
	 * 
	 * @return a set of edges
	 */
	public Set< ? extends IEdge> getExternalOutputs();

}
