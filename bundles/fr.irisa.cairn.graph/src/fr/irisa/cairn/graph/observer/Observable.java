package fr.irisa.cairn.graph.observer;

import java.util.Collection;
import java.util.HashSet;

public class Observable implements IObservable {

	private Collection<IObserver>	observers;
	private boolean					deliver;
	
	public Observable() {
		deliver = true;
	}

	@Override
	public Collection<IObserver> getObservers() {
		if (this.observers == null) {
			this.observers = new HashSet<IObserver>();
		}
		return this.observers;
	}

	@Override
	public void addObserver(IObserver o) {
		this.getObservers().add(o);

	}

	@Override
	public boolean removeObserver(IObserver o) {
		if (this.observers == null) {
			return false;
		}
		return this.observers.remove(o);
	}

	@Override
	public boolean containObserver(IObserver o) {
		return this.getObservers().contains(o);
	}

	@Override
	public void notify(INotification notification) {
		for (IObserver observer : getObservers()) {
			observer.notify(notification);
		}

	}

	public boolean hasObservers() {
		return observers != null && !observers.isEmpty();
	}

	@Override
	public boolean canBeDeliver() {
		return deliver;
	}

	@Override
	public void setDeliver(boolean deliver) {
		this.deliver = deliver;

	}

	/**
	 * Returns whether {@link #eNotify eNotify} needs to be called. This may
	 * return <code>true</code> even when {@link #eDeliver eDeliver} is
	 * <code>false</code> or when {@link #eAdapters eAdapters} is empty.
	 * 
	 * @return whether {@link #eNotify eNotify} needs to be called.
	 */
	public boolean isNotificationRequired() {
		return hasObservers() && canBeDeliver();
	}

}
