package fr.irisa.cairn.graph.observer;

public enum EventType {
	
	ADD, REMOVE, CONNECT, DISCONNECT, RECONNECT, SET,MAP

}
