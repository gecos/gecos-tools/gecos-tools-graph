package fr.irisa.cairn.graph.observer;


/**
 * notification are used in the observater pattern
 * @author jguidoux
 *
 */
public interface INotification {
	
	/**
	 * 
	 * @return the object which has launch the notification
	 */
	Object getNotifier();

}
