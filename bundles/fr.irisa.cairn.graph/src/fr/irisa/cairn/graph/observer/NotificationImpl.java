package fr.irisa.cairn.graph.observer;

/**
 * Implementation of a notification
 * notification are used in the observater pattern
 * 
 * @author jguidoux
 *
 */
public class NotificationImpl implements IModificationNotification {

	private final Object		notifier;
	private final Object		feature;
	private final FeatureType	featureID;
	private final EventType		event;

	public NotificationImpl(final Object notifier, final Object feature,
			final FeatureType featureID, final EventType event) {
		super();
		this.notifier = notifier;
		this.feature = feature;
		this.featureID = featureID;
		this.event = event;
	}

	@Override
	public EventType getEvent() {
		return this.event;
	}

	@Override
	public Object getFeature() {
		return this.feature;
	}

	@Override
	public FeatureType getFeatureID() {
		return this.featureID;
	}

	@Override
	public Object getNotifier() {
		return this.notifier;
	}
	
	@Override
	public String toString() {
		return   "("+event+","+featureID+","+feature+")";
	}

}
