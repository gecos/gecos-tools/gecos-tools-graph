package fr.irisa.cairn.graph.observer;

import java.util.Collection;

/**
 * An observable object may have some {@link IObserver} that will catch launched
 * notifications .
 * 
 * @author jguidoux
 * 
 */
public interface IObservable {

	/**
	 * 
	 * @return the list his observers
	 */
	Collection<IObserver> getObservers();

	/**
	 * Add an observer
	 * 
	 * @param o
	 *            the observer to add
	 */
	void addObserver(IObserver o);

	/**
	 * Remove an observer
	 * 
	 * @param o
	 *            the observer to remove
	 * @return true of the observer has been removed
	 */
	boolean removeObserver(IObserver o);

	/**
	 * Test if the observable object contains an observer.
	 * 
	 * @param o
	 *            the observer
	 * @return true if the object contain the observer, false otherwise
	 */
	boolean containObserver(IObserver o);

	/**
	 * Launch a notification watched by observers.
	 * 
	 * @param notification
	 * 
	 */
	void notify(INotification notification);

	/**
	 * Returns whether this notifier will deliver notifications to the adapters.
	 * 
	 * @return whether notifications will be delivered.
	 * @see #eSetDeliver
	 */
	boolean canBeDeliver();

	/**
	 * Sets whether this notifier will deliver notifications to the adapters.
	 * 
	 * @param deliver
	 *            whether or not to deliver.
	 * @see #canBeDeliver()
	 */
	void setDeliver(boolean deliver);

}
