package fr.irisa.cairn.graph.observer;

/**
 * interface for notification
 * used when a notification update an  object 
 * @author jguidoux
 *
 */
public interface IModificationNotification extends INotification {
	
	/**
	 * 
	 * @return the feature which has been add or remove
	 */
	Object getFeature();
	
	/**
	 * the ID of the feature
	 * @return
	 */
	FeatureType getFeatureID();
	
	/**
	 * 
	 * @return of the method who launch the notification
	 */
	EventType getEvent();

}
