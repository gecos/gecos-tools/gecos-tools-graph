package fr.irisa.cairn.graph.observer;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IPort;

/**
 * Abstract class to catch {@link IModificationNotification} of an observed
 * graph.
 * 
 * @author antoine
 * 
 */
public abstract class AbstractGraphStructureObserver implements IObserver {

	@Override
	public void notify(INotification notification) {
		if (notification instanceof IModificationNotification) {
			IModificationNotification update = (IModificationNotification) notification;
			switch (update.getEvent()) {
			case ADD:
				notifyAdd(update);
				break;
			case REMOVE:
				notifyRemove(update);
				break;
			case CONNECT:
				notifyConnect(update);
				break;
			case DISCONNECT:
				notifyDisconnect(update);
				break;
			case RECONNECT:
				notifyReconnect(update);
				break;
			case SET:
				notifySet(update);
				break;
			case MAP:
				notifyMap(update);
				break;
			default:
				throw new UnsupportedOperationException(
						"Unsupported notification: " + update);
			}
		}
	}

	private void notifyMap(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case COLLAPSED_INPUT:
			notifyMapInput(notif);
			break;
		case COLLAPSED_OUTPUT:
			notifyMapOutput(notif);
			break;
		default:
			throw new UnsupportedOperationException(
					"Unsupported notification: " + notif);
		}
	}

	protected void notifyMapOutput(IModificationNotification notif) {
		
	}

	protected void notifyMapInput(IModificationNotification notif) {
		
	}

	private void notifyAdd(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case GRAPH_NODE:
			notifyAddNode(notif);
			break;
		case GRAPH_EDGE:
			notifyAddEdge(notif);
			break;
		case NODE_INPORT:
			notifyAddInputPort(notif);
			break;
		case NODE_OUTPORT:
			notifyAddOutputPort(notif);
			break;
		default:
			throw new UnsupportedOperationException(
					"Unsupported notification: " + notif);
		}
	}

	protected void notifyAddNode(IModificationNotification notif) {

	}

	protected void notifyAddEdge(IModificationNotification notif) {

	}

	protected void notifyAddInputPort(IModificationNotification notif) {

	}

	protected void notifyAddOutputPort(IModificationNotification notif) {

	}

	private void notifyRemove(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case GRAPH_NODE:
			notifyRemoveNode(notif);
			break;
		case GRAPH_EDGE:
			notifyRemoveEdge(notif);
			break;
		case NODE_INPORT:
			notifyRemoveInputPort(notif);
			break;
		case NODE_OUTPORT:
			notifyRemoveOutputPort(notif);
			break;
		default:
			throw new UnsupportedOperationException(
					"Unsupported notification: " + notif);
		}
	}

	protected void notifyRemoveNode(IModificationNotification notif) {

	}

	protected void notifyRemoveEdge(IModificationNotification notif) {

	}

	protected void notifyRemoveInputPort(IModificationNotification notif) {

	}

	protected void notifyRemoveOutputPort(IModificationNotification notif) {

	}

	private void notifyConnect(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case PORT_EDGE:
			notifyConnectEdge(notif);
			break;
		default:
			throw new UnsupportedOperationException(
					"Unsupported notification: " + notif);
		}
	}

	/**
	 * Notify the connection of an {@link IEdge} to an {@link IPort}
	 * 
	 * @param notif
	 */
	protected void notifyConnectEdge(IModificationNotification notif) {

	}

	private void notifyDisconnect(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case PORT_EDGE:
			notifyDisconnectEdge(notif);
			break;
		default:
			throw new UnsupportedOperationException(
					"Unsupported notification: " + notif);
		}
	}

	/**
	 * Notify the disconnection of an {@link IEdge} of an {@link IPort}
	 * 
	 * @param notif
	 */
	protected void notifyDisconnectEdge(IModificationNotification notif) {

	}

	protected void notifyReconnect(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case EDGE_SOURCE:
			notifyReconnectEdgeSource(notif);
			break;
		case EDGE_SINK:
			notifyReconnectEdgeSink(notif);
			break;
		default:
			throw new UnsupportedOperationException(
					"Unsupported notification: " + notif);
		}
	}

	/**
	 * Notify the reconnection of an {@link IEdge} to a new source {@link IPort}
	 * 
	 * @param notif
	 */
	protected void notifyReconnectEdgeSource(IModificationNotification notif) {

	}

	/**
	 * Notify the reconnection of an {@link IEdge} to a new sink {@link IPort}
	 * 
	 * @param notif
	 */
	protected void notifyReconnectEdgeSink(IModificationNotification notif) {

	}

	private void notifySet(IModificationNotification notif) {
		switch (notif.getFeatureID()) {
		case SUBGRAPH_CONTENT:
			notifySetSubgraphContent(notif);
			break;
		default:
			throw new UnsupportedOperationException();
		}
	}

	protected void notifySetSubgraphContent(IModificationNotification notif) {

	}

}
