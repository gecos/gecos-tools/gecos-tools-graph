package fr.irisa.cairn.graph.observer;

public interface IObserver {
	
	void notify(INotification notification); 

}
