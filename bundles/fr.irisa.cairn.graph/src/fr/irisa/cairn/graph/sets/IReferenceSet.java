package fr.irisa.cairn.graph.sets;

import org.eclipse.emf.common.util.BasicEList.UnmodifiableEList;

/**
 * Provide set operations on a referenced {@link UnmodifiableEList}.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 *
 * @param <T>
 */
public interface IReferenceSet<T>{
	public UnmodifiableEList<T> getReferencedList();
	public IReferenceSet<T> union(IReferenceSet<T> set);
	public IReferenceSet<T> intersection(IReferenceSet<T> set);
	public IReferenceSet<T> difference(IReferenceSet<T> set);
}
