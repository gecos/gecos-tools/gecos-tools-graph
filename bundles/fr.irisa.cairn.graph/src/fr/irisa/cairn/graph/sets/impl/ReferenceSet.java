package fr.irisa.cairn.graph.sets.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList.UnmodifiableEList;

import fr.irisa.cairn.graph.sets.IReferenceSet;

public class ReferenceSet<T> implements IReferenceSet<T> {
	private boolean[] existence;
	private UnmodifiableEList<T> referenced;

	public ReferenceSet(final UnmodifiableEList<T> referenced, final boolean[] existence) {
		this.existence = existence;
		this.referenced = referenced;
	}

	public ReferenceSet(final UnmodifiableEList<T> referenced, final List<T> nodes) {
		this.referenced = referenced;
		this.existence = new boolean[referenced.size()];
		for (T n : nodes) {
			existence[referenced.indexOf(n)] = true;
		}
	}

	@Override
	public IReferenceSet<T> intersection(IReferenceSet<T> set) {
		if(referenced!=set.getReferencedList())
			throw new  UnsupportedOperationException("Sets are not referencing  the same list");
		boolean res[] = new boolean[existence.length];
		for (int i = 0; i < existence.length; ++i) {
			res[i] = existence[i] && ((ReferenceSet<T>) set).existence[i];
		}
		return new ReferenceSet<T>(referenced, res);
	}

	@Override
	public IReferenceSet<T> union(IReferenceSet<T> set) {
		if(referenced!=set.getReferencedList())
			throw new  UnsupportedOperationException("Sets are not referencing the same list");
		boolean res[] = new boolean[existence.length];
		for (int i = 0; i < existence.length; ++i) {
			res[i] = existence[i] || ((ReferenceSet<T>) set).existence[i];
		}
		return new ReferenceSet<T>(referenced, res);
	}

	@Override
	public IReferenceSet<T> difference(IReferenceSet<T> set) {
		if(referenced!=set.getReferencedList())
			throw new  UnsupportedOperationException("Sets are not referencing  the same list");
		boolean res[] = new boolean[existence.length];
		for (int i = 0; i < existence.length; ++i) {
			res[i] = existence[i] != ((ReferenceSet<T>) set).existence[i];
		}
		return new ReferenceSet<T>(referenced, res);
	}

	@Override
	public UnmodifiableEList<T> getReferencedList() {
		return referenced;
	}

	
}
