package fr.irisa.cairn.graph.tools;

import java.util.Comparator;
// added by Chenglong
public abstract class AbstractPriorityComparator<T> implements Comparator<T> {
	public int compare(T o1, T o2) {
		return new Double(quality(o1)).compareTo(quality(o2));
	}

	/**
	 * Quality of a an element.
	 * 
	 * @param e
	 * @return
	 */
	protected abstract double quality(T e);
}
