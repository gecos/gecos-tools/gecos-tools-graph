package fr.irisa.cairn.graph.tools;

import java.util.Comparator;

/**
 * Abstract class for comparator based on quality of compared objects. If two
 * objects have same quality then a tiebreaker comparator is called.
 * 
 * @author antoine
 * 
 * @param <T>
 */
public abstract class AbstractHierachicalQualityComparator<T> extends
		AbstractQualityComparator<T> {

	private Comparator<T> tiebreaker;

	public AbstractHierachicalQualityComparator() {
	}

	public AbstractHierachicalQualityComparator(
			Comparator<T> tiebreaker) {
		this.tiebreaker = tiebreaker;
	}

	public int compare(T o1, T o2) {
		int c = new Integer(quality(o1)).compareTo(quality(o2));
		if (tiebreaker != null && c == 0) {
			c = tiebreaker.compare(o1, o2);
		}
		return c;
	}
}
