package fr.irisa.cairn.graph.tools;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.graph.providers.IGraphFilter;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

@SuppressWarnings("rawtypes")
public class GraphTools {

	/**
	 * Get all connected nodes to/from a port.
	 * 
	 * @param p
	 * @return
	 */
	public static List<INode> getConnectedNodes(IPort p) {
		List<INode> nodes = new ArrayList<INode>();
		for (IEdge e : p.getEdges()) {
			if (e.getSinkPort() != null && e.getSinkPort() != p)
				nodes.add(e.getSinkPort().getNode());
			if (e.getSourcePort() != null && e.getSourcePort() != p)
				nodes.add(e.getSourcePort().getNode());
		}
		return nodes;
	}

	/**
	 * Get all sinks of the {@link IGraph}. A sink is an {@link INode} which
	 * have no internal outputs.
	 * 
	 * @param g
	 * @return list of all sinks.
	 */
	public static List<INode> getSinkNodes(IGraph g) {
		List<INode> sinks = new ArrayList<INode>();

		for (INode n : g.getNodes()) {
			if (n.getIncomingEdges().size() > 0) {
				boolean sink = true;
				for (IEdge out : n.getOutgoingEdges()) {
					if (!isExternalEdge(out))
						sink = false;
				}
				if (sink)
					sinks.add(n);
			}
		}
		return sinks;
	}

	/**
	 * Get all sources of the {@link IGraph}. A source is an {@link INode} which
	 * have no internal inputs.
	 * 
	 * @param g
	 * @return list of all sources.
	 */
	public static List<INode> getSourceNodes(IGraph g) {
		List<INode> sources = new ArrayList<INode>();

		for (INode n : g.getNodes()) {
			boolean source = true;
			for (IEdge in : n.getIncomingEdges()) {
				if (!isExternalEdge(in))
					source = false;
			}
			if (source)
				sources.add(n);

		}
		return sources;
	}

	public static IPort getInputPort(INode n, int p) throws GraphException {
		if (n.getInputPorts().size() == 0)
			throw new GraphException("Node has no input ports");
		if (p > n.getInputPorts().size()) {
			throw new GraphException("Unknown input port: p" + p);
		}
		return n.getInputPorts().get(p);
	}

	public static IPort getOutputPort(INode n, int p) throws GraphException {
		if (n.getOutputPorts().size() == 0)
			throw new GraphException("Node has no output ports");
		if (p > n.getOutputPorts().size()) {
			throw new GraphException("Unknown output port: p" + p);
		}
		return n.getOutputPorts().get(p);
	}

	public static INode getSourceNode(IEdge e) {
		return e.getSourcePort().getNode();
	}

	public static INode getSinkNode(IEdge e) {
		return e.getSinkPort().getNode();
	}

	/**
	 * Get the opposite node of a reference in an edge.
	 * 
	 * @param e
	 * @param n
	 *            reference node
	 * @return null if the edge isn't linked to the reference node.
	 */
	public static INode getOppositeNode(IEdge e, INode n) {
		IPort sourcePort = e.getSourcePort();
		IPort sinkPort = e.getSinkPort();
		if (sinkPort != null && sourcePort != null) {
			if (sourcePort.getNode() == n)
				return sinkPort.getNode();
			if (sinkPort.getNode() == n)
				return sourcePort.getNode();
		}
		return null;
	}

	public static IPort getOppositePort(IEdge e, IPort p) {
		if (e.getSourcePort() == p)
			return e.getSinkPort();
		if (e.getSinkPort() == p)
			return e.getSourcePort();
		throw new IllegalArgumentException("Port isn't connected to the edge.");
	}

	public static Integer getSourcePortNumber(IEdge e) {
		INode n = getSourceNode(e);
		return n.getOutputPorts().indexOf(e.getSourcePort());
	}

	public static Integer getSinkPortNumber(IEdge e) {
		INode n = getSinkNode(e);
		return n.getInputPorts().indexOf(e.getSinkPort());
	}

	public static IGraph cloneGraph(IGraph g, ICloneProvider cloner) {
		GraphCopy<IGraph, INode, IEdge, ISubGraphNode> copy = new GraphCopy<IGraph, INode, IEdge, ISubGraphNode>(
				cloner);
		IGraph cloneGraph = copy.copyGraph(g);
		return cloneGraph;
	}

	/**
	 * Reconnect all inputs/outputs of a node to another node. Nodes must have
	 * same number of inputs/outputs ports.
	 * 
	 * @param oldNode
	 * @param newNode
	 */
	public static void replaceNode(INode oldNode, INode newNode) {
		int oldInputSize = oldNode.getInputPorts().size();
		int oldOutputSize = oldNode.getOutputPorts().size();

		if (oldInputSize != newNode.getInputPorts().size()
				|| oldOutputSize != newNode.getOutputPorts().size())
			throw new UnsupportedOperationException(
					"Nodes must have same number of inputs/outputs ports.");

		for (IEdge in : oldNode.getIncomingEdges()) {
			int p = oldNode.getInputPorts().indexOf(in.getSinkPort());
			in.reconnectSinkPort(newNode.getInputPorts().get(p));
		}

		for (IEdge out : oldNode.getOutgoingEdges()) {
			int p = oldNode.getOutputPorts().indexOf(out.getSourcePort());
			out.reconnectSourcePort(newNode.getOutputPorts().get(p));
		}
	}

	/**
	 * Disconnect all outputs of a node and connect them to a new node. All
	 * outputs ports are cloned to the new node.
	 * 
	 * @param oldNode
	 * @param newNode
	 * @throws GraphException
	 */
	@SuppressWarnings("unchecked")
	public static void reconnectNodeOutputs(INode oldNode, INode newNode,
			ICloneProvider cloner) throws GraphException {

		Set<IEdge> removedEdges = new LinkedHashSet<IEdge>();
		IGraph graph = oldNode.getGraph();
		for (IEdge out : oldNode.getOutgoingEdges()) {
			IEdge clone = cloner.cloneEdge(out);
			cloner.reconnectClone(clone, out, newNode, out.getSinkPort()
					.getNode());
			removedEdges.add(out);
			graph.addEdge(clone);
		}

		for (IEdge e : removedEdges) {
			graph.removeEdge(e);
		}
	}

	/**
	 * Test if a graph is a subgraph.
	 * 
	 * @param g
	 * @return true if the graph is isn't a subgraph.
	 */
	public static boolean isRootGraph(IGraph g) {
		return !(g instanceof ISubGraphNode);
	}

	/**
	 * Search the super node in the same graph of a reference node. Return the
	 * super node in which the start node is nested or the start node if the two
	 * nodes are in the same graph.
	 * 
	 * @param node
	 *            reference node
	 * @param sn
	 *            start node of the hierarchical search
	 * @return null if no shared graph is found from start node hierarchical
	 *         level
	 */
	public static INode findSharedSubGraph(INode ref, INode sn) {

		while (!isRootGraph(sn.getGraph()) && !isNodeIn(ref, sn.getGraph())) {
			sn = (INode) sn.getGraph();
		}

		if (isNodeIn(ref, sn.getGraph())) {
			return sn;
		}

		if (sn.getGraph() != ref.getGraph()) {
			return null;
		} else
			return sn;
	}

	/**
	 * Test if a node is in a hierarchical graph.
	 * 
	 * @param n
	 * @param graph
	 * @return
	 */
	public static boolean isNodeIn(INode n, IGraph graph) {
		if (graph == n.getGraph())
			return true;
		else {
			while (!isRootGraph(n.getGraph())) {
				n = (INode) n.getGraph();
				if (graph == n.getGraph())
					return true;
			}
			return false;
		}
	}

	/**
	 * Get the cost of a path.
	 * 
	 * @param path
	 * @param provider
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static int getPathLatency(List<IEdge> path, ILatencyProvider provider) {
		int cost = 0;
		if (path.size() > 0) {
			cost += provider.getNodeLatency(path.get(0).getSourcePort()
					.getNode());
			for (IEdge e : path) {
				cost += provider.getEdgeLatency(e);
				cost += provider.getNodeLatency(e.getSinkPort().getNode());
			}
		}
		return cost;
	}

	/**
	 * Test if an edge has a source node and a sink node in different graphs.
	 * 
	 * @param e
	 * @return true is source and sink are in different graphs.
	 */
	public static boolean isExternalEdge(IEdge e) {
		return (e.getSinkPort() == null || e.getSourcePort() == null || e
				.getSourcePort().getNode().getGraph() != e.getSinkPort()
				.getNode().getGraph());
	}

	/**
	 * Test if an edge is graph input (unconnected source port).
	 * 
	 * @param e
	 * @return true if e is a graph input
	 */
	public static boolean isGraphInputEdge(IEdge e) {
		return e.getSourcePort() == null;
	}

	/**
	 * Test if an edge is graph output (unconnected sink port).
	 * 
	 * @param e
	 * @return true if e is a graph output
	 */
	public static boolean isGraphOutputEdge(IEdge e) {
		return e.getSinkPort() == null;
	}

	/**
	 * Get all node incoming edges in the same hierarchy level.
	 * 
	 * @param n
	 * @return
	 */
	public static List<IEdge> getNonExternalInputs(INode n) {
		List<IEdge> edges = new ArrayList<IEdge>();
		for (IEdge e : n.getIncomingEdges()) {
			if (!isExternalEdge(e))
				edges.add(e);
		}
		return edges;
	}

	/**
	 * Get all node outgoing edges in the same hierarchy level.
	 * 
	 * @param n
	 * @return
	 */
	public static List<IEdge> getNonExternalOutputs(INode n) {
		List<IEdge> edges = new ArrayList<IEdge>();
		for (IEdge e : n.getOutgoingEdges()) {
			if (!isExternalEdge(e))
				edges.add(e);
		}
		return edges;
	}

	/**
	 * Test if node is after a reference node in a directed graph. If nodes
	 * aren't at the same hierarchical level then the subgraph of the potential
	 * successor node at the same level of the reference is used.
	 * 
	 * @param ref
	 *            reference node
	 * @param node
	 *            potential successor node
	 * @return
	 */
	public static boolean isAfter(INode ref, INode node) {
		if (ref.getGraph().isDirected() && node.getGraph().isDirected()) {
			// search shared subgraph
			if (ref.getGraph() != node.getGraph()) {
				INode sharedFromRef = findSharedSubGraph(ref, node);
				if (sharedFromRef != null) {
					node = sharedFromRef;
				} else
					return false;
			}
			for (IEdge l : ref.getOutgoingEdges()) {
				if (l.getSinkPort().getNode() == node)
					return true;
				else
					return isAfter(l.getSinkPort().getNode(), node);

			}
			return false;
		} else
			throw new IllegalArgumentException("Graph has to be directed.");
	}

	/**
	 * Test if node is before a reference node in a directed graph. If nodes
	 * aren't at the same hierarchical level then the subgraph of the potential
	 * predecessor node at the same level of the reference is used.
	 * 
	 * @param ref
	 * @param node
	 * @return
	 */
	public static boolean isBefore(INode ref, INode node) {
		if (ref.getGraph().isDirected() && node.getGraph().isDirected()) {
			// search shared subgraph
			if (ref.getGraph() != node.getGraph()) {
				INode sharedFromRef = findSharedSubGraph(ref, node);
				if (sharedFromRef != null) {
					node = sharedFromRef;
				} else
					return false;
			}

			for (IEdge l : ref.getIncomingEdges()) {
				if (l.getSourcePort().getNode() == node)
					return true;
				else
					return isBefore(l.getSourcePort().getNode(), node);

			}
			return false;

		} else
			throw new IllegalArgumentException("Graph has to be directed.");
	}

	/**
	 * Disconnect two nodes.
	 * 
	 * @param n1
	 * @param n2
	 */
	public static void disconnect(INode n1, INode n2) {
		for (IEdge inN1 : n1.getIncomingEdges()) {
			if (inN1.getSourcePort().getNode() == n2) {
				n1.getGraph().removeEdge(inN1);
				n2.getGraph().removeEdge(inN1);
			}
		}

		for (IEdge inN2 : n2.getIncomingEdges()) {
			if (inN2.getSourcePort().getNode() == n1) {
				n1.getGraph().removeEdge(inN2);
				n2.getGraph().removeEdge(inN2);
			}
		}
	}

	/**
	 * Get all shared hierarchical graph level from a given node to a reference.
	 * 
	 * @param n
	 *            analysed node
	 * @param ref
	 *            top level reference node
	 * @return
	 */
	public static List<? extends ISubGraphNode> getHierarchicalNest(INode n,
			INode ref) {
		List<ISubGraphNode> nest = new ArrayList<ISubGraphNode>();
		if (!isRootGraph(n.getGraph())) {
			nest.add((ISubGraphNode) n.getGraph());
			nest.addAll(getHierarchicalNest((INode) n.getGraph(), ref));
			return nest;
		}
		return nest;
	}

	/**
	 * Find all predecessors of a node in a graph.
	 * 
	 * @param n
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <N extends INode> Set<N> getAllPredecessors(N n) {
		if (!n.getGraph().isDirected())
			throw new UnsupportedOperationException("Graph isn't directed.");
		Set<N> predecessors = new LinkedHashSet<N>();
		for (INode pred : n.getPredecessors()) {
			predecessors.add((N) pred);
			predecessors.addAll(getAllPredecessors((N) pred));
		}
		return predecessors;
	}

	/**
	 * Find all successors of a node in a graph.
	 * 
	 * @param n
	 * @return
	 */
	public static Set<? extends INode> getAllSuccessors(INode n) {
		if (!n.getGraph().isDirected())
			throw new UnsupportedOperationException("Graph isn't directed.");
		Set<INode> successors = new LinkedHashSet<INode>();
		for (INode succ : n.getSuccessors()) {
			if (!successors.contains(succ)) {
				successors.add((INode) succ);
			}
			Set<? extends INode> allSuccessors = getAllSuccessors(succ);
			for (INode successor : allSuccessors) {
				if (!successors.contains(successor)) {
					successors.add(successor);
				}
			}
		}
		return successors;
	}

	public static Set<? extends INode> getSharedAncestors(INode n1, INode n2) {
		Set<? extends INode> ancestors = getAllPredecessors(n1);
		ancestors.retainAll(getAllPredecessors(n2));
		return ancestors;
	}

	public static Set<? extends INode> getAllConnectedNodes(INode n) {
		Set<INode> connectedNodes = new LinkedHashSet<INode>();
		connectedNodes.addAll(getAllSuccessors(n));
		connectedNodes.addAll(getAllPredecessors(n));
		return connectedNodes;
	}

	/**
	 * Search all non subgraph filtered nodes which have a path to/from a given
	 * node. Parameter node is included in produced set of connected nodes.
	 * 
	 * @param n
	 * @param filter
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<INode> getAllFilteredConnectedNodes(INode n,
			IGraphFilter filter) {
		Set<INode> allConnectedNodes = new LinkedHashSet<INode>();

		boolean growing = true;
		Set<INode> nodeList = new LinkedHashSet<INode>();
		nodeList.add(n);
		allConnectedNodes.add(n);
		while (growing) {
			Set<INode> connectedNodes = new LinkedHashSet<INode>();
			for (INode node : nodeList) {
				if (!filter.isFiltered(node)) {
					for (IEdge in : node.getIncomingEdges()) {
						if (!filter.isFiltered(in)
								&& !GraphTools.isExternalEdge(in)) {
							INode inputNode = in.getSourcePort().getNode();
							if (!filter.isFiltered(inputNode)
									&& (!(inputNode instanceof ISubGraphNode) || (inputNode instanceof ICollapsedNode)))
								connectedNodes.add(inputNode);
						}
					}

					for (IEdge out : node.getOutgoingEdges()) {
						if (!filter.isFiltered(out)
								&& !GraphTools.isExternalEdge(out)) {
							INode outputNode = out.getSinkPort().getNode();
							if (!filter.isFiltered(outputNode)
									&& (!(outputNode instanceof ISubGraphNode) || (outputNode instanceof ICollapsedNode)))
								connectedNodes.add(outputNode);
						}
					}
				}

			}
			connectedNodes.removeAll(nodeList);
			connectedNodes.removeAll(allConnectedNodes);
			allConnectedNodes.addAll(connectedNodes);
			nodeList = connectedNodes;
			growing = !(connectedNodes.size() == 0);
		}

		return allConnectedNodes;
	}

	/**
	 * Search all non subgraph filtered nodes in a graph
	 * 
	 * @param n
	 * @param filter
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Set<INode> getAllFilteredNodes(IGraph g, IGraphFilter filter) {
		Set<INode> nodeList = new LinkedHashSet<INode>();
		for (INode node : g.getNodes()) {
			if (!filter.isFiltered(node)) {
				nodeList.add(node);
			}

		}

		return nodeList;
	}

	/**
	 * Build a cloned graph where only selected nodes from a source graph are
	 * cloned.
	 * 
	 * @param subject
	 *            source graph
	 * @param selectedNodes
	 *            nodes to clone in the new graph component
	 * @param cloner
	 *            graph cloner
	 * @return
	 */
	public static IGraph buildGraphComponent(IGraph subject,
			Set<INode> selectedNodes,
			GraphCopy<IGraph, INode, IEdge, ISubGraphNode> copy) {
		IGraph graph = copy.getCloner().createGraph(subject.isDirected());

		for (INode n : selectedNodes) {
			INode cn = copy.copyNode(n);
			graph.addNode(cn);

		}

		for (INode n : selectedNodes) {
			for (IEdge output : n.getOutgoingEdges()) {
				if (output.getSinkPort() != null) {
					if (selectedNodes.contains(output.getSinkPort().getNode())) {
						graph.addEdge(copy.copyEdge(output));
					}
				}
			}
		}
		return graph;
	}

	public static boolean isConvex(List<INode> nodes) {
		for (INode node : nodes) {
			List<? extends INode> predecessors = node.getPredecessors();
			for (INode node2 : predecessors) {
				if (!nodes.contains(node2)) {
					Set<? extends INode> allPredecessors = GraphTools
							.getAllPredecessors(node2);

					for (INode node3 : allPredecessors) {
						if (nodes.contains(node3)) {
							return false;
						}
					}

				}
			}

		}
		return true;

	}

	public static boolean hasNoPredecessor(INode node) {
		if (getAllPredecessors(node).size() == 0)
			return true;
		return false;
	}

	public static boolean hasNoSuccessor(INode n) {
		if (getAllSuccessors(n).size() == 0)
			return true;
		return false;
	}
}
