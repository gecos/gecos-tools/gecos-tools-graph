package fr.irisa.cairn.graph.tools;

import java.util.Iterator;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.analysis.GraphAnalysis;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.graph.util.BidirectionnalHashMap;

/**
 * A tool providing copy functions for an {@link IGraph}. Copy/original links
 * are stored in order to resolve uncontained references.
 * 
 * @author antoine
 * 
 */

public class GraphCopy<G extends IGraph, V extends INode, E extends IEdge, SN extends ISubGraphNode>
		implements IGraphVisitor {

	@SuppressWarnings("rawtypes")
	protected ICloneProvider cloner;
	protected BidirectionnalHashMap<INode, INode> oldToNewNodesBidiMap;
	protected BidirectionnalHashMap<IPort, IPort> oldToNewPortsBidiMap;
	
	@Inject
	@SuppressWarnings("rawtypes")
	public GraphCopy(ICloneProvider cloner) {
		this.oldToNewNodesBidiMap = new BidirectionnalHashMap<INode, INode>();
		this.oldToNewPortsBidiMap = new BidirectionnalHashMap<IPort, IPort>();
		this.cloner = cloner;
	}

	/**
	 * Copy a node and store the links: original->clone and clone->original. If
	 * the original node has already been copied, the previous copy is returned.
	 * 
	 * @param n
	 * @return
	 */
	public V copyNode(V n) {
		return getNewNode(n);
	}

	@SuppressWarnings("unchecked")
	public SN copySubGraph(SN sn) {
		SN copy = (SN) oldToNewNodesBidiMap.get(sn);
		if (copy == null) {
			copy = (SN) cloner.cloneNode(sn);
			declareMapping(sn, copy);

			Iterator<INode> asapIterator = GraphAnalysis
					.topologicalIterator(sn);
			while (asapIterator.hasNext()) {
				INode next = asapIterator.next();
				next.accept(this, copy);
			}

			for (IEdge e : sn.getEdges()) {
				e.accept(this, copy);
			}

		}

		return copy;
	}

	protected void declareMapping(INode oldNode, INode newNode) {
		oldToNewNodesBidiMap.put(oldNode, newNode);
		for (int p = 0; p < oldNode.getInputPorts().size(); ++p) {
			if (newNode.getInputPorts().size() > p)
				oldToNewPortsBidiMap.put(oldNode.getInputPorts().get(p),
						newNode.getInputPorts().get(p));
		}
		for (int p = 0; p < oldNode.getOutputPorts().size(); ++p) {
			if (newNode.getOutputPorts().size() > p)
				oldToNewPortsBidiMap.put(oldNode.getOutputPorts().get(p),
						newNode.getOutputPorts().get(p));
		}
	}

	/**
	 * Copy an edge. Source and sink nodes are copied if they had'nt already
	 * been.
	 * 
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public E copyEdge(E e) {
		E copy = (E) cloner.cloneEdge(e);

		V oldSource = (V) GraphTools.getSourceNode(e);
		V oldSink = (V) GraphTools.getSinkNode(e);

		V newSource = getNewNode(oldSource);
		V newSink = getNewNode(oldSink);
		if (newSource == null) {
			newSource = copyNode((V) oldSource);
		}
		if (newSink == null) {
			newSink = copyNode((V) oldSink);
		}

		try {
			cloner.reconnectClone(copy, e, newSource, newSink);
		} catch (GraphException e1) {
			throw new RuntimeException(e1);
		}
		return copy;
	}

	/**
	 * Copy a graph.
	 * 
	 * @param g
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public G copyGraph(G g) {
		IGraph graph = cloner.createGraph(g.isDirected());
		Iterator<INode> asapIterator = GraphAnalysis.topologicalIterator(g);
		while (asapIterator.hasNext()) {
			INode next = asapIterator.next();
			next.accept(this, graph);
		}
		for (IEdge e : g.getEdges()) {
			e.accept(this, graph);
		}
		return (G) graph;
	}

	/**
	 * Get the copy node.
	 * 
	 * @param n
	 *            original node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public V getNewNode(V n) {
		V copy = (V) oldToNewNodesBidiMap.get(n);
		if (copy == null) {
			copy = (V) cloner.cloneNode(n);
			declareMapping(n, copy);
		}
		return (V) oldToNewNodesBidiMap.get(n);
	}

	public IPort getNewPort(IPort p) {
		return oldToNewPortsBidiMap.get(p);
	}

	public IPort getOldPort(IPort p) {
		return oldToNewPortsBidiMap.getKey(p);
	}

	/**
	 * Get the original node.
	 * 
	 * @param n
	 *            copy node
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public V getOldNode(V n) {
		return (V) oldToNewNodesBidiMap.getKey(n);
	}

	@SuppressWarnings("unchecked")
	public void visitEdge(IEdge e, Object graph) {
		((IGraph) graph).addEdge(copyEdge((E) e));
	}

	@SuppressWarnings("unchecked")
	public void visitNode(INode n, Object graph) {
		((IGraph) graph).addNode(copyNode((V) n));
	}

	@SuppressWarnings("unchecked")
	public void visitSubGraph(ISubGraphNode sn, Object graph) {
		((IGraph) graph).addNode(copySubGraph((SN) sn));

	}

	public void clear(){
		oldToNewNodesBidiMap.clear();
		oldToNewPortsBidiMap.clear();
	}
	/**
	 * @return the cloner
	 */
	@SuppressWarnings("rawtypes")
	public ICloneProvider getCloner() {
		return cloner;
	}

	@SuppressWarnings("unchecked")
	public void visitCollapsedNode(ICollapsedNode cn, Object graph) {
		// copy contained subgraph
		ICollapsedNode copy = (ICollapsedNode) copySubGraph((SN) cn);
		((IGraph) graph).addNode(copy);

		// map inputs/outputs
		for (IPort pin : cn.getInputPorts()) {
			IPort subport = cn.getMappedInputPort(pin);
			if (subport != null) {
				IPort newpin = getNewPort(pin);
				IPort newsubport = getNewPort(subport);
				copy.mapInput(newpin, newsubport);
			}
		}

		for (IPort pout : cn.getOutputPorts()) {
			IPort subport = cn.getMappedOutputPort(pout);
			if (subport != null) {
				copy.mapOutput(getNewPort(pout), getNewPort(subport));
			}
		}
	}
}
