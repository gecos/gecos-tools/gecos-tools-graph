package fr.irisa.cairn.graph.tools;

import java.util.Comparator;

/**
 * Abstract class for comparator based on quality of compared objects.
 * 
 * @author antoine
 * 
 * @param <T>
 */
public abstract class AbstractQualityComparator<T> implements Comparator<T> {

	public int compare(T o1, T o2) {
		return new Integer(quality(o1)).compareTo(quality(o2));
	}

	/**
	 * Quality of a an element.
	 * 
	 * @param e
	 * @return
	 */
	protected abstract int quality(T e);

}
