package fr.irisa.cairn.graph.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.ext.IntegerEdgeNameProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.MatrixExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.xml.sax.SAXException;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.implement.providers.NodeLabelProvider;
import fr.irisa.cairn.graph.io.DOTExport;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;
@SuppressWarnings("rawtypes")
public class GraphIOTools {

	/**
	 * Default dot output for a graph
	 * 
	 * @param out
	 * @param graph
	 */
	
	public static void dotExport(PrintStream out, IGraph graph) {
		NodeLabelProvider labelProvider = new NodeLabelProvider();
		DOTExport export = new DOTExport(labelProvider, null,
				new DotAttributsProvider());
		export.export(out, graph);
	}

	/**
	 * Default DOT export for a graph.
	 * 
	 * @param file
	 * @param graph
	 */
	public static void dotExport(String file, IGraph graph) {
		try {
			dotExport(new PrintStream(new File(file)), graph);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void graphMLExport(String file, IGraph graph) {
		try {
			OutputStream out = new FileOutputStream(new File(file));
			graphMLExport(out, graph);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void graphMLExport(OutputStream out, IGraph graph) {
		INodeLabelProvider labelProvider = new NodeLabelProvider();
		GraphMLExporter<INode, IEdge> exporter = new GraphMLExporter<INode, IEdge>(
				new IntegerNameProvider<INode>(), new GraphVertexLabelProvider(
						labelProvider), new IntegerEdgeNameProvider<IEdge>(),
				null);
		try {
			exporter.export(new OutputStreamWriter(out), getGraph(graph));
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public static void graphMLExport(OutputStream out, IGraph graph,
			INodeLabelProvider labelProvider) {
		GraphMLExporter<INode, IEdge> exporter = new GraphMLExporter<INode, IEdge>(
				new IntegerNameProvider<INode>(), new GraphVertexLabelProvider(
						labelProvider), new IntegerEdgeNameProvider<IEdge>(),
				null);
		try {
			exporter.export(new OutputStreamWriter(out), getGraph(graph));
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public static void graphMLExport(String file, IGraph graph,
			INodeLabelProvider labelProvider) {
		try {
			OutputStream out = new FileOutputStream(new File(file));
			graphMLExport(out, graph, labelProvider);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static void matrixAdjancyExport(OutputStream out, IGraph g) {
		MatrixExporter<INode, IEdge> exporter = new MatrixExporter<INode, IEdge>();
		if (g.isDirected()) {
			exporter.exportAdjacencyMatrix(new OutputStreamWriter(out),
					getDirectedGraph(g));
		} else
			exporter.exportAdjacencyMatrix(new OutputStreamWriter(out),
					(UndirectedGraph<INode, IEdge>) getGraph(g));

	}

	private static class GraphVertexLabelProvider implements
			VertexNameProvider<INode> {
		private INodeLabelProvider provider;

		public GraphVertexLabelProvider(INodeLabelProvider provider) {
			this.provider = provider;
		}

		@SuppressWarnings("unchecked")
		public String getVertexName(INode n) {
			return provider.getNodeLabel(n);
		}

	}

	/**
	 * Build Jgrapht graph from an {@link IGraph}
	 * 
	 * @param g
	 * @return
	 */
	private static Graph<INode, IEdge> getGraph(IGraph g) {

		Graph<INode, IEdge> graph;
		if (g.isDirected()) {
			graph = new SimpleDirectedGraph<INode, IEdge>(IEdge.class);
		} else {
			graph = new SimpleGraph<INode, IEdge>(IEdge.class);
		}
		for (INode n : g.getNodes()) {
			graph.addVertex(n);
		}
		for (IEdge e : g.getEdges()) {
			graph.addEdge(e.getSourcePort().getNode(), e.getSinkPort()
					.getNode(), e);
		}
		return graph;
	}

	/**
	 * Build Jgrapht directed graph from an {@link IGraph}
	 * 
	 * @param g
	 * @return
	 */
	private static DirectedGraph<INode, IEdge> getDirectedGraph(IGraph g) {

		DirectedGraph<INode, IEdge> graph;
		if (g.isDirected()) {
			graph = new SimpleDirectedGraph<INode, IEdge>(IEdge.class);
		} else {
			throw new IllegalArgumentException("Graph isn't directed");
		}
		for (INode n : g.getNodes()) {
			graph.addVertex(n);
		}
		for (IEdge e : g.getEdges()) {
			if (e.getSinkPort().getNode().getGraph() == e.getSourcePort()
					.getNode().getGraph())
				graph.addEdge(e.getSourcePort().getNode(), e.getSinkPort()
						.getNode(), e);
		}
		return graph;
	}
}
