package fr.irisa.cairn.graph.tools;

import static fr.irisa.cairn.graph.tools.GraphTools.getInputPort;
import static fr.irisa.cairn.graph.tools.GraphTools.getOutputPort;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.VertexFactory;
import org.jgrapht.generate.RandomGraphGenerator;
import org.jgrapht.graph.SimpleGraph;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.implement.GraphFactory;

public class GraphGenerator {
	
	private IGraphFactory factory = new GraphFactory();

	/**
	 * Generate a random undirected graph
	 * 
	 * @param vertices
	 *            number of vertices
	 * @param edges
	 *            number of edges
	 * @return
	 */
	public IGraph randomGraphGenerator(int vertices, int edges) {
		RandomGraphGenerator<INode, IEdge> generator = new RandomGraphGenerator<INode, IEdge>(
				vertices, edges);
		IGraph g = factory.createGraph(false);
		Graph<INode, IEdge> graph = new SimpleGraph<INode, IEdge>(
				new DefaultEdgeFactory());
		generator.generateGraph(graph, new DefaultVertexFactory(),
				new Hashtable<String, INode>());

		for (INode n : graph.vertexSet()) {
			g.addNode(n);
		}
		for (IEdge e : graph.edgeSet()) {
			g.addEdge(e);
		}
		return g;
	}

	protected class DefaultVertexFactory implements VertexFactory<INode> {
		public INode createVertex() {
			return factory.createNode();
		}

	}

	protected class DefaultEdgeFactory implements EdgeFactory<INode, IEdge> {

		public IEdge createEdge(INode source, INode sink) {
			if (source.getOutputPorts().size() == 0) {
				source.addOutputPort(factory.createPort());
			}
			IEdge e;
			try {
				IPort p = factory.createPort();
				sink.addInputPort(p);
				e = factory.connect(getOutputPort(source, 0), p);
			} catch (GraphException e1) {
				throw new RuntimeException(e1);
			}
			return e;
		}

	}

	/**
	 * Generate an acyclic directed graph.
	 * 
	 * @param vertices
	 *            number of vertices
	 * @return
	 */
	public IGraph acyclicGraphGenerator(int vertices) {
		IGraph graph = factory.createGraph(true);
		List<INode> nodes = new ArrayList<INode>();
		assert (vertices > 15);
		for (int i = 0; i < vertices; i++) {
			INode n = factory.createNode();
			n.addInputPort(factory.createPort());
			n.addInputPort(factory.createPort());
			n.addOutputPort(factory.createPort());
			graph.addNode(n);
			nodes.add(n);
		}

		double sqrt = Math.sqrt(vertices);
		for (int i = (int) (1 + Math.sqrt(vertices)); i < vertices; i++) {
			int srcIndex0 = i - 1 - (int) (Math.random() * sqrt);
			int srcIndex1 = i - 1 - (int) (Math.random() * sqrt);
			while (srcIndex1 == srcIndex0) {
				srcIndex1 = i - 1 - (int) (Math.random() * sqrt);
			}

			INode sinkNode = nodes.get(i);
			INode sourceNode0 = nodes.get(srcIndex0);
			INode sourceNode1 = nodes.get(srcIndex1);

			try {
				factory.connect(getOutputPort(sourceNode0, 0), getInputPort(
						sinkNode, 0));
				factory.connect(getOutputPort(sourceNode1, 0), getInputPort(
						sinkNode, 1));
			} catch (GraphException e) {
				throw new RuntimeException(e);
			}

		}
		return graph;
	}
}
