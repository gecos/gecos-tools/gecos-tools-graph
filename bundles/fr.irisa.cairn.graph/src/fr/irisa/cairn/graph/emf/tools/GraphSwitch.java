/**
 */
package fr.irisa.cairn.graph.emf.tools;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import fr.irisa.cairn.graph.GraphPackage;
import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.graph.GraphPackage
 * @generated
 */
public class GraphSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static GraphPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphSwitch() {
		if (modelPackage == null) {
			modelPackage = GraphPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case GraphPackage.IEDGE: {
				IEdge iEdge = (IEdge)theEObject;
				T result = caseIEdge(iEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.IGRAPH: {
				IGraph iGraph = (IGraph)theEObject;
				T result = caseIGraph(iGraph);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.IGRAPH_FACTORY: {
				IGraphFactory iGraphFactory = (IGraphFactory)theEObject;
				T result = caseIGraphFactory(iGraphFactory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.IGRAPH_VISITOR: {
				IGraphVisitor iGraphVisitor = (IGraphVisitor)theEObject;
				T result = caseIGraphVisitor(iGraphVisitor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.INODE: {
				INode iNode = (INode)theEObject;
				T result = caseINode(iNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.IPORT: {
				IPort iPort = (IPort)theEObject;
				T result = caseIPort(iPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.ISUB_GRAPH_NODE: {
				ISubGraphNode iSubGraphNode = (ISubGraphNode)theEObject;
				T result = caseISubGraphNode(iSubGraphNode);
				if (result == null) result = caseIGraph(iSubGraphNode);
				if (result == null) result = caseINode(iSubGraphNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case GraphPackage.ICOLLAPSED_NODE: {
				ICollapsedNode iCollapsedNode = (ICollapsedNode)theEObject;
				T result = caseICollapsedNode(iCollapsedNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IEdge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IEdge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIEdge(IEdge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IGraph</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IGraph</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIGraph(IGraph object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IGraph Factory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IGraph Factory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIGraphFactory(IGraphFactory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IGraph Visitor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IGraph Visitor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIGraphVisitor(IGraphVisitor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>INode</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>INode</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseINode(INode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IPort</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IPort</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIPort(IPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ISub Graph Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ISub Graph Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseISubGraphNode(ISubGraphNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ICollapsed Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ICollapsed Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseICollapsedNode(ICollapsedNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //GraphSwitch
