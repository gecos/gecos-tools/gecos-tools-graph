/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.graph.emf.implement;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fr.irisa.cairn.graph.GraphFactory;
import fr.irisa.cairn.graph.GraphPackage;
import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GraphPackageImpl extends EPackageImpl implements GraphPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iEdgeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iGraphEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iGraphFactoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iGraphVisitorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iSubGraphNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iCollapsedNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType setEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.irisa.cairn.graph.GraphPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GraphPackageImpl() {
		super(eNS_URI, GraphFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GraphPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GraphPackage init() {
		if (isInited) return (GraphPackage)EPackage.Registry.INSTANCE.getEPackage(GraphPackage.eNS_URI);

		// Obtain or create and register package
		GraphPackageImpl theGraphPackage = (GraphPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GraphPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GraphPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theGraphPackage.createPackageContents();

		// Initialize created meta-data
		theGraphPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGraphPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GraphPackage.eNS_URI, theGraphPackage);
		return theGraphPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIEdge() {
		return iEdgeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIEdge__ReconnectSourcePort__IPort() {
		return iEdgeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIEdge__ReconnectSinkPort__IPort() {
		return iEdgeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIEdge__Accept__IGraphVisitor_Object() {
		return iEdgeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIEdge__GetSourcePort() {
		return iEdgeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIEdge__GetSinkPort() {
		return iEdgeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIGraph() {
		return iGraphEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__AddEdge__IEdge() {
		return iGraphEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__AddNode__INode() {
		return iGraphEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__RemoveEdge__IEdge() {
		return iGraphEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__RemoveNode__INode() {
		return iGraphEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__IsDirected() {
		return iGraphEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__GetNodes() {
		return iGraphEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__GetEdges() {
		return iGraphEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__GetExternalInputs() {
		return iGraphEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraph__GetExternalOutputs() {
		return iGraphEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIGraphFactory() {
		return iGraphFactoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphFactory__CreateSubgraph__boolean() {
		return iGraphFactoryEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphFactory__CreateNode() {
		return iGraphFactoryEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphFactory__CreateGraph__boolean() {
		return iGraphFactoryEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphFactory__Connect__IPort_IPort() {
		return iGraphFactoryEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphFactory__CreatePort() {
		return iGraphFactoryEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIGraphVisitor() {
		return iGraphVisitorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphVisitor__VisitNode__INode_Object() {
		return iGraphVisitorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphVisitor__VisitEdge__IEdge_Object() {
		return iGraphVisitorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIGraphVisitor__VisitSubGraph__ISubGraphNode_Object() {
		return iGraphVisitorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getINode() {
		return iNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__AddInputPort__IPort() {
		return iNodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__AddOutputPort__IPort() {
		return iNodeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__IsPredecessorOf__INode() {
		return iNodeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__IsSuccessorTo__INode() {
		return iNodeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__IsConnectedTo__INode() {
		return iNodeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__Accept__IGraphVisitor_Object() {
		return iNodeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetPredecessors() {
		return iNodeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetSuccessors() {
		return iNodeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetInputPorts() {
		return iNodeEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetOutputPorts() {
		return iNodeEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetIncomingEdges() {
		return iNodeEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetOutgoingEdges() {
		return iNodeEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetDegree() {
		return iNodeEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetInDegree() {
		return iNodeEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetOutDegree() {
		return iNodeEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getINode__GetGraph() {
		return iNodeEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIPort() {
		return iPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIPort__GetNode() {
		return iPortEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIPort__GetEdges() {
		return iPortEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getISubGraphNode() {
		return iSubGraphNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getICollapsedNode() {
		return iCollapsedNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSet() {
		return setEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphFactory getGraphFactory() {
		return (GraphFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		iEdgeEClass = createEClass(IEDGE);
		createEOperation(iEdgeEClass, IEDGE___RECONNECT_SOURCE_PORT__IPORT);
		createEOperation(iEdgeEClass, IEDGE___RECONNECT_SINK_PORT__IPORT);
		createEOperation(iEdgeEClass, IEDGE___ACCEPT__IGRAPHVISITOR_OBJECT);
		createEOperation(iEdgeEClass, IEDGE___GET_SOURCE_PORT);
		createEOperation(iEdgeEClass, IEDGE___GET_SINK_PORT);

		iGraphEClass = createEClass(IGRAPH);
		createEOperation(iGraphEClass, IGRAPH___ADD_EDGE__IEDGE);
		createEOperation(iGraphEClass, IGRAPH___ADD_NODE__INODE);
		createEOperation(iGraphEClass, IGRAPH___REMOVE_EDGE__IEDGE);
		createEOperation(iGraphEClass, IGRAPH___REMOVE_NODE__INODE);
		createEOperation(iGraphEClass, IGRAPH___IS_DIRECTED);
		createEOperation(iGraphEClass, IGRAPH___GET_NODES);
		createEOperation(iGraphEClass, IGRAPH___GET_EDGES);
		createEOperation(iGraphEClass, IGRAPH___GET_EXTERNAL_INPUTS);
		createEOperation(iGraphEClass, IGRAPH___GET_EXTERNAL_OUTPUTS);

		iGraphFactoryEClass = createEClass(IGRAPH_FACTORY);
		createEOperation(iGraphFactoryEClass, IGRAPH_FACTORY___CREATE_SUBGRAPH__BOOLEAN);
		createEOperation(iGraphFactoryEClass, IGRAPH_FACTORY___CREATE_NODE);
		createEOperation(iGraphFactoryEClass, IGRAPH_FACTORY___CREATE_GRAPH__BOOLEAN);
		createEOperation(iGraphFactoryEClass, IGRAPH_FACTORY___CONNECT__IPORT_IPORT);
		createEOperation(iGraphFactoryEClass, IGRAPH_FACTORY___CREATE_PORT);

		iGraphVisitorEClass = createEClass(IGRAPH_VISITOR);
		createEOperation(iGraphVisitorEClass, IGRAPH_VISITOR___VISIT_NODE__INODE_OBJECT);
		createEOperation(iGraphVisitorEClass, IGRAPH_VISITOR___VISIT_EDGE__IEDGE_OBJECT);
		createEOperation(iGraphVisitorEClass, IGRAPH_VISITOR___VISIT_SUB_GRAPH__ISUBGRAPHNODE_OBJECT);

		iNodeEClass = createEClass(INODE);
		createEOperation(iNodeEClass, INODE___ADD_INPUT_PORT__IPORT);
		createEOperation(iNodeEClass, INODE___ADD_OUTPUT_PORT__IPORT);
		createEOperation(iNodeEClass, INODE___IS_PREDECESSOR_OF__INODE);
		createEOperation(iNodeEClass, INODE___IS_SUCCESSOR_TO__INODE);
		createEOperation(iNodeEClass, INODE___IS_CONNECTED_TO__INODE);
		createEOperation(iNodeEClass, INODE___ACCEPT__IGRAPHVISITOR_OBJECT);
		createEOperation(iNodeEClass, INODE___GET_PREDECESSORS);
		createEOperation(iNodeEClass, INODE___GET_SUCCESSORS);
		createEOperation(iNodeEClass, INODE___GET_INPUT_PORTS);
		createEOperation(iNodeEClass, INODE___GET_OUTPUT_PORTS);
		createEOperation(iNodeEClass, INODE___GET_INCOMING_EDGES);
		createEOperation(iNodeEClass, INODE___GET_OUTGOING_EDGES);
		createEOperation(iNodeEClass, INODE___GET_DEGREE);
		createEOperation(iNodeEClass, INODE___GET_IN_DEGREE);
		createEOperation(iNodeEClass, INODE___GET_OUT_DEGREE);
		createEOperation(iNodeEClass, INODE___GET_GRAPH);

		iPortEClass = createEClass(IPORT);
		createEOperation(iPortEClass, IPORT___GET_NODE);
		createEOperation(iPortEClass, IPORT___GET_EDGES);

		iSubGraphNodeEClass = createEClass(ISUB_GRAPH_NODE);

		iCollapsedNodeEClass = createEClass(ICOLLAPSED_NODE);

		// Create data types
		setEDataType = createEDataType(SET);
		listEDataType = createEDataType(LIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters
		addETypeParameter(setEDataType, "T");
		addETypeParameter(listEDataType, "T");

		// Set bounds for type parameters

		// Add supertypes to classes
		iSubGraphNodeEClass.getESuperTypes().add(this.getIGraph());
		iSubGraphNodeEClass.getESuperTypes().add(this.getINode());

		// Initialize classes, features, and operations; add parameters
		initEClass(iEdgeEClass, IEdge.class, "IEdge", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIEdge__ReconnectSourcePort__IPort(), null, "reconnectSourcePort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIPort(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIEdge__ReconnectSinkPort__IPort(), null, "reconnectSinkPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIPort(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIEdge__Accept__IGraphVisitor_Object(), null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIGraphVisitor(), "graphVistor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "arg", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIEdge__GetSourcePort(), this.getIPort(), "getSourcePort", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIEdge__GetSinkPort(), this.getIPort(), "getSinkPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iGraphEClass, IGraph.class, "IGraph", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIGraph__AddEdge__IEdge(), null, "addEdge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIEdge(), "e", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraph__AddNode__INode(), null, "addNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINode(), "e", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraph__RemoveEdge__IEdge(), null, "removeEdge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIEdge(), "e", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraph__RemoveNode__INode(), null, "removeNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINode(), "e", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIGraph__IsDirected(), ecorePackage.getEBoolean(), "isDirected", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraph__GetNodes(), null, "getNodes", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(this.getSet());
		EGenericType g2 = createEGenericType(this.getINode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getIGraph__GetEdges(), null, "getEdges", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSet());
		g2 = createEGenericType(this.getIEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getIGraph__GetExternalInputs(), null, "getExternalInputs", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSet());
		g2 = createEGenericType(this.getIEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getIGraph__GetExternalOutputs(), null, "getExternalOutputs", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSet());
		g2 = createEGenericType(this.getIEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(iGraphFactoryEClass, IGraphFactory.class, "IGraphFactory", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIGraphFactory__CreateSubgraph__boolean(), this.getISubGraphNode(), "createSubgraph", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "directed", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIGraphFactory__CreateNode(), ecorePackage.getEObject(), "createNode", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraphFactory__CreateGraph__boolean(), this.getIGraph(), "createGraph", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBoolean(), "directed", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraphFactory__Connect__IPort_IPort(), this.getIEdge(), "connect", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIPort(), "source", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIPort(), "sink", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getIGraphFactory__CreatePort(), this.getIPort(), "createPort", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iGraphVisitorEClass, IGraphVisitor.class, "IGraphVisitor", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIGraphVisitor__VisitNode__INode_Object(), null, "visitNode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINode(), "n", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "arg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraphVisitor__VisitEdge__IEdge_Object(), null, "visitEdge", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIEdge(), "e", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "arg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIGraphVisitor__VisitSubGraph__ISubGraphNode_Object(), null, "visitSubGraph", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getISubGraphNode(), "sn", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "arg", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iNodeEClass, INode.class, "INode", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getINode__AddInputPort__IPort(), null, "addInputPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIPort(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getINode__AddOutputPort__IPort(), null, "addOutputPort", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIPort(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getINode__IsPredecessorOf__INode(), ecorePackage.getEBoolean(), "isPredecessorOf", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getINode__IsSuccessorTo__INode(), ecorePackage.getEBoolean(), "isSuccessorTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getINode__IsConnectedTo__INode(), ecorePackage.getEBoolean(), "isConnectedTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINode(), "node", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getINode__Accept__IGraphVisitor_Object(), null, "accept", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIGraphVisitor(), "graphVistor", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "arg", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getINode__GetPredecessors(), null, "getPredecessors", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getINode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getINode__GetSuccessors(), null, "getSuccessors", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getINode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getINode__GetInputPorts(), null, "getInputPorts", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getIPort());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getINode__GetOutputPorts(), null, "getOutputPorts", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getIPort());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getINode__GetIncomingEdges(), null, "getIncomingEdges", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getIEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getINode__GetOutgoingEdges(), null, "getOutgoingEdges", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getIEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEOperation(getINode__GetDegree(), ecorePackage.getEInt(), "getDegree", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getINode__GetInDegree(), ecorePackage.getEInt(), "getInDegree", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getINode__GetOutDegree(), ecorePackage.getEInt(), "getOutDegree", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getINode__GetGraph(), this.getIGraph(), "getGraph", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(iPortEClass, IPort.class, "IPort", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIPort__GetNode(), this.getINode(), "getNode", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIPort__GetEdges(), null, "getEdges", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSet());
		g2 = createEGenericType(this.getIEdge());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(iSubGraphNodeEClass, ISubGraphNode.class, "ISubGraphNode", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		initEClass(iCollapsedNodeEClass, ICollapsedNode.class, "ICollapsedNode", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(setEDataType, Set.class, "Set", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //GraphPackageImpl
