package fr.irisa.cairn.graph;

import java.util.List;

public interface ICollapsedNode extends ISubGraphNode {
	/**
	 * Map a node input port to an input port in the collapsed graph
	 * 
	 * @param in
	 *            node input port
	 * @param nestedPort
	 *            input port in the collapsed graph
	 */
	public void mapInput(IPort in, IPort nestedPort);

	/**
	 * Map a node output port to an output port in the collapsed graph
	 * 
	 * @param in
	 *            node output port
	 * @param nestedPort
	 *            output port in the collapsed graph
	 */
	public void mapOutput(IPort out, IPort nestedPort);

	/**
	 * Get the port in the collapsed graph mapped to a node input port.
	 * 
	 * @param in
	 *            node input port
	 * @return collapsed graph input port
	 */
	public IPort getMappedInputPort(IPort in);

	/**
	 * Get the port in the collapsed graph mapped to a node output port.
	 * 
	 * @param in
	 *            node input port
	 * @return collapsed graph input port
	 */
	public IPort getMappedOutputPort(IPort out);

	/**
	 * Expand the collapsed graph in the node graph.
	 * 
	 * @return list of created nodes
	 */
	public List<? extends INode> expand();
}
