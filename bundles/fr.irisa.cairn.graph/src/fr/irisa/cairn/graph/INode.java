package fr.irisa.cairn.graph;

import java.util.List;

/**
 * An {@link INode} has an ordered list of inputs ports and another one of
 * output ports. A node can be only in one single graph.
 * 
 * @author antoine
 * 
 */
/**
 * @model
 */
public interface INode {

	/**
	 * Get the graph containing the node.
	 * 
	 * @model default=""
	 * 
	 * @return the node graph or <code>null</code> if graph hasn't been set
	 */
	public IGraph getGraph();

	/**
	 * Get the number of connected inputs to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public int getInDegree();

	/**
	 * Get the number of connected outputs from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public int getOutDegree();

	/**
	 * Get the number of connected inputs and outputs of the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public int getDegree();

	/**
	 * Add an input port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	public void addInputPort(IPort p);

	public void removeInputPort(IPort p);

	/**
	 * Add an output port to the node.
	 * 
	 * @model default=""
	 * 
	 * @return created port
	 */
	public void addOutputPort(IPort p);

	public void removeOutputPort(IPort p);

	/**
	 * Get a list of all incoming edges to the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public List<? extends IEdge> getIncomingEdges();

	/**
	 * Get a list of all outgoing edges from the node.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public List<? extends IEdge> getOutgoingEdges();

	/**
	 * Get the list of all input ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public List<? extends IPort> getInputPorts();

	/**
	 * Get the list of all output ports.
	 * 
	 * @model default=""
	 * 
	 * @return
	 */
	public List<? extends IPort> getOutputPorts();

	/**
	 * Get predecessors of this node in its graph.
	 * 
	 * @model default=""
	 */
	public List<? extends INode> getPredecessors();

	/**
	 * Get successors of this node in its graph.
	 * 
	 * @model default=""
	 */
	public List<? extends INode> getSuccessors();

	/**
	 * @model default=""
	 */
	public boolean isPredecessorOf(INode node);

	/**
	 * @model default=""
	 */
	public boolean isSuccessorTo(INode node);

	/**
	 * @model default=""
	 */
	public boolean isConnectedTo(INode node);

	public void setLabel(String label);
	
	public String getLabel();
	/**
	 * @model default=""
	 */
	public void accept(IGraphVisitor graphVistor, Object arg);

}
