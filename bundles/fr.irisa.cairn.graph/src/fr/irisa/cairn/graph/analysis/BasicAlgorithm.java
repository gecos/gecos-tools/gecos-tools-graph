package fr.irisa.cairn.graph.analysis;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;

/**
 * Basic ordered algorithm. All nodes and edges are computed in the first step.
 * 
 * @author antoine
 * 
 */
@SuppressWarnings({"rawtypes"}) 
public class BasicAlgorithm extends AbstractIterativeAlgorithm {

	public BasicAlgorithm(IBehaviorProvider behavior) {
		super(behavior);

	}

	@Override
	public void initializeFirstStep(IGraph g) {
		for (INode n : g.getNodes()) {
			routeNode(n, null);
		}
		for (IEdge e : g.getEdges()) {
			routeEdge(e, null);
		}
	}

	@Override
	protected void routeEdge(IEdge e, Object arg) {
		nextEdges.add(e);
	}

	@Override
	protected void routeNode(INode n, Object arg) {
		nextNodes.add(n);
	}

}
