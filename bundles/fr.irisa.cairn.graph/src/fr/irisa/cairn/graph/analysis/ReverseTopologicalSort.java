package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;
import fr.irisa.cairn.graph.tools.GraphTools;

public class ReverseTopologicalSort extends AbstractIterativeAlgorithm {

	public ReverseTopologicalSort() {
		super(new ReverseTopologicalSortBehavior());
	}

	@Override
	public void initializeFirstStep(IGraph g) {
		nextNodes.addAll(GraphTools.getSinkNodes(g));
	}

	public List<INode> getSortedList() {
		return ((ReverseTopologicalSortBehavior) behavior).getSortedList();
	}

	@Override
	public void routeNode(INode n, Object arg) {
		ReverseTopologicalSortBehavior topoBehavior = (ReverseTopologicalSortBehavior) behavior;
		if (!topoBehavior.getSortedList().contains(n)) {
			nextNodes.add(n);
		}else{
			for(INode pred: n.getPredecessors()){
				nextNodes.add(pred);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private static class ReverseTopologicalSortBehavior implements
			IBehaviorProvider {

		private List<INode> sortedList;

		public ReverseTopologicalSortBehavior() {
			this.sortedList = new ArrayList<INode>();
		}

		public List<INode> getSortedList() {
			return sortedList;
		}

		public void computeEdge(IEdge e, Object arg) {

		}

		public void computeNode(INode n, Object arg) {
			List<? extends INode> successors = n.getSuccessors();
			List<INode> current = new ArrayList<INode>(successors);
			current.removeAll(sortedList);
			if (current.size()==0 && !sortedList.contains(n)) {
				sortedList.add(n);
			}
		}
	}
}
