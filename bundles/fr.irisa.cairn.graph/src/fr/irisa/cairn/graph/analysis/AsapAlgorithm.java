package fr.irisa.cairn.graph.analysis;

import static fr.irisa.cairn.graph.tools.GraphTools.getSinkNode;
import static fr.irisa.cairn.graph.tools.GraphTools.getSourceNode;
import static fr.irisa.cairn.graph.tools.GraphTools.getSourceNodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;
import fr.irisa.cairn.graph.providers.ILatencyProvider;
import fr.irisa.cairn.graph.tools.GraphTools;
@SuppressWarnings({"rawtypes","unchecked"}) 
public class AsapAlgorithm extends AbstractIterativeAlgorithm {

	protected ILatencyProvider latencyProvider;
	protected Set<INode> visitedNodes;
	protected int crtTime;
	protected Map<INode, Integer> startTime;

	public AsapAlgorithm(IBehaviorProvider behavior,
			ILatencyProvider latencyProvider) {
		super(behavior);
		this.latencyProvider = latencyProvider;
		this.visitedNodes = new HashSet<INode>();
		this.startTime = new Hashtable<INode, Integer>();
	}

	public AsapAlgorithm(ILatencyProvider latencyProvider) {
		this(null, latencyProvider);
	}

	public AsapAlgorithm(ILatencyProvider latencyProvider, IGraph graph) {
		this(graph, null, latencyProvider);
	}

	/**
	 * Initialize an ASAP algorithm for a given directed graph.
	 * 
	 * @param g
	 * @param behavior
	 * @param latencyProvider
	 */
	public AsapAlgorithm(IGraph g, IBehaviorProvider behavior,
			ILatencyProvider latencyProvider) {
		this(behavior, latencyProvider);
		initializeFirstStep(g);
	}

	public Map<INode, Integer> getNodesSteps() {
		return startTime;
	}

	public Integer getNodeStep(INode n) {
		return startTime.get(n);
	}

	/**
	 * Get nodes ordered by their steps.
	 * 
	 * @return
	 */
	public List<INode> getOrderedNodes() {
		List<INode> asapNodes = new ArrayList<INode>();
		asapNodes.addAll(startTime.keySet());
		Collections.sort(asapNodes, new NodeStepComparator());
		return asapNodes;
	}

	public int getCriticalPathLength() {
		int max = 0;
		for (INode n : getOrderedNodes()) {
			int end = endTime(n);
			if (max < end)
				max = end;
		}
		return max;
	}

	private class NodeStepComparator implements Comparator<INode> {

		public int compare(INode o1, INode o2) {
			Integer step1 = getNodeStep(o1);
			Integer step2 = getNodeStep(o2);
			return step1.compareTo(step2);
		}

	}

	@Override
	public void initializeFirstStep(IGraph g) {
		if (!g.isDirected())
			throw new IllegalArgumentException("Graph has to be directed.");
		nextNodes.addAll(getSourceNodes(g));
		for (INode n : nextNodes) {
			startTime.put(n, 0);
		}

	}

	@Override
	protected void routeNode(INode n, Object arg) {
		if (isReady(n)) {
			startNode(n);
			for (IEdge e : n.getOutgoingEdges()) {
				if (!GraphTools.isExternalEdge(e))
					nextEdges.add(e);
			}
		} else {
			if (!nextNodes.contains(n))
				nextNodes.add(n);
		}
	}

	@Override
	protected void routeEdge(IEdge e, Object arg) {
		if (!nextNodes.contains(getSinkNode(e)))
			nextNodes.add(getSinkNode(e));
	}

	@Override
	protected void computeNodeStep(Object arg) {
		Set<INode> computedNodes = new HashSet<INode>(nextNodes);
		updateTime();
		super.computeNodeStep(arg);
		computedNodes.removeAll(nextNodes);

	}

	@Override
	protected void computeNode(INode n, Object arg) {
		if (isReady(n)) {
			super.computeNode(n, arg);
			if (n instanceof ISubGraphNode && !(n instanceof ICollapsedNode)) {
				ISubGraphNode sn = (ISubGraphNode) n;
				nestedAsap(sn).compute(arg);
			}
		}
	}

	protected AsapAlgorithm nestedAsap(ISubGraphNode sn) {
		return new AsapAlgorithm(sn, behavior, latencyProvider);
	}

	@Override
	protected void computeEdge(IEdge e, Object arg) {
		if (!GraphTools.isExternalEdge(e))
			if (isComputed(e.getSourcePort().getNode()))
				super.computeEdge(e, arg);
	}

	protected boolean isReady(INode n) {
		for (IEdge e : n.getIncomingEdges()) {
			if (!GraphTools.isExternalEdge(e)) {
				if (!isTransmitted(e))
					return false;
			}

		}
		return true;
	}

	protected void startNode(INode n) {
		visitedNodes.add(n);
		startTime.put(n, findStartTime(n));
	}

	private void updateTime() {
		crtTime = nextStartTime();
	}

	/**
	 * Find the start time of a node (max of its predecessors end)
	 * 
	 * @param n
	 * @return
	 */
	protected int findStartTime(INode n) {
		int start = 0;
		for (IEdge in : n.getIncomingEdges()) {
			if (!GraphTools.isExternalEdge(in)) {
				int endInput = endTime(in.getSourcePort().getNode())
						+ latencyProvider.getEdgeLatency(in);
				if (endInput > start) {
					start = endInput;
				}
			}
		}
		return start;
	}

	/**
	 * Next start time is the min of next nodes start
	 * 
	 * @return
	 */
	protected int nextStartTime() {
		if (nextNodes.size() > 0) {
			int next = -1;
			for (INode n : nextNodes) {
				boolean ready = true;
				for (IEdge in : n.getIncomingEdges()) {
					if (!visitedNodes.contains(in.getSourcePort().getNode())) {
						ready = false;
					}
				}
				if (ready) {
					int start = findStartTime(n);
					if (next == -1 || next < start) {
						next = start;
					}
				}
			}
			if (next == -1) {
				return crtTime;
			}
			return next;
		} else
			return crtTime;
	}

	protected boolean isTransmitted(IEdge e) {
		INode s = getSourceNode(e);
		if (isComputed(s)) {
			if (crtTime >= endTime(s) + latencyProvider.getEdgeLatency(e))
				return true;
		}
		return false;
	}

	protected boolean isComputed(INode n) {
		if (visitedNodes.contains(n)) {
			if (crtTime >= endTime(n))
				return true;
		}
		return false;
	}

	protected int endTime(INode n) {

		int t0 = startTime.get(n);
		int d = latencyProvider.getNodeLatency(n);
		return t0 + d;

	}

}
