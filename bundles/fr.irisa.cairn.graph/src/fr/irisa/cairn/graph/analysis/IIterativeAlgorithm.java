package fr.irisa.cairn.graph.analysis;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;

/**
 * Interface for iterative algorithm on an {@link IGraph}. An iteration is
 * composed of two steps: nodeStep and edgeStep. For all steps, behavior of the
 * algorithm specify computed treatment on each step object.
 * 
 * @author antoine
 * 
 */
public interface IIterativeAlgorithm {
	/** Compute the whole algorithm */
	public void compute();

	/** Compute the whole algorithm */
	public void compute(Object arg);

	/** Get the computed behavior in each algorithm step */
	@SuppressWarnings("rawtypes")
	public IBehaviorProvider getBehavior();
}
