package fr.irisa.cairn.graph.analysis;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;

/**
 * {@link AbstractIterativeAlgorithm} with backtrack possibilities. When a node/edge is
 * backtracked, it's added to the next node/edge steps.
 * 
 * @author antoine
 * 
 */
public abstract class AbstractBactrackedAlgorithm extends AbstractIterativeAlgorithm
		implements IBacktrackedAlgorithm {
	@SuppressWarnings("rawtypes") 
	public AbstractBactrackedAlgorithm(IBehaviorProvider behavior) {
		super(behavior);
	}

	/**
	 * Backtrack a node.
	 */
	public void backtrack(INode node) {
		this.nextNodes.add(node);
	}

	/**
	 * Backtrack an edge.
	 */
	public void backtrack(IEdge edge) {
		this.nextEdges.add(edge);
	}

}
