package fr.irisa.cairn.graph.analysis;

import static fr.irisa.cairn.graph.tools.GraphTools.getSinkNode;
import static fr.irisa.cairn.graph.tools.GraphTools.getSinkNodes;
import static fr.irisa.cairn.graph.tools.GraphTools.getSourceNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;
import fr.irisa.cairn.graph.providers.ILatencyProvider;
import fr.irisa.cairn.graph.tools.GraphTools;
@SuppressWarnings({"rawtypes","unchecked"}) 
public class AlapAlgorithm extends AsapAlgorithm {
	
	public AlapAlgorithm(IBehaviorProvider behavior,
			ILatencyProvider latencyProvider) {
		super(behavior, latencyProvider);
	}
	
	public AlapAlgorithm(ILatencyProvider latencyProvider){
		this(null,latencyProvider);
	}

	public void routeNode(INode n, Object arg) {
		if (isReady(n)) {
			startNode(n);
			for (IEdge e : n.getIncomingEdges()) {
				if (!GraphTools.isExternalEdge(e))
					nextEdges.add(e);
			}
		} else {
			if (!nextNodes.contains(n))
				nextNodes.add(n);
		}
	}

	public void initializeFirstStep(IGraph g) {
		if (!g.isDirected())
			throw new IllegalArgumentException("Graph has to be directed.");
		nextNodes.addAll(getSinkNodes(g));
		for (INode n : nextNodes) {
			startTime.put(n, 0);
		}
	}
	
	protected boolean isReady(INode n) {
		for (IEdge e : n.getOutgoingEdges()) {
			if (!GraphTools.isExternalEdge(e))
				if (!isTransmitted(e))
					return false;
		}
		return true;
	}

	protected boolean isTransmitted(IEdge e) {
		INode s = getSinkNode(e);
		if (isComputed(s)) {
			if (crtTime >= endTime(s) + latencyProvider.getEdgeLatency(e))
				return true;
		}
		return false;
	}

	@Override
	protected void computeNodeStep(Object arg) {
		// TODO Auto-generated method stub
		super.computeNodeStep(arg);
	}
	/**
	 * return the ALAP step of a node
	 * NB: crtTime is here the max step
	 */
	public Integer getNodeStep(INode n) {
		if (crtTime > 0)
			return this.crtTime - startTime.get(n) - 1 ;
		else
			return 0;
		}

	protected void computeEdge(IEdge e, Object arg) {
		if (!GraphTools.isExternalEdge(e))
			if (isComputed(e.getSinkPort().getNode()))
				if (behavior != null)
					behavior.computeEdge(e, arg);
	}

	public void routeEdge(IEdge e, Object arg) {
		if (!nextNodes.contains(getSourceNode(e)))
			nextNodes.add(getSourceNode(e));
	}
	
	public List<INode> getOrderedNodes() {
		List<INode> alapNodes = new ArrayList<INode>();
		alapNodes.addAll(startTime.keySet());
		Collections.sort(alapNodes, new NodeStepComparator());
		return alapNodes;
	}

	/**
	 * Find the start time of a node (max of its predecessors end)
	 * 
	 * @param n
	 * @return
	 */
	protected int findStartTime(INode n) {
		int start = 0;
		for (IEdge out : n.getOutgoingEdges()) {
			if (!GraphTools.isExternalEdge(out)) {
				int endInput = endTime(out.getSinkPort().getNode())
						+ latencyProvider.getEdgeLatency(out);
				if (endInput > start) {
					start = endInput;
				}
			}
		}
		return start;
	}

	/**
	 * Next start time is the min of next nodes start
	 * 
	 * @return
	 */
	protected int nextStartTime() {
		if (nextNodes.size() > 0) {
			int next = -1;
			for (INode n : nextNodes) {
				boolean ready = true;
				for (IEdge out : n.getOutgoingEdges()) {
					if (out.getSinkPort()!=null && !visitedNodes.contains(out.getSinkPort().getNode())) {
						ready = false;
					}
				}
				if (ready) {
					int start = findStartTime(n);
					if (next == -1 || next < start) {
						next = start;
					}
				}
			}
			if (next == -1) {
				return crtTime;
			}
			return next;
		} else
			return crtTime;
	}
	
	private class NodeStepComparator implements Comparator<INode> {

		public int compare(INode o1, INode o2) {
			Integer step1 = getNodeStep(o1);
			Integer step2 = getNodeStep(o2);
			return step2.compareTo(step1);
		}

	}
		
}
