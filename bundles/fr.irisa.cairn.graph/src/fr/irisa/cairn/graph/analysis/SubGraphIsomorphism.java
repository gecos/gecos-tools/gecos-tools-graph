package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.internal.BooleanMatrix;
import fr.irisa.cairn.graph.internal.BooleanVector;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;

/**
 * Sub-graph isomorphism using Ullman algorithm
 * (cf. http://doi.acm.org/10.1145/321921.321925).
 * 
 * @author lhours
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"}) 
public class SubGraphIsomorphism 
{

	private IGraph subject;
	private List<IGraph> patterns;
	private List<INode> subjectNodes;
	private List<INode> patternNodes;
	private IEquivalenceProvider eqProvider;
	private boolean only_one; //true if we look for only one permutation of isomorphisme

	public SubGraphIsomorphism(IGraph subject, List<IGraph> patterns,
			IEquivalenceProvider eqProvider) 
	{
		this.subject = subject;
		this.patterns = patterns;
		this.eqProvider = eqProvider;
		only_one = false;
	}

	/**
	 * Recherche de l'isomorphisme
	 * @return liste de sous-graphes mapp�s
	 */
	public List<SubgraphMapping> compute() 
	{
		List<SubgraphMapping> res = new ArrayList<SubgraphMapping>();
		for (IGraph pattern : patterns) 
		{
			List<BooleanMatrix> solutions = match(pattern, subject);
			for (BooleanMatrix m : solutions) 
				res.add(createMapping(m, pattern));
		}
		return res;
	}

	/**
	 * Set if we look for only one SubgraphIsomorphisme
	 * @param only_one : boolean
	 */
	public void setOnlyOne(boolean only_one)
	{
		this.only_one = only_one;
	}

	private SubgraphMapping createMapping(BooleanMatrix matrix, IGraph pattern) 
	{

		SubgraphMapping res = new SubgraphMapping();
		for (int i = 0; i < subject.getNodes().size(); ++i) 
		{
			for (int j = 0; j < pattern.getNodes().size(); ++j) 
			{
				if (matrix.get(j, i))
					res.put(subjectNodes.get(i), patternNodes.get(j));
			}
		}
		return res;
	}

	private List<BooleanMatrix> match(IGraph pattern, IGraph subject) 
	{
		subjectNodes = new ArrayList<INode>(subject.getNodes());
		patternNodes = new ArrayList<INode>(pattern.getNodes());
		List<BooleanMatrix> solutions = new ArrayList<BooleanMatrix>();

		Stack<BooleanMatrix> stack = new Stack<BooleanMatrix>();
		BooleanMatrix M = new BooleanMatrix(patternNodes.size(), subjectNodes
				.size());
		fillMatrix(M, only_one);
		if (!refine(M))
			return solutions;

		int d = 0;
		BooleanVector F = new BooleanVector(subject.getNodes().size());
		int[] H = new int[pattern.getNodes().size()];
		H[d] = 0;

		BooleanMatrix Md = M.copy();
		int k = 0;
		boolean backtrack = true;
		boolean util_pile; //booleen indiquant si on utilise la pile (push ou pop)
		while (true) 
		{
			util_pile = true;
			int foundK = -1;
			while (k < M.columnCount()) 
			{
				if (M.get(d, k) && !F.get(k)) 
				{
					foundK = k;
					break;
				}
				++k;
			}
			if (foundK >= 0) 
			{
				// make row d contain only one 1
				for (int j = 0; j < M.columnCount(); ++j) 
				{
					if (j != k)
						M.unset(d, j);
				}
				if (refine(M)) 
				{
					if (d == (pattern.getNodes().size() - 1)) 
					{
						solutions.add(M.copy());
						if(!only_one)
						{
							//look for a new solution
							M = Md.copy();
							++k;
							util_pile = false;
						}
						else
						{
							/* We found that two graphs are isomorphic. 
							 * We do not seek other possible permutations */
							return solutions;
						}
					} 
					else 
					{
						backtrack = false; //We continue to build the solution
					}
				}
				else
				{
					//Bad choice for k, we try with a higher k
					backtrack = false;
					util_pile = false;
					M = Md.copy();
					++k;
				}
			} 
			else
				backtrack = true;
			if(util_pile)
			{
				if(backtrack)
				{
					--d;
					if (d < 0)
						return solutions;
					// pop context
					Md = stack.pop();
					M = Md.copy();
					k = H[d];
					F.unset(k);
					++k;
				}
				else
				{
					// push context
					H[d] = k;
					F.set(k);
					++d;
					stack.push(Md);
					Md = M.copy();
					k = 0;
				}
			}			
		}
	}

	private boolean refine(BooleanMatrix m) 
	{
		boolean changed;
		int nb1;
		do 
		{
			changed = false;
			for (int i = 0; i < m.rowCount(); ++i) 
			{
				nb1 = 0; //comptabilise le nb de 1 dans la ligne i
				for (int j = 0; j < m.columnCount(); ++j) 
				{
					if (m.get(i, j)) 
					{
						nb1++;
						INode inode = patternNodes.get(i);
						INode jnode = subjectNodes.get(j);
						if (!testAdjacency(m, inode, jnode)) 
						{
							m.unset(i, j);
							nb1--;
							changed = true;
						}
					}
				}
				//on verifie qu'il reste au moins un 1 dans la ligne i
				if(nb1 < 1)
					return false;				
			}
		} while (changed);
		return true;
	}

	/**
	 * if pattern_i maps to subject_j verify that their adjacent nodes also
	 * maps.
	 * 
	 * @param m
	 * @param i
	 *            pattern node i
	 * @param j
	 *            subject node j
	 * @return
	 */
	protected boolean testAdjacency(BooleanMatrix m, INode inode, INode jnode) 
	{
		int x = 0;
		for (INode pnode : patternNodes) 
		{
			if (pnode.isSuccessorTo(inode)) 
			{
				boolean foundOne = false;
				int y = 0;
				for (INode snode : subjectNodes) 
				{
					if (m.get(x, y) && snode.isSuccessorTo(jnode)) 
					{
						foundOne = true;
						break;
					}
					y++;
				}
				if (!foundOne)
					return false;
			}
			x++;
		}
		return true;
	}

	private void fillMatrix(BooleanMatrix m, boolean isopur)
	{
		int i = 0;
		for (INode pnode : patternNodes)
		{
			int j = 0;
			for (INode snode : subjectNodes)
			{
				if (areCompatible(pnode, snode, isopur))
					m.set(i, j);
				++j;
			}
			++i;
		}
	}

	/**
	 * the compatibility of two nodes is based on node degrees and the provided
	 * equivalence
	 * @param pnode : node du pattern
	 * @param snode : node du graphe source
	 * @return true if pnode and snode are compatible.
	 */
	protected boolean areCompatible(INode pnode, INode snode, boolean isopur)
	{
		return eqProvider.nodesEquivalence(pnode, snode);
	}
}
