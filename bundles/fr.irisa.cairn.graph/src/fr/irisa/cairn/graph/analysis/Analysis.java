package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.alg.ChromaticNumber;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.alg.VertexCovers;
import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.TraversalListener;
import org.jgrapht.event.VertexTraversalEvent;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.traverse.TopologicalOrderIterator;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

@Singleton
@SuppressWarnings({"rawtypes","unchecked"}) 
public class Analysis<N extends INode, E extends IEdge> {

	@Inject
	public Analysis() {
	}
	

	public Iterator<N> topologicalIterator(IGraph g) {
		TopologicalOrderIterator<INode, IEdge> iterator = new TopologicalOrderIterator<INode, IEdge>(
				getDirectedGraph(g));
		Iterator<INode> i = iterator;

		return (Iterator<N>) i;
	}


	public Map<N, Integer> topologicalSteps(IGraph g) {
		TopologicalOrderIterator<INode, IEdge> iterator = new TopologicalOrderIterator<INode, IEdge>(
				getDirectedGraph(g));
		TopologicalStepTraversalListener listener = new TopologicalStepTraversalListener();
		iterator.addTraversalListener(listener);

		Map<INode, Integer> map = listener.steps;
		while (iterator.hasNext())
			iterator.next();
		return (Map<N, Integer>) map;
	}

	private static class TopologicalStepTraversalListener implements
			TraversalListener<INode, IEdge> {
		private Map<INode, Integer> steps = new Hashtable<INode, Integer>();

		public void connectedComponentFinished(
				ConnectedComponentTraversalEvent arg0) {
			// TODO Auto-generated method stub

		}

		public void connectedComponentStarted(
				ConnectedComponentTraversalEvent arg0) {
			// TODO Auto-generated method stub

		}

		public void edgeTraversed(EdgeTraversalEvent<INode, IEdge> arg0) {
			// TODO Auto-generated method stub

		}

		public void vertexFinished(VertexTraversalEvent<INode> arg0) {

		}

		private int step(INode n) {
			int max = 0;
			for (INode pred : n.getPredecessors()) {
				int pstep = steps.get(pred);
				max = max < pstep ? pstep : max;
			}
			return max + 1;
		}

		public void vertexTraversed(VertexTraversalEvent<INode> arg0) {
			INode n = arg0.getVertex();
			int step = step(n);
			steps.put(n, step);

		}

	}


	public Iterator<N> depthFirstIterator(IGraph g) {
		DepthFirstIterator<INode, IEdge> iterator = new DepthFirstIterator<INode, IEdge>(
				getGraph(g));
		Iterator<INode> i = iterator;
		return (Iterator<N>) i;
	}


	public Iterator<N> beadthFirstIterator(IGraph g) {
		BreadthFirstIterator<INode, IEdge> iterator = new BreadthFirstIterator<INode, IEdge>(
				getGraph(g));
		Iterator<INode> i = iterator;
		return (Iterator<N>) i;
	}


	public List<N> topologicalSort(IGraph g) {
		TopologicalOrderIterator<INode, IEdge> iterator = new TopologicalOrderIterator<INode, IEdge>(
				getDirectedGraph(g));
		List<N> sortedList = new ArrayList<N>();

		while (iterator.hasNext()) {
			sortedList.add((N) iterator.next());
		}
		return sortedList;
	}
	
	/**
	 * Topological sort in a ALAP order
	 * 
	 * @param g
	 * @return
	 */
	public List<INode> topologicalAlapSort(IGraph g) {
		ReverseTopologicalSort sort = new ReverseTopologicalSort();
		sort.initializeFirstStep(g);
		sort.compute();
		List<INode> alap = sort.getSortedList();
		Collections.reverse(alap);
		return alap;
	}

	/**
	 * Find all maximal cliques of a graph using Bron Kerbosch algorithm.
	 * 
	 * @param g
	 * @return
	 */

	public Collection<Set<N>> findMaxCliques(IGraph g) {
		Graph<INode, IEdge> jgraph = getGraph(g);
		BronKerboschCliqueFinder<INode, IEdge> finder = new BronKerboschCliqueFinder<INode, IEdge>(
				jgraph);

		Collection<Set<INode>> cliques = finder.getAllMaximalCliques();
		Collection<Set<N>> t = new HashSet<Set<N>>();
		for (Set<INode> c : cliques) {
			t.add((Set<N>) c);
		}
		return t;
	}

	/**
	 * Find the maximal weighted clique.
	 * 
	 * @param g
	 * @return
	 */

	public Set<N> findMaxWeightedClique(IGraph g,
			ILatencyProvider weightsProvider) {
		MaxWeightedClique clique = new MaxWeightedClique(weightsProvider);
		return (Set<N>) clique.solve(g);
	}

	/**
	 * Test if directed graph has any cycle.
	 * 
	 * @param g
	 * @return
	 */
	public boolean hasCycle(IGraph g) {
		if (!g.isDirected()) {
			throw new UnsupportedOperationException(
					"Graph has to be directed for testing cycle");
		} else {
			try {
				DirectedGraph<INode, IEdge> jgraph = (DirectedGraph<INode, IEdge>) getDirectedGraph(g);
				CycleDetector<INode, IEdge> detector = new CycleDetector<INode, IEdge>(
						jgraph);
				return detector.detectCycles();
			} catch (IllegalArgumentException e) {
				return true;
			}
		}
	}
	
	/**
	 * Find a cycle in a directed graph and return it
	 * 
	 * @param g
	 * @return cycle
	 */
	public Set<INode> findCycle(IGraph g) {
		if (!hasCycle(g))
			return null;
		else {
			DirectedGraph<INode, IEdge> jgraph = (DirectedGraph<INode, IEdge>) getDirectedGraph(g);
			CycleDetector<INode, IEdge> detector = new CycleDetector<INode, IEdge>(jgraph);
			return detector.findCycles();
		}
	}
	
	
	/**
	 * Find the shortest path between two nodes using Dijkstra algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @return
	 */

	public List<E> dijkstraShortestPath(INode from, INode to) {
		List<IEdge> path = DijkstraShortestPath.findPathBetween(
				getGraph(from.getGraph()), from, to);
		return (List<E>) path;
	}

	/**
	 * Find the longest path between two nodes using Ford algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @param latencyProvider
	 *            gives the weight of each edge
	 * @return int the value of the longest path
	 */
	public int fordLongestPath(N from, N to, ILatencyProvider latencyProvider) {
		FordAlgorithm fordAlgorithm = new FordAlgorithm(from.getGraph(),
				latencyProvider, from, to);
		return fordAlgorithm.getLongestPathValue();
	}

	/**
	 * Find the shortest path between two nodes using Ford algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @param latencyProvider
	 *            gives the weight of each edge
	 * @return int the value of the shortest path
	 */
	public int fordShortestPath(N from, N to, ILatencyProvider latencyProvider) {
		FordAlgorithm fordAlgorithm = new FordAlgorithm(from.getGraph(),
				latencyProvider, from, to);
		return fordAlgorithm.getShortestPathValue();
	}

	/**
	 * Find all hamiltonian paths in a graph (NP-complete).
	 * 
	 * @param g
	 * @return
	 */

	public Set<List<E>> findAllHamiltonianPaths(IGraph g) {
		Set<List<E>> res = new HashSet<List<E>>();
		Set<HamiltonianPath> paths = HamiltonianPath.getAllHamiltonianPaths(g);
		for (HamiltonianPath p : paths) {
			res.add((List<E>) p.getEdges());
		}
		return res;
	}

	public boolean isConnected(IGraph g) {
		Graph<INode, IEdge> graph;

		ConnectivityInspector<INode, IEdge> inspector;
		if (g.isDirected()) {
			graph = getDirectedGraph(g);
			inspector = new ConnectivityInspector<INode, IEdge>(
					(DirectedGraph<INode, IEdge>) graph);
		} else {
			graph = getGraph(g);
			inspector = new ConnectivityInspector<INode, IEdge>(
					(UndirectedGraph<INode, IEdge>) graph);
		}
		return inspector.isGraphConnected();

	}

	public boolean isThereAPath(N from, N to) {
		Graph<INode, IEdge> graph;
		if (from.getGraph() != to.getGraph()) {
			return false;
		}
		ConnectivityInspector<INode, IEdge> inspector;
		if (from.getGraph().isDirected()) {
			graph = getDirectedGraph(from.getGraph());
			inspector = new ConnectivityInspector<INode, IEdge>(
					(DirectedGraph<INode, IEdge>) graph);
		} else {
			graph = getGraph(from.getGraph());
			inspector = new ConnectivityInspector<INode, IEdge>(
					(UndirectedGraph<INode, IEdge>) graph);
		}
		return inspector.pathExists(from, to);

	}


	public List<Set<N>> findConnectedSets(IGraph g) {
		Graph<INode, IEdge> graph;

		ConnectivityInspector<N, E> inspector;
		if (g.isDirected()) {
			graph = getDirectedGraph(g);
			inspector = new ConnectivityInspector<N, E>(
					(DirectedGraph<N, E>) graph);
		} else {
			graph = getGraph(g);
			inspector = new ConnectivityInspector<N, E>(
					(UndirectedGraph<N, E>) graph);
		}
		return inspector.connectedSets();

	}

	public boolean isStronglyConnected(IGraph g) {
		Graph<INode, IEdge> graph;

		StrongConnectivityInspector<INode, IEdge> inspector;
		if (g.isDirected()) {
			graph = getDirectedGraph(g);
			inspector = new StrongConnectivityInspector<INode, IEdge>(
					(DirectedGraph<INode, IEdge>) graph);
		} else {
			throw new IllegalArgumentException("Graph isn't directed");
		}
		return inspector.isStronglyConnected();

	}


	public List<Set<N>> findStronglyConnectedSets(IGraph g) {
		Graph<INode, IEdge> graph;

		StrongConnectivityInspector<N, E> inspector;
		if (g.isDirected()) {
			graph = getDirectedGraph(g);
			inspector = new StrongConnectivityInspector<N, E>(
					(DirectedGraph<N, E>) graph);
		} else {
			throw new IllegalArgumentException("Graph isn't directed");
		}
		return inspector.stronglyConnectedSets();

	}

	/**
	 * Allows the chromatic number of a graph to be calculated. This is the
	 * minimal number of colors needed to color each vertex such that no two
	 * adjacent vertices share the same color. This algorithm will not find the
	 * true chromatic number, since this is an NP-complete problem. So, a greedy
	 * algorithm will find an approximate chromatic number. (JGraphT algorithm)
	 * 
	 * @param g
	 * @return
	 */
	public int findGreedyChromaticNumber(IGraph g) {
		 
		return ChromaticNumber.findGreedyChromaticNumber(getGraph(g));
	}

	/**
	 * Algorithms to find a vertex cover for a graph. A vertex cover is a set of
	 * vertices that touches all the edges in the graph. The graph's vertex set
	 * is a trivial cover. However, a minimal vertex set (or at least an
	 * approximation for it) is usually desired. Finding a true minimal vertex
	 * cover is an NP-Complete problem. For more on the vertex cover problem,
	 * see http://mathworld.wolfram.com/VertexCover.html. (JGraphT algorithm)
	 * 
	 * @param g
	 * @param approximation
	 * @return
	 */

	public Set<N> findMinimalVertexCover(IGraph g, boolean approximation) {
		if (approximation) {
			return (Set<N>) VertexCovers.find2ApproximationCover(getGraph(g));
		}

		return (Set<N>) VertexCovers.find2ApproximationCover(getGraph(g));

	}

	/**
	 * Build Jgrapht graph from an {@link IGraph}
	 * 
	 * @param g
	 * @return
	 */
	private static SimpleGraph<INode, IEdge> getGraph(IGraph g) {

		SimpleGraph<INode, IEdge> graph = new SimpleGraph<INode, IEdge>(
				IEdge.class);

		for (INode n : g.getNodes()) {
			graph.addVertex(n);
		}
		for (IEdge e : g.getEdges()) {
			graph.addEdge(e.getSourcePort().getNode(), e.getSinkPort()
					.getNode(), e);
		}
		
		return graph;
	}

	/**
	 * Build Jgrapht directed graph from an {@link IGraph}
	 * 
	 * @param g
	 * @return
	 */
	private static DirectedGraph<INode, IEdge> getDirectedGraph(IGraph g) {

		DirectedGraph<INode, IEdge> graph;
		if (g.isDirected()) {
			graph = new DefaultDirectedGraph<INode, IEdge>(IEdge.class);
		} else {
			throw new IllegalArgumentException("Graph isn't directed");
		}
		for (INode n : g.getNodes()) {
			graph.addVertex(n);
		}
		for (IEdge e : g.getEdges()) {
			if (e.getSinkPort() != null && e.getSourcePort() != null) {
				if (e.getSinkPort().getNode().getGraph() == e.getSourcePort()
						.getNode().getGraph()) {
					INode sourceNode = e.getSourcePort().getNode();
					INode sinkNode = e.getSinkPort().getNode();
					graph.addEdge(sourceNode, sinkNode, e);
				}
			}
		}
		return graph;
	}
}
