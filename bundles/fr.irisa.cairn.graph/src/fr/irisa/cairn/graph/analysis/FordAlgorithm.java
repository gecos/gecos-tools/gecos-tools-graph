/*
 * File : FordAlgorithm.java
 * Created on Sep 24, 2008
 * Project : fr.irisa.cairn.graph
 */
package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

/*
 * package : fr.irisa.cairn.graph.analysis
 */
/**
 * Implementation of the Ford algorithm for graphs that implements the IGraph
 * interface. The Ford algorithm finds the shortest path and the longest path in
 * a given graph, from a given from node to a given to node.
 * 
 * @author Kevin Martin
 */
@SuppressWarnings({"rawtypes","unchecked"}) 
public class FordAlgorithm {

	private IGraph graph;
	private ILatencyProvider latencyProvider;
	private INode fromNode;
	private INode toNode;
	private Map<INode, Integer> nodeValue = new HashMap<INode, Integer>();
	private Map<IEdge, Integer> edgeValue = new HashMap<IEdge, Integer>();
	
	private List<INode> longestPathNodes = new ArrayList<INode>();
	private List<INode> shortestPathNodes = new ArrayList<INode>();
	private boolean longestPathComputed = false;
	private boolean shortestPathComputed = false;

	private int longestPathValue;
	private int shortestPathValue;
	private Map<INode, IEdge> longestPathMap = new HashMap<INode, IEdge>();
	private Map<INode, IEdge> shortestPathMap = new HashMap<INode, IEdge>();

	/**
	 * 
	 * @param graph
	 *            the graph
	 * @param latencyProvider
	 *            the weight of each edges
	 * @param from
	 *            the first node of the path
	 * @param to
	 *            the last node of the path
	 */
	public FordAlgorithm(IGraph graph, ILatencyProvider latencyProvider,
			INode from, INode to) {
		this.graph = graph;
		this.latencyProvider = latencyProvider;
		this.fromNode = from;
		this.toNode = to;
	}

	/**
	 * Finds the longest path between the two nodes. 1) Initialization 2) Find a
	 * longer path while it is possible.
	 * 
	 * void
	 */
	public void findLongestPath() {
		initialize(0);

		boolean change = true;
		while (change) {
			change = setMaximum();
		}
		//buildLongestPath();
		this.longestPathValue = nodeValue.get(toNode);
		this.longestPathComputed = true;
	}

//	/**
//	 * We don't put the fromNode and toNode in the longest path as they
//	 * basically belong to the path...
//	 * XXX : buggy
//	 * void
//	 */
//	private void buildLongestPath() {
//
//		INode node = longestPathMap.get(toNode).getSourcePort().getNode();
//
//		while (node != fromNode) {
//			longestPathNodes.add(node);
//			node = longestPathMap.get(node).getSourcePort().getNode();
//		}
//	}

	/**
	 * For each edge, tries to find a longer path than the existing ones.
	 * 
	 * @return boolean true if a longer path is found.
	 */
	private boolean setMaximum() {
		boolean change = false;

		for (IEdge edge : graph.getEdges()) {
			INode edgeFromNode = edge.getSourcePort().getNode();
			INode edgeToNode = edge.getSinkPort().getNode();
			if (nodeValue.get(edgeToNode) - nodeValue.get(edgeFromNode) < edgeValue
					.get(edge)) {

				nodeValue.put(edgeToNode, nodeValue.get(edgeFromNode)
						+ edgeValue.get(edge));

				longestPathMap.put(edgeToNode, edge);
				change = true;
			}
		}
		return change;
	}

	/**
	 * Finds the shortest path between the two nodes. 1) Initialization 2) Find
	 * a shorter path while it is possible.
	 * 
	 * void
	 */
	public void findShortestPath() {
		initialize(Integer.MAX_VALUE);

		boolean change = true;
		while (change) {
			change = setMinimum();
		}
		buildShortestPath();
		this.shortestPathValue = nodeValue.get(toNode);
		this.shortestPathComputed = true;
	}

	/**
	 * We don't put the fromNode and toNode in the list as they
	 * basically belong to the path...
	 * XXX : buggy ?
	 * void
	 */
	private void buildShortestPath() {
		INode node = shortestPathMap.get(toNode).getSourcePort().getNode();

		while (node != fromNode) {
			shortestPathNodes.add(node);
			node = shortestPathMap.get(node).getSourcePort().getNode();
		}
	}

	/**
	 * For each edge, tries to find a shorter path than the existing ones.
	 * 
	 * @return boolean true if a shorter path is found.
	 */
	private boolean setMinimum() {
		boolean change = false;

		for (IEdge edge : graph.getEdges()) {
			INode edgeFromNode = edge.getSourcePort().getNode();
			INode edgeToNode = edge.getSinkPort().getNode();
			if (nodeValue.get(edgeToNode) - nodeValue.get(edgeFromNode) > edgeValue
					.get(edge)) {
				nodeValue.put(edgeToNode, nodeValue.get(edgeFromNode)
						+ edgeValue.get(edge));
				
				shortestPathMap.put(edgeToNode, edge);
				change = true;
			}
		}
		return change;
	}

	/**
	 * Initializes the values of each node of the graph. Typically, 0 to find
	 * the longest path, Integer.MAXVALUE to find the shortest path.
	 * 
	 * @param i
	 *            the value to set
	 */
	private void initialize(int i) {
		// need to clear the hashmap before 
		if (longestPathComputed || shortestPathComputed) {
			nodeValue.clear();
			edgeValue.clear();
		}
		for (INode node : graph.getNodes()) {
			if (node != fromNode) {
				nodeValue.put(node, i);
			}
		}
		// the value of the from node is always 0
		nodeValue.put(fromNode, 0);

		for (IEdge edge : graph.getEdges()) {
			edgeValue.put(edge, latencyProvider.getEdgeLatency(edge));
		}
	}

	/**
	 * Finds the longest path between the from node and destination node and
	 * gives the value of this longest path
	 * 
	 * @return int the value of the longest path
	 */
	public int getLongestPathValue() {
		if (!longestPathComputed) {
			findLongestPath();
		}
		return this.longestPathValue;
	}

	/**
	 * 
	 * @return int
	 */
	public int getShortestPathValue() {
		if (!shortestPathComputed) {
			findShortestPath();
		}
		return this.shortestPathValue;
	}

	/**
	 * We don't put the fromNode and toNode in the longest path as they
	 * basically belong to the path...
	 * 
	 * @return List<INode> the list of nodes composing the longest path
	 */
	public List<INode> getLongestPathNodes() {
		if (!longestPathComputed) {
			findLongestPath();
		}
		return this.longestPathNodes;
	}
	
	/**
	 * We don't put the fromNode and toNode in the shortest path as they
	 * basically belong to the path...
	 * 
	 * @return
	 * List<INode> the list of nodes composing the shortest path
	 */
	public List<INode> getShortestPathNodes() {
		if (!shortestPathComputed) {
			findShortestPath();
		}
		return this.shortestPathNodes;
	}

}
