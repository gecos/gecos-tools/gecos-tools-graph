package fr.irisa.cairn.graph.analysis;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;

public class DepthFirstSearchBT extends DepthFirstSearch implements
		IBacktrackedAlgorithm {

	@SuppressWarnings("rawtypes")
	public DepthFirstSearchBT(IBehaviorProvider behavior) {
		super(behavior);
	}

	public void backtrack(INode node) {
		nextNodes.add(node);
		visitedNodes.remove(node);
	}

	public void backtrack(IEdge edge) {
		nextEdges.add(edge);
		visitedEdges.remove(edge);
	}

}
