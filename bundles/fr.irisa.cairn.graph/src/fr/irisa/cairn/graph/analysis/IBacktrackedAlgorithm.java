package fr.irisa.cairn.graph.analysis;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;

/**
 * Interface for algorithms with backtracks.
 * 
 * @author antoine
 * 
 */
public interface IBacktrackedAlgorithm extends IIterativeAlgorithm{
	/**
	 * Backtrack the algorithm: add a node to the next node step.
	 * 
	 * @param node
	 */
	public void backtrack(INode node);

	/**
	 * Backtrack the algorithm: add an edge to the next edge step.
	 * 
	 * @param edge
	 */
	public void backtrack(IEdge edge);

}
