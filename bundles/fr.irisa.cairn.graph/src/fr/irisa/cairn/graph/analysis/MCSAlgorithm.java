package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;

/**
 * Maximum Common Subgraph algorithm
 * 
 * from the article:
 * "W. Henry Suters, Faisal N. Abu-Khzam, Yun Zhang, Christopher T. Symons,
 *  Nagiza F. Samatova and Michael A. Langston,
 *  A new approach and faster exact methods for the maximum common subgraph problem"
 *  
 */
@SuppressWarnings({"rawtypes","unchecked"}) 
public class MCSAlgorithm {
	
	private static class CNode {
		int i;
		int j;
		INode ni;
		INode nj;
		Set<CNode> neighbours = new HashSet<CNode>();
		
		public CNode(int i, int j, INode ni, INode nj) {
			this.i = i;
			this.j = j;
			this.ni = ni;
			this.nj = nj;
		}
		
		@Override
		public int hashCode() {
			return (i * 3625) ^ (j * 217);
		}
		
		@Override
		public boolean equals(Object obj) {
			return ((CNode)obj).i == i && ((CNode)obj).j == j;
		}
		
		@Override
		public String toString() {
			return "(" + ni + "," + nj + ")";
		}
	}
	
	private IGraph g1;
	private IGraph g2;
	private IEquivalenceProvider eqProvider;
	
	private Set<CNode> minimumCover;
	private Set<CNode> currentCover;
	private boolean[] excludeColumns;
	private CNode[][] matrix;
	private List<CNode> nodes;
	private int g1Size;
	private int g2Size;
	
	public MCSAlgorithm(IGraph g1, IGraph g2, IEquivalenceProvider eqProvider) {
		this.g1 = g1;
		this.g2 = g2;
		this.eqProvider = eqProvider;
	}
	
	public SubgraphMapping compute() {
		boolean reverse = false;
		// g1 is the smallest
		if (g1.getNodes().size() > g2.getNodes().size()) {
			IGraph t = g1;
			g1 = g2;
			g2 = t;
			reverse = true;
		}
		buildAssociationGraph();
		minimumCover = new HashSet<CNode>(nodes);
		currentCover = new HashSet<CNode>(nodes);
		excludeColumns = new boolean[g2.getNodes().size()];
		branch(0);
		//branch_2();
		SubgraphMapping mapping = new SubgraphMapping();
		for (CNode node : nodes) {
			if (! minimumCover.contains(node)) {
				if (reverse)
					mapping.put(node.nj, node.ni);
				else
					mapping.put(node.ni, node.nj);
			}
		}
		return mapping;
	}

	private void buildAssociationGraph() {
		List<INode> g1Nodes = new ArrayList<INode>(g1.getNodes());
		List<INode> g2Nodes = new ArrayList<INode>(g2.getNodes());
		g1Size = g1Nodes.size();
		g2Size = g2Nodes.size();
		matrix = new CNode[g2Size][g1Size];
		nodes = new ArrayList<CNode>();
		for (int i = 0; i < matrix.length; ++i) {
			for (int j = 0; j < matrix[i].length; ++j) {
				if (eqProvider.nodesEquivalence(g2Nodes.get(i), g1Nodes.get(j))) {
					matrix[i][j] = new CNode(i, j, g2Nodes.get(i), g1Nodes.get(j));
					nodes.add(matrix[i][j]);
				}
			}
		}
		for (CNode n1 : nodes) {
			for (CNode n2 : nodes) {
				// complement of association graph
				if ( ! (n1.ni.isConnectedTo(n2.ni) && n1.nj.isConnectedTo(n2.nj)
						|| !n1.ni.isConnectedTo(n2.ni) && !n1.nj.isConnectedTo(n2.nj))) {
					n1.neighbours.add(n2);
					n2.neighbours.add(n1);
				}
			}
		}
		System.out.println("MCS: " + matrix.length + "x" + matrix[0].length);
		//outputGraph();
	}

//	private void outputGraph() {
//		try {
//			PrintStream str = new PrintStream(new FileOutputStream("cover.dot"));
//			str.println("graph G {");
//			for (int i = 0; i < matrix.length; ++i) {
//				str.println("{ rank=same;");
//				for (int j = 0; j < matrix[i].length; ++j) {
//					CNode node = matrix[i][j];
//					if (node == null)
//						continue;
//					str.println("n" + node.hashCode() + "[label=\"("+node.i+","+node.j+")\"];");
//				}
//				str.println("}");
//			}
//			for (int i = 0; i < matrix.length; ++i) {
//				for (int j = 0; j < matrix[i].length; ++j) {
//					CNode node = matrix[i][j];
//					if (node == null)
//						continue;
//					for (CNode nei : node.neighbours) {
//						if (nei.i > node.i || nei.i == node.i && nei.j > node.j) {
//							str.println("n" + node.hashCode() + " -- n" + nei.hashCode() + ";");
//						}
//					}
//				}
//			}
//			str.println("}");
//			str.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Recursive version of the algorithm
	 * 
	 * @param i
	 */
	private void branch(int i) {
		if (i >= g2Size) {
			if (currentCover.size() < minimumCover.size())
				minimumCover = new HashSet<CNode>(currentCover);
		} else {
			for (int j = 0; j < g1Size; ++j) {
				if (excludeColumns[j])
					continue;
				if (matrix[i][j] == null)
					continue;
				if (hasAllNeighbourInCurrentCover(i, j, currentCover)) {
					excludeColumns[j] = true;
					removeFromCurrentCover(i, j);
					
					branch(i+1);
					
					excludeColumns[j] = false;
					addToCurrentCover(i, j);
				}
			}
			branch(i+1);
		}
	}
	
	private static class Context {

		private int i;
		private int j;
		private CNode exclude;

		public Context(int i, int j) {
			this.i = i;
			this.j = j;
		}

		public Context(int i, int j, CNode exclude) {
			this.exclude = exclude;
			this.i = i;
			this.j = j;
		}
		
	}
	
	/**
	 * Non recursive version of the algorithm
	 * XXX to be strongly verified
	 * 
	 */
	void branch_2() {
		Stack<Context> contexts = new Stack<Context>();
		contexts.push(new Context(0, 0));
		while (contexts.size() > 0) {
			Context ctxt = contexts.pop();
			if (ctxt.i >= g2Size) {
				if (currentCover.size() < minimumCover.size())
					minimumCover = new HashSet<CNode>(currentCover);
				if (ctxt.exclude != null) {
					excludeColumns[ctxt.exclude.j] = false;
					currentCover.add(ctxt.exclude);
				}
			} else {
				if (ctxt.j < g1Size) {
					contexts.push(new Context(ctxt.i, ctxt.j+1, null));
					if (excludeColumns[ctxt.j] || matrix[ctxt.i][ctxt.j] == null)
						continue;
					if (hasAllNeighbourInCurrentCover(ctxt.i, ctxt.j, currentCover)) {
						excludeColumns[ctxt.j] = true;
						removeFromCurrentCover(ctxt.i, ctxt.j);
						
						contexts.push(new Context(ctxt.i+1, 0, matrix[ctxt.i][ctxt.j]));
					} 
				} else
					contexts.push(new Context(ctxt.i+1, 0, null));
			}
		}
	}

	private boolean hasAllNeighbourInCurrentCover(int i, int j, Set<CNode> cover) {
		CNode node = matrix[i][j];
		for (CNode n : node.neighbours) {
			if (! cover.contains(n))
				return false;
		}
		return true;
	}
	
	private void removeFromCurrentCover(int i, int j) {
		CNode node = matrix[i][j];
		currentCover.remove(node);
	}
	
	private void addToCurrentCover(int i, int j) {
		CNode node = matrix[i][j];
		currentCover.add(node);
	}

}
