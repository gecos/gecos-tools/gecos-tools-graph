package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.tools.GraphTools;

/**
 * A Hamiltonian path is a path in an (un)directed graph which visits each
 * vertex exactly once.
 * 
 * @author antoine
 * 
 */
public class HamiltonianPath {

	/**
	 * Get all hamiltonian paths in a graph.
	 * 
	 * @param g
	 * @return
	 */
	public static Set<HamiltonianPath> getAllHamiltonianPaths(IGraph g) {
		Set<HamiltonianPath> paths = new HashSet<HamiltonianPath>();
		for (INode n : g.getNodes()) {
			HamiltonianPath path = new HamiltonianPath(n);
			for (HamiltonianPath completePath : allExtendedPaths(path)) {
				paths.add(completePath);
			}
		}
		return paths;
	}

	private static Set<HamiltonianPath> allExtendedPaths(HamiltonianPath path) {

		Set<HamiltonianPath> allPaths = new HashSet<HamiltonianPath>();
		for (HamiltonianPath p : path.extend()) {
			if (p.isComplete())
				allPaths.add(p);
			else {
				allPaths.addAll(allExtendedPaths(p));
			}
		}
		return allPaths;
	}

	private List<IEdge> path;
	private List<INode> nodes;

	public HamiltonianPath(INode seed) {

		this.path = new ArrayList<IEdge>();
		this.nodes = new ArrayList<INode>();
		this.nodes.add(seed);
	}

	public List<IEdge> getEdges() {
		return path;
	}

	public List<INode> getNodes() {
		return nodes;
	}

	/**
	 * Test the completeness of the hamiltonian path.
	 * 
	 * @return
	 */
	public boolean isComplete() {
		return seedNode().getGraph().getNodes().size() == nodes.size();
	}

	/**
	 * Test the admissibility of the path for a next extension.
	 * 
	 * @return
	 */
	public boolean isAdmissible() {
		return !nodes.containsAll(lastNode().getSuccessors());
	}

	/**
	 * Extend the path to its admissible neighbors nodes.
	 * 
	 * @return
	 */
	public List<HamiltonianPath> extend() {
		List<HamiltonianPath> childrenPath = new ArrayList<HamiltonianPath>();
		for (IEdge out : lastNode().getOutgoingEdges()) {
			INode neighbor = GraphTools.getOppositeNode(out, lastNode());
			if (!nodes.contains(neighbor)) {
				HamiltonianPath child = new HamiltonianPath(seedNode());
				for (IEdge e : path) {
					child.path.add(e);
				}
				for (INode n : nodes) {
					if (n != seedNode())
						child.nodes.add(n);
				}
				child.path.add(out);
				child.nodes.add(neighbor);
				if (child.isAdmissible() || child.isComplete())
					childrenPath.add(child);
			}
		}
		return childrenPath;
	}

	private INode seedNode() {
		return nodes.get(0);
	}

	private INode lastNode() {
		return nodes.get(nodes.size() - 1);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HamiltonianPath other = (HamiltonianPath) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

}
