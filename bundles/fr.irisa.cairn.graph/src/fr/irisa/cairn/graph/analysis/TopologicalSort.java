package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.List;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;

public class TopologicalSort extends AbstractIterativeAlgorithm {

	public TopologicalSort() {
		super(new TopologicalSortBehavior());
	}
	
	public TopologicalSort(IGraph g) {
		super(new TopologicalSortBehavior());
		initializeFirstStep(g);
	}

	@Override
	public void initializeFirstStep(IGraph g) {
		nextNodes.addAll(g.getNodes());
	}

	@Override
	public void routeNode(INode n, Object arg) {
		TopologicalSortBehavior topoBehavior = (TopologicalSortBehavior) behavior;
		if (!topoBehavior.getSortedList().contains(n)) {
			nextNodes.add(n);
		}
	}

	public List<INode> getSortedList() {
		TopologicalSortBehavior topoBehavior = (TopologicalSortBehavior) behavior;
		return topoBehavior.getSortedList();
	}

	@SuppressWarnings("rawtypes")
	private static class TopologicalSortBehavior implements IBehaviorProvider {

		private List<INode> sortedList;

		public TopologicalSortBehavior() {
			this.sortedList = new ArrayList<INode>();
		}

		public List<INode> getSortedList() {
			return sortedList;
		}

		public void computeEdge(IEdge e, Object arg) {
		}

		public void computeNode(INode n, Object arg) {
			List<? extends INode> preds = n.getPredecessors();
			if (sortedList.containsAll(preds)) {
				sortedList.add(n);
			}

		}
	}
}
