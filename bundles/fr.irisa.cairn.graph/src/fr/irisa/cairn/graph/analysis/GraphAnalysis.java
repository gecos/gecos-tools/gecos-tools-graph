package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.irisa.cairn.experimental.graphMerging.GraphMerger;
import fr.irisa.cairn.experimental.graphMerging.ILibComponentProvider;
import fr.irisa.cairn.experimental.graphRetiming.FEASRes;
import fr.irisa.cairn.experimental.graphRetiming.GraphRetiming;
import fr.irisa.cairn.experimental.graphRetiming.IRetimingProvider;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.implement.providers.LatencyProvider;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

/**
 * Provide some graph theory analysis tools on {@link IGraph}.
 * 
 * @author antoine
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"}) 
public final class GraphAnalysis {
	private final static Analysis<INode, IEdge> ANALYSER = new Analysis<INode, IEdge>();

	public static <N extends INode> Iterator<N> topologicalIterator(IGraph g) {

		return (Iterator<N>) ANALYSER.topologicalIterator(g);
	}

	public static Map<INode, Integer> topologicalSteps(IGraph g) {

		return ANALYSER.topologicalSteps(g);
	}

	public static <N extends INode> Iterator<N> depthFirstIterator(IGraph g) {

		return (Iterator<N>) ANALYSER.depthFirstIterator(g);
	}

	public static <N extends INode> Iterator<N> beadthFirstIterator(IGraph g) {

		return (Iterator<N>) ANALYSER.beadthFirstIterator(g);
	}

	public static <N extends INode> List<N> topologicalSort(IGraph g) {

		return (List<N>) ANALYSER.topologicalSort(g);
	}

	public static <N extends INode> List<N> topologicalAlapSort(IGraph g) {

		return (List<N>) ANALYSER.topologicalSort(g);
	}

	public static <N extends INode> List<N> depthFirstSort(IGraph g) {
		List<N> nodes = new ArrayList<N>();
		Iterator<N> nodeIterator = depthFirstIterator(g);
		while (nodeIterator.hasNext()) {
			nodes.add(nodeIterator.next());
		}
		return nodes;
	}

	public static <N extends INode> List<N> breadthFirstSort(IGraph g) {
		List<N> nodes = new ArrayList<N>();
		Iterator<N> nodeIterator = beadthFirstIterator(g);
		while (nodeIterator.hasNext()) {
			nodes.add(nodeIterator.next());
		}
		return nodes;
	}

	public static int criticalPathLength(IGraph g, ILatencyProvider latency) {
		AsapAlgorithm asap = new AsapAlgorithm(latency);
		asap.initializeFirstStep(g);
		asap.compute();
		return asap.getCriticalPathLength();
	}

	public static <N extends INode> List<N> criticalTopologicalPath(IGraph g) {
		return criticalPath(g, new LatencyProvider());
	}

	public static <N extends INode> List<N> criticalPath(IGraph g,
			ILatencyProvider latency) {
		// XXX: implement a better algorithm
		AsapAlgorithm asap = new AsapAlgorithm(latency);
		asap.initializeFirstStep(g);
		asap.compute();
		List<N> path = new ArrayList<N>();
		N last = (N) asap.getOrderedNodes().get(
				asap.getOrderedNodes().size() - 1);

		path.add(last);
		N previous = previousNodeOnCriticalPath(last, asap);
		while (previous != null) {
			path.add(previous);
			previous = previousNodeOnCriticalPath(previous, asap);
		}

		Collections.reverse(path);
		return path;
	}

	private static <N extends INode> N previousNodeOnCriticalPath(N n,
			AsapAlgorithm asap) {
		int max = -1;
		N maxNode = null;
		for (INode pred : n.getPredecessors()) {
			int step = asap.getNodeStep(pred);
			if (step > max) {
				maxNode = (N) pred;
				max = step;
			}
		}
		return maxNode;
	}

	/**
	 * Find all maximal cliques of a graph using Bron Kerbosch algorithm.
	 * 
	 * @param g
	 * @return
	 */
	public static Collection<Set<INode>> findMaxCliques(IGraph g) {

		return ANALYSER.findMaxCliques(g);
	}

	/**
	 * Find the maximal weighted clique.
	 * 
	 * @param g
	 * @return
	 */
	public static Set<INode> findMaxWeightedClique(IGraph g,
			ILatencyProvider weightsProvider) {

		return ANALYSER.findMaxWeightedClique(g, weightsProvider);
	}

	/**
	 * Test if directed graph has any cycle.
	 * 
	 * @param g
	 * @return
	 */
	public static boolean hasCycle(IGraph g) {

		return ANALYSER.hasCycle(g);
	}

	/**
	 * Find the shortest path between two nodes using Dijkstra algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @return
	 */
	public static <E extends IEdge> List<E> dijkstraShortestPath(INode from,
			INode to) {

		return (List<E>) ANALYSER.dijkstraShortestPath(from, to);
	}

	/**
	 * Find the longest path between two nodes using Ford algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @param latencyProvider
	 *            gives the weight of each edge
	 * @return int the value of the longest path
	 */
	public static int fordLongestPath(INode from, INode to,
			ILatencyProvider latencyProvider) {

		return ANALYSER.fordLongestPath(from, to, latencyProvider);
	}

	/**
	 * Find the shortest path between two nodes using Ford algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @param latencyProvider
	 *            gives the weight of each edge
	 * @return int the value of the shortest path
	 */
	public static int fordShortestPath(INode from, INode to,
			ILatencyProvider latencyProvider) {

		return ANALYSER.fordShortestPath(from, to, latencyProvider);
	}

	/**
	 * Find all hamiltonian paths in a graph (NP-complete).
	 * 
	 * @param g
	 * @return
	 */
	public static Set<List<IEdge>> findAllHamiltonianPaths(IGraph g) {

		return ANALYSER.findAllHamiltonianPaths(g);
	}

	public static <N extends INode> Iterator<N> asapIterator(IGraph g,
			ILatencyProvider provider) {
		AsapAlgorithm asap = new AsapAlgorithm(provider);

		asap.initializeFirstStep(g);
		asap.compute();
		return (Iterator<N>) asap.getOrderedNodes().iterator();
	}

	public static <N extends INode> Iterator<N> alapIterator(IGraph g,
			ILatencyProvider provider) {
		AlapAlgorithm alap = new AlapAlgorithm(provider);

		alap.initializeFirstStep(g);
		alap.compute();
		return (Iterator<N>) alap.getOrderedNodes().iterator();
	}

	public static boolean isConnected(IGraph g) {

		return ANALYSER.isConnected(g);
	}

	public static boolean isThereAPath(INode from, INode to) {
		return ANALYSER.isThereAPath(from, to);

	}

	public static List<Set<INode>> findConnectedSets(IGraph g) {
		return ANALYSER.findConnectedSets(g);
	}

	public static List<Set<INode>> findStronglyConnectedSets(IGraph g) {
		return ANALYSER.findStronglyConnectedSets(g);
	}

	/**
	 * Merge a list of graph following the algorithm of Moreano.
	 * 
	 * @param lgraphs
	 *            : list of graph to merge
	 * @param latency
	 *            : provider for the cost of nodes
	 * @param libcompo
	 *            : provider to define the architecture of graphs to be merged
	 *            (equivalence, commutativity, conflicts, ...)
	 * @return the merged graph
	 */
	public static IGraph graphMerging(List<IGraph> lgraphs,
			ILatencyProvider latency, ILibComponentProvider libcompo) {
		GraphMerger gm = new GraphMerger(lgraphs.get(0), lgraphs.subList(1,
				lgraphs.size()), latency, libcompo);
		IGraph gres = gm.compute();
		return gres;
	}

	/**
	 * Retime a graph following the algorithm of Leiserson and Saxe
	 * 
	 * @param g
	 *            : graph to retime
	 * @param apply
	 *            : true to apply the retiming at g
	 * @param ret_provider
	 *            : provider for latencies of nodes and weights of edges
	 * @return a FEASRes : it contains the clock period, the retiming for each
	 *         nodes, and the new weights of edges.
	 */
	public static FEASRes graphRetiming(IGraph g, boolean apply,
			IRetimingProvider ret_provider) {
		GraphRetiming gr = new GraphRetiming(g, ret_provider);
		FEASRes res = gr.OPT2();
		if (apply)
			gr.applyRetiming(res);
		return res;
	}

	/**
	 * Allows the chromatic number of a graph to be calculated. This is the
	 * minimal number of colors needed to color each vertex such that no two
	 * adjacent vertices share the same color. This algorithm will not find the
	 * true chromatic number, since this is an NP-complete problem. So, a greedy
	 * algorithm will find an approximate chromatic number. (JGraphT algorithm)
	 * 
	 * @param g
	 * @return
	 */
	public static int findGreedyChromaticNumber(IGraph g) {
		return ANALYSER.findGreedyChromaticNumber(g);
	}

	/**
	 * Algorithms to find a vertex cover for a graph. A vertex cover is a set of
	 * vertices that touches all the edges in the graph. The graph's vertex set
	 * is a trivial cover. However, a minimal vertex set (or at least an
	 * approximation for it) is usually desired. Finding a true minimal vertex
	 * cover is an NP-Complete problem. For more on the vertex cover problem,
	 * see http://mathworld.wolfram.com/VertexCover.html. (JGraphT algorithm)
	 * 
	 * @param g
	 * @param approximation
	 *            if false then a greedy algorithm is used
	 * @return
	 */
	public static <N extends INode> Set<N> findMinimalVertexCover(IGraph g,
			boolean approximation) {
		return (Set<N>) ANALYSER.findMinimalVertexCover(g, approximation);

	}

}
