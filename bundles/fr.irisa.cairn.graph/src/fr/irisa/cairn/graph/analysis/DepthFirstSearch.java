package fr.irisa.cairn.graph.analysis;

import java.util.HashSet;
import java.util.Set;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;
import fr.irisa.cairn.graph.tools.GraphTools;
@SuppressWarnings({"rawtypes"}) 
public class DepthFirstSearch extends AbstractIterativeAlgorithm{

	protected Set<INode> visitedNodes;
	protected Set<IEdge> visitedEdges;

	public DepthFirstSearch(IBehaviorProvider behavior) {
		super(behavior);
		this.visitedNodes = new HashSet<INode>();
		this.visitedEdges = new HashSet<IEdge>();
	}

	@Override
	public void initializeFirstStep(IGraph g) throws GraphException {
		if(!g.isDirected())
			throw new IllegalArgumentException("Graph has to be directed.");
		for (INode n : g.getNodes()) {
			if (n.getInDegree() == 0) {
				nextNodes.add(n);
			}
		}
		if(nextNodes.size()==0){
			throw new GraphException("No roots in this graph.");
		}

	}

	@Override
	protected void routeEdge(IEdge e, Object arg) {
		if (!visitedEdges.contains(e)) {
			visitedEdges.add(e);
			nextNodes.add(GraphTools.getSinkNode(e));
		}
	}

	@Override
	protected void routeNode(INode n, Object arg) {
		if (!visitedNodes.contains(n)) {
			visitedNodes.add(n);
			for (IEdge e : n.getOutgoingEdges()) {
				nextEdges.add(e);
			}
		}
	}

}
