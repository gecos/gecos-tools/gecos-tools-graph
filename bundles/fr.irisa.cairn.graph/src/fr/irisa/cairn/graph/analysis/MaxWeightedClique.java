package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.ILatencyProvider;
@SuppressWarnings({"rawtypes","unchecked"}) 
public class MaxWeightedClique {

	private List<INode> nodes;
	private int[] set;
	private int[] clique;

	private int[] rec;
	private int record;
	private int rec_level;
	private ILatencyProvider weights;

	public MaxWeightedClique(ILatencyProvider weightsProvider) {
		this.weights = weightsProvider;
	}

	private void initialize(IGraph graph){
		nodes = new ArrayList<INode>();
		nodes.addAll(graph.getNodes());
		clique = new int[nodes.size()];
		set = new int[nodes.size()];
		rec = new int[nodes.size()];
		record = 0;
	}
		
	public Set<INode> solve(IGraph graph) {
		initialize(graph);
		int[] pos = computeOrder(graph);
	
		int wth = 0;
		
		for (int i = 0; i < nodes.size(); ++i) {
			//System.out.println("compute node number " + i + " ...");
			wth += getWeight(nodes.get(pos[i]));
			sub(graph, i, pos, 0, 0, wth);
			clique[pos[i]] = record;
		}
		int res[] = new int[rec_level];
		System.arraycopy(rec, 0, res, 0, rec_level);
		
		Set<INode> maxclique = new HashSet<INode>();
		for(Integer nid: res){
			maxclique.add(nodes.get(nid));
		}
		return maxclique;
	}

	private int[] computeOrder(IGraph graph) {
		//System.out.println("ComputeOrder...");
		int nwt[] = new int[nodes.size()];
		for (int i = 0; i < nodes.size(); ++i) {
			nwt[i] = 0;
			for (int j = 0; j < nodes.size(); ++j) {
				if (nodes.get(i).isConnectedTo(nodes.get(j)))
					nwt[i] += getWeight(nodes.get(j));
			}
		}
		boolean used[] = new boolean[nodes.size()];
		for (int i = 0; i < used.length; ++i)
			used[i] = false;
		int pos[] = new int[nodes.size()];
		for (int i = nodes.size()- 1; i >= 0; --i) {
			float max_wt = -1;
			int max_nwt = -1;
			int p = 0;
			for (int j = 0; j < nodes.size(); ++j) {
				if (!used[j]
						&& (getWeight(nodes.get(j)) > max_wt
								|| getWeight(nodes.get(j)) == max_wt && nwt[j] > max_nwt)) {
					max_wt = getWeight(nodes.get(j));
					max_nwt = nwt[j];
					p = j;
				}
			}
			pos[i] = p;
			used[p] = true;
			for (int j = 0; j < nodes.size(); ++j)
				if (!used[j] && j != p && nodes.get(p).isConnectedTo(nodes.get(j)))
					nwt[j] -= getWeight(nodes.get(p));
		}
		return pos;
	}

	private void sub(IGraph graph, int ct, int[] table, int level,
			int weight, int l_weight) {
		
		if (ct <= 0) { // 0 or 1 elements left; include these
			if (ct == 0) {
				set[level++] = table[0];
				weight += l_weight;
			}
			if (weight > record) {
				record = weight;
				//System.out.println("MAX : " + record);
				rec_level = level;
				for (int i = 0; i < level; ++i){
					rec[i] = set[i];
					//System.out.println((rec[i]+1) + " ");
				}
			}
			//System.out.println(" fin");
			return;
		}

		for (int i = ct; i >= 0; --i) {
			if (level == 0 && i < ct)
				return;

			int k = table[i];
			if (level > 0 && clique[k] <= (record - weight))
				return; // prune

			set[level] = k;
			int curr_weight = weight + getWeight(nodes.get(k));
			l_weight -= getWeight(nodes.get(k));
			if (l_weight <= (record - curr_weight))
				return; // prune

			int[] newtable = new int[nodes.size()];
			int p1 = 0;
			int p2 = 0;
			int left_weight = 0;
			while (p2 < i) {
				int j = table[p2++];
				if (nodes.get(j).isConnectedTo(nodes.get(k))) {
					newtable[p1++] = j;
					left_weight += getWeight(nodes.get(j));
				}
			}
			if (left_weight <= (record - curr_weight))
				continue;
			sub(graph, p1 - 1, newtable, level + 1, curr_weight, left_weight);
		}
	}
	
	private int getWeight(INode n){
		return weights.getNodeLatency(n);
	}
	
	/**
	 * Get the weight of the last max weighted clique solved.
	 * @return
	 */
	public Integer getMaxWeight(){
		return record;
	}

}
