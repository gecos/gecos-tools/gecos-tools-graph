package fr.irisa.cairn.graph.analysis;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IBehaviorProvider;

/**
 * Abstract class for an iterative graph traverse algorithm. Each iterative step
 * is composed of a node step and an edge step. For each step a behavior (
 * {@link IBehaviorProvider}) is computed on nodes and edges. <br>
 * Functions <i> routeEdge() </i> and <i> routeNode() </i> define how nodes and
 * edges will be added to the next step. Algorithm is executed while end
 * condition is not reached (default is when there isn't any edge or nodes to
 * compute at next step). <br>
 * For this algorithm to work correctly the <b>graph must not be modified
 * </b>during iteration. The results of such modifications are undefined. If an
 * algorithm need to be used on modified graph, please use an
 * {@link AbstractBactrackedAlgorithm} and backtrack modified nodes.
 * 
 * @author antoine
 * 
 */
@SuppressWarnings({"rawtypes","unchecked"}) 
public abstract class AbstractIterativeAlgorithm implements IIterativeAlgorithm {
	/** Behavior (actions on nodes/edges) of the algorithm */
	protected IBehaviorProvider behavior;
	protected LinkedHashSet<INode> nextNodes;
	protected List<IEdge> nextEdges;

	private boolean nodeDeadlock;
	private boolean edgeDeadlock;

	public AbstractIterativeAlgorithm(IBehaviorProvider behavior) {
		this.behavior = behavior;
		this.nextNodes = new LinkedHashSet<INode>();
		this.nextEdges = new ArrayList<IEdge>();
	}

	public IBehaviorProvider getBehavior() {
		return behavior;
	}

	public void compute() {
		compute(null);
	}

	public void compute(Object arg) {
		nodeDeadlock = false;
		edgeDeadlock = false;
		while (needNextStep()) {
			computeNodeStep(arg);
			computeEdgeStep(arg);
			if((nodeDeadlock && edgeDeadlock) && (nextNodes.size()>0 || nextEdges.size()>0)){
				throw new RuntimeException("Deadlock in the iterative algorithm: no changes after an iterative step." );
			}
		}
	}

	/** Stop the algorithm */
	protected void stop() {
		nextEdges = new ArrayList<IEdge>();
		nextNodes = new LinkedHashSet<INode>();
	}

	private void newNodeStep() {
		nextNodes = new LinkedHashSet<INode>();
	}

	/**
	 * Initialize the first steps for a given graph
	 * 
	 * @throws GraphException
	 */
	public abstract void initializeFirstStep(IGraph g) throws GraphException;

	/** Test if a next algorithm step is needed */
	protected boolean needNextStep() {
		if (nextNodesStep().size() > 0 || nextEdgesStep().size() > 0)
			return true;
		else
			return false;
	}

	/** Get nodes of the next step */
	private LinkedHashSet<INode> nextNodesStep() {
		return nextNodes;
	}

	/** Get edges of the next step */
	private List<IEdge> nextEdgesStep() {
		return nextEdges;
	}

	/** Compute the behavior algorithm for next step nodes */
	protected void computeNodeStep(Object arg) {
		LinkedHashSet<INode> nodes = nextNodesStep();
		newNodeStep();
		for (INode n : nodes) {
			computeNode(n, arg);
			routeNode(n, arg);
		}
		if (nextNodes.equals(nodes)) {
			nodeDeadlock = true;
		}
	}


	protected void computeNode(INode n, Object arg) {
		if (behavior != null)
			behavior.computeNode(n, arg);
	}

	/** Compute the behavior algorithm for next step edges */
	protected void computeEdgeStep(Object arg) {
		List<IEdge> edges = nextEdgesStep();
		newEdgeStep();
		for (IEdge e : edges) {
			computeEdge(e, arg);
			routeEdge(e, arg);
		}
		if (nextEdges.equals(edges)) {
			edgeDeadlock = true;
		}
	}

	protected void computeEdge(IEdge e, Object arg) {
		if (behavior != null)
			behavior.computeEdge(e, arg);
	}

	private void newEdgeStep() {
		nextEdges = new ArrayList<IEdge>();
	}

	/** Define iterative route algorithm behavior for an edge */
	protected void routeEdge(IEdge e, Object arg) {

	}

	/** Define iterative route algorithm behavior for a node */
	protected void routeNode(INode n, Object arg) {

	}

}
