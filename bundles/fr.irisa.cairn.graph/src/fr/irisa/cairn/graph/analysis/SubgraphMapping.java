package fr.irisa.cairn.graph.analysis;

import java.util.Set;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.util.BidirectionnalHashMap;

/**
 * Result of subgraph isomorphism. Map nodes of an {@link IGraph} to
 * corresponding nodes in a pattern.
 * 
 * @author lhours
 * 
 */
public class SubgraphMapping implements Cloneable{

	BidirectionnalHashMap<INode, INode> mapping = new BidirectionnalHashMap<INode, INode>();
	BidirectionnalHashMap<IPort, IPort> portsBidirectionnalMap = new BidirectionnalHashMap<IPort, IPort>();

	/**
	 * Add a pattern node identification
	 * 
	 * @param subjectNode
	 *            node in the subject graph
	 * @param patternNode
	 *            pattern node
	 */
	public void put(INode subjectNode, INode patternNode) {
		mapping.put(subjectNode, patternNode);
	}

	/**
	 * Get all identified pattern nodes in the subject graph.
	 * 
	 * @return
	 */
	public Set<INode> subjectNodes() {
		return mapping.keySet();
	}

	/**
	 * Get the pattern node of an identified subject node.
	 * 
	 * @param node
	 * @return
	 */
	public INode getPatternNode(INode node) {
		return mapping.get(node);
	}
	
	public INode getSubjectNode(INode node) {
		return mapping.getKey(node);
	}

	public IPort getPatternPort(IPort p) {
		return portsBidirectionnalMap.getKey(p);
	}

	public IPort getSubjectPort(IPort p) {
		return portsBidirectionnalMap.get(p);
	}

	public void put(IPort patternPort, IPort subjectPort) {
		portsBidirectionnalMap.put(patternPort, subjectPort);
	}
	
	public boolean isMapped(INode subjectNode){
		return mapping.containsKey(subjectNode);
	}
	
	public boolean isMatched(INode patternNode){
		return mapping.containsValue(patternNode);
	}
	
	@Override
	public SubgraphMapping clone(){
		SubgraphMapping m = new SubgraphMapping();
		for(INode n: mapping.keySet()){
			m.mapping.put(n, mapping.get(n));
		}
		for(IPort p: portsBidirectionnalMap.keySet()){
			m.portsBidirectionnalMap.put(p, portsBidirectionnalMap.get(p));
		}
		return m;
	}
	
	public boolean isEquivalent(SubgraphMapping other){
		Set<INode> nodes = other.mapping.keySet();
		Set<INode> otherNodes = other.mapping.keySet();
		if(nodes.size() != otherNodes.size())
			return false;
		for(INode n: otherNodes){
			if(!nodes.contains(n))
				return false;
		}
		for(INode n: nodes){
			if(!otherNodes.contains(n))
				return false;
		}
		return true;
	}
}
