package fr.irisa.cairn.graph.implement.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

/**
 * Default latency provider. All nodes have a latency of one abstract time.
 * 
 * @author antoine
 * 
 * 
 */
public class LatencyProvider<N extends INode,E extends IEdge> implements ILatencyProvider<N,E> {

	public int getEdgeLatency(E e) {
		return 0;
	}

	public int getNodeLatency(N n) {
		return 1;
	}

}
