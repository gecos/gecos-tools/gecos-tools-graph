package fr.irisa.cairn.graph.implement.providers;

import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.providers.IAttributsProvider;

public abstract class AbstractAttributsProvider<N extends INode,E extends IEdge> implements IAttributsProvider<N,E> {
	protected Map<INode, Properties> nodesProperties;
	protected Map<IEdge, Properties> edgesProperties;

	@Inject
	public AbstractAttributsProvider() {
		this.edgesProperties = new Hashtable<IEdge, Properties>();
		this.nodesProperties = new Hashtable<INode, Properties>();
	}

	public Properties getEdgeAttributs(E e) {
		if (!edgesProperties.containsKey(e)) {
			findEdgeAttributs(e);
		}
		return edgesProperties.get(e);
	}

	public Properties getNodeAttributs(N n) {
		if (!nodesProperties.containsKey(n)) {
			findNodeAttributs(n);
		}
		return nodesProperties.get(n);
	}

	protected abstract void findEdgeAttributs(E e);

	protected abstract void findNodeAttributs(N n);

	protected void addEdgeProperty(E e, String key, Object value) {
		addEdgeProperty(e, key, value,false);
	}
	
	protected void addEdgeProperty(E e, String key, Object value, boolean overwrite) {
		Properties properties = edgesProperties.get(e);
		if (properties == null) {
			properties = new Properties();
			edgesProperties.put(e, properties);
		}
		if (overwrite || !properties.containsKey(key))
			properties.put(key, value);
	}

	protected void addNodeProperty(N e, String key, Object value) {
		addNodeProperty(e, key, value,true);
	}
	
	protected void addNodeProperty(N e, String key, Object value, boolean overwrite) {
		Properties properties = nodesProperties.get(e);
		if (properties == null) {
			properties = new Properties();
			nodesProperties.put(e, properties);
		}
		if (overwrite || !properties.containsKey(key))
			properties.put(key, value);
	}

}
