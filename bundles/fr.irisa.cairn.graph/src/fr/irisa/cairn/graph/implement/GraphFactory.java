package fr.irisa.cairn.graph.implement;

import java.util.Comparator;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;

@Singleton
public class GraphFactory implements IGraphFactory {

	public static IEdge connectNodes(final INode source, final INode sink) {
		final IGraphFactory f = new GraphFactory();
		return f.connect(source, sink);
	}

	@Inject
	private Comparator<INode>	nodesComparator;
	@Inject
	private Comparator<IEdge>	edgesComparator;

	@Inject
	private Comparator<IPort>	portsComparator;

	@Inject
	public GraphFactory() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Connect nodes on the first output and a new input port. Ports are created
	 * if necessary. Edge is added to the graph.
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	@Override
	public IEdge connect(final INode source, final INode sink) {
		if (source.getOutputPorts().size() == 0) {
			source.addOutputPort(this.createPort());
		}

		final IPort pdest = this.createPort();
		sink.addInputPort(pdest);

		final IPort psource = source.getOutputPorts().get(0);
		final IEdge e = this.connect(psource, pdest);
		return e;
	}

	/**
	 * Connect two ports and add the builded link to the nodes graphs
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	@Override
	public IEdge connect(final IPort source, final IPort sink) {
		final IEdge e = this.createEdge(source, sink);
		final IGraph gSource = source.getNode().getGraph();
		final IGraph gSink = sink.getNode().getGraph();
		gSource.addEdge(e);
		if (gSource != gSink) {
			gSink.addEdge(e);
		}
		return e;
	}

	@Override
	public ICollapsedNode createCollapsedNode(final boolean directed) {
		return new CollapsedNode(directed);
	}

	/**
	 * Create an edge without adding to the graphs.
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	@Override
	public IEdge createEdge(final IPort source, final IPort sink) {
		final IEdge e = new Edge(source, sink);
		return e;
	}

	@Override
	public IGraph createGraph(final boolean directed) {
		final Graph graph = new Graph(directed);
		if (this.getNodesCoparator() != null) {
			graph.setNodeComparator(this.getNodesCoparator());
		}
		if (this.getEdgesComparator() != null) {
			graph.setEdgeComparator(this.getEdgesComparator());
		}
		return graph;
	}

	@Override
	public INode createNode() {
		final Node node = new Node();
		if (this.getPortsCoparator() != null) {
			node.setPortComparator(this.getPortsCoparator());
		}
		return node;
	}

	@Override
	public INode createNode(final int nbInPorts, final int nbOutPorts) {
		final INode node = this.createNode();
		for (int i = 0; i < nbInPorts; ++i) {
			node.addInputPort(this.createPort());
		}
		for (int i = 0; i < nbOutPorts; ++i) {
			node.addOutputPort(this.createPort());
		}
		return node;
	}

	@Override
	public IPort createPort() {
		return new Port();
	}

	@Override
	public ISubGraphNode createSubgraph(final boolean directed) {
		final SubGraphNode n = new SubGraphNode(directed);
		return n;
	}

	public Comparator<IEdge> getEdgesComparator() {
		return this.edgesComparator;
	}

	public Comparator<INode> getNodesCoparator() {
		return this.nodesComparator;
	}

	public Comparator<IPort> getPortsCoparator() {
		return this.portsComparator;
	}

	public void setEdgesComparator(final Comparator<IEdge> edgesComparator) {
		this.edgesComparator = edgesComparator;
	}

	public void setNodesCoparator(final Comparator<INode> nodesCoparator) {
		this.nodesComparator = nodesCoparator;
	}

	public void setPortsCoparator(final Comparator<IPort> portsCoparator) {
		this.portsComparator = portsCoparator;
	}
}
