package fr.irisa.cairn.graph.implement.providers;

import static fr.irisa.cairn.graph.tools.GraphTools.getInputPort;
import static fr.irisa.cairn.graph.tools.GraphTools.getOutputPort;
import static fr.irisa.cairn.graph.tools.GraphTools.getSinkPortNumber;
import static fr.irisa.cairn.graph.tools.GraphTools.getSourcePortNumber;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.providers.ICloneProvider;

public class ClonerProvider implements ICloneProvider<IGraph, INode, IEdge, IPort>{
	private IGraphFactory factory;

	@Inject
	public ClonerProvider(IGraphFactory factory) {
		this.factory = factory;
	}

	public IEdge cloneEdge(IEdge e) {
		return factory.createEdge(e.getSourcePort(), e.getSinkPort());
	}

	public INode cloneNode(INode n) {
		INode node = factory.createNode();
		for (int i = 0; i < n.getInputPorts().size(); ++i)
			node.addInputPort(clonePort(n.getInputPorts().get(i)));
		for (int i = 0; i < n.getOutputPorts().size(); ++i)
			node.addOutputPort(clonePort(n.getOutputPorts().get(i)));
		return node;
	}

	public IPort clonePort(IPort p) {
		return factory.createPort();
	}

	public IGraph createGraph(boolean directed) {
		return factory.createGraph(directed);
	}

	public void reconnectClone(IEdge clone, IEdge edge, INode newSource,
			INode newSink) throws GraphException {
		int pSource = getSourcePortNumber(edge);
		int pSink = getSinkPortNumber(edge);

		clone.reconnectSourcePort(getOutputPort(newSource, pSource));
		clone.reconnectSinkPort(getInputPort(newSink, pSink));
	}

}
