package fr.irisa.cairn.graph.implement.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;

/**
 * Default Equivalence provider for an {@link IGraph}.Nodes, Edges and Ports are
 * equivalents if they are instances of their respective same classes.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class EquivalenceProvider<E extends IEdge, N extends INode, P extends IPort>
		implements IEquivalenceProvider<E, N, P> {

	@Override
	public boolean edgesEquivalence(E e1, E e2) {
		return e1.getClass() == e2.getClass();
	}

	@Override
	public boolean nodesEquivalence(N n1, N n2) {
		return n1.getClass() == n2.getClass();
	}

	@Override
	public boolean portsEquivalence(P p1, P p2) {
		return p1.getClass() == p2.getClass();
	}

}
