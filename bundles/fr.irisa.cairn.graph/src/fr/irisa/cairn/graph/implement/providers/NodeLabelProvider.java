package fr.irisa.cairn.graph.implement.providers;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;

public class NodeLabelProvider <N extends INode,SN extends ISubGraphNode>implements INodeLabelProvider<N, SN> {
	private Map<INode, String> nodesId;
	private Map<INode, String> clustersId;

	private int id;
	protected int clusterId;

	@Inject
	public NodeLabelProvider() {
		this.nodesId = new Hashtable<INode, String>();
		this.clustersId = new Hashtable<INode, String>();
	}

	public String getNodeName(N node) {
		String nodeId = nodesId.get(node);
		if (nodeId == null) {
			nodeId = nextId();
			nodesId.put(node, nodeId);
		}
		return nodeId;
	}

	protected String nextId() {
		String s = "n" + id;
		id++;
		return s;
	}

	protected String nextClusterId() {
		String s = "cluster" + clusterId;
		clusterId++;
		return s;
	}

	public String getNodeLabel(N n) {
		if(n.getLabel()!=null)
			return n.getLabel();
		List<INode> nodes = new ArrayList<INode>(n.getGraph().getNodes());
		int i = nodes.indexOf(n);
		return "n"+i;
	}

	public String getClusterLabel(SN node) {
		return getClusterName(node);
	}

	public String getClusterName(SN node) {
		String nodeId = clustersId.get(node);
		if (nodeId == null) {
			nodeId = nextClusterId();
			clustersId.put(node, nodeId);
		}
		return nodeId;
	}

}
