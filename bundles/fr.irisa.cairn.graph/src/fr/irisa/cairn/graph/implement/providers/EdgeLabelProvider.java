package fr.irisa.cairn.graph.implement.providers;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;

public class EdgeLabelProvider<E extends IEdge> implements IEdgeLabelProvider<E>{

	@Inject
	public EdgeLabelProvider() {
		// TODO Auto-generated constructor stub
	}
	public String getEdgeLabel(E edge) {
		if(edge.getSinkPort()!=null){
			return ""+edge.getSinkPort().getNode().getInputPorts().indexOf(edge.getSinkPort());
		}
		return "";
	}

}
