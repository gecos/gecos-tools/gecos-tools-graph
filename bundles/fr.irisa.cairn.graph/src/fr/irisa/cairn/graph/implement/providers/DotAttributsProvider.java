package fr.irisa.cairn.graph.implement.providers;

import java.util.Properties;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.io.DOTExport.DotAttributs;
import fr.irisa.cairn.graph.io.util.ColorPalette;


public class DotAttributsProvider<N extends INode,E extends IEdge> extends AbstractAttributsProvider<N,E> {

	@Inject
	public DotAttributsProvider() {
		super();
	}

	public final static String SHAPE_ELLIPSE = "ellipse";
	public final static String SHAPE_CIRCLE = "circle";
	public final static String SHAPE_RECTANCLE = "box";
	public final static String SHAPE_SQUARE = "square";
	public final static String SHAPE_POINT     = "point";
	public final static String SHAPE_HOUSE    = "house";
	public final static String SHAPE_INV_HOUSE    = "invhouse";
	public final static String SHAPE_DOUBLE_CIRCLE    = "doublecircle";
	public final static String SHAPE_DOUBLE_OCTAGONE    = "doubleoctagon";
	public final static String SHAPE_MCIRCLE    = "Mcircle";
	public final static String SHAPE_DIAMOND    = "diamond";
	public final static String SHAPE_BOX3D    = "box3d";
	
	public final static String STYLE_FILLED= "filled";
	public final static String STYLE_INVISIBLE = "invisible";
	public final static String STYLE_DASHED = "dashed";
	public final static String STYLE_DIAGONALS = "diagonals";
	
	public final static String ARROW_NONE = "none";
	public final static String ARROW_EMPTY = "empty";
	public final static String ARROW_DIAMOND = "diamond";
	public final static String ARROW_DIAMOND_EMPTY = "odiamond";
	public final static String ARROW_DOT= "dot";
	public final static String ARROW_DOT_EMPTY= "odot";
	
	protected ColorPalette palette = new ColorPalette();
	
	@Override
	protected void findEdgeAttributs(E e) {
		edgesProperties.put(e, new Properties());
	}

	@Override
	protected void findNodeAttributs(N n) {
		addNodeProperty(n, DotAttributs.NODE_SHAPE, SHAPE_ELLIPSE);
	}

}
