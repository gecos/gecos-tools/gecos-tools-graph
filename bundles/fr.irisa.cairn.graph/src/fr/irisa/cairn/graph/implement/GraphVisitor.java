package fr.irisa.cairn.graph.implement;

import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.ISubGraphNode;

public class GraphVisitor implements IGraphVisitor{

	public void visitEdge(IEdge e, Object arg) {
	}

	public void visitNode(INode n, Object arg) {
	}

	public void visitSubGraph(ISubGraphNode sn, Object arg) {
	}

	public void visitCollapsedNode(ICollapsedNode cn, Object arg) {
		
	}

}
