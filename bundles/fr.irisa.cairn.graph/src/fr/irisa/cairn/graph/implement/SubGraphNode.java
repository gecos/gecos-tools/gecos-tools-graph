package fr.irisa.cairn.graph.implement;

import java.util.Comparator;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.observer.EventType;
import fr.irisa.cairn.graph.observer.FeatureType;
import fr.irisa.cairn.graph.observer.NotificationImpl;

public class SubGraphNode extends Node implements ISubGraphNode {
	protected Graph	subgraph;

	public SubGraphNode(final boolean directed) {
		super();
		this.initialize(directed);

	}

	@Override
	public void accept(final IGraphVisitor graphVistor, final Object arg) {
		graphVistor.visitSubGraph(this, arg);
	}

	@Override
	public void addEdge(final IEdge e) {
		this.subgraph.addEdge(e);
	}

	@Override
	public void addNode(final INode n) {
		this.subgraph.addNode(n);
	}

	@Override
	public Set<? extends IEdge> getEdges() {
		return this.subgraph.getEdges();
	}

	@Override
	public Set<? extends IEdge> getExternalInputs() {
		return this.subgraph.getExternalInputs();
	}

	@Override
	public Set<? extends IEdge> getExternalOutputs() {
		return this.subgraph.getExternalOutputs();
	}

	@Override
	public Set<? extends INode> getNodes() {
		return this.subgraph.getNodes();
	}
	
	protected void initialize(final boolean directed) {
		this.subgraph = new Graph(directed);
		this.subgraph.setParent(this);
		
		if(isNotificationRequired()){
			notify(new NotificationImpl(this, this.subgraph, FeatureType.SUBGRAPH_CONTENT,
					EventType.SET));
		}
	}

	@Override
	public boolean isDirected() {
		return this.subgraph.isDirected();
	
	}

	@Override
	public void removeEdge(final IEdge e) {
		this.subgraph.removeEdge(e);
	
	}

	@Override
	public void removeNode(final INode n) {
		this.subgraph.removeNode(n);
		
	}

	public void sortNodes() {
		this.subgraph.sortNodes();
	}
	
	public void sortedges() {
		this.subgraph.sortEdges();
	}
	
	public void sortNodes(Comparator<INode> comparator) {
		this.subgraph.sortNodes(comparator);
	}
	
	public void sortedges(Comparator<IEdge> comparator) {
		this.subgraph.sortEdges(comparator);
	}
	
	
	public Graph getSubgraph() {
		return subgraph;
	}
	
	@Override
	public String toString() {
		return "subgraph:" + this.getNodes().size() + " nodes, " + this.getEdges().size()
				+ " edges";
	}
}
