/*
 * File : UllmanIsomorphismProvider.java
 * Created on 13 Nov 2008
 * Project : fr.irisa.cairn.graph
 */
package fr.irisa.cairn.graph.implement.providers;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.analysis.SubGraphIsomorphism;
import fr.irisa.cairn.graph.analysis.SubgraphMapping;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.graph.providers.IIsomorphismProvider;

/**
 * An isomorphism provider based on Ullman algorithm {@link SubGraphIsomorphism}
 * . Can be used in the pattern searching algorithm. One must provide the
 * equivalence provider.
 * <p>
 * 
 * 
 * @see SubGraphIsomorphism
 * @author Kevin Martin
 */
@SuppressWarnings("rawtypes")
public class UllmanIsomorphismProvider<G extends IGraph> implements
		IIsomorphismProvider<G> {

	private IEquivalenceProvider eqProvider;

	/**
	 * @param equivalenceProvider
	 *            : the equivalence provider on nodes and edges of the IGraph
	 */
	@Inject
	public UllmanIsomorphismProvider(IEquivalenceProvider equivalenceProvider) {
		this.eqProvider = equivalenceProvider;
	}

	public boolean isG1IsomorpheToG2(G g1, G g2) {
		// the two graphs must have the same number of nodes
		if (g1.getNodes().size() == g2.getNodes().size()) {
			// and the same number of edges
			if (g1.getEdges().size() == g2.getEdges().size()) {
				List<IGraph> patterns = new ArrayList<IGraph>();
				patterns.add(g2);
				SubGraphIsomorphism graphIsomorphism = new SubGraphIsomorphism(
						g1, patterns, eqProvider);
				graphIsomorphism.setOnlyOne(true);
				List<SubgraphMapping> result = graphIsomorphism.compute();
				// all nodes of the two graphs must be mapped
				return ((result.size() >= 1) && (result.get(0).subjectNodes()
						.size() == g1.getNodes().size()));
			}
		}
		return false;
	}

	public List<SubgraphMapping> getAllMatches(G currentGraph, G pattern) {
		List<IGraph> patterns = new ArrayList<IGraph>();
		patterns.add(pattern);
		SubGraphIsomorphism graphIsomorphism = new SubGraphIsomorphism(
				currentGraph, patterns, eqProvider);
		graphIsomorphism.setOnlyOne(false);
		return graphIsomorphism.compute();
	}

	@Override
	public SubgraphMapping getMatch(G g1, G g2) {
		throw new RuntimeException("getMatch(G,G) is not implemented");
	}
}
