package fr.irisa.cairn.graph.implement;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.implement.providers.ClonerProvider;
import fr.irisa.cairn.graph.observer.EventType;
import fr.irisa.cairn.graph.observer.FeatureType;
import fr.irisa.cairn.graph.observer.NotificationImpl;
import fr.irisa.cairn.graph.providers.ICloneProvider;

public class CollapsedNode extends SubGraphNode implements ICollapsedNode {
	private Map<IPort, IPort> inputsMap;
	private Map<IPort, IPort> outputsMap;
	protected ClonerProvider cloner;

	public CollapsedNode(boolean directed) {
		super(directed);
		this.inputsMap = new Hashtable<IPort, IPort>();
		this.outputsMap = new Hashtable<IPort, IPort>();
		this.cloner = new ClonerProvider(new GraphFactory());
	}

	@SuppressWarnings("unchecked")
	public static <G extends IGraph, N extends INode, E extends IEdge, P extends IPort> List<N> expandableAspect(
			ICollapsedNode c, ICloneProvider<G, N, E, P> cloner) {
		List<N> nodes = new ArrayList<N>();
		Map<IPort, IPort> expandedPortsMap = new Hashtable<IPort, IPort>();

		// expand nodes
		for (INode n : c.getNodes()) {

			N expanded = cloner.cloneNode((N) n);
			nodes.add(expanded);
			c.getGraph().addNode(expanded);

			// Map expanded ports
			for (int i = 0; i < n.getInputPorts().size(); ++i) {
				expandedPortsMap.put(n.getInputPorts().get(i), expanded
						.getInputPorts().get(i));
			}
			for (int i = 0; i < n.getOutputPorts().size(); ++i) {
				expandedPortsMap.put(n.getOutputPorts().get(i), expanded
						.getOutputPorts().get(i));
			}
		}

		// reconnect edges
		for (IPort in : c.getInputPorts()) {
			IPort dest = c.getMappedInputPort(in);
			for (IEdge e : in.getEdges()) {
				e.reconnectSinkPort(expandedPortsMap.get(dest));
			}
		}
		for (IPort out : c.getOutputPorts()) {
			IPort src = c.getMappedOutputPort(out);
			for (IEdge e : out.getEdges()) {
				e.reconnectSourcePort(expandedPortsMap.get(src));
			}
		}
		c.getGraph().removeNode(c);
		return nodes;
	}

	public List<? extends INode> expand() {
		return expandableAspect(this, cloner);
	}

	public IPort getMappedInputPort(IPort in) {
		return inputsMap.get(in);
	}

	public IPort getMappedOutputPort(IPort out) {
		return outputsMap.get(out);
	}

	public void mapInput(IPort in, IPort nestedPort) {
		inputsMap.put(in, nestedPort);

		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this,
					new SimpleEntry<IPort, IPort>(in, nestedPort),
					FeatureType.COLLAPSED_INPUT, EventType.MAP));
		}
	}

	public void mapOutput(IPort out, IPort nestedPort) {
		outputsMap.put(out, nestedPort);
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this,
					new SimpleEntry<IPort, IPort>(out, nestedPort),
					FeatureType.COLLAPSED_OUTPUT, EventType.MAP));
		}
	}

	@Override
	public void accept(IGraphVisitor graphVistor, Object arg) {
		graphVistor.visitCollapsedNode(this, arg);
	}

}
