package fr.irisa.cairn.graph.implement;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.observer.EventType;
import fr.irisa.cairn.graph.observer.FeatureType;
import fr.irisa.cairn.graph.observer.NotificationImpl;
import fr.irisa.cairn.graph.observer.Observable;

public class Edge extends Observable implements IEdge {

	private IPort sinkPort;
	private IPort sourcePort;

	public Edge(final IPort source, final IPort sink) {
		this.sinkPort = sink;
		this.sourcePort = source;
	
		if (source != null) {
			source.connect(this);
		}
		if (sink != null) {
			sink.connect(this);
		}
	}

	@Override
	public void accept(final IGraphVisitor graphVistor, final Object arg) {
		graphVistor.visitEdge(this, arg);
	}

	@Override
	public IPort getSinkPort() {
		return this.sinkPort;
	}

	@Override
	public IPort getSourcePort() {
		return this.sourcePort;
	}

	@Override
	public boolean isExternalEdge() {
		return this.getSourcePort().getNode().getGraph() != this.getSinkPort()
				.getNode().getGraph();
	}

	@Override
	public void reconnectSinkPort(final IPort p) {
		final Port port = (Port) this.sinkPort;
		if (port != null) {
			port.disconnect(this);
		}
		this.sinkPort = p;
		if (p != null) {
			p.connect(this);
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, p, FeatureType.EDGE_SINK,
					EventType.RECONNECT));

		}
	}

	@Override
	public void reconnectSourcePort(final IPort p) {
		final Port port = (Port) this.sourcePort;
		if (port != null) {
			port.disconnect(this);
		}
		this.sourcePort = p;
		if (p != null) {
			p.connect(this);
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, p, FeatureType.EDGE_SOURCE,
					EventType.RECONNECT));
		}
	}

	@Override
	public String toString() {
		return this.sourcePort.getNode() + "->" + ((this.sinkPort == null) ? "null" : this.sinkPort.getNode());
	}

}
