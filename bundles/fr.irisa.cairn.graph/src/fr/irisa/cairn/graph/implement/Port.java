package fr.irisa.cairn.graph.implement;

import java.util.HashSet;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.observer.EventType;
import fr.irisa.cairn.graph.observer.FeatureType;
import fr.irisa.cairn.graph.observer.NotificationImpl;
import fr.irisa.cairn.graph.observer.Observable;

public class Port extends Observable implements IPort {
	private INode node;
	private final Set<IEdge> edges;

	public Port() {
		this.edges = new HashSet<IEdge>();
	}

	@Override
	public void connect(final IEdge e) {
		this.edges.add(e);
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, e, FeatureType.PORT_EDGE,
					EventType.CONNECT));
		}
	}

	@Override
	public void disconnect(final IEdge e) {
		this.edges.remove(e);
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, e, FeatureType.PORT_EDGE,
					EventType.DISCONNECT));
		}
	}

	@Override
	public int getDegree() {
		return this.getEdges().size();
	}

	@Override
	public Set<? extends IEdge> getEdges() {
		return this.edges;
	}

	@Override
	public INode getNode() {
		return this.node;
	}

	public void setNode(final INode n) {
		this.node = n;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("p");
		if (this.node != null) {
			if (this.node.getInputPorts().contains(this)) {
				sb.append("in").append(this.node.getInputPorts().indexOf(this));
			} else if (this.node.getOutputPorts().contains(this)) {
				sb.append("out").append(
						this.node.getOutputPorts().indexOf(this));
			}
		}
		sb.append("(").append(this.edges.size() + " edges)");
		return sb.toString();
	}

}
