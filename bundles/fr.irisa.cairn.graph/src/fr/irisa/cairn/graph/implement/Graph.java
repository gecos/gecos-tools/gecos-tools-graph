package fr.irisa.cairn.graph.implement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.observer.EventType;
import fr.irisa.cairn.graph.observer.FeatureType;
import fr.irisa.cairn.graph.observer.NotificationImpl;
import fr.irisa.cairn.graph.observer.Observable;
import fr.irisa.cairn.graph.tools.GraphTools;
import fr.irisa.cairn.graph.util.WeakReferenceObserver;

public class Graph extends Observable implements IGraph {
	protected List<IEdge>		edges;
	protected List<INode>		nodes;
	private final boolean		directed;

	private ISubGraphNode		parent;

	private Comparator<INode>	nodesComparator;
	private Comparator<IEdge>	edgesComparator;

	public Graph(final boolean directed) {
		this.edges = new ArrayList<IEdge>();
		this.nodes = new ArrayList<INode>();
		this.directed = directed;
		this.getObservers().add(new WeakReferenceObserver());
	}

	@Override
	public void addEdge(final IEdge e) {
		if (!this.edges.contains(e)) {
			this.edges.add(e);
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, e, FeatureType.GRAPH_EDGE, EventType.ADD));
		}
	}

	@Override
	public void addNode(final INode n) {
		if (!this.nodes.contains(n)) {
			this.nodes.add(n);
			((Node) n).setGraph(this);
		}

		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, n, FeatureType.GRAPH_NODE, EventType.ADD));
		}
	}

	@Override
	public Set<? extends IEdge> getEdges() {
		return new LinkedHashSet<IEdge>(this.edges);
	}

	@Override
	public Set<? extends IEdge> getExternalInputs() {
		final Set<IEdge> inputs = new HashSet<IEdge>();
		for (final IEdge e : this.edges) {
			if (e.getSinkPort().getNode().getGraph() != e.getSourcePort().getNode().getGraph()) {
				if (GraphTools.isNodeIn(e.getSinkPort().getNode(), this)
						&& GraphTools.getSourceNode(e).getGraph() != this) {
					inputs.add(e);
				}
			}
		}
		return inputs;
	}

	@Override
	public Set<? extends IEdge> getExternalOutputs() {
		final Set<IEdge> outputs = new HashSet<IEdge>();
		for (final IEdge e : this.edges) {
			if (e.getSinkPort().getNode().getGraph() != e.getSourcePort().getNode().getGraph()) {
				if (GraphTools.isNodeIn(e.getSourcePort().getNode(), this)
						&& GraphTools.getSinkNode(e).getGraph() != this) {
					outputs.add(e);
				}
			}
		}
		return outputs;
	}

	@Override
	public Set<? extends INode> getNodes() {
		return new LinkedHashSet<INode>(this.nodes);
	}

	public ISubGraphNode getParent() {
		return this.parent;
	}

	public boolean hasParent() {
		return this.parent != null;
	}

	@Override
	public boolean isDirected() {
		return this.directed;
	}

	@Override
	public void removeEdge(final IEdge e) {
		this.edges.remove(e);
		if (e.getSinkPort() != null) {
			e.getSinkPort().disconnect(e);
		}
		if (e.getSourcePort() != null) {
			e.getSourcePort().disconnect(e);
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, e, FeatureType.GRAPH_EDGE, EventType.REMOVE));
		}
	}

	@Override
	public void removeNode(final INode n) {
		this.nodes.remove(n);
		for (final IEdge e : n.getIncomingEdges()) {
			final IGraph sourceGraph = e.getSourcePort().getNode().getGraph();
			final IGraph sinkGraph = e.getSinkPort().getNode().getGraph();
			sourceGraph.removeEdge(e);
			sinkGraph.removeEdge(e);
		}
		for (final IEdge e : n.getOutgoingEdges()) {
			final IGraph sourceGraph = e.getSourcePort().getNode().getGraph();
			final IGraph sinkGraph = e.getSinkPort().getNode().getGraph();
			sourceGraph.removeEdge(e);
			sinkGraph.removeEdge(e);
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, n, FeatureType.GRAPH_NODE, EventType.REMOVE));
		}
	}

	public void setEdgeComparator(final Comparator<IEdge> c) {
		this.edgesComparator = c;
	}

	public void setNodeComparator(final Comparator<INode> c) {
		this.nodesComparator = c;
	}

	public void setParent(final ISubGraphNode container) {
		this.parent = container;
	}

	public void sortEdges() {
		this.sortEdges(this.edgesComparator);
	}

	public void sortEdges(final Comparator<IEdge> comparator) {
		if (comparator != null) {
			Collections.sort(this.edges, comparator);
		}
	}

	public void sortNodes() {
		this.sortNodes(this.nodesComparator);
	}

	public void sortNodes(final Comparator<INode> comparator) {
		if (comparator != null) {
			Collections.sort(this.nodes, comparator);
		}
	}

}
