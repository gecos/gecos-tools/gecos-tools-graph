package fr.irisa.cairn.graph.implement;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.observer.EventType;
import fr.irisa.cairn.graph.observer.FeatureType;
import fr.irisa.cairn.graph.observer.NotificationImpl;
import fr.irisa.cairn.graph.observer.Observable;
import fr.irisa.cairn.graph.tools.GraphTools;

public class Node extends Observable implements INode {
	protected List<IPort> inputs;
	protected List<IPort> outputs;
	private IGraph graph;
	private String label;

	private Comparator<IPort> portsComparator;

	public Node() {
		this.inputs = new ArrayList<IPort>();
		this.outputs = new ArrayList<IPort>();
	}

	@Override
	public void accept(final IGraphVisitor graphVistor, final Object arg) {
		graphVistor.visitNode(this, arg);
	}

	@Override
	public void addInputPort(final IPort p) {
		final Port port = (Port) p;
		this.inputs.add(p);
		port.setNode(this);
		this.sortInports();
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, p, FeatureType.NODE_INPORT,
					EventType.ADD));
		}
	}

	@Override
	public void addOutputPort(final IPort p) {
		final Port port = (Port) p;
		this.outputs.add(p);
		port.setNode(this);
		this.sortOutports();
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, p, FeatureType.NODE_OUTPORT,
					EventType.ADD));
		}
	}

	@Override
	public int getDegree() {
		return this.getOutgoingEdges().size() + this.getIncomingEdges().size();
	}

	@Override
	public IGraph getGraph() {

		return this.graph;
	}

	@Override
	public List<? extends IEdge> getIncomingEdges() {
		final List<IEdge> edges = new ArrayList<IEdge>();
		for (final IPort in : this.getInputPorts()) {
			edges.addAll(in.getEdges());
		}
		if (!this.graph.isDirected()) {
			for (final IPort out : this.getOutputPorts()) {
				edges.addAll(out.getEdges());
			}
		}
		return edges;
	}

	@Override
	public int getInDegree() {
		return this.getIncomingEdges().size();
	}

	@Override
	public List<? extends IPort> getInputPorts() {
		return this.inputs;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	@Override
	public int getOutDegree() {
		return this.getOutgoingEdges().size();
	}

	@Override
	public List<? extends IEdge> getOutgoingEdges() {
		final List<IEdge> edges = new ArrayList<IEdge>();
		for (final IPort out : this.getOutputPorts()) {
			edges.addAll(out.getEdges());
		}
		if (!this.graph.isDirected()) {
			for (final IPort in : this.getInputPorts()) {
				edges.addAll(in.getEdges());
			}
		}
		return edges;
	}

	@Override
	public List<? extends IPort> getOutputPorts() {
		return this.outputs;
	}

	private WeakReference<List<INode>> predecessors=new WeakReference<List<INode>>(new ArrayList<INode>());

	public WeakReference<List<INode>> getPredcessorsWeakReference() {
		return predecessors;
	}

	@Override
	public List<? extends INode> getPredecessors() {
		List<INode> list = predecessors.get();
		if (list == null ||list.size()==0) {
			
			final List<INode> nodes = new ArrayList<INode>();
			for (final IEdge e : this.getIncomingEdges()) {
				if (!GraphTools.isExternalEdge(e)) {
					final INode node = GraphTools.getOppositeNode(e, this);
					if (!nodes.contains(node) && node != null) {
						nodes.add(node);
					}
				}
			}
			predecessors = new WeakReference<List<INode>>(nodes);
			list = nodes;
		}
		return list;
	}

	private WeakReference<List<INode>> successors = new WeakReference<List<INode>>(
			new ArrayList<INode>());

	public WeakReference<List<INode>> getSuccessorsWeakReference() {
		return successors;
	}

	@Override
	public List<? extends INode> getSuccessors() {
		List<INode> list = successors.get();
		if (list == null ||list.size()==0) {
			final List<INode> nodes = new ArrayList<INode>();
			for (final IEdge e : this.getOutgoingEdges()) {
				if (!GraphTools.isExternalEdge(e)) {
					final INode node = GraphTools.getOppositeNode(e, this);
					if (!nodes.contains(node) && node != null) {
						nodes.add(node);
					}
				}
			}
			successors = new WeakReference<List<INode>>(nodes);
			list = nodes;
		}
		return list;
	}

	@Override
	public boolean isConnectedTo(final INode node) {
		return this.isPredecessorOf(node) || this.isSuccessorTo(node);
	}

	@Override
	public boolean isPredecessorOf(final INode node) {
		return node.getPredecessors().contains(this);
	}

	@Override
	public boolean isSuccessorTo(final INode node) {
		return node.getSuccessors().contains(this);
	}

	@Override
	public void removeInputPort(final IPort p) {
		final boolean in = this.inputs.remove(p);
		if (in) {
			final Set<IEdge> edges = new HashSet<IEdge>();
			edges.addAll(p.getEdges());
			for (final IEdge e : edges) {
				this.graph.removeEdge(e);
			}
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, p, FeatureType.NODE_INPORT,
					EventType.REMOVE));
		}
	}

	@Override
	public void removeOutputPort(final IPort p) {
		final boolean out = this.outputs.remove(p);
		if (out) {
			final Set<IEdge> edges = new HashSet<IEdge>();
			edges.addAll(p.getEdges());
			for (final IEdge e : edges) {
				this.graph.removeEdge(e);
			}
		}
		if (this.isNotificationRequired()) {
			this.notify(new NotificationImpl(this, p, FeatureType.NODE_OUTPORT,
					EventType.REMOVE));
		}
	}

	/**
	 * @param graph
	 *            the graph to set
	 */
	public void setGraph(final IGraph graph) {
		final Graph g = (Graph) graph;
		if (g.hasParent()) {
			this.graph = g.getParent();
		} else {
			this.graph = graph;
		}
	}

	@Override
	public void setLabel(final String label) {
		this.label = label;
	}

	public void setPortComparator(final Comparator<IPort> c) {
		this.portsComparator = c;
	}

	public void sortInports() {
		this.sortInports(this.portsComparator);
	}

	public void sortInports(final Comparator<IPort> comparator) {
		if (comparator != null) {
			Collections.sort(this.inputs, comparator);
		}
	}

	public void sortOutports() {
		this.sortOutports(this.portsComparator);
	}

	public void sortOutports(final Comparator<IPort> comparator) {
		if (comparator != null) {
			Collections.sort(this.outputs, comparator);
		}
	}

	@Override
	public String toString() {
		if (this.label != null) {
			return this.label;
		} else {
			return super.toString();
		}
	}

}
