package fr.irisa.cairn.graph;

/**
 * A subgraph is a node containing a graph. It has {@link INode} and
 * {@link IGraph} behaviors.
 * 
 * @author antoine
 * 
 */
/**
 * @model
 */
public interface ISubGraphNode extends IGraph, INode {
}
