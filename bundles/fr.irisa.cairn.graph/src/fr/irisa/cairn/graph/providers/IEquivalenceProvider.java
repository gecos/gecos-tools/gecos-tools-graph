package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;

public  interface  IEquivalenceProvider<E extends IEdge,N extends INode, P extends IPort> {
	/**
	 * Defined if two edges are equivalents
	 * 
	 * @param e1
	 *            : first edge
	 * @param e2
	 *            : second edge
	 * @return true if the two edges are equivalents
	 */
	public  boolean edgesEquivalence(E e1, E e2);

	/**
	 * Defined if two nodes are equivalents
	 * 
	 * @param n1
	 *            : first node
	 * @param n2
	 *            : second node
	 * @return true if the two node are equivalents
	 */
	public boolean nodesEquivalence(N n1, N n2);

	public boolean portsEquivalence(P p1, P p2);
}
