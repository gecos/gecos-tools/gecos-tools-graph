package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.ISubGraphNode;

public interface INodeLabelProvider <N extends INode, SN extends ISubGraphNode>{
	public String getClusterName(SN node);
	public String getClusterLabel(SN node);
	public String getNodeName(N node);
	public String getNodeLabel(N n);
}
