package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;

public interface ILatencyProvider<N extends INode,E extends IEdge> {
	public int getNodeLatency(N n);
	public int getEdgeLatency(E e);
}
