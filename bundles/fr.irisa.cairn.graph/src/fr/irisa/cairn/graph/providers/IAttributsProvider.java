package fr.irisa.cairn.graph.providers;

import java.util.Properties;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;

public interface IAttributsProvider<N extends INode,E extends IEdge> {

	public Properties getEdgeAttributs(E e);

	public Properties getNodeAttributs(N n);
}
