package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.INode;

public interface ICommutativityProvider<N extends INode> {
	public boolean isCommutative(N n);
}
