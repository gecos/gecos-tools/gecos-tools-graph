package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;

/**
 * Provide a computed behavior for nodes and edges.
 * 
 * @author antoine
 * 
 */
public interface IBehaviorProvider <N extends INode,E extends IEdge>{
	public void computeNode(N n, Object arg);

	public void computeEdge(E e, Object arg);
}
