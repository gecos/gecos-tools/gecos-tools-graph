package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.GraphException;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;

/**
 * Provide basic cloning facilities for edges, nodes and ports. 
 * 
 * @author antoine
 * 
 */
public interface ICloneProvider<G extends IGraph,N extends INode,E extends IEdge,P extends IPort> {
	public G createGraph(boolean directed);

	/**
	 * Clone a node. Inputs and outputs ports of the node are also cloned.
	 * 
	 * @param n
	 * @return cloned node.
	 */
	public N cloneNode(N n);

	/**
	 * Clone an edge.
	 * 
	 * @param e
	 * @return cloned edge.
	 */
	public E cloneEdge(E e);

	/**
	 * Clone a port.
	 * 
	 * @param p
	 * @return cloned port.
	 */
	public P clonePort(P p);

	/**
	 * Reconnect a cloned edge to a new source and destination. May preserve
	 * ports order if it has a sense in the analyzed graph.
	 * 
	 * @param clone
	 * @param edge
	 * @param newSource
	 * @param newSink
	 * @throws GraphException
	 */
	public void reconnectClone(E clone, E edge, N newSource,
			N newSink) throws GraphException;
}
