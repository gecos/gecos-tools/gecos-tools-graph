package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.IEdge;

public interface IEdgeLabelProvider <E extends IEdge>{
	public String getEdgeLabel(E edge);
}
