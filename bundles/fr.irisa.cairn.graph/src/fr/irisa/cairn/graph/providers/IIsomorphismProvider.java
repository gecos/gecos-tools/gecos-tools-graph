/*
 * File : IsomorphismStrategy.java
 * Created on Sep 22, 2008
 * Project : fr.irisa.cairn.graph
 */
package fr.irisa.cairn.graph.providers;

import java.util.List;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.analysis.SubgraphMapping;

/**
 * Interface of IGraph isomorphism strategy. 
 * 
 * @author Kevin Martin
 */
public interface IIsomorphismProvider<G extends IGraph> {

	/**
	 * Tests the isomorphism between two given graphs 
	 * 
	 * @param g1 the first graph
	 * @param g2 the second graph
	 * @return
	 *  boolean true if the two graphs are isomorph
	 */
	public boolean isG1IsomorpheToG2(G g1, G g2);

	/**
	 * Finds all the matches of the given pattern in the given subject graphs.
	 * The size of the return list is the number of matches.
	 * 
	 * @param currentGraph the subject graph
	 * @param pattern the pattern
	 * @return
	 * List<SubgraphMapping> the mapping between the subject graph nodes and the pattern nodes
	 */
	public List<SubgraphMapping> getAllMatches(G currentGraph,
			G pattern);
	
	/**
	 * 
	 * @param g1
	 * @param g2
	 * @return
	 * 
	 * @generated NOT
	 */
	public SubgraphMapping getMatch(G g1, G g2);
}
