package fr.irisa.cairn.graph.providers;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;

public interface IGraphFilter <N extends INode,E extends IEdge, P extends IPort>{
	public int getComponentMinSize();
	public boolean isFiltered(N n);
	public boolean isFiltered(E e);
	public boolean isFiltered(P p);

}
