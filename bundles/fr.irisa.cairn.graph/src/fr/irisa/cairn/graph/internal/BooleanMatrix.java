package fr.irisa.cairn.graph.internal;

public class BooleanMatrix {
	
	private BooleanVector[] rows;
	private int columns;

	public BooleanMatrix(int rows, int columns) {
		this.rows = new BooleanVector[rows];
		this.columns = columns;
		for (int i = 0; i < this.rows.length; ++i) {
			this.rows[i] = new BooleanVector(columns);
		}
	}

	public void set(int i, int j) {
		rows[i].set(j);
	}
	
	public void unset(int i, int j) {
		rows[i].unset(j);
	}

	
	public boolean get(int i, int j) {
		return rows[i].get(j);
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (BooleanVector row : rows) {
			buffer.append(row.toString());
			buffer.append('\n');
		}
		return buffer.toString();
	}

	public int columnCount() {
		return columns;
	}
	public int rowCount() {
		return rows.length;
	}

	public BooleanMatrix copy() {
		BooleanMatrix m = new BooleanMatrix(rows.length, columns);
		for (int i = 0; i < rows.length; ++i) {
			for (int j = 0; j < columns; ++j) {
				if (get(i, j))
					m.set(i, j);
			}
		}
		return m;
	}
	
}
