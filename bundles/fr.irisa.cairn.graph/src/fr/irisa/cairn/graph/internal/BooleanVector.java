package fr.irisa.cairn.graph.internal;

public class BooleanVector {

	private static final int INT_SIZE = 32;
	private static int[] mask;
	
	static {
		mask = new int[32];
		for (int i = 0; i < 32; ++i)
			mask[i] = (1<<i);
	}
	
	private int[] values;
	private int size;

	public BooleanVector(int size) {
		this.size = size;
		this.values = new int[(size + INT_SIZE - 1)/ INT_SIZE];
	}

	public void set(int j) {
		values[j/INT_SIZE] |= mask[j%INT_SIZE];
	}
	
	public void unset(int j) {
		values[j/INT_SIZE] &= ~mask[j%INT_SIZE];
	}
	
	public boolean get(int j) {
		return (values[j/INT_SIZE] & mask[j%INT_SIZE]) != 0;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (int j = 0; j < size; ++j) {
			buffer.append(get(j) ? '1' : '0');
		}
		return buffer.toString();
	}
	
}
