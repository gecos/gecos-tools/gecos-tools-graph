package fr.irisa.cairn.graph;

/**
 * @model
 */
public class GraphException extends Exception {

	private static final long serialVersionUID = 745783220857971787L;

	/**
	 * @model default="" 
	 */
	public GraphException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @model default=""
	 */
	public GraphException(String message) {
		super(message);
	}

	/**
	 * @model default=""
	 */
	public GraphException(Throwable cause) {
		super(cause);
	}

}
