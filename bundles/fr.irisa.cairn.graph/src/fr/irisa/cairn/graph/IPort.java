package fr.irisa.cairn.graph;

import java.util.Set;

/**
 * An IPort is defined by a node and a sets of connected {@link IEdge}.
 * 
 * @author antoine
 * 
 */
/**
 * @model
 */

public interface IPort {

	/** Get the port node */
	/**
	 * @model default=""
	 */
	public INode getNode();

	/** Get all connected edges */
	/**
	 * @model default=""
	 */
	public Set<? extends IEdge> getEdges();

	/**
	 * Get port degree.
	 * 
	 * @return
	 */
	public int getDegree();

	// /** Connnect an edge to this port */
	public void connect(IEdge e);

	public void disconnect(IEdge e);

}
