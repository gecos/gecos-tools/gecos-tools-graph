/*
 * File : DOTColors.java
 * Created on Nov 10, 2008
 * Project : fr.irisa.cairn.graph
 */
package fr.irisa.cairn.graph.io;

/**
 * A non exhaustive (for the moment) list of dot colors.
 * 
 * @author Kevin Martin
 */
public class DOTColors {

	public static final String aliceblue = "aliceblue";
	public static final String antiquewhite = "antiquewhite";
	public static final String antiquewhite1 = "antiquewhite1";
	public static final String antiquewhite2 = "antiquewhite2";
	public static final String antiquewhite3 = "antiquewhite3";
	public static final String antiquewhite4 = "antiquewhite4";
	public static final String aquamarine = "aquamarine";
	public static final String aquamarine1 = "aquamarine1";
	public static final String aquamarine2 = "aquamarine2";
	public static final String aquamarine3 = "aquamarine3";
	public static final String aquamarine4 = "aquamarine4";
	public static final String azure = "azure";
	public static final String azure1 = "azure1";
	public static final String azure2 = "azure2";
	public static final String azure3 = "azure3";
	public static final String azure4 = "azure4";
	public static final String beige = "beige";
	public static final String bisque = "bisque";
	public static final String bisque1 = "bisque1";
	public static final String bisque2 = "bisque2";
	public static final String bisque3 = "bisque3";
	public static final String bisque4 = "bisque4";
	/**
	 * black is for the moment not included in the dotColors array
	 */
	public static final String black = "black";
	public static final String blanchedalmond = "blanchedalmond";
	public static final String blue = "blue";
	public static final String blue1 = "blue1";
	public static final String blue2 = "blue2";
	public static final String blue3 = "blue3";
	public static final String blue4 = "blue4";
	public static final String blueviolet = "blueviolet";
	public static final String brown = "brown";
	public static final String brown1 = "brown1";
	public static final String brown2 = "brown2";
	public static final String brown3 = "brown3";
	public static final String brown4 = "brown4";
	public static final String burlywood = "burlywood";
	public static final String burlywood1 = "burlywood1";
	public static final String burlywood2 = "burlywood2";
	public static final String burlywood3 = "burlywood3";
	public static final String burlywood4 = "burlywood4";
	public static final String cadetblue = "cadetblue";
	public static final String cadetblue1 = "cadetblue1";
	public static final String cadetblue2 = "cadetblue2";
	public static final String cadetblue3 = "cadetblue3";
	public static final String cadetblue4 = "cadetblue4";
	public static final String chartreuse = "chartreuse";
	public static final String chartreuse1 = "chartreuse1";
	public static final String chartreuse2 = "chartreuse2";
	public static final String chartreuse3 = "chartreuse3";
	public static final String chartreuse4 = "chartreuse4";
	public static final String chocolate = "chocolate";
	public static final String chocolate1 = "chocolate1";
	public static final String chocolate2 = "chocolate2";
	public static final String chocolate3 = "chocolate3";
	public static final String chocolate4 = "chocolate4";
	public static final String coral = "coral";
	public static final String coral1 = "coral1";
	public static final String coral2 = "coral2";
	public static final String coral3 = "coral3";
	public static final String coral4 = "coral4";
	public static final String cornflowerblue = "cornflowerblue";
	public static final String cornsilk = "cornsilk";
	public static final String cornsilk1 = "cornsilk1";
	public static final String cornsilk2 = "cornsilk2";
	public static final String cornsilk3 = "cornsilk3";
	public static final String cornsilk4 = "cornsilk4";
	public static final String crimson = "crimson";
	public static final String cyan = "cyan";
	public static final String cyan1 = "cyan1";
	public static final String cyan2 = "cyan2";
	public static final String cyan3 = "cyan3";
	public static final String cyan4 = "cyan4";
	public static final String darkgoldenrod = "darkgoldenrod";
	public static final String darkgoldenrod1 = "darkgoldenrod1";
	public static final String darkgoldenrod2 = "darkgoldenrod2";
	public static final String darkgoldenrod3 = "darkgoldenrod3";
	public static final String darkgoldenrod4 = "darkgoldenrod4";
	public static final String darkgreen = "darkgreen";
	public static final String darkkhaki = "darkkhaki";
	public static final String darkolivegreen = "darkolivegreen";
	public static final String darkolivegreen1 = "darkolivegreen1";
	public static final String darkolivegreen2 = "darkolivegreen2";
	public static final String darkolivegreen3 = "darkolivegreen3";
	public static final String darkolivegreen4 = "darkolivegreen4";
	public static final String darkorange = "darkorange";
	public static final String darkorange1 = "darkorange1";
	public static final String darkorange2 = "darkorange2";
	public static final String darkorange3 = "darkorange3";
	public static final String darkorange4 = "darkorange4";
	public static final String darkorchid = "darkorchid";
	public static final String darkorchid1 = "darkorchid1";
	public static final String darkorchid2 = "darkorchid2";
	public static final String darkorchid3 = "darkorchid3";
	public static final String darkorchid4 = "darkorchid4";
	public static final String darksalmon = "darksalmon";
	public static final String darkseagreen = "darkseagreen";
	public static final String darkseagreen1 = "darkseagreen1";
	public static final String darkseagreen2 = "darkseagreen2";
	public static final String darkseagreen3 = "darkseagreen3";
	public static final String darkseagreen4 = "darkseagreen4";
	public static final String darkslateblue = "darkslateblue";
	public static final String darkslategray = "darkslategray";
	public static final String darkslategray1 = "darkslategray1";
	public static final String darkslategray2 = "darkslategray2";
	public static final String darkslategray3 = "darkslategray3";
	public static final String darkslategray4 = "darkslategray4";
	public static final String darkslategrey = "darkslategrey";
	public static final String darkturquoise = "darkturquoise";
	public static final String darkviolet = "darkviolet";
	public static final String deeppink = "deeppink";
	public static final String deeppink1 = "deeppink1";
	public static final String deeppink2 = "deeppink2";
	public static final String deeppink3 = "deeppink3";
	public static final String deeppink4 = "deeppink4";
	public static final String deepskyblue = "deepskyblue";
	public static final String deepskyblue1 = "deepskyblue1";
	public static final String deepskyblue2 = "deepskyblue2";
	public static final String deepskyblue3 = "deepskyblue3";
	public static final String deepskyblue4 = "deepskyblue4";
	public static final String dimgray = "dimgray";
	public static final String dimgrey = "dimgrey";
	public static final String dodgerblue = "dodgerblue";
	public static final String dodgerblue1 = "dodgerblue1";
	public static final String dodgerblue2 = "dodgerblue2";
	public static final String dodgerblue3 = "dodgerblue3";
	public static final String dodgerblue4 = "dodgerblue4";
	public static final String firebrick = "firebrick";
	public static final String firebrick1 = "firebrick1";
	public static final String firebrick2 = "firebrick2";
	public static final String firebrick3 = "firebrick3";
	public static final String firebrick4 = "firebrick4";
	public static final String floralwhite = "floralwhite";
	public static final String forestgreen = "forestgreen";
	public static final String gainsboro = "gainsboro";
	public static final String ghostwhite = "ghostwhite";
	public static final String gold = "gold";
	public static final String gold1 = "gold1";
	public static final String gold2 = "gold2";
	public static final String gold3 = "gold3";
	public static final String gold4 = "gold4";
	public static final String goldenrod = "goldenrod";
	public static final String goldenrod1 = "goldenrod1";
	public static final String goldenrod2 = "goldenrod2";
	public static final String goldenrod3 = "goldenrod3";
	public static final String goldenrod4 = "goldenrod4";
	public static final String gray = "gray";

	public static String[] dotColors = { aliceblue, antiquewhite1,
			antiquewhite2, antiquewhite3, antiquewhite4, aquamarine,
			aquamarine1, aquamarine2, aquamarine3, aquamarine4, azure, azure1,
			azure2, azure3, azure4, beige, bisque, bisque1, bisque2, bisque3,
			bisque4, blanchedalmond, blue, blue1, blue2, blue3, blue4,
			blueviolet, brown, brown1, brown2, brown3, brown4, burlywood,
			burlywood1, burlywood2, burlywood3, burlywood4, cadetblue,
			cadetblue1, cadetblue2, cadetblue3, cadetblue4, chartreuse,
			chartreuse1, chartreuse2, chartreuse3, chartreuse4, chocolate,
			chocolate1, chocolate2, chocolate3, chocolate4, coral, coral1,
			coral2, coral3, coral4, cornflowerblue, cornsilk, cornsilk1,
			cornsilk2, cornsilk3, cornsilk4, crimson, cyan, cyan1, cyan2,
			cyan3, cyan4, darkgoldenrod, darkgoldenrod1, darkgoldenrod2,
			darkgoldenrod3, darkgoldenrod4, darkgreen, darkkhaki,
			darkolivegreen, darkolivegreen1, darkolivegreen2, darkolivegreen3,
			darkolivegreen4, darkorange, darkorange1, darkorange2, darkorange3,
			darkorange4, darkorchid, darkorchid1, darkorchid2, darkorchid3,
			darkorchid4, darksalmon, darkseagreen, darkseagreen1,
			darkseagreen2, darkseagreen3, darkseagreen4, darkslateblue,
			darkslategray, darkslategray1, darkslategray2, darkslategray3,
			darkslategray4, darkslategrey, darkturquoise, darkviolet, deeppink,
			deeppink1, deeppink2, deeppink3, deeppink4, deepskyblue,
			deepskyblue1, deepskyblue2, deepskyblue3, deepskyblue4, dimgray,
			dimgrey, dodgerblue, dodgerblue1, dodgerblue2, dodgerblue3,
			dodgerblue4, firebrick, firebrick1, firebrick2, firebrick3,
			firebrick4, floralwhite, forestgreen, gainsboro, ghostwhite, gold,
			gold1, gold2, gold3, gold4, goldenrod, goldenrod1, goldenrod2,
			goldenrod3, goldenrod4, antiquewhite, gray };

	public static String getColor(int i) {
		return dotColors[i];
	}

	public static int number() {
		return dotColors.length;
	}

}
