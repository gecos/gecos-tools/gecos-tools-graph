package fr.irisa.cairn.graph.io.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A color generator starting from an extensible Palette. If an unknown
 * color id is asked then a new color is generated between two neighbors
 * colors.
 * 
 * @author antoine
 * 
 */
public class ColorPalette {

	private List<Color> palette;
	private Map<Integer, Color> colors;


	
	public ColorPalette() {		
		this(defaultColors());
	}
	public ColorPalette(List<Color> base) {
		colors = new HashMap<Integer, Color>();
		palette = new ArrayList<Color>();
		for(Color c: base){
			addColor(c);
		}
	}

	public static String getHexColor(Color c) {
		String s;
		int argb = c.getRGB();
		int rgb = argb & 0x00ffffff;
		s = Integer.toHexString(rgb);
		while (s.length() < 6) {
			s = "0" + s;
		}
		return "#" + s;
	}
	
	public String getHexColor(int id) {
		return getHexColor(getColor(id));
	}
	
	public Color getColor(int id) {
		Color color = colors.get(id);
		if (color == null) {
			int ref1 = (int) (Math.random()*colors.size());
			int ref2 = ref1 + 1;

			int max = max();
			if (ref2 > max) {
				ref1 = max ;
				ref2 = 0;
			}

			color = generateColor(getColor(ref1), getColor(ref2));
			colors.put(id, color);
		}
		return color;
	}

	public int max() {
		return Collections.max(colors.keySet());
	}
	
	public Color nextColor(){
		return getColor(max()+1);
	}

	private Color generateColor(Color c1, Color c2) {
		Color middle = new Color(
				middle(c1.getRed(), c2.getRed()), 
				middle(c1.getGreen(), c2.getGreen()), 
				middle(c1.getBlue(), c2.getBlue()));
		return middle;
	}

	private int middle(int a, int b) {
		int max = a;
		int min = b;
		if (max < b)
			max = b;
		if (min > a)
			min = a;

		return ((max - min) / 2) +min;
	}

	private static List<Color> defaultColors() {
		List<Color> colors = new ArrayList<Color>();
		//Orange
		colors.add(new Color(251,168,10));
		//Green
		colors.add(new Color(183,251,169));
		//Blue
		colors.add(new Color(169,194,251));
		//Purple
		colors.add(new Color(251,169,250));
		//Red		
		colors.add(new Color(251,100,100));
		//Yellow
		colors.add(new Color(251,242,0));
		return colors;
	}

	private void addColor(Color c) {
		int i = (int)(Math.random()*palette.size());
		palette.add(i,c);
		colors.put(palette.size() - 1, c);
	}

	public void addColorInPalette(Color c) {
		palette.add(c);
	}

}