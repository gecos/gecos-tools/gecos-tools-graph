package fr.irisa.cairn.graph.io.util;

import java.awt.Point;
import java.io.PrintStream;

public class SVGFileRenderer {
	
	private PrintStream p;
	
	public SVGFileRenderer(PrintStream  out, int height, int width) {
		p = out;
			
		p.print("<?xml version=\"1.0\" standalone=\"no\"?>\n");
		p.print("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n"); 
		p.print("\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
		p.print("<svg width=\""+width+"\" height=\""+height+"\" version=\"1.1\"");
		p.print(" xmlns=\"http://www.w3.org/2000/svg\">\n");
		
		//initializeMarkers();
	}
	
//	private void initializeMarkers(){
//		p.print("<defs>");
//		
//		//Arrow marker
//		p.print("" +
//			"<g id=\"arrowMarker\"> \n"+
//	        "  <g stroke=\"black\" stroke-width=\"1\"> \n" +
//	        "       <line x1=\"6\" y1=\"-2\" x2=\"0\" y2=\"0\"/> \n"+
//	        "       <line x1=\"6\" y1=\"+2\" x2=\"0\" y2=\"0\"/> \n" +
//	        "  </g> " +
//          "</g>"); 
//		
//		p.print("</defs>");
//	}
	
	public void drawLabeledRectange(int x0, int y0, int width, int height, int offx, int offy, String color, String text) {
		//System.out.println("Drawing Labeled rectange at x="+x0+", y="+y0);
		drawSVGRectange(x0,y0,width,height,color);
		drawSVGText(x0+(width/2)-offx, y0+(height/2)-offy, text);
	}

	public void drawLabeledOvale(int x0, int y0, int width, int height, int offx, int offy, String color, String text) {
		drawSVGOvale(x0,y0,width,height,color);
		drawSVGText(x0+(width/2)-offx, y0+(height/2)-offy, text);
	}

	public void drawSVGLine( int x0, int y0, int x1, int y1, String color) {
		p.print("<line x1=\""+x0+"\" y1=\""+y0+"\" x2=\""+x1+"\" y2=\""+y1+"\"\n");
		p.print(" stroke=\"");
		p.print(color);
		p.print("\" stroke-width=\"1\"/>\n");
	}
	
	public void drawSVGRectange(int x, int y, int width, int height, String color) {
		//System.out.println("rectangle ["+x+","+y+"]");
		p.print("\n<rect fill=\""+color+"\" stroke=\"black\" x=\""+x+"\" y=\""+y+"\" width=\""+width+"\" height=\""+height+"\" fill-opacity=\"0.5\"/>\n");  
	}

//	public void drawSVGRoundRectange(int x, int y, int width, int height,int rx, int ry String color) {
//		//System.out.println("rectangle ["+x+","+y+"]");
//		p.print("\n<rect fill=\""+color+"\" stroke=\"black\" x=\""+x+"\" y=\""+y+"\" width=\""+width+"\" height=\""+height+"\"/>\n");  
//	}
//	
	public void drawSVGOvale(int x, int y, int width, int height, String color) {
		p.print("\n<ellipse fill=\""+color+"\" stroke=\"black\" x=\""+x+"\" y=\""+y+"\" rx=\""+width+"\" ry=\""+height+"\"/>\n");  
	}

	public void drawSVGText( int x, int y, String text ) {
		//System.out.println("Drawing text \""+text+"\" at x="+x+", y="+y);
		p.print("<text class=\"serif\" x=\""+x+"\" y=\""+y+"\">"+text+"</text>\n");  

	}
	
	public void drawSVGText( int x, int y, String text , int fontSize) {
		//System.out.println("Drawing text \""+text+"\" at x="+x+", y="+y);
		p.print("<text font-family=\"Verdana\" font-size=\""+fontSize+"\"  x=\""+x+"\" y=\""+y+"\">"+text+"</text>\n");  

	}

	public void drawPolygone(int x[], int y[], String color) {
		p.print("<polygon points=\"");
		for (int i=0;i<Math.min(x.length,y.length);i++) {
			p.print(x[i]+","+y[i]+" ");
		}
		p.print("\" fill=\"");
		p.print(color);
		p.print("\" stroke=\"");
		p.print(color);
		p.print("\" stroke-width=\"1\"/>\n");
	}
	
	public void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3, String color) {
		drawPolygone(new int[]{x1,x2,x3}, new int[]{y1,y2,y3},color);
	}

	public void drawSVGArrow(int x1, int y1, int x2, int y2, String color) {
		drawSVGLine(x1,y1,x2,y2, color);
		double arrowSize=10;
		double length = Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(x1-x2));
		double cos   = (x2-x1)/length;
		double sin   = (y2-y1)/length;
		Point x = new Point(x2,y2);
		x.translate((int)(arrowSize*cos), (int)(arrowSize*sin));

		Point t1 = new Point(x);
		t1.translate((int)(arrowSize*cos), (int)(arrowSize*sin));
		
		Point t2 = new Point(t1);
		t2.translate((int)(arrowSize*sin),(int)(-arrowSize*cos));
		//drawTriangle(x2-10, y2-20, x2, y2, x1+20,y1+20,color);
		
		Point t3 = new Point(t1);
		t3.translate((int)(-arrowSize*sin),(int)(arrowSize*cos) );

		//drawTriangle(t1.x, t1.y, t2.x, t2.y, t3.x, t3.y);
	}
	
	public String getHexColor(Color c) {
		String s;
		int argb = c.getRGB();
		int rgb = argb & 0x00ffffff;
		s = Integer.toHexString(rgb);
		while (s.length() < 6) {
			s = "0" + s;
		}
		return "#" + s;
	}

	public void traceGrid(int x, int y, int xStep, int yStep, int nbHorizontalLines, int nbVerticalLines) {
		int width = (nbVerticalLines-1)*xStep;
		int height = (nbHorizontalLines-1)*yStep;
		
		for(int h=0;h<nbHorizontalLines;++h){
			drawSVGLine(x, y+h*yStep,x+width, y+h*yStep, "lightgray");
		}
		
		for(int v=0;v<nbVerticalLines;++v){
			drawSVGLine(x+v*xStep, y, x+v*xStep,y+height, "lightgray");
		}

	}
	
	public void close() {
		p.println("</svg>");
		p.close();
	}

}
