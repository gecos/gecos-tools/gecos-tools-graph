package fr.irisa.cairn.graph.io;

import static fr.irisa.cairn.graph.tools.GraphTools.isGraphOutputEdge;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.implement.providers.EdgeLabelProvider;
import fr.irisa.cairn.graph.implement.providers.NodeLabelProvider;
import fr.irisa.cairn.graph.io.util.Color;
import fr.irisa.cairn.graph.io.util.ColorPalette;
import fr.irisa.cairn.graph.providers.IAttributsProvider;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;


@SuppressWarnings({"rawtypes","unchecked"}) 
public class DOTExport implements IGraphVisitor {
	public boolean showCollapsedGraph = false;
	public boolean showUnconnectedPort = true;

	protected INodeLabelProvider nodeLabelProvider;
	protected IEdgeLabelProvider edgeLabelProvider;
	protected IAttributsProvider attributsProvider;
	protected PrintStream out;
	protected String indent = "  ";
	protected String connector;
	protected Set<String> nodeKeys;
	protected Set<String> edgeKeys;

	protected int crtLevel;

	protected Set<IEdge> visitededges;
	protected Set<INode> visitedNodes;
	private Map<IPort, String> portNodesId = new Hashtable<IPort, String>();

	public boolean drawExternalEdges = false;

	public static void export(String file, IGraph graph) {
		export(file, graph, new NodeLabelProvider());
	}

	public static void export(String file, IGraph graph,
			NodeLabelProvider nodeLabelProvider) {
		export(file, graph, nodeLabelProvider, new DotAttributsProvider());

	}

	public static void export(String file, IGraph graph,
			INodeLabelProvider nodeLabelProvider,
			DotAttributsProvider dotAttributsProvider) {
		DOTExport export = new DOTExport(nodeLabelProvider,
				new EdgeLabelProvider(), dotAttributsProvider);
		try {
			PrintStream out2 = new PrintStream(file);
			export.export(out2, graph);
			out2.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static void export(String file, IGraph graph,
			INodeLabelProvider nodeLabelProvider,
			IEdgeLabelProvider edgeLabelProvider,
			IAttributsProvider<INode, IEdge> dotAttributsProvider) {
		DOTExport export = new DOTExport(nodeLabelProvider, edgeLabelProvider,
				dotAttributsProvider);
		try {
			PrintStream out2 = new PrintStream(file);
			export.export(out2, graph);
			out2.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public DOTExport(INodeLabelProvider nodeLabels,
			IEdgeLabelProvider edgeLabels) {
		this(nodeLabels, edgeLabels, new DotAttributsProvider());
	}

	public DOTExport(INodeLabelProvider nodeLabels) {
		this(nodeLabels, null, new DotAttributsProvider());
	}

	public DOTExport(IAttributsProvider attributsProvider) {
		this(null, null, attributsProvider);
	}

	public DOTExport(INodeLabelProvider nodeLabels,
			IEdgeLabelProvider edgeLabels, IAttributsProvider attributsProvider) {
		this();
		this.attributsProvider = attributsProvider;
		this.nodeLabelProvider = nodeLabels;
		this.edgeLabelProvider = edgeLabels;

	}

	protected DOTExport() {
		this.nodeKeys = DotAttributs.nodeKeys();
		this.edgeKeys = DotAttributs.edgeKeys();
	}

	protected void initialize() {
		visitededges = new HashSet<IEdge>();
		visitedNodes = new HashSet<INode>();
	}

	/**
	 * Exports a graph into a plain text file in DOT format.
	 * 
	 * @param writer
	 *            the writer to which the graph to be exported
	 * @param g
	 *            the graph to be exported
	 */
	public void export(PrintStream out, IGraph g) {
		initialize();
		this.out = out;
		if (g.isDirected()) {
			out.println("digraph G {");
			connector = " -> ";
		} else {
			out.println("graph G {");
			connector = " -- ";
		}

		for (INode n : g.getNodes()) {
			n.accept(this, null);
		}

		Set<IEdge> externalEdges = new HashSet<IEdge>();
		for (IEdge e : g.getEdges()) {
			if (e.getSinkPort() != null && e.getSourcePort() != null) {
				if (internalEdge(e)) {
					e.accept(this, null);
				} else
					externalEdges.add(e);

			}
		}
		crtLevel--;
		indent();
		out.print("}\n");
		if (drawExternalEdges) {
			for (IEdge e : externalEdges) {
				e.accept(this, null);
			}
		}
		out.flush();
		// DO NOT close the PrintStream as we did not open it!
		// out.close();
	}

	public void visitEdge(IEdge e, Object arg) {
		if (!visitededges.contains(e)) {
			if (visitedNodes.contains(e.getSourcePort().getNode())
					&& visitedNodes.contains(e.getSinkPort().getNode())) {

				visitededges.add(e);
				String source = getVertexID(e.getSourcePort().getNode());
				String target = getVertexID(e.getSinkPort().getNode());

				indent();
				out.print(source + connector + target);

				if (edgeLabelProvider != null || attributsProvider != null) {
					out.print("[");
					boolean first = true;
					if (edgeLabelProvider != null) {
						out.print(" label = \""
								+ edgeLabelProvider.getEdgeLabel(e) + "\"");
						first = false;
					}
					if (attributsProvider != null) {
						Iterator<String> keyIterator = edgeKeys.iterator();

						while (keyIterator.hasNext()) {
							String key = keyIterator.next();
							if (attributsProvider.getEdgeAttributs(e) != null
									&& attributsProvider.getEdgeAttributs(e)
											.containsKey(key)) {
								if (!first) {
									out.print(",");
									first = false;
								}
								out.print(key
										+ "=\""
										+ attributsProvider.getEdgeAttributs(e)
												.getProperty(key) + "\"");
							}

						}
					}
					out.print("]");
				}
				out.println(";");
			}
		}
	}

	public void visitNode(INode n, Object arg) {
		visitedNodes.add(n);

		indent();
		out.print(getVertexID(n));
		out.print("[");
		if (nodeLabelProvider != null)
			out.print("label = \"" + nodeLabelProvider.getNodeLabel(n) + "\"");
		if (attributsProvider != null) {
			out.print(",");
			Iterator<String> keyIterator = nodeKeys.iterator();
			Properties properties = attributsProvider.getNodeAttributs(n);
			while (keyIterator.hasNext()) {
				String key = keyIterator.next();
				if (properties.containsKey(key)) {
					out.print(key + "=\"" + properties.getProperty(key) + "\"");
					if (keyIterator.hasNext()) {
						out.print(",");
					}
				}
			}
		}
		out.print("]");
		out.println(";");
		if (showUnconnectedPort)
			printUnconnectedPorts(n);

	}

	protected void printUnconnectedPorts(INode n) {
		for (int i = 0; i < n.getOutputPorts().size(); ++i) {
			IPort out = n.getOutputPorts().get(i);

			if (out.getDegree() == 0) {
				printUnconnectedPort(out);
			} else {
				boolean isOutputPort = false;
				Iterator<? extends IEdge> edgeIterator = out.getEdges()
						.iterator();
				while (edgeIterator.hasNext() && !isOutputPort) {
					if (isGraphOutputEdge(edgeIterator.next()))
						isOutputPort = true;
				}
				if (isOutputPort)
					printUnconnectedPort(out);
			}
		}
		for (int i = 0; i < n.getInputPorts().size(); ++i) {
			IPort in = n.getInputPorts().get(i);
			if (in.getDegree() == 0) {
				printUnconnectedPort(in);
			}
		}
	}

	protected boolean internalEdge(IEdge e) {
		return e.getSinkPort().getNode().getGraph() == e.getSourcePort()
				.getNode().getGraph();
	}

	public void visitSubGraph(ISubGraphNode sn, Object arg) {
		indent();

		out.print("subgraph");
		out.print(indent + nodeLabelProvider.getClusterName(sn));
		out.print("{\n");
		crtLevel++;

		visitNode(sn, arg);

		for (INode n : sn.getNodes()) {
			n.accept(this, null);
		}
		Set<IEdge> externalEdges = new HashSet<IEdge>();
		for (IEdge e : sn.getEdges()) {
			if (internalEdge(e)) {
				e.accept(this, null);
			} else
				externalEdges.add(e);
		}
		crtLevel--;
		indent();
		out.print("}\n");
		if (drawExternalEdges) {
			for (IEdge e : externalEdges) {
				e.accept(this, null);
			}
		}
	}

	public void indent() {
		for (int i = 0; i <= crtLevel; ++i) {
			out.print(indent);
		}
	}

	protected String getVertexID(INode n) {
		return nodeLabelProvider.getNodeName(n);
	}

	public static class DotAttributs {
		public final static String STYLE = "style";
		public final static String NODE_SHAPE = "shape";
		public final static String NODE_COLOR = "fillcolor";

		public final static String EDGE_COLOR = "color";
		// if edge constraint set to false, it is NOT used for scheduling
		public final static String EDGE_CONSTRAINT = "constraint";
		public final static String ARROW_HEAD = "arrowhead";

		public static Set<String> nodeKeys() {
			Set<String> nodeKeys = new HashSet<String>();
			nodeKeys.add(NODE_SHAPE);
			nodeKeys.add(NODE_COLOR);
			nodeKeys.add(STYLE);
			return nodeKeys;
		}

		public static Set<String> edgeKeys() {
			Set<String> edgeKeys = new HashSet<String>();
			edgeKeys.add(EDGE_COLOR);
			edgeKeys.add(STYLE);
			edgeKeys.add(ARROW_HEAD);
			edgeKeys.add(EDGE_CONSTRAINT);
			return edgeKeys;
		}

	}

	/**
	 * @param attributsProvider
	 *            the attributsProvider to set
	 */
	public void setAttributsProvider(IAttributsProvider attributsProvider) {
		this.attributsProvider = attributsProvider;
	}

	public void visitCollapsedNode(ICollapsedNode cn, Object arg) {
		if (showCollapsedGraph) {
			printCollapsedSubgraph(cn, arg);

		} else
			visitNode(cn, arg);
	}

	protected void printCollapsedGraphPorts(ICollapsedNode cn) {

		for (int i = 0; i < cn.getInputPorts().size(); ++i) {
			IPort in = cn.getInputPorts().get(i);
			IPort nestedPort = cn.getMappedInputPort(in);
			if (nestedPort != null) {
				printUnconnectedPort(nestedPort);
			} else {
				// throw new RuntimeException();
			}
		}
		for (int i = 0; i < cn.getOutputPorts().size(); ++i) {
			IPort out = cn.getOutputPorts().get(i);
			IPort nestedPort = cn.getMappedOutputPort(out);
			if (nestedPort != null) {
				printUnconnectedPort(nestedPort);
			}
		}

	}

	private void printCollapsedSubgraph(ICollapsedNode sn, Object arg) {
		indent();

		out.print("subgraph");
		out.print(indent + nodeLabelProvider.getClusterName(sn));
		out.print("{\n");
		crtLevel++;

		visitNode(sn, arg);

		for (INode n : sn.getNodes()) {
			n.accept(this, null);
		}
		Set<IEdge> externalEdges = new HashSet<IEdge>();
		for (IEdge e : sn.getEdges()) {
			if (internalEdge(e)) {
				e.accept(this, null);
			} else
				externalEdges.add(e);
		}
		crtLevel--;
		indent();
		if (!showUnconnectedPort)
			printCollapsedGraphPorts(sn);
		out.print("}\n");
		if (drawExternalEdges) {
			for (IEdge e : externalEdges) {
				e.accept(this, null);
			}
		}

	}

	protected void printUnconnectedPort(IPort p) {

		printPort(p);
		boolean isInput = p.getNode().getInputPorts().contains(p);
		printUnconnectedPortConnection(getVertexID(p.getNode()), p, isInput);
		indent();

	}

	protected void printPort(IPort p) {
		// add an input node
		indent();
		out.print(getPortNodeId(p));
		out.print("[");
		out.print("style=\"filled\",shape=\"point\",height=\"0.2\", color=\""
				+ ColorPalette.getHexColor(Color.LIGHT_GRAY) + "\"];");
		out.println();
	}

	protected String getPortNodeId(IPort p) {
		String node = portNodesId.get(p);
		if (node == null) {
			boolean isInput = p.getNode().getInputPorts().contains(p);

			int i;
			if (isInput)
				i = p.getNode().getInputPorts().indexOf(p);
			else
				i = p.getNode().getOutputPorts().indexOf(p);
			StringBuffer sb = new StringBuffer();
			if (isInput) {
				sb.append("in_");
			} else {
				sb.append("out_");
			}

			sb.append(getVertexID(p.getNode())).append("_").append(i);
			node = sb.toString();
			portNodesId.put(p, node);
		}
		return node;
	}

	protected void printPortConnection(String connected, IPort p,
			boolean isInput) {

		if (isInput) {
			out.print(getPortNodeId(p) + connector + connected);
		} else {
			out.print(connected + connector + getPortNodeId(p));
		}
		out.println();
	}

	private void printUnconnectedPortConnection(String connected, IPort p,
			boolean isInput) {

		if (isInput) {
			out.print(getPortNodeId(p) + connector + connected);
		} else {
			out.print(connected + connector + getPortNodeId(p));
		}
		out.print("[color=\"" + ColorPalette.getHexColor(Color.DARK_GRAY)
				+ "\",style=\"" + DotAttributsProvider.STYLE_DASHED + "\"];");
		out.println();
	}
}
