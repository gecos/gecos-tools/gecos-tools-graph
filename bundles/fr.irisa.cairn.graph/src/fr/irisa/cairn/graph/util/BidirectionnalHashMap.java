package fr.irisa.cairn.graph.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BidirectionnalHashMap<K, V> extends HashMap<K, V>{
	private static final long serialVersionUID = -7045607171793714073L;
	private Map<V, K> valueToKey;
	

	public BidirectionnalHashMap(){
		valueToKey = new HashMap<V, K>();
	}
	
	public K getKey(V value){
		return valueToKey.get(value);
	}

	@Override
	public V put(K key, V value) {
		valueToKey.put(value, key);
		return super.put(key, value);
	}

	@Override
	public V remove(Object key) {
		V value = super.remove(key);
		if(value!=null)
			valueToKey.remove(value);
		return value;
	}
	
	public Set<V> valuesSet(){
		return valueToKey.keySet();
	}
	
	@Override
	public void clear() {
		valueToKey.clear();
		super.clear();
	}
}