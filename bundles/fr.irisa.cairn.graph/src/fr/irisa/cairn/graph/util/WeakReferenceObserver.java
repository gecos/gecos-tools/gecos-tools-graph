package fr.irisa.cairn.graph.util;

import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.implement.Node;
import fr.irisa.cairn.graph.observer.AbstractGraphStructureObserver;
import fr.irisa.cairn.graph.observer.IModificationNotification;

/**
 * Update all graph WeakReference caches when a structural modification occurs.
 * 
 * @author Antoine Floc'h - Initial contribution and API
 * 
 */
public class WeakReferenceObserver extends AbstractGraphStructureObserver {
	@Override
	protected void notifyAddEdge(IModificationNotification notif) {
		IEdge edge = (IEdge) notif.getFeature();
		clearEdgeWeakReferences(edge);
	}

	@Override
	protected void notifyRemoveEdge(IModificationNotification notif) {
		IEdge edge = (IEdge) notif.getFeature();
		clearEdgeWeakReferences(edge);
	}

	private void clearEdgeWeakReferences(IEdge edge) {
		if (edge.getSinkPort() != null && edge.getSourcePort() != null) {
			Node source = (Node) edge.getSourcePort().getNode();
			source.getSuccessorsWeakReference().clear();
			Node dest = (Node) edge.getSinkPort().getNode();
			dest.getSuccessorsWeakReference().clear();
		}
	}
}
