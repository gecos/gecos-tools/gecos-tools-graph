/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fr.irisa.cairn.graph;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.irisa.cairn.graph.GraphFactory
 * @model kind="package"
 * @generated
 */
public interface GraphPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "graph";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "fr.irisa.cairn.graph";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fr.irisa.cairn.graph";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GraphPackage eINSTANCE = fr.irisa.cairn.graph.emf.implement.GraphPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.IEdge <em>IEdge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.IEdge
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIEdge()
	 * @generated
	 */
	int IEDGE = 0;

	/**
	 * The number of structural features of the '<em>IEdge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Reconnect Source Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE___RECONNECT_SOURCE_PORT__IPORT = 0;

	/**
	 * The operation id for the '<em>Reconnect Sink Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE___RECONNECT_SINK_PORT__IPORT = 1;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE___ACCEPT__IGRAPHVISITOR_OBJECT = 2;

	/**
	 * The operation id for the '<em>Get Source Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE___GET_SOURCE_PORT = 3;

	/**
	 * The operation id for the '<em>Get Sink Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE___GET_SINK_PORT = 4;

	/**
	 * The number of operations of the '<em>IEdge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IEDGE_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.IGraph <em>IGraph</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.IGraph
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIGraph()
	 * @generated
	 */
	int IGRAPH = 1;

	/**
	 * The number of structural features of the '<em>IGraph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Edge</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___ADD_EDGE__IEDGE = 0;

	/**
	 * The operation id for the '<em>Add Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___ADD_NODE__INODE = 1;

	/**
	 * The operation id for the '<em>Remove Edge</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___REMOVE_EDGE__IEDGE = 2;

	/**
	 * The operation id for the '<em>Remove Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___REMOVE_NODE__INODE = 3;

	/**
	 * The operation id for the '<em>Is Directed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___IS_DIRECTED = 4;

	/**
	 * The operation id for the '<em>Get Nodes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___GET_NODES = 5;

	/**
	 * The operation id for the '<em>Get Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___GET_EDGES = 6;

	/**
	 * The operation id for the '<em>Get External Inputs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___GET_EXTERNAL_INPUTS = 7;

	/**
	 * The operation id for the '<em>Get External Outputs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH___GET_EXTERNAL_OUTPUTS = 8;

	/**
	 * The number of operations of the '<em>IGraph</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.IGraphFactory <em>IGraph Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.IGraphFactory
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIGraphFactory()
	 * @generated
	 */
	int IGRAPH_FACTORY = 2;

	/**
	 * The number of structural features of the '<em>IGraph Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Create Subgraph</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY___CREATE_SUBGRAPH__BOOLEAN = 0;

	/**
	 * The operation id for the '<em>Create Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY___CREATE_NODE = 1;

	/**
	 * The operation id for the '<em>Create Graph</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY___CREATE_GRAPH__BOOLEAN = 2;

	/**
	 * The operation id for the '<em>Connect</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY___CONNECT__IPORT_IPORT = 3;

	/**
	 * The operation id for the '<em>Create Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY___CREATE_PORT = 4;

	/**
	 * The number of operations of the '<em>IGraph Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_FACTORY_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.IGraphVisitor <em>IGraph Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.IGraphVisitor
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIGraphVisitor()
	 * @generated
	 */
	int IGRAPH_VISITOR = 3;

	/**
	 * The number of structural features of the '<em>IGraph Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_VISITOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Visit Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_VISITOR___VISIT_NODE__INODE_OBJECT = 0;

	/**
	 * The operation id for the '<em>Visit Edge</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_VISITOR___VISIT_EDGE__IEDGE_OBJECT = 1;

	/**
	 * The operation id for the '<em>Visit Sub Graph</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_VISITOR___VISIT_SUB_GRAPH__ISUBGRAPHNODE_OBJECT = 2;

	/**
	 * The number of operations of the '<em>IGraph Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IGRAPH_VISITOR_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.INode <em>INode</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.INode
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getINode()
	 * @generated
	 */
	int INODE = 4;

	/**
	 * The number of structural features of the '<em>INode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Input Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___ADD_INPUT_PORT__IPORT = 0;

	/**
	 * The operation id for the '<em>Add Output Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___ADD_OUTPUT_PORT__IPORT = 1;

	/**
	 * The operation id for the '<em>Is Predecessor Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___IS_PREDECESSOR_OF__INODE = 2;

	/**
	 * The operation id for the '<em>Is Successor To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___IS_SUCCESSOR_TO__INODE = 3;

	/**
	 * The operation id for the '<em>Is Connected To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___IS_CONNECTED_TO__INODE = 4;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___ACCEPT__IGRAPHVISITOR_OBJECT = 5;

	/**
	 * The operation id for the '<em>Get Predecessors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_PREDECESSORS = 6;

	/**
	 * The operation id for the '<em>Get Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_SUCCESSORS = 7;

	/**
	 * The operation id for the '<em>Get Input Ports</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_INPUT_PORTS = 8;

	/**
	 * The operation id for the '<em>Get Output Ports</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_OUTPUT_PORTS = 9;

	/**
	 * The operation id for the '<em>Get Incoming Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_INCOMING_EDGES = 10;

	/**
	 * The operation id for the '<em>Get Outgoing Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_OUTGOING_EDGES = 11;

	/**
	 * The operation id for the '<em>Get Degree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_DEGREE = 12;

	/**
	 * The operation id for the '<em>Get In Degree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_IN_DEGREE = 13;

	/**
	 * The operation id for the '<em>Get Out Degree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_OUT_DEGREE = 14;

	/**
	 * The operation id for the '<em>Get Graph</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE___GET_GRAPH = 15;

	/**
	 * The number of operations of the '<em>INode</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INODE_OPERATION_COUNT = 16;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.IPort <em>IPort</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.IPort
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIPort()
	 * @generated
	 */
	int IPORT = 5;

	/**
	 * The number of structural features of the '<em>IPort</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT___GET_NODE = 0;

	/**
	 * The operation id for the '<em>Get Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT___GET_EDGES = 1;

	/**
	 * The number of operations of the '<em>IPort</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPORT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.ISubGraphNode <em>ISub Graph Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.ISubGraphNode
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getISubGraphNode()
	 * @generated
	 */
	int ISUB_GRAPH_NODE = 6;

	/**
	 * The number of structural features of the '<em>ISub Graph Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE_FEATURE_COUNT = IGRAPH_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Add Edge</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___ADD_EDGE__IEDGE = IGRAPH___ADD_EDGE__IEDGE;

	/**
	 * The operation id for the '<em>Add Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___ADD_NODE__INODE = IGRAPH___ADD_NODE__INODE;

	/**
	 * The operation id for the '<em>Remove Edge</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___REMOVE_EDGE__IEDGE = IGRAPH___REMOVE_EDGE__IEDGE;

	/**
	 * The operation id for the '<em>Remove Node</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___REMOVE_NODE__INODE = IGRAPH___REMOVE_NODE__INODE;

	/**
	 * The operation id for the '<em>Is Directed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___IS_DIRECTED = IGRAPH___IS_DIRECTED;

	/**
	 * The operation id for the '<em>Get Nodes</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_NODES = IGRAPH___GET_NODES;

	/**
	 * The operation id for the '<em>Get Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_EDGES = IGRAPH___GET_EDGES;

	/**
	 * The operation id for the '<em>Get External Inputs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_EXTERNAL_INPUTS = IGRAPH___GET_EXTERNAL_INPUTS;

	/**
	 * The operation id for the '<em>Get External Outputs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_EXTERNAL_OUTPUTS = IGRAPH___GET_EXTERNAL_OUTPUTS;

	/**
	 * The operation id for the '<em>Add Input Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___ADD_INPUT_PORT__IPORT = IGRAPH_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Add Output Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___ADD_OUTPUT_PORT__IPORT = IGRAPH_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Predecessor Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___IS_PREDECESSOR_OF__INODE = IGRAPH_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Successor To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___IS_SUCCESSOR_TO__INODE = IGRAPH_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Connected To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___IS_CONNECTED_TO__INODE = IGRAPH_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___ACCEPT__IGRAPHVISITOR_OBJECT = IGRAPH_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Predecessors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_PREDECESSORS = IGRAPH_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Successors</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_SUCCESSORS = IGRAPH_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Input Ports</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_INPUT_PORTS = IGRAPH_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Get Output Ports</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_OUTPUT_PORTS = IGRAPH_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Incoming Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_INCOMING_EDGES = IGRAPH_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Get Outgoing Edges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_OUTGOING_EDGES = IGRAPH_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>Get Degree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_DEGREE = IGRAPH_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>Get In Degree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_IN_DEGREE = IGRAPH_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>Get Out Degree</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_OUT_DEGREE = IGRAPH_OPERATION_COUNT + 14;

	/**
	 * The operation id for the '<em>Get Graph</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE___GET_GRAPH = IGRAPH_OPERATION_COUNT + 15;

	/**
	 * The number of operations of the '<em>ISub Graph Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISUB_GRAPH_NODE_OPERATION_COUNT = IGRAPH_OPERATION_COUNT + 16;

	/**
	 * The meta object id for the '{@link fr.irisa.cairn.graph.ICollapsedNode <em>ICollapsed Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.irisa.cairn.graph.ICollapsedNode
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getICollapsedNode()
	 * @generated
	 */
	int ICOLLAPSED_NODE = 7;

	/**
	 * The number of structural features of the '<em>ICollapsed Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOLLAPSED_NODE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>ICollapsed Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOLLAPSED_NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '<em>Set</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Set
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getSet()
	 * @generated
	 */
	int SET = 8;

	/**
	 * The meta object id for the '<em>List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getList()
	 * @generated
	 */
	int LIST = 9;


	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.IEdge <em>IEdge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IEdge</em>'.
	 * @see fr.irisa.cairn.graph.IEdge
	 * @model instanceClass="fr.irisa.cairn.graph.IEdge"
	 * @generated
	 */
	EClass getIEdge();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IEdge#reconnectSourcePort(fr.irisa.cairn.graph.IPort) <em>Reconnect Source Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reconnect Source Port</em>' operation.
	 * @see fr.irisa.cairn.graph.IEdge#reconnectSourcePort(fr.irisa.cairn.graph.IPort)
	 * @generated
	 */
	EOperation getIEdge__ReconnectSourcePort__IPort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IEdge#reconnectSinkPort(fr.irisa.cairn.graph.IPort) <em>Reconnect Sink Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reconnect Sink Port</em>' operation.
	 * @see fr.irisa.cairn.graph.IEdge#reconnectSinkPort(fr.irisa.cairn.graph.IPort)
	 * @generated
	 */
	EOperation getIEdge__ReconnectSinkPort__IPort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IEdge#accept(fr.irisa.cairn.graph.IGraphVisitor, java.lang.Object) <em>Accept</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Accept</em>' operation.
	 * @see fr.irisa.cairn.graph.IEdge#accept(fr.irisa.cairn.graph.IGraphVisitor, java.lang.Object)
	 * @generated
	 */
	EOperation getIEdge__Accept__IGraphVisitor_Object();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IEdge#getSourcePort() <em>Get Source Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Source Port</em>' operation.
	 * @see fr.irisa.cairn.graph.IEdge#getSourcePort()
	 * @generated
	 */
	EOperation getIEdge__GetSourcePort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IEdge#getSinkPort() <em>Get Sink Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Sink Port</em>' operation.
	 * @see fr.irisa.cairn.graph.IEdge#getSinkPort()
	 * @generated
	 */
	EOperation getIEdge__GetSinkPort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.IGraph <em>IGraph</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IGraph</em>'.
	 * @see fr.irisa.cairn.graph.IGraph
	 * @model instanceClass="fr.irisa.cairn.graph.IGraph"
	 * @generated
	 */
	EClass getIGraph();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#addEdge(fr.irisa.cairn.graph.IEdge) <em>Add Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Edge</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#addEdge(fr.irisa.cairn.graph.IEdge)
	 * @generated
	 */
	EOperation getIGraph__AddEdge__IEdge();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#addNode(fr.irisa.cairn.graph.INode) <em>Add Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Node</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#addNode(fr.irisa.cairn.graph.INode)
	 * @generated
	 */
	EOperation getIGraph__AddNode__INode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#removeEdge(fr.irisa.cairn.graph.IEdge) <em>Remove Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Edge</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#removeEdge(fr.irisa.cairn.graph.IEdge)
	 * @generated
	 */
	EOperation getIGraph__RemoveEdge__IEdge();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#removeNode(fr.irisa.cairn.graph.INode) <em>Remove Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Node</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#removeNode(fr.irisa.cairn.graph.INode)
	 * @generated
	 */
	EOperation getIGraph__RemoveNode__INode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#isDirected() <em>Is Directed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Directed</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#isDirected()
	 * @generated
	 */
	EOperation getIGraph__IsDirected();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#getNodes() <em>Get Nodes</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Nodes</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#getNodes()
	 * @generated
	 */
	EOperation getIGraph__GetNodes();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#getEdges() <em>Get Edges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Edges</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#getEdges()
	 * @generated
	 */
	EOperation getIGraph__GetEdges();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#getExternalInputs() <em>Get External Inputs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get External Inputs</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#getExternalInputs()
	 * @generated
	 */
	EOperation getIGraph__GetExternalInputs();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraph#getExternalOutputs() <em>Get External Outputs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get External Outputs</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraph#getExternalOutputs()
	 * @generated
	 */
	EOperation getIGraph__GetExternalOutputs();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.IGraphFactory <em>IGraph Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IGraph Factory</em>'.
	 * @see fr.irisa.cairn.graph.IGraphFactory
	 * @model instanceClass="fr.irisa.cairn.graph.IGraphFactory"
	 * @generated
	 */
	EClass getIGraphFactory();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphFactory#createSubgraph(boolean) <em>Create Subgraph</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Subgraph</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphFactory#createSubgraph(boolean)
	 * @generated
	 */
	EOperation getIGraphFactory__CreateSubgraph__boolean();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphFactory#createNode() <em>Create Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Node</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphFactory#createNode()
	 * @generated
	 */
	EOperation getIGraphFactory__CreateNode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphFactory#createGraph(boolean) <em>Create Graph</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Graph</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphFactory#createGraph(boolean)
	 * @generated
	 */
	EOperation getIGraphFactory__CreateGraph__boolean();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphFactory#connect(fr.irisa.cairn.graph.IPort, fr.irisa.cairn.graph.IPort) <em>Connect</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Connect</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphFactory#connect(fr.irisa.cairn.graph.IPort, fr.irisa.cairn.graph.IPort)
	 * @generated
	 */
	EOperation getIGraphFactory__Connect__IPort_IPort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphFactory#createPort() <em>Create Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Port</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphFactory#createPort()
	 * @generated
	 */
	EOperation getIGraphFactory__CreatePort();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.IGraphVisitor <em>IGraph Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IGraph Visitor</em>'.
	 * @see fr.irisa.cairn.graph.IGraphVisitor
	 * @model instanceClass="fr.irisa.cairn.graph.IGraphVisitor"
	 * @generated
	 */
	EClass getIGraphVisitor();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphVisitor#visitNode(fr.irisa.cairn.graph.INode, java.lang.Object) <em>Visit Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Visit Node</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphVisitor#visitNode(fr.irisa.cairn.graph.INode, java.lang.Object)
	 * @generated
	 */
	EOperation getIGraphVisitor__VisitNode__INode_Object();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphVisitor#visitEdge(fr.irisa.cairn.graph.IEdge, java.lang.Object) <em>Visit Edge</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Visit Edge</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphVisitor#visitEdge(fr.irisa.cairn.graph.IEdge, java.lang.Object)
	 * @generated
	 */
	EOperation getIGraphVisitor__VisitEdge__IEdge_Object();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IGraphVisitor#visitSubGraph(fr.irisa.cairn.graph.ISubGraphNode, java.lang.Object) <em>Visit Sub Graph</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Visit Sub Graph</em>' operation.
	 * @see fr.irisa.cairn.graph.IGraphVisitor#visitSubGraph(fr.irisa.cairn.graph.ISubGraphNode, java.lang.Object)
	 * @generated
	 */
	EOperation getIGraphVisitor__VisitSubGraph__ISubGraphNode_Object();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.INode <em>INode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>INode</em>'.
	 * @see fr.irisa.cairn.graph.INode
	 * @model instanceClass="fr.irisa.cairn.graph.INode"
	 * @generated
	 */
	EClass getINode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#addInputPort(fr.irisa.cairn.graph.IPort) <em>Add Input Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Input Port</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#addInputPort(fr.irisa.cairn.graph.IPort)
	 * @generated
	 */
	EOperation getINode__AddInputPort__IPort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#addOutputPort(fr.irisa.cairn.graph.IPort) <em>Add Output Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Output Port</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#addOutputPort(fr.irisa.cairn.graph.IPort)
	 * @generated
	 */
	EOperation getINode__AddOutputPort__IPort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#isPredecessorOf(fr.irisa.cairn.graph.INode) <em>Is Predecessor Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Predecessor Of</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#isPredecessorOf(fr.irisa.cairn.graph.INode)
	 * @generated
	 */
	EOperation getINode__IsPredecessorOf__INode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#isSuccessorTo(fr.irisa.cairn.graph.INode) <em>Is Successor To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Successor To</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#isSuccessorTo(fr.irisa.cairn.graph.INode)
	 * @generated
	 */
	EOperation getINode__IsSuccessorTo__INode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#isConnectedTo(fr.irisa.cairn.graph.INode) <em>Is Connected To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Connected To</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#isConnectedTo(fr.irisa.cairn.graph.INode)
	 * @generated
	 */
	EOperation getINode__IsConnectedTo__INode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#accept(fr.irisa.cairn.graph.IGraphVisitor, java.lang.Object) <em>Accept</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Accept</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#accept(fr.irisa.cairn.graph.IGraphVisitor, java.lang.Object)
	 * @generated
	 */
	EOperation getINode__Accept__IGraphVisitor_Object();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getPredecessors() <em>Get Predecessors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Predecessors</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getPredecessors()
	 * @generated
	 */
	EOperation getINode__GetPredecessors();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getSuccessors() <em>Get Successors</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Successors</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getSuccessors()
	 * @generated
	 */
	EOperation getINode__GetSuccessors();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getInputPorts() <em>Get Input Ports</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Input Ports</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getInputPorts()
	 * @generated
	 */
	EOperation getINode__GetInputPorts();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getOutputPorts() <em>Get Output Ports</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Output Ports</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getOutputPorts()
	 * @generated
	 */
	EOperation getINode__GetOutputPorts();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getIncomingEdges() <em>Get Incoming Edges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Incoming Edges</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getIncomingEdges()
	 * @generated
	 */
	EOperation getINode__GetIncomingEdges();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getOutgoingEdges() <em>Get Outgoing Edges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Outgoing Edges</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getOutgoingEdges()
	 * @generated
	 */
	EOperation getINode__GetOutgoingEdges();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getDegree() <em>Get Degree</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Degree</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getDegree()
	 * @generated
	 */
	EOperation getINode__GetDegree();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getInDegree() <em>Get In Degree</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get In Degree</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getInDegree()
	 * @generated
	 */
	EOperation getINode__GetInDegree();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getOutDegree() <em>Get Out Degree</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Out Degree</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getOutDegree()
	 * @generated
	 */
	EOperation getINode__GetOutDegree();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.INode#getGraph() <em>Get Graph</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Graph</em>' operation.
	 * @see fr.irisa.cairn.graph.INode#getGraph()
	 * @generated
	 */
	EOperation getINode__GetGraph();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.IPort <em>IPort</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPort</em>'.
	 * @see fr.irisa.cairn.graph.IPort
	 * @model instanceClass="fr.irisa.cairn.graph.IPort"
	 * @generated
	 */
	EClass getIPort();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IPort#getNode() <em>Get Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Node</em>' operation.
	 * @see fr.irisa.cairn.graph.IPort#getNode()
	 * @generated
	 */
	EOperation getIPort__GetNode();

	/**
	 * Returns the meta object for the '{@link fr.irisa.cairn.graph.IPort#getEdges() <em>Get Edges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Edges</em>' operation.
	 * @see fr.irisa.cairn.graph.IPort#getEdges()
	 * @generated
	 */
	EOperation getIPort__GetEdges();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.ISubGraphNode <em>ISub Graph Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISub Graph Node</em>'.
	 * @see fr.irisa.cairn.graph.ISubGraphNode
	 * @model instanceClass="fr.irisa.cairn.graph.ISubGraphNode" superTypes="fr.irisa.cairn.graph.IGraph fr.irisa.cairn.graph.INode"
	 * @generated
	 */
	EClass getISubGraphNode();

	/**
	 * Returns the meta object for class '{@link fr.irisa.cairn.graph.ICollapsedNode <em>ICollapsed Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ICollapsed Node</em>'.
	 * @see fr.irisa.cairn.graph.ICollapsedNode
	 * @model instanceClass="fr.irisa.cairn.graph.ICollapsedNode"
	 * @generated
	 */
	EClass getICollapsedNode();

	/**
	 * Returns the meta object for data type '{@link java.util.Set <em>Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Set</em>'.
	 * @see java.util.Set
	 * @model instanceClass="java.util.Set" typeParameters="T"
	 * @generated
	 */
	EDataType getSet();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>List</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List" typeParameters="T"
	 * @generated
	 */
	EDataType getList();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GraphFactory getGraphFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.IEdge <em>IEdge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.IEdge
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIEdge()
		 * @generated
		 */
		EClass IEDGE = eINSTANCE.getIEdge();

		/**
		 * The meta object literal for the '<em><b>Reconnect Source Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEDGE___RECONNECT_SOURCE_PORT__IPORT = eINSTANCE.getIEdge__ReconnectSourcePort__IPort();

		/**
		 * The meta object literal for the '<em><b>Reconnect Sink Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEDGE___RECONNECT_SINK_PORT__IPORT = eINSTANCE.getIEdge__ReconnectSinkPort__IPort();

		/**
		 * The meta object literal for the '<em><b>Accept</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEDGE___ACCEPT__IGRAPHVISITOR_OBJECT = eINSTANCE.getIEdge__Accept__IGraphVisitor_Object();

		/**
		 * The meta object literal for the '<em><b>Get Source Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEDGE___GET_SOURCE_PORT = eINSTANCE.getIEdge__GetSourcePort();

		/**
		 * The meta object literal for the '<em><b>Get Sink Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IEDGE___GET_SINK_PORT = eINSTANCE.getIEdge__GetSinkPort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.IGraph <em>IGraph</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.IGraph
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIGraph()
		 * @generated
		 */
		EClass IGRAPH = eINSTANCE.getIGraph();

		/**
		 * The meta object literal for the '<em><b>Add Edge</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___ADD_EDGE__IEDGE = eINSTANCE.getIGraph__AddEdge__IEdge();

		/**
		 * The meta object literal for the '<em><b>Add Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___ADD_NODE__INODE = eINSTANCE.getIGraph__AddNode__INode();

		/**
		 * The meta object literal for the '<em><b>Remove Edge</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___REMOVE_EDGE__IEDGE = eINSTANCE.getIGraph__RemoveEdge__IEdge();

		/**
		 * The meta object literal for the '<em><b>Remove Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___REMOVE_NODE__INODE = eINSTANCE.getIGraph__RemoveNode__INode();

		/**
		 * The meta object literal for the '<em><b>Is Directed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___IS_DIRECTED = eINSTANCE.getIGraph__IsDirected();

		/**
		 * The meta object literal for the '<em><b>Get Nodes</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___GET_NODES = eINSTANCE.getIGraph__GetNodes();

		/**
		 * The meta object literal for the '<em><b>Get Edges</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___GET_EDGES = eINSTANCE.getIGraph__GetEdges();

		/**
		 * The meta object literal for the '<em><b>Get External Inputs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___GET_EXTERNAL_INPUTS = eINSTANCE.getIGraph__GetExternalInputs();

		/**
		 * The meta object literal for the '<em><b>Get External Outputs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH___GET_EXTERNAL_OUTPUTS = eINSTANCE.getIGraph__GetExternalOutputs();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.IGraphFactory <em>IGraph Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.IGraphFactory
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIGraphFactory()
		 * @generated
		 */
		EClass IGRAPH_FACTORY = eINSTANCE.getIGraphFactory();

		/**
		 * The meta object literal for the '<em><b>Create Subgraph</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_FACTORY___CREATE_SUBGRAPH__BOOLEAN = eINSTANCE.getIGraphFactory__CreateSubgraph__boolean();

		/**
		 * The meta object literal for the '<em><b>Create Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_FACTORY___CREATE_NODE = eINSTANCE.getIGraphFactory__CreateNode();

		/**
		 * The meta object literal for the '<em><b>Create Graph</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_FACTORY___CREATE_GRAPH__BOOLEAN = eINSTANCE.getIGraphFactory__CreateGraph__boolean();

		/**
		 * The meta object literal for the '<em><b>Connect</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_FACTORY___CONNECT__IPORT_IPORT = eINSTANCE.getIGraphFactory__Connect__IPort_IPort();

		/**
		 * The meta object literal for the '<em><b>Create Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_FACTORY___CREATE_PORT = eINSTANCE.getIGraphFactory__CreatePort();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.IGraphVisitor <em>IGraph Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.IGraphVisitor
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIGraphVisitor()
		 * @generated
		 */
		EClass IGRAPH_VISITOR = eINSTANCE.getIGraphVisitor();

		/**
		 * The meta object literal for the '<em><b>Visit Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_VISITOR___VISIT_NODE__INODE_OBJECT = eINSTANCE.getIGraphVisitor__VisitNode__INode_Object();

		/**
		 * The meta object literal for the '<em><b>Visit Edge</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_VISITOR___VISIT_EDGE__IEDGE_OBJECT = eINSTANCE.getIGraphVisitor__VisitEdge__IEdge_Object();

		/**
		 * The meta object literal for the '<em><b>Visit Sub Graph</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IGRAPH_VISITOR___VISIT_SUB_GRAPH__ISUBGRAPHNODE_OBJECT = eINSTANCE.getIGraphVisitor__VisitSubGraph__ISubGraphNode_Object();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.INode <em>INode</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.INode
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getINode()
		 * @generated
		 */
		EClass INODE = eINSTANCE.getINode();

		/**
		 * The meta object literal for the '<em><b>Add Input Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___ADD_INPUT_PORT__IPORT = eINSTANCE.getINode__AddInputPort__IPort();

		/**
		 * The meta object literal for the '<em><b>Add Output Port</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___ADD_OUTPUT_PORT__IPORT = eINSTANCE.getINode__AddOutputPort__IPort();

		/**
		 * The meta object literal for the '<em><b>Is Predecessor Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___IS_PREDECESSOR_OF__INODE = eINSTANCE.getINode__IsPredecessorOf__INode();

		/**
		 * The meta object literal for the '<em><b>Is Successor To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___IS_SUCCESSOR_TO__INODE = eINSTANCE.getINode__IsSuccessorTo__INode();

		/**
		 * The meta object literal for the '<em><b>Is Connected To</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___IS_CONNECTED_TO__INODE = eINSTANCE.getINode__IsConnectedTo__INode();

		/**
		 * The meta object literal for the '<em><b>Accept</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___ACCEPT__IGRAPHVISITOR_OBJECT = eINSTANCE.getINode__Accept__IGraphVisitor_Object();

		/**
		 * The meta object literal for the '<em><b>Get Predecessors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_PREDECESSORS = eINSTANCE.getINode__GetPredecessors();

		/**
		 * The meta object literal for the '<em><b>Get Successors</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_SUCCESSORS = eINSTANCE.getINode__GetSuccessors();

		/**
		 * The meta object literal for the '<em><b>Get Input Ports</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_INPUT_PORTS = eINSTANCE.getINode__GetInputPorts();

		/**
		 * The meta object literal for the '<em><b>Get Output Ports</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_OUTPUT_PORTS = eINSTANCE.getINode__GetOutputPorts();

		/**
		 * The meta object literal for the '<em><b>Get Incoming Edges</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_INCOMING_EDGES = eINSTANCE.getINode__GetIncomingEdges();

		/**
		 * The meta object literal for the '<em><b>Get Outgoing Edges</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_OUTGOING_EDGES = eINSTANCE.getINode__GetOutgoingEdges();

		/**
		 * The meta object literal for the '<em><b>Get Degree</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_DEGREE = eINSTANCE.getINode__GetDegree();

		/**
		 * The meta object literal for the '<em><b>Get In Degree</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_IN_DEGREE = eINSTANCE.getINode__GetInDegree();

		/**
		 * The meta object literal for the '<em><b>Get Out Degree</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_OUT_DEGREE = eINSTANCE.getINode__GetOutDegree();

		/**
		 * The meta object literal for the '<em><b>Get Graph</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INODE___GET_GRAPH = eINSTANCE.getINode__GetGraph();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.IPort <em>IPort</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.IPort
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getIPort()
		 * @generated
		 */
		EClass IPORT = eINSTANCE.getIPort();

		/**
		 * The meta object literal for the '<em><b>Get Node</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IPORT___GET_NODE = eINSTANCE.getIPort__GetNode();

		/**
		 * The meta object literal for the '<em><b>Get Edges</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IPORT___GET_EDGES = eINSTANCE.getIPort__GetEdges();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.ISubGraphNode <em>ISub Graph Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.ISubGraphNode
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getISubGraphNode()
		 * @generated
		 */
		EClass ISUB_GRAPH_NODE = eINSTANCE.getISubGraphNode();

		/**
		 * The meta object literal for the '{@link fr.irisa.cairn.graph.ICollapsedNode <em>ICollapsed Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.irisa.cairn.graph.ICollapsedNode
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getICollapsedNode()
		 * @generated
		 */
		EClass ICOLLAPSED_NODE = eINSTANCE.getICollapsedNode();

		/**
		 * The meta object literal for the '<em>Set</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Set
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getSet()
		 * @generated
		 */
		EDataType SET = eINSTANCE.getSet();

		/**
		 * The meta object literal for the '<em>List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see fr.irisa.cairn.graph.emf.implement.GraphPackageImpl#getList()
		 * @generated
		 */
		EDataType LIST = eINSTANCE.getList();

	}

} //GraphPackage
