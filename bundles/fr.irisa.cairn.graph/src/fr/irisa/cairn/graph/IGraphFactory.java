package fr.irisa.cairn.graph;

import fr.irisa.cairn.graph.implement.GraphFactory;

/**
 * @model
 */
public interface IGraphFactory {

	public static final IGraphFactory instance = new GraphFactory();

	/**
	 * @model default=""
	 */
	public ISubGraphNode createSubgraph(boolean directed);

	/**
	 * @model default=""
	 */
	public INode createNode();

	public INode createNode(int nbInPorts, int nbOutPorts);

	public ICollapsedNode createCollapsedNode(boolean directed);
	
	/**
	 * @model default=""
	 */
	public IGraph createGraph(boolean directed);

	/**
	 * Default connection between two nodes.
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	public IEdge connect(INode source, INode sink);

	/**
	 * Connect two ports and add the builded link to the nodes graphs
	 * 
	 * @model default=""
	 * 
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	public IEdge connect(IPort source, IPort sink);

	/**
	 * @model default=""
	 */
	public IPort createPort();

	public abstract IEdge createEdge(IPort source, IPort sink);

}
