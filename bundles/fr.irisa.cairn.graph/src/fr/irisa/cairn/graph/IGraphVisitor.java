package fr.irisa.cairn.graph;

/**
 * @model
 */
public interface IGraphVisitor {
	/**
	 * @model default=""
	 */
	public void visitNode(INode n, Object arg);

	/**
	 * @model default=""
	 */
	public void visitEdge(IEdge e, Object arg);

	/**
	 * @model default=""
	 */
	public void visitSubGraph(ISubGraphNode sn, Object arg);
	
	public void visitCollapsedNode(ICollapsedNode cn, Object arg);
}
