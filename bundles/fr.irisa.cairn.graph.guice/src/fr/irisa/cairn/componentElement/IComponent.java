package fr.irisa.cairn.componentElement;


public interface IComponent {

	/**
	 * install a service to the component
	 * 
	 * @param serviceModule
	 *            the service to install
	 */
	public void addService(final ServiceModule serviceModule);

	/**
	 * get a service of the component
	 * 
	 * @param <T>
	 * @param serviceClassName
	 * @return
	 */
	public <T extends IComponentService> T getService(final Class<T> serviceClassName);

	/**
	 * install services to the component
	 * 
	 * @param servicesModules
	 *            services to install
	 */
	public void installServices(final Iterable<ServiceModule> servicesModules);

	/**
	 * install services to the component
	 * 
	 * @param servicesModules
	 *            services to install
	 */
	public void installServices(final ServiceModule... servicesModules);

	/**
	 * replace a service with an other service
	 * 
	 * @param oldServiceModule
	 *            the old service to remove
	 * @param newServiceModule
	 *            the new service to install
	 * @throws IllegalArgumentException
	 *             if the component does not have the old service an exception
	 *             is launched
	 */
	public void replaceService(final ServiceModule oldServiceModule,
			final ServiceModule newServiceModule);

	/**
	 * uninstall a service
	 * 
	 * @param serviceModule
	 *            service to uninstall
	 * @throws IllegalArgumentException
	 *             id the component does not have the service
	 */
	public void unInstallService(final ServiceModule serviceModule);

	/**
	 * uninstall some services
	 * 
	 * @param serviceModules
	 *            services to unsinstall
	 * @throws IllegalArgumentException
	 *             if one of this service is not install in the component
	 */
	public void unInstallServices(final ServiceModule... serviceModules);
	
	
	public <T, U extends T> U getImplementation(Class<T> clazz);
}
