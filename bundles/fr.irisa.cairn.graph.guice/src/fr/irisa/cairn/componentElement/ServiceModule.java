package fr.irisa.cairn.componentElement;

import com.google.inject.AbstractModule;

/**
 * abstract class which can make a binding between an interface service and his port implementation 
 * @author jguidoux
 *
 */
public abstract class ServiceModule extends AbstractModule {

}
