package fr.irisa.cairn.componentElement;

import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.Set;

import com.google.common.collect.Sets;
import com.google.inject.Binding;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;

/**
 * Abstract class which describe a component providing services. A service or a
 * set of services are installed in the component by the installation of some
 * {@link ServiceModule}.
 * 
 * @author jguidoux, Antoine Floc'h
 * 
 */
public class AbstractComponent {

	private final Set<ServiceModule> modules;
	private Injector injector;

	public AbstractComponent() {
		this.modules = Sets.newHashSet();
	}

	/**
	 * Get the installed instance of a module if existing.
	 * 
	 * @param moduleClass
	 * @return the module or null if the the component has no module of this
	 *         class
	 */
	public ServiceModule getModule(
			final Class<? extends ServiceModule> moduleClass) {
		ServiceModule moduleRes = null;
		boolean find = false;
		for (final Iterator<ServiceModule> iterator = this.modules.iterator(); !find
				&& iterator.hasNext();) {
			moduleRes = iterator.next();
			find = moduleRes.getClass().equals(moduleClass);

		}
		return find ? moduleRes : null;
	}

	/**
	 * Get the installed instance of a service class. 
	 * 
	 * @param <T>
	 * @param serviceClassName
	 * @return
	 */
	public <T> T getService(final Class<T> serviceClassName) {
		return this.injector.getInstance(serviceClassName);
	}

	/**
	 * Install a service in the component.
	 * 
	 * @param serviceModule
	 *            the service to install
	 */
	public void installModule(final ServiceModule serviceModule) {
		this.modules.add(serviceModule);
		this.injector = Guice.createInjector(this.modules);
		// Map<Key<?>, Binding<?>> bindings = this.injector.getBindings();
	}

	/**
	 * Install services in the component
	 * 
	 * @param servicesModules
	 *            services to install
	 */
	public void installModules(final Iterable<ServiceModule> servicesModules) {
		for (final ServiceModule serviceModule : servicesModules) {
			this.modules.add(serviceModule);
		}
		this.injector = Guice.createInjector(servicesModules);
	}

	/**
	 * Install services in the component
	 * 
	 * @param servicesModules
	 *            services to install
	 */
	public void installModules(final ServiceModule... servicesModules) {
		for (final ServiceModule serviceModule : servicesModules) {
			this.modules.add(serviceModule);
		}
		this.injector = Guice.createInjector(servicesModules);
	}

	/**
	 * Replace a module instance by a new one.
	 * 
	 * @param oldServiceModule
	 *            the old service to remove
	 * @param newServiceModule
	 *            the new service to install
	 * @throws IllegalArgumentException
	 *             if the component does not have the old service an exception
	 *             is thrown
	 */
	public void replaceModule(final ServiceModule oldServiceModule,
			final ServiceModule newServiceModule) {
		if (this.modules.remove(oldServiceModule)) {
			this.modules.add(newServiceModule);
			this.injector = Guice.createInjector(this.modules);
		} else {
			throw new IllegalArgumentException(
					"ComponentException ERROR : serviceModule "
							+ oldServiceModule
							+ "does not exist, so it can't be removed");
		}
	}

	/**
	 * Replace the installed instance of a module class by a new one.
	 * @param oldServiceModuleClass
	 * @param newServiceModule
	 */
	public void replaceModule(
			final Class<? extends ServiceModule> oldServiceModuleClass,
			final ServiceModule newServiceModule) {
		ServiceModule oldModule = getModule(oldServiceModuleClass);
		replaceModule(oldModule, newServiceModule);
	}

	/**
	 * Remove a service from the component.
	 * 
	 * @param serviceModule
	 *            service to uninstall
	 * @throws IllegalArgumentException
	 *             if the component does not have the service
	 */
	public void unInstallService(final ServiceModule serviceModule) {
		if (this.modules.remove(serviceModule)) {
			this.injector = Guice.createInjector(this.modules);
		} else {
			throw new IllegalArgumentException(
					"ComponentException ERROR : serviceModule " + serviceModule
							+ "does not exist, so it can't be removed");
		}
	}

	/**
	 * Remove some services from the component.
	 * 
	 * @param serviceModules
	 *            services to unsinstall
	 * @throws IllegalArgumentException
	 *             if one of this service is not install in the component
	 */
	public void unInstallServices(final ServiceModule... serviceModules) {
		for (final ServiceModule serviceModule : this.modules) {
			if (this.modules.remove(serviceModule)) {
			} else {
				throw new IllegalArgumentException(
						"ComponentException ERROR : serviceModule "
								+ serviceModule
								+ "does not exist, so it can't be removed");
			}
		}
		this.injector = Guice.createInjector(this.modules);
	}

	private <T> Class<? extends T> getImplementationClass(Key<T> key) {
		Binding<T> binding = injector.getBinding(key);
		Class<? extends T> implementationClassType = binding
				.acceptTargetVisitor(new BindingTargetImplementationVisitor<T>());
		return implementationClassType;
	}

	public <T> Class<? extends T> getImplementationClass(Class<T> type) {
		Key<T> key = Key.get(type);
		Class<? extends T> implementationClassType = getImplementationClass(key);
		return implementationClassType;
	}
	
	public <T> Class<? extends T> getImplementationClass(Class<T> type,
			Annotation annotation) {
		Key<T> key = Key.get(type, annotation);
		Class<? extends T> implementationClassType = getImplementationClass(key);
		return implementationClassType;
	}

	// public <T> T getInstance(Class<T> type) {
	// Key<T> key = Key.get(type);
	// T instance = getInstance(key);
	// return instance;
	// }
	//
	//
	// public <T> T getInstance(Class<T> type, Annotation annotation) {
	// Key<T> key = Key.get(type, annotation);
	// T instance = getInstance(key);
	// return instance;
	// }
	//
	// private <T> T getInstance(Key<T> key) {
	// Binding<T> binding = injector.getBinding(key);
	// T instance = binding.acceptTargetVisitor(new
	// BindingTargetInstanceVisitor<T>());
	// return instance;
	// }

}
