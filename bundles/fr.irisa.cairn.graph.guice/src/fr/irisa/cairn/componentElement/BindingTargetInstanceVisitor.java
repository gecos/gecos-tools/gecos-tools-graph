package fr.irisa.cairn.componentElement;

import com.google.inject.spi.DefaultBindingTargetVisitor;
import com.google.inject.spi.InstanceBinding;

public class BindingTargetInstanceVisitor<T> extends DefaultBindingTargetVisitor<T, T> {
	
	@Override
	public T visit(InstanceBinding<? extends T> instanceBinding) {
		
		return instanceBinding.getInstance();
	}

}
