package fr.irisa.cairn.componentElement;

import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.DefaultBindingTargetVisitor;
import com.google.inject.spi.LinkedKeyBinding;
import com.google.inject.spi.ProviderKeyBinding;

public class BindingTargetImplementationVisitor<T> extends DefaultBindingTargetVisitor<T, Class<? extends T>> {
	
	@Override
	public Class<? extends T> visit(LinkedKeyBinding<? extends T> linkedKeyBinding) {
		// TODO Auto-generated method stub
		Key<?  extends T> linkedKey = linkedKeyBinding.getLinkedKey();
		
		TypeLiteral<? extends T> typeLiteral = linkedKey.getTypeLiteral();
		@SuppressWarnings("unchecked")
		Class<? extends T> rawType = (Class<? extends T>)typeLiteral.getRawType();
		
		return rawType;
	}
	
	
	
	
	@Override
	public Class<? extends T> visit(ProviderKeyBinding<? extends T> providerKeyBinding) {
		providerKeyBinding.getProvider().get();
		return super.visit(providerKeyBinding);
	}
	

}
