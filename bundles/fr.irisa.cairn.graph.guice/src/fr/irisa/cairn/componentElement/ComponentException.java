package fr.irisa.cairn.componentElement;

public class ComponentException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 3258568029575653014L;
	
	public ComponentException() {
		// TODO Auto-generated constructor stub
	}
	

	public ComponentException(String message) {
		super(message);
	}

}
