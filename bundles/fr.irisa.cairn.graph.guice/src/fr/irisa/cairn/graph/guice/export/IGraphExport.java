package fr.irisa.cairn.graph.guice.export;

import java.io.File;
import java.io.PrintStream;

import fr.irisa.cairn.componentElement.IComponentService;
import fr.irisa.cairn.graph.IGraph;

public interface IGraphExport extends IComponentService {
	
	void export(File file, IGraph g);

	/**
	 * Exports a graph into a plain text file in DOT format.
	 * 
	 * @param writer
	 *            the writer to which the graph to be exported
	 * @param g
	 *            the graph to be exported
	 */
	void export(PrintStream out, IGraph g);

	void export(String path, IGraph g);

	boolean isShowCollapsedGraph();

	boolean isShowUnconnectedPort();

	void setShowCollapsedGraph(boolean showCollapsedGraph);

	void setShowUnconnectedPort(boolean showUnconnectedPort);


}
