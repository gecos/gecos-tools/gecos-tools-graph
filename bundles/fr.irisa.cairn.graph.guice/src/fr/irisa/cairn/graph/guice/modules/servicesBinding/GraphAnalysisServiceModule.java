package fr.irisa.cairn.graph.guice.modules.servicesBinding;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.guice.analysis.GraphAnalysisPort;
import fr.irisa.cairn.graph.guice.analysis.IGraphAnalysisService;

public class GraphAnalysisServiceModule extends ServiceModule {

	protected void bindIGraphAnalysisService() {
		this.bind(IGraphAnalysisService.class).to(GraphAnalysisPort.class).in(Singleton.class);
	}

	@Override
	protected void configure() {
		this.bindIGraphAnalysisService();

	}

}
