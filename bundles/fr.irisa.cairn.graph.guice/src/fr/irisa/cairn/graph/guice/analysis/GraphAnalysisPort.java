package fr.irisa.cairn.graph.guice.analysis;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.IComponentPort;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.analysis.Analysis;
import fr.irisa.cairn.graph.analysis.GraphAnalysis;
import fr.irisa.cairn.graph.providers.ILatencyProvider;

@Singleton
@SuppressWarnings("rawtypes")
public class GraphAnalysisPort implements IGraphAnalysisService, IComponentPort {
	private final Analysis<INode, IEdge> analyser;
	private final ILatencyProvider latencyProvider;

	@Inject
	protected GraphAnalysisPort(final Analysis<INode, IEdge> analyser,
			final ILatencyProvider latencyProvider) {
		this.analyser = analyser;
		this.latencyProvider = latencyProvider;
	}

	public Iterator<INode> alapIterator(final IGraph g) {
		return GraphAnalysis.alapIterator(g, latencyProvider);
	}

	public Iterator<INode> asapIterator(final IGraph g) {
		return GraphAnalysis.asapIterator(g, latencyProvider);
	}

	public Iterator<INode> beadthFirstIterator(final IGraph g) {
		return this.analyser.beadthFirstIterator(g);
	}

	public List<INode> breadthFirstSort(final IGraph g) {
		return GraphAnalysis.breadthFirstSort(g);
	}

	public List<INode> criticalPath(final IGraph g) {
		return GraphAnalysis.criticalPath(g, latencyProvider);
	}

	public int criticalPathLength(final IGraph g) {
		return GraphAnalysis.criticalPathLength(g, latencyProvider);
	}

	public List<INode> criticalTopologicalPath(final IGraph g) {
		return GraphAnalysis.criticalTopologicalPath(g);
	}

	public Iterator<INode> depthFirstIterator(final IGraph g) {
		return this.analyser.depthFirstIterator(g);
	}

	public List<INode> depthFirstSort(final IGraph g) {
		return GraphAnalysis.depthFirstSort(g);
	}

	public List<IEdge> dijkstraShortestPath(final INode from, final INode to) {
		return this.analyser.dijkstraShortestPath(from, to);
	}

	public Set<List<IEdge>> findAllHamiltonianPaths(final IGraph g) {
		return this.analyser.findAllHamiltonianPaths(g);
	}

	public Collection<Set<INode>> findMaxCliques(final IGraph g) {
		return this.analyser.findMaxCliques(g);
	}

	public Set<INode> findMaxWeightedClique(final IGraph g) {
		return this.analyser.findMaxWeightedClique(g, this.latencyProvider);
	}

	public int fordLongestPath(final INode from, final INode to) {
		return this.analyser.fordLongestPath(from, to, this.latencyProvider);
	}

	public int fordShortestPath(final INode from, final INode to) {
		return this.analyser.fordShortestPath(from, to, this.latencyProvider);
	}

	public IGraph graphMerging(final List<IGraph> lgraphs) {
		throw new UnsupportedOperationException();
	}

	public boolean hasCycle(final IGraph g) {
		return this.analyser.hasCycle(g);
	}

	public boolean isConnected(final IGraph g) {
		return this.analyser.isConnected(g);
	}

	public boolean isStronglyConnected(final IGraph g) {
		return this.analyser.isStronglyConnected(g);
	}

	public Iterator<INode> topologicalIterator(final IGraph g) {
		return this.analyser.topologicalIterator(g);
	}

	public List<INode> topologicalSort(final IGraph g) {
		return this.analyser.topologicalSort(g);
	}

	public Map<INode, Integer> topologicalSteps(final IGraph g) {
		return this.analyser.topologicalSteps(g);
	}

	public List<Set<INode>> findConnectedSets(IGraph g) {
		return this.analyser.findConnectedSets(g);
	}

	public List<Set<INode>> findStronglyConnectedSets(IGraph g) {
		return this.analyser.findStronglyConnectedSets(g);
	}

	public int findGreedyChromaticNumber(IGraph g) {
		return this.analyser.findGreedyChromaticNumber(g);
	}

	public Set<INode> findMinimalVertexCover(IGraph g, boolean approximation) {
		return this.analyser.findMinimalVertexCover(g, approximation);
	}
	
	public boolean isThereAPath(INode from, INode to) {
		return this.analyser.isThereAPath(from, to);
	}
}