package fr.irisa.cairn.graph.guice.modules.providersbinding;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.implement.GraphFactory;

public class SimpleGraphFactoryModule extends ServiceModule {

	private void bindIGraphFctory() {
		bind(IGraphFactory.class).to(GraphFactory.class).in(Singleton.class);
	}

	@Override
	protected void configure() {
		this.bindIGraphFctory();

	}

}
