package fr.irisa.cairn.graph.guice.modules.servicesBinding;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.guice.export.GraphExportPort;
import fr.irisa.cairn.graph.guice.export.IGraphExportService;

public class ExportServiceModule extends ServiceModule {

	protected void bindIGraphExportService() {
		this.bind(IGraphExportService.class).to(GraphExportPort.class).in(Singleton.class);
	}

	@Override
	protected void configure() {
		this.bindIGraphExportService();

	}

}
