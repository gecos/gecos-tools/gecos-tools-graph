package fr.irisa.cairn.graph.guice.export;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphVisitor;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.io.util.Color;
import fr.irisa.cairn.graph.io.util.ColorPalette;
import fr.irisa.cairn.graph.providers.IAttributsProvider;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;
import fr.irisa.cairn.graph.tools.GraphTools;

@Singleton
@SuppressWarnings({"rawtypes","unchecked"}) 
public class GuiceDotExport implements IGraphExport, IGraphVisitor {

	public static class DotAttributs {
		public final static String	STYLE			= "style";
		public final static String	NODE_SHAPE		= "shape";
		public final static String	NODE_COLOR		= "fillcolor";

		public final static String	EDGE_COLOR		= "color";
		// if edge constraint set to false, it is NOT used for scheduling
		public final static String	EDGE_CONSTRAINT	= "constraint";
		public final static String	ARROW_HEAD		= "arrowhead";

		public static Set<String> edgeKeys() {
			final Set<String> edgeKeys = new HashSet<String>();
			edgeKeys.add(DotAttributs.EDGE_COLOR);
			edgeKeys.add(DotAttributs.STYLE);
			edgeKeys.add(DotAttributs.ARROW_HEAD);
			edgeKeys.add(DotAttributs.EDGE_CONSTRAINT);
			return edgeKeys;
		}

		public static Set<String> nodeKeys() {
			final Set<String> nodeKeys = new HashSet<String>();
			nodeKeys.add(DotAttributs.NODE_SHAPE);
			nodeKeys.add(DotAttributs.NODE_COLOR);
			nodeKeys.add(DotAttributs.STYLE);
			return nodeKeys;
		}

	}

	// public static void export(final String file, final IGraph graph) {
	// GuiceDotExport.export(file, graph, new NodeLabelProvider());
	// }
	//
	// public static void export(final String file, final IGraph graph,
	// final INodeLabelProvider nodeLabelProvider,
	// final DotAttributsProvider dotAttributsProvider) {
	// final DOTExport export = new DOTExport(nodeLabelProvider, new
	// EdgeLabelProvider(),
	// dotAttributsProvider);
	// try {
	// final PrintStream out2 = new PrintStream(file);
	// export.export(out2, graph);
	// out2.close();
	// } catch (final FileNotFoundException e) {
	// throw new RuntimeException(e);
	// }
	// }
	//
	// public static void export(final String file, final IGraph graph,
	// final INodeLabelProvider nodeLabelProvider, final IEdgeLabelProvider
	// edgeLabelProvider,
	// final DotAttributsProvider dotAttributsProvider) {
	// final DOTExport export = new DOTExport(nodeLabelProvider,
	// edgeLabelProvider,
	// dotAttributsProvider);
	// try {
	// final PrintStream out2 = new PrintStream(file);
	// export.export(out2, graph);
	// out2.close();
	// } catch (final FileNotFoundException e) {
	// throw new RuntimeException(e);
	// }
	// }
	//
	// public static void export(final String file, final IGraph graph,
	// final NodeLabelProvider nodeLabelProvider) {
	// GuiceDotExport.export(file, graph, nodeLabelProvider, new
	// DotAttributsProvider());
	//
	// }

	private boolean						showCollapsedGraph	= false;
	private boolean						showUnconnectedPort	= true;
	protected INodeLabelProvider		nodeLabelProvider;
	protected IEdgeLabelProvider		edgeLabelProvider;
	protected IAttributsProvider		attributsProvider;

	protected PrintStream				out;

	protected String					indent				= "  ";
	protected String					connector;
	protected Set<String>				nodeKeys;

	protected Set<String>				edgeKeys;

	protected int						crtLevel;

	protected Set<IEdge>				visitededges;

	protected Set<INode>				visitedNodes;

	private final Map<IPort, String>	portNodesId			= new Hashtable<IPort, String>();

	public boolean						drawExternalEdges	= false;

	public GuiceDotExport(final IAttributsProvider attributsProvider) {
		this(null, null, attributsProvider);
	}

	public GuiceDotExport(final INodeLabelProvider nodeLabels) {
		this(nodeLabels, null, new DotAttributsProvider());
	}

	public GuiceDotExport(final INodeLabelProvider nodeLabels, final IEdgeLabelProvider edgeLabels) {
		this(nodeLabels, edgeLabels, new DotAttributsProvider());
	}

	@Inject
	public GuiceDotExport(final INodeLabelProvider nodeLabels, final IEdgeLabelProvider edgeLabels,
			final IAttributsProvider attributsProvider) {
		this.attributsProvider = attributsProvider;
		this.nodeLabelProvider = nodeLabels;
		this.edgeLabelProvider = edgeLabels;
		this.initializeAttributs();
	}

	public void export(final File file, final IGraph g) {
		try {
			final PrintStream stream = new PrintStream(file);
			this.export(stream, g);
		} catch (final FileNotFoundException e) {
			System.err.println(e);
			e.printStackTrace();
		}
	}

	/**
	 * Exports a graph into a plain text file in DOT format.
	 * 
	 * @param writer
	 *            the writer to which the graph to be exported
	 * @param g
	 *            the graph to be exported
	 */
	public void export(final PrintStream out, final IGraph g) {
		this.initializeVisiteds();
		this.out = out;
		if (g.isDirected()) {
			out.println("digraph G {");
			this.connector = " -> ";
		} else {
			out.println("graph G {");
			this.connector = " -- ";
		}

		for (final INode n : g.getNodes()) {
			n.accept(this, null);
		}

		final Set<IEdge> externalEdges = new HashSet<IEdge>();
		for (final IEdge e : g.getEdges()) {
			if (e.getSinkPort() != null && e.getSourcePort() != null) {
				if (this.internalEdge(e)) {
					e.accept(this, null);
				} else {
					externalEdges.add(e);
				}

			}
		}
		this.crtLevel--;
		this.indent();
		out.print("}\n");
		if (this.drawExternalEdges) {
			for (final IEdge e : externalEdges) {
				e.accept(this, null);
			}
		}
		out.flush();
		// DO NOT close the PrintStream as we did not open it!
		// out.close();
	}

	public void export(final String path, final IGraph g) {
		final File file = new File(path);
		this.export(file, g);
	}

	protected String getPortNodeId(final IPort p) {
		String node = this.portNodesId.get(p);
		if (node == null) {
			final boolean isInput = p.getNode().getInputPorts().contains(p);

			int i;
			if (isInput) {
				i = p.getNode().getInputPorts().indexOf(p);
			} else {
				i = p.getNode().getOutputPorts().indexOf(p);
			}
			final StringBuffer sb = new StringBuffer();
			if (isInput) {
				sb.append("in_");
			} else {
				sb.append("out_");
			}

			sb.append(this.getVertexID(p.getNode())).append("_").append(i);
			node = sb.toString();
			this.portNodesId.put(p, node);
		}
		return node;
	}

	protected String getVertexID(final INode n) {
		return this.nodeLabelProvider.getNodeName(n);
	}

	public void indent() {
		for (int i = 0; i <= this.crtLevel; ++i) {
			this.out.print(this.indent);
		}
	}

	private void initializeAttributs() {
		this.nodeKeys = DotAttributs.nodeKeys();
		this.edgeKeys = DotAttributs.edgeKeys();
	}

	protected void initializeVisiteds() {
		this.visitededges = new HashSet<IEdge>();
		this.visitedNodes = new HashSet<INode>();
	}

	protected boolean internalEdge(final IEdge e) {
		return e.getSinkPort().getNode().getGraph() == e.getSourcePort().getNode().getGraph();
	}

	public boolean isShowCollapsedGraph() {
		return this.showCollapsedGraph;
	}

	public boolean isShowUnconnectedPort() {
		return this.showUnconnectedPort;
	}

	protected void printCollapsedGraphPorts(final ICollapsedNode cn) {

		for (int i = 0; i < cn.getInputPorts().size(); ++i) {
			final IPort in = cn.getInputPorts().get(i);
			final IPort nestedPort = cn.getMappedInputPort(in);
			if (nestedPort != null) {
				this.printUnconnectedPort(nestedPort);
			} else {
				// throw new RuntimeException();
			}
		}
		for (int i = 0; i < cn.getOutputPorts().size(); ++i) {
			final IPort out = cn.getOutputPorts().get(i);
			final IPort nestedPort = cn.getMappedOutputPort(out);
			if (nestedPort != null) {
				this.printUnconnectedPort(nestedPort);
			}
		}

	}

	private void printCollapsedSubgraph(final ICollapsedNode sn, final Object arg) {
		this.indent();

		this.out.print("subgraph");
		this.out.print(this.indent + this.nodeLabelProvider.getClusterName(sn));
		this.out.print("{\n");
		this.crtLevel++;

		this.visitNode(sn, arg);

		for (final INode n : sn.getNodes()) {
			n.accept(this, null);
		}
		final Set<IEdge> externalEdges = new HashSet<IEdge>();
		for (final IEdge e : sn.getEdges()) {
			if (this.internalEdge(e)) {
				e.accept(this, null);
			} else {
				externalEdges.add(e);
			}
		}
		this.crtLevel--;
		this.indent();
		if (!this.isShowUnconnectedPort()) {
			this.printCollapsedGraphPorts(sn);
		}
		this.out.print("}\n");
		if (this.drawExternalEdges) {
			for (final IEdge e : externalEdges) {
				e.accept(this, null);
			}
		}

	}

	protected void printPort(final IPort p) {
		// add an input node
		this.indent();
		this.out.print(this.getPortNodeId(p));
		this.out.print("[");
		this.out.print("style=\"filled\",shape=\"point\",height=\"0.2\", color=\""
				+ ColorPalette.getHexColor(Color.LIGHT_GRAY) + "\"];");
		this.out.println();
	}

	protected void printPortConnection(final String connected, final IPort p, final boolean isInput) {

		if (isInput) {
			this.out.print(this.getPortNodeId(p) + this.connector + connected);
		} else {
			this.out.print(connected + this.connector + this.getPortNodeId(p));
		}
		this.out.println();
	}

	protected void printUnconnectedPort(final IPort p) {

		this.printPort(p);
		final boolean isInput = p.getNode().getInputPorts().contains(p);
		this.printUnconnectedPortConnection(this.getVertexID(p.getNode()), p, isInput);
		this.indent();

	}

	private void printUnconnectedPortConnection(final String connected, final IPort p,
			final boolean isInput) {

		if (isInput) {
			this.out.print(this.getPortNodeId(p) + this.connector + connected);
		} else {
			this.out.print(connected + this.connector + this.getPortNodeId(p));
		}
		this.out.print("[color=\"" + ColorPalette.getHexColor(Color.DARK_GRAY) + "\",style=\""
				+ DotAttributsProvider.STYLE_DASHED + "\"];");
		this.out.println();
	}

	protected void printUnconnectedPorts(final INode n) {
		for (int i = 0; i < n.getOutputPorts().size(); ++i) {
			final IPort out = n.getOutputPorts().get(i);

			if (out.getDegree() == 0) {
				this.printUnconnectedPort(out);
			} else {
				boolean isOutputPort = false;
				final Iterator<? extends IEdge> edgeIterator = out.getEdges().iterator();
				while (edgeIterator.hasNext() && !isOutputPort) {
					if (GraphTools.isGraphOutputEdge(edgeIterator.next())) {
						isOutputPort = true;
					}
				}
				if (isOutputPort) {
					this.printUnconnectedPort(out);
				}
			}
		}
		for (int i = 0; i < n.getInputPorts().size(); ++i) {
			final IPort in = n.getInputPorts().get(i);
			if (in.getDegree() == 0) {
				this.printUnconnectedPort(in);
			}
		}
	}

	/**
	 * @param attributsProvider
	 *            the attributsProvider to set
	 */
	public void setAttributsProvider(final IAttributsProvider attributsProvider) {
		this.attributsProvider = attributsProvider;
	}

	public void setShowCollapsedGraph(final boolean showCollapsedGraph) {
		this.showCollapsedGraph = showCollapsedGraph;
	}

	public void setShowUnconnectedPort(final boolean showUnconnectedPort) {
		this.showUnconnectedPort = showUnconnectedPort;
	}

	public void visitCollapsedNode(final ICollapsedNode cn, final Object arg) {
		if (this.isShowCollapsedGraph()) {
			this.printCollapsedSubgraph(cn, arg);

		} else {
			this.visitNode(cn, arg);
		}
	}

	public void visitEdge(final IEdge e, final Object arg) {
		if (!this.visitededges.contains(e)) {
			if (this.visitedNodes.contains(e.getSourcePort().getNode())
					&& this.visitedNodes.contains(e.getSinkPort().getNode())) {

				this.visitededges.add(e);
				final String source = this.getVertexID(e.getSourcePort().getNode());
				final String target = this.getVertexID(e.getSinkPort().getNode());

				this.indent();
				this.out.print(source + this.connector + target);

				if (this.edgeLabelProvider != null || this.attributsProvider != null) {
					this.out.print("[");
					boolean first = true;
					if (this.edgeLabelProvider != null) {
						this.out.print(" label = \"" + this.edgeLabelProvider.getEdgeLabel(e)
								+ "\"");
						first = false;
					}
					if (this.attributsProvider != null) {
						final Iterator<String> keyIterator = this.edgeKeys.iterator();

						while (keyIterator.hasNext()) {
							final String key = keyIterator.next();
							if (this.attributsProvider.getEdgeAttributs(e) != null
									&& this.attributsProvider.getEdgeAttributs(e).containsKey(key)) {
								if (!first) {
									this.out.print(",");
									first = false;
								}
								this.out.print(key
										+ "=\""
										+ this.attributsProvider.getEdgeAttributs(e).getProperty(
												key) + "\"");
							}

						}
					}
					this.out.print("]");
				}
				this.out.println(";");
			}
		}
	}

	public void visitNode(final INode n, final Object arg) {
		this.visitedNodes.add(n);

		this.indent();
		this.out.print(this.getVertexID(n));
		this.out.print("[");
		if (this.nodeLabelProvider != null) {
			this.out.print("label = \"" + this.nodeLabelProvider.getNodeLabel(n) + "\"");
		}
		if (this.attributsProvider != null) {
			this.out.print(",");
			final Iterator<String> keyIterator = this.nodeKeys.iterator();
			final Properties properties = this.attributsProvider.getNodeAttributs(n);
			while (keyIterator.hasNext()) {
				final String key = keyIterator.next();
				if (properties.containsKey(key)) {
					this.out.print(key + "=\"" + properties.getProperty(key) + "\"");
					if (keyIterator.hasNext()) {
						this.out.print(",");
					}
				}
			}
		}
		this.out.print("]");
		this.out.println(";");
		if (this.isShowUnconnectedPort()) {
			this.printUnconnectedPorts(n);
		}

	}

	public void visitSubGraph(final ISubGraphNode sn, final Object arg) {
		this.indent();

		this.out.print("subgraph");
		this.out.print(this.indent + this.nodeLabelProvider.getClusterName(sn));
		this.out.print("{\n");
		this.crtLevel++;

		this.visitNode(sn, arg);

		for (final INode n : sn.getNodes()) {
			n.accept(this, null);
		}
		final Set<IEdge> externalEdges = new HashSet<IEdge>();
		for (final IEdge e : sn.getEdges()) {
			if (this.internalEdge(e)) {
				e.accept(this, null);
			} else {
				externalEdges.add(e);
			}
		}
		this.crtLevel--;
		this.indent();
		this.out.print("}\n");
		if (this.drawExternalEdges) {
			for (final IEdge e : externalEdges) {
				e.accept(this, null);
			}
		}
	}
}