package fr.irisa.cairn.graph.guice.analysis;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.irisa.cairn.componentElement.IComponentService;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.analysis.GraphAnalysis;
import fr.irisa.cairn.graph.guice.GraphComponent;
import fr.irisa.cairn.graph.guice.modules.providersbinding.AnalysisProviderModule;

/**
 * {@link GraphAnalysis} library using {@link GraphComponent} installed
 * providers.
 * 
 * @see AnalysisProviderModule
 * @author jguidoux, Antoine Floc'h
 * 
 */
public interface IGraphAnalysisService extends IComponentService {

	<N extends INode> Iterator<N> alapIterator(IGraph g);

	<N extends INode> Iterator<N> asapIterator(IGraph g);

	<N extends INode> Iterator<N> beadthFirstIterator(IGraph g);

	<N extends INode> List<N> breadthFirstSort(IGraph g);

	<N extends INode> List<N> criticalPath(IGraph g);

	int criticalPathLength(IGraph g);

	<N extends INode> List<N> criticalTopologicalPath(IGraph g);

	<N extends INode> Iterator<N> depthFirstIterator(IGraph g);

	<N extends INode> List<N> depthFirstSort(IGraph g);

	/**
	 * Find the shortest path between two nodes using Dijkstra algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @return
	 */
	<E extends IEdge> List<E> dijkstraShortestPath(INode from, INode to);

	/**
	 * Find all hamiltonian paths in a graph (NP-complete).
	 * 
	 * @param g
	 * @return
	 */
	<E extends IEdge> Set<List<E>> findAllHamiltonianPaths(IGraph g);

	/**
	 * Find all maximal cliques of a graph using Bron Kerbosch algorithm.
	 * 
	 * @param g
	 * @return
	 */
	<N extends INode> Collection<Set<N>> findMaxCliques(IGraph g);

	// INode previousNodeOnCriticalPath(INode n, AsapAlgorithm asap);

	/**
	 * Find the maximal weighted clique.
	 * 
	 * @param g
	 * @return
	 */
	<N extends INode> Set<N> findMaxWeightedClique(IGraph g);

	/**
	 * Find the longest path between two nodes using Ford algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @param latencyProvider
	 *            gives the weight of each edge
	 * @return int the value of the longest path
	 */
	int fordLongestPath(INode from, INode to);

	/**
	 * Find the shortest path between two nodes using Ford algorithm.
	 * 
	 * @param from
	 *            first node of the path
	 * @param to
	 *            last node of the path
	 * @param latencyProvider
	 *            gives the weight of each edge
	 * @return int the value of the shortest path
	 */
	int fordShortestPath(INode from, INode to);

	// ICloneProvider getICloneProvider();
	//
	// ILatencyProvider getILatenctProvider();

	/**
	 * Merge a list of graph following the algorithm of Moreano.
	 * 
	 * @param lgraphs
	 *            : list of graph to merge
	 * @param latency
	 *            : provider for the cost of nodes
	 * @param libcompo
	 *            : provider to define the architecture of graphs to be merged
	 *            (equivalence, commutativity, conflicts, ...)
	 * @return the merged graph
	 */
	IGraph graphMerging(List<IGraph> lgraphs);

	/**
	 * Test if directed graph has any cycle.
	 * 
	 * @param g
	 * @return
	 */
	boolean hasCycle(IGraph g);

	boolean isConnected(IGraph g);

	boolean isStronglyConnected(IGraph g);

	<N extends INode> Iterator<N> topologicalIterator(IGraph g);

	<N extends INode> List<N> topologicalSort(IGraph g);

	<N extends INode> Map<N, Integer> topologicalSteps(IGraph g);

	<N extends INode> List<Set<N>> findConnectedSets(IGraph g);

	<N extends INode> List<Set<N>> findStronglyConnectedSets(IGraph g);

	/**
	 * Allows the chromatic number of a graph to be calculated. This is the
	 * minimal number of colors needed to color each vertex such that no two
	 * adjacent vertices share the same color. This algorithm will not find the
	 * true chromatic number, since this is an NP-complete problem. So, a greedy
	 * algorithm will find an approximate chromatic number. (JGraphT algorithm)
	 * 
	 * @param g
	 * @return
	 */
	public int findGreedyChromaticNumber(IGraph g);

	/**
	 * Algorithms to find a vertex cover for a graph. A vertex cover is a set of
	 * vertices that touches all the edges in the graph. The graph's vertex set
	 * is a trivial cover. However, a minimal vertex set (or at least an
	 * approximation for it) is usually desired. Finding a true minimal vertex
	 * cover is an NP-Complete problem. For more on the vertex cover problem,
	 * see http://mathworld.wolfram.com/VertexCover.html. (JGraphT algorithm)
	 * 
	 * @param g
	 * @param approximation
	 *            if false then a greedy algorithm is used
	 * @return
	 */
	public <N extends INode> Set<N> findMinimalVertexCover(IGraph g,
			boolean approximation);

	public boolean isThereAPath(INode from, INode to);
}
