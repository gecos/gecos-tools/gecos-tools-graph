package fr.irisa.cairn.graph.guice.modules.servicesBinding;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.guice.factory.GraphFactoryPort;
import fr.irisa.cairn.graph.guice.factory.IGraphFactoryService;

public class FactoryServiceModule extends ServiceModule {

	protected void bindIGraphFactoryService() {
		this.bind(IGraphFactoryService.class).to(GraphFactoryPort.class).in(Singleton.class);
	}

	@Override
	protected void configure() {
		this.bindIGraphFactoryService();

	}

}
