package fr.irisa.cairn.graph.guice.modules.servicesBinding;

import java.util.Comparator;

import com.google.inject.TypeLiteral;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.guice.comparator.BasicEdgeComparator;
import fr.irisa.cairn.graph.guice.comparator.BasicNodeComparator;
import fr.irisa.cairn.graph.guice.comparator.BasicPortComparator;

public class BasicComparatorServiceModule extends ServiceModule {

	protected void bindEdgeComparator() {
		this.bind(new TypeLiteral<Comparator<IEdge>>() {
		}).to(BasicEdgeComparator.class);
	}

	protected void bindNodeComparator() {
		this.bind(new TypeLiteral<Comparator<INode>>() {
		}).to(BasicNodeComparator.class);
	}

	protected void bindPortComparator() {
		this.bind(new TypeLiteral<Comparator<IPort>>() {
		}).to(BasicPortComparator.class);
	}

	@Override
	protected void configure() {
		this.bindNodeComparator();
		this.bindEdgeComparator();
		this.bindPortComparator();

	}

}
