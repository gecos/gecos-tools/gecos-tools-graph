package fr.irisa.cairn.graph.guice.modules.providersbinding;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.analysis.Analysis;
import fr.irisa.cairn.graph.implement.providers.ClonerProvider;
import fr.irisa.cairn.graph.implement.providers.EquivalenceProvider;
import fr.irisa.cairn.graph.implement.providers.LatencyProvider;
import fr.irisa.cairn.graph.providers.ICloneProvider;
import fr.irisa.cairn.graph.providers.ICommutativityProvider;
import fr.irisa.cairn.graph.providers.IEquivalenceProvider;
import fr.irisa.cairn.graph.providers.ILatencyProvider;
import fr.irisa.cairn.graph.tools.GraphCopy;

public class AnalysisProviderModule extends ServiceModule {

	protected void bindAnalysis() {
		this.bind(Analysis.class).in(Singleton.class);
	}

	protected void bindICloneProvider() {
		this.bind(ICloneProvider.class).to(ClonerProvider.class);
	}

	protected void bindILatencyProvider() {
		this.bind(ILatencyProvider.class).to(LatencyProvider.class);
	}
	
	
	protected void bindIEquivalenceProvider(){
		this.bind(IEquivalenceProvider.class).to(EquivalenceProvider.class);
	}
	
	protected void bindGraphCopier(){
		this.bind(GraphCopy.class).in(Singleton.class);
	}
	
	protected void bindICommutativityProvider(){
		this.bind(ICommutativityProvider.class).toInstance(new ICommutativityProvider<INode>() {
			public boolean isCommutative(INode n) {
				return true;
			}
			
		});
	}

	protected void configure() {
		this.bindICloneProvider();
		this.bindAnalysis();
		this.bindILatencyProvider();
		this.bindIEquivalenceProvider();
		this.bindGraphCopier();
		this.bindICommutativityProvider();
	}
}