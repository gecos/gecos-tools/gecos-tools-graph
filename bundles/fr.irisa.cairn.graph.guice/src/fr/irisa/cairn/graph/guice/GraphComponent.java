package fr.irisa.cairn.graph.guice;

import fr.irisa.cairn.componentElement.AbstractComponent;
import fr.irisa.cairn.graph.guice.analysis.IGraphAnalysisService;
import fr.irisa.cairn.graph.guice.export.IGraphExportService;
import fr.irisa.cairn.graph.guice.factory.IGraphFactoryService;
import fr.irisa.cairn.graph.guice.modules.providersbinding.AnalysisProviderModule;
import fr.irisa.cairn.graph.guice.modules.providersbinding.LabelProviderModule;
import fr.irisa.cairn.graph.guice.modules.providersbinding.SimpleGraphFactoryModule;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.BasicComparatorServiceModule;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.ExportServiceModule;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.FactoryServiceModule;
import fr.irisa.cairn.graph.guice.modules.servicesBinding.GraphAnalysisServiceModule;

/**
 * An IGraph component providing three services: <li> {@link IGraphFactoryService} : methods
 * to create an IGraph</Li> <li>
 * {@link IGraphExportService} : export a graph in dot or other format</Li> <li>
 * {@link IGraphAnalysisService} : library of algorithms for IGraph</Li>
 * 
 * 
 * @author jguidoux
 * 
 */
public class GraphComponent extends AbstractComponent {

	private final static GraphComponent INSTANCE = new GraphComponent();

	public static GraphComponent getINSTANCE() {
		return GraphComponent.INSTANCE;
	}

	public GraphComponent() {
		this.installModules(new GraphAnalysisServiceModule(),
				new FactoryServiceModule(), new ExportServiceModule(),
				new SimpleGraphFactoryModule(), new LabelProviderModule(),
				new AnalysisProviderModule(),
				new BasicComparatorServiceModule());
	}

	public IGraphAnalysisService getGraphAnalysisService() {
		return this.getService(IGraphAnalysisService.class);
	}

	public IGraphExportService getGraphExportService() {
		return this.getService(IGraphExportService.class);
	}

	public IGraphFactoryService getGraphFactoryService() {
		return this.getService(IGraphFactoryService.class);
	}
}
