package fr.irisa.cairn.graph.guice.factory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.IComponentPort;
import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.IGraphFactory;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;

@Singleton
public class GraphFactoryPort implements IGraphFactoryService, IComponentPort {

//	private Injector			injector;
	private final IGraphFactory	factory;

	@Inject
	private GraphFactoryPort(IGraphFactory factory) {
//		this.setInjector();
		this.factory = factory;
	}

	public IEdge connect(final INode source, final INode sink) {
		// TODO Auto-generated method stub
		return this.factory.connect(source, sink);
	}

	public IEdge connect(final IPort source, final IPort sink) {
		// TODO Auto-generated method stub
		return this.factory.connect(source, sink);
	}

	public IGraph createGraph(final boolean directed) {
		// TODO Auto-generated method stub
		return this.factory.createGraph(directed);
	}

	public INode createNode() {
		// TODO Auto-generated method stub
		return this.factory.createNode();
	}

	public INode createNode(final int nbInPorts, final int nbOutPorts) {
		// TODO Auto-generated method stub
		return this.factory.createNode(nbInPorts, nbOutPorts);
	}

	public IPort createPort() {
		// TODO Auto-generated method stub
		return this.factory.createPort();
	}

	public ISubGraphNode createSubgraph(final boolean directed) {
		// TODO Auto-generated method stub
		return this.factory.createSubgraph(directed);
	}

	public ICollapsedNode createCollapsedNode(boolean directed) {
		return this.factory.createCollapsedNode(directed);
	}

//	public IGraphFactory getIGraphFactory() {
//		return this.injector.getInstance(IGraphFactory.class);
//	}

//	private void setInjector() {
//		this.injector = Guice.createInjector(new SimpleGraphFactoryModule());
//	}
}