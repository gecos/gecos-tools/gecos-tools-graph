package fr.irisa.cairn.graph.guice.export;

import java.io.File;
import java.io.PrintStream;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.IComponentPort;
import fr.irisa.cairn.graph.IGraph;

@Singleton
public class GraphExportPort implements IGraphExportService, IComponentPort {




	private final IGraphExport	dotExport;

	@Inject
	protected GraphExportPort(IGraphExport	dotExport) {
		this.dotExport = dotExport;
	}

	public void export(final File file, final IGraph g) {
		this.dotExport.export(file, g);
	}

	// public void setModule(final Module module) {
	// this.module = module;
	// this.setInjector();
	// }

	public void export(final PrintStream stream, final IGraph g) {
		this.dotExport.export(stream, g);
	}

	// public INodeLabelProvider getINodeLabelProvider() {
	// return this.injector.getInstance(INodeLabelProvider.class);
	// }
	//
	// public IEdgeLabelProvider getIEdgeLabelProvider() {
	// return this.injector.getInstance(IEdgeLabelProvider.class);
	// }
	//
	// public IAttributsProvider getIAttributsProvider() {
	// return this.injector.getInstance(IAttributsProvider.class);
	// }

	public void export(final String path, final IGraph g) {
		this.dotExport.export(path, g);
	}

//	private IExport getIDotExport() {
//		return this.injector.getInstance(IExport.class);
//	}

	public boolean isShowCollapsedGraph() {
		// TODO Auto-generated method stub
		return this.dotExport.isShowCollapsedGraph();
	}

	public boolean isShowUnconnectedPort() {
		// TODO Auto-generated method stub
		return this.dotExport.isShowUnconnectedPort();
	}

//	protected void setInjector() {
//		this.injector = Guice.createInjector(new SimpleGraphFactoryModule(), new SimpleGraphDotExportModule());
//	}

	public void setShowCollapsedGraph(final boolean showCollapsedGraph) {
		this.dotExport.setShowCollapsedGraph(showCollapsedGraph);

	}

	public void setShowUnconnectedPort(final boolean showUnconnectedPort) {
		this.dotExport.setShowUnconnectedPort(showUnconnectedPort);

	}
}