package fr.irisa.cairn.graph.guice.modules.providersbinding;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.guice.export.EmptyEdgeLabelProvider;
import fr.irisa.cairn.graph.guice.export.GuiceDotExport;
import fr.irisa.cairn.graph.guice.export.IGraphExport;
import fr.irisa.cairn.graph.implement.providers.AbstractAttributsProvider;
import fr.irisa.cairn.graph.implement.providers.DotAttributsProvider;
import fr.irisa.cairn.graph.implement.providers.NodeLabelProvider;
import fr.irisa.cairn.graph.providers.IAttributsProvider;
import fr.irisa.cairn.graph.providers.IEdgeLabelProvider;
import fr.irisa.cairn.graph.providers.INodeLabelProvider;

public class LabelProviderModule extends ServiceModule {

	protected void bindIAttributeProvider() {
		this.bind(IAttributsProvider.class).to(AbstractAttributsProvider.class);
		this.bind(AbstractAttributsProvider.class).toInstance(new DotAttributsProvider<INode, IEdge>());
	}

	protected void bindIDotExport() {
		this.bind(IGraphExport.class).to(GuiceDotExport.class).in(Singleton.class);
	}

	protected void bindIEdgeLabelProvider() {
		this.bind(IEdgeLabelProvider.class).to(EmptyEdgeLabelProvider.class);
	}

	protected void bindINodeLabelProvider() {
		this.bind(INodeLabelProvider.class).to(NodeLabelProvider.class);
	}

	@Override
	protected void configure() {
		this.bindIAttributeProvider();
		this.bindIDotExport();
		this.bindIEdgeLabelProvider();
		this.bindINodeLabelProvider();

	}

}
