package fr.irisa.cairn.graph.guice.modules.graphBinding;


import fr.irisa.cairn.componentElement.ServiceModule;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.implement.Edge;
import fr.irisa.cairn.graph.implement.Graph;
import fr.irisa.cairn.graph.implement.Node;
import fr.irisa.cairn.graph.implement.Port;

public class GraphBinding extends ServiceModule {

	@Override
	protected void configure() {
		// TODO Auto-generated method stub

	}

	protected void bindGraph() {
		bind(IGraph.class).to(Graph.class);
	}

	protected void bindNode() {
		bind(INode.class).to(Node.class);
	}

	protected void bindEdge() {
		bind(IEdge.class).to(Edge.class);
	}

	protected void bindPort() {
		bind(IPort.class).to(Port.class);
	}
}
