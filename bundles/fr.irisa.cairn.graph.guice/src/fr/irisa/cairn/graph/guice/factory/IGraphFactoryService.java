package fr.irisa.cairn.graph.guice.factory;

import fr.irisa.cairn.componentElement.IComponentService;
import fr.irisa.cairn.graph.ICollapsedNode;
import fr.irisa.cairn.graph.IEdge;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.IPort;
import fr.irisa.cairn.graph.ISubGraphNode;

public interface IGraphFactoryService extends IComponentService {
	public ISubGraphNode createSubgraph(boolean directed);
	public INode createNode();
	public INode createNode(int nbInPorts, int nbOutPorts);
	public IGraph createGraph(boolean directed);
	public IPort createPort();
	public ICollapsedNode createCollapsedNode(boolean directed);

	/**
	 * Default connection between two nodes.
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	public IEdge connect(INode source, INode sink);

	/**
	 * Connect two ports and add the builded link to the nodes graphs
	 * 
	 * @model default=""
	 * 
	 * 
	 * @param source
	 * @param sink
	 * @return
	 */
	public IEdge connect(IPort source, IPort sink);

}
