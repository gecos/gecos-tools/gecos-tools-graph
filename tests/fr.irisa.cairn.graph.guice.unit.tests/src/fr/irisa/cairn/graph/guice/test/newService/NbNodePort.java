package fr.irisa.cairn.graph.guice.test.newService;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.IComponentPort;
import fr.irisa.cairn.graph.IGraph;

@Singleton
public class NbNodePort implements INbNodeService, IComponentPort {

	@Inject
	private NbNodePort() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public int nbNodes(IGraph g) {
		return g.getNodes().size();
	}

}
