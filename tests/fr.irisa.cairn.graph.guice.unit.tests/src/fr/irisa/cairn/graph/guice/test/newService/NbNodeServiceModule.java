package fr.irisa.cairn.graph.guice.test.newService;

import com.google.inject.Singleton;

import fr.irisa.cairn.componentElement.ServiceModule;

public class NbNodeServiceModule extends ServiceModule {

	protected void binfINBNodeService() {
		this.bind(INbNodeService.class).to(NbNodePort.class).in(Singleton.class);
	}

	@Override
	protected void configure() {
		this.binfINBNodeService();

	}

}
