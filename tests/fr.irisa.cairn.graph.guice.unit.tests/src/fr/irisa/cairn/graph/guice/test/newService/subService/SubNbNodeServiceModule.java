package fr.irisa.cairn.graph.guice.test.newService.subService;

import com.google.inject.Singleton;

import fr.irisa.cairn.graph.guice.test.newService.INbNodeService;
import fr.irisa.cairn.graph.guice.test.newService.NbNodeServiceModule;

public class SubNbNodeServiceModule extends NbNodeServiceModule {
	
	protected void binfINBNodeService() {
		this.bind(INbNodeService.class).to(NbNodeMultPerCentPort.class).in(Singleton.class);
	}

}
