package fr.irisa.cairn.graph.guice.test;

import java.util.Iterator;

import org.junit.Test;

import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.INode;
import fr.irisa.cairn.graph.guice.GraphComponent;
import fr.irisa.cairn.graph.guice.factory.IGraphFactoryService;
import fr.irisa.cairn.graph.guice.test.newService.INbNodeService;
import fr.irisa.cairn.graph.guice.test.newService.NbNodeServiceModule;
import fr.irisa.cairn.graph.guice.test.newService.subService.SubNbNodeServiceModule;

public class MainTest {

	private static IGraph initGraph() {
//		Class<? extends INodeLabelProvider> implementationClass = GraphComponent
//				.getINSTANCE().getImplementationClass(INodeLabelProvider.class);
		// AbstractAttributsProvider instance =
		// GraphComponent.getINSTANCE().getInstance(AbstractAttributsProvider.class);
//		IAttributsProvider<?, ?> service = GraphComponent.getINSTANCE().getService(
//				IAttributsProvider.class);
		final IGraphFactoryService factory = GraphComponent.getINSTANCE()
				.getGraphFactoryService();
		final IGraph g = factory.createGraph(true);
		final INode n1 = factory.createNode();
		final INode n2 = factory.createNode();

		g.addNode(n1);
		g.addNode(n2);
		factory.connect(n1, n2);
		return g;
	}

	private static IGraph initTypesdGraph() {
		IGraphFactoryService factory = GraphComponent.getINSTANCE()
				.getGraphFactoryService();

		final IGraph g = factory.createGraph(true);
		final INode n1 = factory.createNode();
		final INode n2 = factory.createNode();
		g.addNode(n1);
		g.addNode(n2);
		factory.connect(n1, n2);
		return g;
	}

	/**
	 * @param args
	 */
	@Test
	public void testGuiceGraph() {
		System.out.println("start graph A");
		final IGraph g = MainTest.initGraph();
		GraphComponent graphComponent = GraphComponent.getINSTANCE();
		graphComponent.getGraphExportService().export("graph.dot", g);
		System.out.println("dot file created");
		final Iterator<INode> topologicalIterator = GraphComponent
				.getINSTANCE().getGraphAnalysisService().topologicalIterator(g);
		for (; topologicalIterator.hasNext();) {
			final INode node = topologicalIterator.next();
			System.out.println(node);
		}

		graphComponent.installModules(new NbNodeServiceModule());
		INbNodeService nbNodesService = graphComponent
				.getService(INbNodeService.class);

		System.out.println("nb nodes = " + nbNodesService.nbNodes(g));
		System.out.println("changing of Modules");
		graphComponent.replaceModule(NbNodeServiceModule.class,
				new SubNbNodeServiceModule());
		nbNodesService = graphComponent.getService(INbNodeService.class);
		System.out.println("nb nodes = " + nbNodesService.nbNodes(g));
		System.out.println("finish for graph A");
		System.out.println();
		System.out.println("/////////////////////////////////////");
		System.out.println();
		System.out.println("start graph A");
		final IGraph graphB = MainTest.initTypesdGraph();
		GraphComponent.getINSTANCE().getGraphExportService()
				.export("typedGraph.dot", graphB);
		final Iterator<INode> topologicalIterator2 = GraphComponent
				.getINSTANCE().getGraphAnalysisService()
				.topologicalIterator(graphB);
		for (; topologicalIterator2.hasNext();) {
			final INode node = topologicalIterator2.next();
			System.out.println(node);
		}
		System.out.println("finish for graph B");

	}

}
