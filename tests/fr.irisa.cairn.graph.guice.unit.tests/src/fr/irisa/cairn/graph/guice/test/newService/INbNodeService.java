package fr.irisa.cairn.graph.guice.test.newService;

import fr.irisa.cairn.componentElement.IComponentService;
import fr.irisa.cairn.graph.IGraph;

public interface INbNodeService extends IComponentService {
	
	int nbNodes(IGraph g);

}
