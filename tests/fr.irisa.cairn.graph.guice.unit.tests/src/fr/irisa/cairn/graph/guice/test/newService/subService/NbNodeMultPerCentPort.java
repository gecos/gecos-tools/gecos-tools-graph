package fr.irisa.cairn.graph.guice.test.newService.subService;

import fr.irisa.cairn.componentElement.IComponentPort;
import fr.irisa.cairn.graph.IGraph;
import fr.irisa.cairn.graph.guice.test.newService.INbNodeService;

public class NbNodeMultPerCentPort implements IComponentPort, INbNodeService {

	@Override
	public int nbNodes(IGraph g) {
		return g.getNodes().size() * 100;
	}

}
